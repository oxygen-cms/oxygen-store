<?php namespace Store\Exceptions;

use Oxygen\Exceptions\OxygenException;

/**
 * @author Sal McDonald
 */
class ProductNotFoundException extends \Oxygen\OxygenException
{
	protected $message = 'store:products:cant_find';
}