<?php namespace Store\Exceptions;

use Oxygen\Exceptions\OxygenException;

/**
 * @author Sal McDonald
 */
class OrderRetrievalException extends \Oxygen\Exceptions\OxygenException
{
	protected $message = 'store:orders:cant_find';
}