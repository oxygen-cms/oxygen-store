<?php namespace Store;

use Oxygen\CommonObject;

/**
 * @author Sal McDonald
 */
class Test extends \Oxygen\CommonObject {

	/**
	 * @constructor
	 */
	public function __construct() {
		// Get the ci instance
		$this->ci = get_instance();
	}

}