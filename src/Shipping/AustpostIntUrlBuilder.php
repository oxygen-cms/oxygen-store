<?php namespace Store\Shipping;

class AustpostIntUrlBuilder extends \Store\Shipping\AustpostEndpoint { 

    protected $params;
    protected $first;
    protected $test;
    protected $hasOption;

    protected $_store_country_code;

    public function __construct($country_code='nz')
    {
        $this->_store_country_code=$country_code;
        $this->isInternational(true);
        $this->clear();
    }

    public function clear() {
        $this->params = '';
        $this->hasOption=false;
        $this->first = true;
        $this->test = false;
        $this->add('country_code', $this->_store_country_code );
 
    }


    public function get()
    {
        return $this->getUrl() . $this->params; 
    }

    public function setCountryCode($code) {
        $this->add('country_code', $code );
    }
    
    public function setServiceCode($code) {
        $this->add('service_code', $code );
    }

    public function setOptionCode($code) {

        if($this->hasOption==true) {
            $this->add('suboption_code', $code );
        }
        else {
            $this->hasOption=true;
            $this->add('option_code', $code );
        }

    }

    public function setExtraCover($amount) {
        $this->add('extra_cover', $amount );
    }

    /*
    from_postcode
to_postcode
length
width
height
weight
service_code
//optional
option_code
suboption_code
extra_cover*/

    public function addBox($item=[])
    {
        // set a very low weight to pass validation if not set
        $item['weight'] = ($item['weight']==0)?0.001:$item['weight'];

        $this->add('weight', $item['weight'] );
        $this->add('width', $item['width']);
        $this->add('height', $item['height']);
        $this->add('length', $item['length']);
    }

    public function add($key,$value)
    {
        $this->params .= $this->first ? '?' : '&';
        $this->params .= "{$key}={$value}";
        $this->first = false;
    }

}