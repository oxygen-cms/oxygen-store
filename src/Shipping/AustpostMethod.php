<?php namespace Store\Shipping;

class AustpostMethod { 

	const parcel  = 'parcel';
	const regular = 'regular';
	const csatchel  = 'csatchel';
	const cparcel = 'cparcel';

	const error = 'error';

	public $method;

    public function __construct($in_method = 'regular') {
    	$this->method = $in_method;
    }

    public function toString() {
    	return $this->method;
    }

    public function hasError() {
    	return ($this->method=='error');
    }    
    
}