<?php namespace Store\Shipping;

class AustpostEndpoint { 

    const API_TEST         = 'https://test.auspost.com.au/api/';
    const API_PUBLIC       = 'https://auspost.com.au/api/';

    
    const DOMESTIC_PARCEL  = 'postage/parcel/domestic/calculate.json';
    const DOMESTIC_LETTER  = 'postage/letter/domestic/calculate.json';

    //international endpoints
    const INT_PARCEL  = 'postage/parcel/international/calculate.json';
    const INT_LETTER  = 'postage/letter/international/calculate.json';

    protected $is_test 		= true;
    protected $is_int       = false;

    public function __construct($is_test=true) {
        $this->is_test = $is_test;
        $this->method = null;
    }

    public function getLetterOrParcelUrl() {

        if($this->method != null) {
            switch ($this->method->toString()) {
                case 'cparcel':             
                case 'parcel':
                    return $this->getDomesticParcelUrl();
                    break;
                case 'regular':
                default:
                    $this->getDomesticLetterUrl();
                    break;
            }   
        }
        return $this->getDomesticLetterUrl();
    }

    public function setMethod($method) {
        $this->method = $method;
    }


    public function isTestMode($is_test=true) {
        $this->is_test = $is_test;
    }
    public function isInternational($is_int=true) {
        $this->is_int = $is_int;
    }

    public function getUrl() {

        $type = $this->getLetterOrParcelUrl();

    	if($this->is_test) {
    		return $this->getTestUrl() . $type;
    	}
		return $this->getLiveUrl() . $type;
    }

    public function getTestUrl() {
    	return self::API_TEST;
    }

    public function getLiveUrl() {
    	return self::API_PUBLIC;
    }   

    public function getDomesticParcelUrl() {
        if($this->is_int) {
            return self::INT_PARCEL;
        }        
        return self::DOMESTIC_PARCEL;
    }

    public function getDomesticLetterUrl() {
        if($this->is_int) {
            return self::INT_LETTER;
        }
        return self::DOMESTIC_LETTER;
    }       

}