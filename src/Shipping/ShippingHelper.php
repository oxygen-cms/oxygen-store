<?php namespace Store\Shipping;

class ShippingHelper { 



    public function __construct() {

    }

    public function __get($var)
    {
        if (isset(get_instance()->$var))
        {
            return get_instance()->$var;
        }
    }

    public function packIntoBoxes($items) {

        $this->load->library('store/packages_library');

        $this->packages_library->pack( $items );

        return $this->packages_library->getShippableContainers();

    }  

    public function shippableBoxes($items) {

        $this->load->library('store/packages_library');

        return $this->packages_library->getShippableContainersCartItems( $items );

    }  



    public static function addHandling( & $shippableObejct, $handling_fee=0 ) {
        $shippableObejct->cost += $handling_fee;
    }

    public static function checkMinimum( & $shippableObejct, $min_value=0 ) {
        if($shippableObejct->cost < $min_value) {
            $shippableObejct->cost = $min_value;
        }
    }


    public static function validateAUDestination( $to_address ) {

        if(strtolower($to_address->country)=='au');
            return true;

        return false;

    }

    public static function getGlobalShippingObject($cost,$message='') {
        $shippable = (object)[];
        $shippable->cost = $cost;
        $shippable->tax  = 0;
        $shippable->packing_slip  = '';
        $shippable->message = $message;
        return $shippable;
    }    

    public static function addCostOfPackage( & $shipping_total, $item ) {
        $shipping_total += $item['additional_charge'] * $item['qty'];  
    }

    /*
    // Set your API key: remember to change this to your live API key in production
$apiKeyUserName = 'anonymous@auspost.com.au';
$apiKeyPassword = 'password';

// Define the service input parameters
$addressLine1 = '111 Bourke St';
$suburb = 'Melbourne';
$state = 'VIC';
$postcode = '3000';
$country = 'Australia';

// Set the query params
$queryParams = array(
  "addressLine1" => $addressLine1,
  "suburb" => $suburb,
  "state" => $state,
  "postcode" => $postcode,
  "country" => $country
);

// Set the URL for the Validate Austalian Address service
$urlPrefix = 'api.auspost.com.au';
$validateURL = 'https://' . $urlPrefix . '/ValidateAddress.xml?' . http_build_query($queryParams);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $validateURL);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Set a cookie in the header, important! if you don't do this authentication won't work.
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: OBBasicAuth=fromDialog'));

// Authentication 
curl_setopt($ch, CURLOPT_USERPWD, $apiKeyUserName . ":" . $apiKeyPassword);  

$rawBody = curl_exec($ch);

// Check the response; if the body is empty then an error has occurred
if(!$rawBody){
  die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
}

// All good, 
$responseXML = $rawBody;*/

}