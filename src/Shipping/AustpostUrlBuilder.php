<?php namespace Store\Shipping;
    /*
    from_postcode
to_postcode
length
width
height
weight
service_code
//optional
option_code
suboption_code
extra_cover*/

class AustpostUrlBuilder extends \Store\Shipping\AustpostEndpoint { 

    protected $params;
    protected $first;
    protected $test;
    protected $hasOption;

    protected $_store_fromPC;
    protected $_store_toPC;

    public function __construct($from_zip='3000',$to_zip='3000')  {
        $this->_store_fromPC=$from_zip;
        $this->_store_toPC=$to_zip;
        $this->clear();
    }

    public function clear() {
        $this->params = '';
        $this->hasOption=false;
        $this->first = true;
        $this->test = false;
        $this->add('from_postcode', $this->_store_fromPC );
        $this->add('to_postcode', $this->_store_toPC ); 
    }


    public function get() {
        return $this->getUrl() . $this->params; 
    }


    public function setServiceCode($code) {
        $this->add('service_code', $code );
    }

    public function setOptionCode($code) {

        if($this->hasOption==false) {
            $this->hasOption=true;            
            $this->add('option_code', $code );
            return;
        }

        $this->add('suboption_code', $code );
       
    }

    public function setExtraCover($amount) {
        $this->add('extra_cover', $amount );
    }

    public function addBox($item=[])  {
        // set a very low weight to pass validation if not set
        $item['weight'] = ($item['weight']==0)?0.001:$item['weight'];

        $this->add('weight', $item['weight'] );
        $this->add('width', $item['width']);
        $this->add('height', $item['height']);
        $this->add('length', $item['length']);
    }

    public function add($key,$value) {
        $this->params .= $this->first ? '?' : '&';
        $this->params .= "{$key}={$value}";
        $this->first = false;
    }

}