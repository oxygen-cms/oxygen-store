<?php namespace Store\Shipping;

class AustPostServiceCode { 

    protected $_code;

    // Large letters
    const AUS_LETTER_REGULAR_LARGE_125  = 'AUS_LETTER_REGULAR_LARGE_125'; //'AUS_LETTER_REGULAR_LARGE';
    const AUS_LETTER_PRIORITY_LARGE_125 = 'AUS_LETTER_PRIORITY_LARGE_125'; 

    // regular letters
    const AUS_LETTER_REGULAR_SMALL      = 'AUS_LETTER_REGULAR_SMALL'; 
    const AUS_LETTER_PRIORITY_SMALL     = 'AUS_LETTER_PRIORITY_SMALL';    
    const AUS_LETTER_EXPRESS_SMALL      = 'AUS_LETTER_EXPRESS_SMALL';

    // Parcel
    const AUS_PARCEL_REGULAR            = 'AUS_PARCEL_REGULAR';
    const AUS_PARCEL_EXPRESS            = 'AUS_PARCEL_EXPRESS';

    // Courier
    const AUS_PARCEL_COURIER            = 'AUS_PARCEL_COURIER';
    const AUS_PARCEL_COURIER_SATCHEL_MEDIUM = 'AUS_PARCEL_COURIER_SATCHEL_MEDIUM';

    // International options
    const INTL_SERVICE_AIR_MAIL         = 'INTL_SERVICE_AIR_MAIL';
    const INTL_SERVICE_ECI_D            = 'INTL_SERVICE_ECI_D';
    const INTL_SERVICE_ECI_M            = 'INTL_SERVICE_ECI_M';
    const INTL_SERVICE_ECI_PLATINUM     = 'INTL_SERVICE_ECI_PLATINUM';
    const INTL_SERVICE_EPI              = 'INTL_SERVICE_EPI';
    const INTL_SERVICE_EPI_B4           = 'INTL_SERVICE_EPI_B4';
    const INTL_SERVICE_EPI_C5           = 'INTL_SERVICE_EPI_C5';
    const INTL_SERVICE_PTI              = 'INTL_SERVICE_PTI';
    const INTL_SERVICE_RPI_B4           = 'INTL_SERVICE_RPI_B4';
    const INTL_SERVICE_RPI_DLE          = 'INTL_SERVICE_RPI_DLE';
    const INTL_SERVICE_SEA_MAIL         = 'INTL_SERVICE_SEA_MAIL';

    public function __construct() {
  
    }

    public function initialize( $method, $is_express=false ) {
        $this->_code = $this->getPostageServiceCode( $method, $is_express );
    }
    
    public function initializeCourierPost( $method ) {
        $this->_code = $this->getPostageServiceCodeCourier( $method );
    }

    /**
     * $method->toString()
     */
    public function getPostageServiceCode( $method, $is_express=false ) {

        if($is_express) {
            if($method->toString()=='regular') {
                return $this->getLetterPostExpress();
            }
            else {
                return $this->getParcelPostExpress();
            }
        }
        else {
            if($method->toString()=='regular') {
                return $this->getLetterPost();
            }
            else {
                return $this->getParcelPost();
            }   
        }

        return $this->getParcelPostExpress();
    }

    public function getPostageServiceCodeCourier( $method ) {

        if($method->toString()=='regular') {
            return $this->getCourierLetterPost();
        }
        else {
            return $this->getCourierParcelPost();
        }
    }

    public static function GetCourier( $method ) {

        if($method->toString()=='csatchel') {
            return self::AUS_PARCEL_COURIER_SATCHEL_MEDIUM;
        }

        return self::AUS_PARCEL_COURIER;
    }  

    public static function GetAP( $method ) {

        if($is_express) {
            if($method->toString()=='regular') {
                return self::AUS_LETTER_EXPRESS_SMALL;
            }
            else {
                return self::AUS_PARCEL_EXPRESS;
            }
        }
        else {
            if($method->toString()=='regular') {
                return self::AUS_LETTER_REGULAR_LARGE;
            }
            else {
                return self::AUS_PARCEL_REGULAR;
            }   
        }

        return self::AUS_PARCEL_EXPRESS;
    }  

    public function get() {
        return $this->_code;
    }

    // Standard Post
    protected function getLetterPost() {
        return self::AUS_LETTER_REGULAR_LARGE_125;
    }

    protected function getLetterPriorityPost() {
        return self::AUS_LETTER_PRIORITY_LARGE_125;
    }

    // Standard Post Express
    protected function getLetterPostExpress() {
        return self::AUS_LETTER_EXPRESS_SMALL;
    }

    // Parcel Post 
    protected function getParcelPost() {
        return self::AUS_PARCEL_REGULAR;
    }

    // Parcel Post (Express)
    protected function getParcelPostExpress() {
        return self::AUS_PARCEL_EXPRESS;
    }    


    // Courier Post
    protected function getCourierLetterPost() {
        return self::AUS_PARCEL_COURIER_SATCHEL_MEDIUM;
    }


    protected function getCourierParcelPost() {
        return self::AUS_PARCEL_COURIER;
    }  

    public function getIntAirmailPost() {

        return self::INTL_SERVICE_AIR_MAIL;
    }  
    public function getIntExpressAirmailPost() {

        return self::INTL_SERVICE_EPI;
    }    

}