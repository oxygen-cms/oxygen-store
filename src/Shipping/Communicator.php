<?php namespace Store\Shipping;

class Communicator { 

    protected $api_key = '';


    public function __construct() {
        $this->error_redir = 'store/cart';
    }

    public function setApi($api_key) {
        $this->api_key = $api_key;
    }

    public function setRedirectError($error_redir) {
        $this->error_redir = $error_redir;
    }

    protected function setApiUrl($url) {
        $this->api_url = $url;
    }


    public function communicate($url_encoded)
    {
        //set the url to send
        $this->setApiUrl($url_encoded);

        // Call AP Service
        $response = $this->_callAPService();



        //var_dump($response['postage_result']['costs']);die;

        return $this->_handleAPServiceResponse($response);
    }

    private function _callAPService( $method='json' )
	{
		$ch = curl_init();
        //echo $this->api_key;
        //var_dump($this);die;
        //$this->api_url = 'https://auspost.com.au/api/postage/letter/domestic/calculate.json?from_postcode=3072&to_postcode=4007&service_code=AUS_PARCEL_REGULAR&weight=0.225&width=33.5&height=24.5&length=2';

		curl_setopt($ch, CURLOPT_URL,$this->api_url );

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  'Auth-Key: ' . $this->api_key
		));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$contents = curl_exec($ch);

        $resp =  curl_getinfo($ch);


        //echo $this->api_url.'<br>';
        //dump($resp);die;


		curl_close($ch);

        //echo CURLOPT_URL.$this->api_url.'<br>';
        //echo $this->api_key.'<br>';
        //dump($contents);die;
		//currently only supports json but future we need to support the xml as well
		//if($method=='json')
		//	return json_decode($contents,true);
		//else

        $resp_obj = (object) [];
        $resp_obj->resp = json_decode($contents,true);
        $resp_obj->curl_resp = $resp;

		return $resp_obj;

	}

    /*
    {
       "postage_result":{
          "service":"Express Post International",
          "total_cost":"45.00",
          "costs":{
             "cost":{
                "cost":"45.00",
                "item":"Express Post International"
             }
          }
       }
    }
    */
    private function _handleAPServiceResponse($response) {


        $additional_message = ' with HTTP Code of `'.$response->curl_resp['http_code'].'`';

        $returnObj =  (object)[];

        $returnObj->message = 'Unknown error response from AustPost' .$additional_message;
        $returnObj->error = true;

        $returnObj->cost = 0;
        $returnObj->costs = [];

        // Check for errors
        if (isset($response->resp['error'])) {
            $returnObj->message = $response->resp['error']['errorMessage'];
        }

        if (isset($response->resp['postage_result']['total_cost']))  {
            $returnObj->cost = $response->resp['postage_result']['total_cost'];
            $returnObj->message = '';
            $returnObj->error = false;
        }

        if (isset($response->resp['postage_result']['total_cost']['costs']))  {
            $returnObj->costs = $response->resp['postage_result']['total_cost']['costs'];
        }



        return $returnObj;

    }
}

