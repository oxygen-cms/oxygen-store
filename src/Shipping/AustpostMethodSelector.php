<?php namespace Store\Shipping;

use Store\Shipping\AustpostMethod as AustpostMethod;

class AustpostMethodSelector { 


	// Max values for letters
	const max_height = 36.0; //cm
	const max_width = 26.0; //cm
	const max_length = 2.00; //cm
	const max_weight = 0.500; //gram

	// Max values for parcels
	const MAX_HEIGHT 	= 35; //only applies if same as width
	const MAX_WIDTH 	= 35; //only applies if same as height
	const MAX_WEIGHT 	= 20; //kgs
	const MAX_LENGTH 	= 105; //cms
	const MAX_GIRTH 	= 140; //cms
	const MIN_GIRTH 	= 16; //cms


	// Courier Satchel
	const max_cr_height = 45.0; //cm
	const max_cr_width = 310.0; //cm
	const max_cr_length = 2.00; //cm
	const max_cr_weight = 3.0; //gram


	// Courier Parcel restrictions
	const MAX_CR_WEIGHT 	= 22; //kgs
	const MAX_CR_LENGTH 	= 105; //cms
	const MAX_CR_CUB_SIZE 	= 0.25; //m3



	// Define the types of post available
	protected $parcelPost = null;
	protected $regularPost = null;
	protected $courierSatchel = null;
	protected $courierParcel = null;

	// max letter size info before it becomes parcel : Weight:125g	W:260 x H:360 x L:20mm
    public function __construct() {

		$this->parcelPost = new AustpostMethod('parcel');
		$this->regularPost = new AustpostMethod('regular');

		$this->courierSatchel = new AustpostMethod('csatchel');
		$this->courierParcel = new AustpostMethod('cparcel');
		$this->errorMethod = new AustpostMethod('error');		
    }


    /**
     * return 'parcel' or 'post' objects
     */
    public function parcelOrPost($box) {
		

		if($this->canRegularPost($box)) {
			return $this->regularPost;
		}

		if($this->canParcelPost($box)) {
			return $this->parcelPost;
		}

    	//passes the regular test
    	return $this->errorMethod;
    	
    }

    public function parcelOrPostCourier($box) {

    	//just use this, not selecting properly
    	return $this->courierParcel;


		$_MAX_CUBIC_WEIGHT 	= $this->getCubicWeight($box);

		if($this->canCourierSatchel($box)) {
			return $this->courierSatchel;
		} 
		else {

			if($this->canCourierParcel($box,$_MAX_CUBIC_WEIGHT)) {
				return $this->courierParcel;
			} 
		}

		return $this->errorMethod;
    } 

    private function cmToMeters($in) {

    	$in = (float) $in;
    	if($in <= 0) {
    		$out = $in / 100;
    		return $out;
    	}

    	return 0;
    }   

    private function getCubicWeight($box) {
		
		$h = $this->cmToMeters($box['height']);
		$w = $this->cmToMeters($box['width']);
		$l = $this->cmToMeters($box['length']);

		return $h * $l * $w * 250;

    }  

    private function canCourierSatchel($box) {
   
   		$h = (float)$box['height'];
		$w = (float)$box['width'];
		$l = (float)$box['length'];
		$weight = (float)$box['weight'];
		
		//switch the h and w
    	if(($h>$this::max_cr_height) AND ($w<=$this::max_cr_height)) {
    		$h = (float)$box['width'];
    		$w = (float)$box['height'];
    	}

    	if($h>$this::max_cr_height)  {
    		return false;
    	}

    	if($w>$this::max_cr_width)  {
    		return false;
    	}

    	if($l>$this::max_cr_length)
    		return false;

    	if($weight>$this::max_cr_weight)
    		return false;

    	// Pass
    	return true;
    }

    private function canCourierParcel($box,$max_cb_weight) {

    	$test_weight = max($box['weight'] , $max_cb_weight);

    	//the length is really the longest of the three
    	$test_length = max($box['length'],$box['width'],$box['height']); 


   		$h = (float)$box['height'];
		$w = (float)$box['width'];
		$l = (float)$box['length'];

		//set min
   		$h = ($h<=0)?1:$h;
		$w = ($w<=0)?1:$w;
		$l = ($l<=0)?1:$l;

    	// in cm
    	$test_cubic_size = ($h/100) * ($w/100)  * ($l/100); 

    	if($test_weight>self::MAX_CR_WEIGHT) {
    		return false;
    	}

    	if($test_length>self::MAX_CR_LENGTH) {
    		return false;
    	}

    	// Now test cubic size
    	if($test_cubic_size>self::MAX_CR_CUB_SIZE) {
    		return false;
    	}

    	// Pass tests
    	return true;
    }


    private function canParcelPost($box) {
    	return true;
    }


    private function canRegularPost($box) {

        //override
        if($box['package_type']=='letter') {
            return true;
        }
        //force parcel, do not alllow regular post
        if($box['package_type']=='parcel') {
            return false;
        }

		// ooops, we have a problem
    	if((float)$box['height']>$this::max_height) {
            //try rotating the hxw
            $t=$box['width'];
            $box['width']=$box['height'];
            $box['height']=$t;
        }


    	if((float)$box['width']>$this::max_width)
    		return false;

    	if((float)$box['length']>$this::max_length)
    		return false;

    	if((float)$box['weight']>$this::max_weight)
    		return false;

    	return true;

    }


}