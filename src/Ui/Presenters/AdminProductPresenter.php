<?php namespace Store\Ui\Presenters;

use \Oxygen\Ui\Presenters\CorePresenter;

/**
 * @author Sal McDonald
 */
class AdminProductPresenter extends \Oxygen\Ui\Presenters\CorePresenter
{

	public function __construct($config)
	{
		parent::__construct();
		$this->show_product_other_tab = $config->config->item('admin/show_product_other_tab');
		$this->mod_path = $config->mod_path;

		$this->template
			->enable_parser(true)           
			->set('base_amount_pricing', NC_BASEPRICING )
            ->append_js('store::admin/util.js')
			->append_metadata('<script type="text/javascript">' . "\n  var MOD_PATH = '" . $this->mod_path . "';" . "\n</script>");
	}

	public function variant($product)
	{
		$this->template->title($this->module_details['name'], lang('store:products:edit'));
		$this->template->enable_parser(true);
		$this->template->set_layout(false);
		$this->default_view = 'admin/products/partials/variant';
		$this->present($product);	
	}

	public function single($product, $mode = 'view')
	{

		$this->template->title($this->module_details['name'], lang('store:products:edit'))
				->set('show_product_other_tab',$this->show_product_other_tab)
				->set('userDisplayMode',$mode)
				->set('mode',$mode)
				->append_js('store::admin/products.js')
				->append_js('store::admin/common.js');

		$this->default_view = 'admin/products/edit';
		$this->present($product);		
	}

	public function create($defaults)
	{
		$this->template->title($this->module_details['name'], lang('store:products:create'))
				->append_js('store::admin/products.js')
				->append_js('store::admin/common.js');
		$this->default_view = 'admin/products/create';
		$this->present($defaults);		
	}


	/*
	 * Static functions below
	 *
	 *
	 *
	 */


	/**
	 * @param $options Array 	- Categories to create product with specific category setup
	 *						 	- ['categories'=> [1,2,3],... ]
	 */
	static public function GetDropdownOptions( & $object_handle, $options=[] )
	{
		$ci = get_instance();

		$ci->load->model('store/admin/packages_admin_m');
		$ci->load->model('store/admin/packages_groups_admin_m');
		$ci->load->model('store/admin/products_types_admin_m');
		$ci->load->model('store/zones_m');

		$object_handle->available_packages = $ci->packages_admin_m->get_for_admin();
		$object_handle->available_groups = $ci->packages_groups_admin_m->get_for_admin();
		$object_handle->available_taxes = $ci->tax_m->get_admin_select();
		$object_handle->available_types = $ci->products_types_admin_m->get_for_admin();
		$object_handle->available_zones = $ci->zones_m->get_for_admin();

		$object_handle->default_taxID = $ci->tax_admin_m->getDefaultID();
		$object_handle->default_typeID = $ci->products_types_admin_m->getDefaultID(); 
		$object_handle->default_groupID = $ci->packages_groups_admin_m->getDefaultID();
		$object_handle->default_zone_ID = $ci->zones_m->getDefaultID();	
	}

}