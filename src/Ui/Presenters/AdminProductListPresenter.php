<?php namespace Store\Ui\Presenters;

use \Oxygen\Ui\Presenters\CorePresenter;

/**
 * @author Sal McDonald
 */
class AdminProductListPresenter extends \Oxygen\Ui\Presenters\CorePresenter
{

	public function __construct($config)
	{
		parent::__construct();
		$this->show_product_other_tab = $config->config->item('admin/show_product_other_tab');
		$this->mod_path = $config->mod_path;

		$this->mod_name = $this->module_details['name'];

        $this->template
        			->set('base_amount_pricing',NC_BASEPRICING)         
					->append_js('store::admin/products.js')
					->append_js('store::admin/common.js')  
					->append_metadata('<script type="text/javascript">' . "\n  var MOD_PATH = '" . $this->mod_path . "';" . "\n</script>")	          
					->enable_parser(true);		
	}

	public function present($object=null)
	{
		$this->template->title($this->mod_name, lang('store:products:edit'))
				->set('show_product_other_tab',$this->show_product_other_tab)
				->set('userDisplayMode',$mode)
				->set('mode',$mode)
				->append_js('store::admin/products.js')
				->append_js('store::admin/common.js');

		$this->default_view = 'admin/products/edit';

		parent::present($object);
	}

	public function initCall($data,$filter_type)
	{
		// Build the view with ROUTE/views/admin/products.php
		$this->template->title($this->mod_name)
					->set('filter_type',$filter_type)
					//->append_js('admin/filter.js')                
                    ->append_js('store::admin/util.js')
                    ->append_css('store::admin/store.css')
					->build('admin/products/products', $data);		
	}


	public function callback_build($data)
	{
		$this->template
				->set_layout(false)
				->set('pagination', $data->pagination)
				->build('admin/products/line_item',$data);
	}	

}