/*
 * Store Public Countries
 *
 */

$(function() {

    $(document).on('change', '.nc_country_select', function(event) 
    {
            var country_id = $(this).val();
            var api =  SITE_URL + 'store/api/countries/country/' + country_id + '/states';

            var alt_class = '.billing';

            if($(this).hasClass( "shipping" ))
            {
                alt_class = '.shipping';
            }

            $.post( api ).done(function(data)
            {
                var obj = jQuery.parseJSON(data);
                if(obj.status == 'success')
                {
                    $(alt_class+".nc_states_select" ).html('');
                    $(alt_class+".nc_states_select").append(obj.html);
                    $(alt_class+".nc_states_select").chosen({width: '100%'});
                    $(alt_class+".nc_states_select").trigger("chosen:updated");
                }
            });

            // Prevent Navigation
            event.preventDefault();

      });      

});