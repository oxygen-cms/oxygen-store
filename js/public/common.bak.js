

$(function() {



    /** 
     * option/attribute changed, update price and message.
     * No product is being added to the cart here
     */
    $(document).on('change', '.eav_oi_select', function(event) 
    {

        var form_id  = '#' +  $(this).closest('form').attr('id');
        var product_id  =  $(this).attr('product_id');
        $('.store-product-message_'+product_id).html('');

        postto = SITE_URL +  NC_ROUTE + '/eavtest/product';
        var $form = $(form_id);

        $.ajax({
            url: postto,
            data: $form.serialize(),
            type: 'POST'
        }).done(function(data) 
        {
                var returnObject = jQuery.parseJSON(data);
                if(returnObject.status=='error')
                {
                    //ajax_error('No items available with these options.');
                    $('.store-product-message_'+product_id).html('No items available with these options.');
                    // hide the add to cart button 
                    $( "#store-section-add-to-cart_"+product_id).hide();
                    //update price to from price
                    var fp = $('#product_fp_'+product_id).attr('value');
                    $('#store_price_'+product_id).html(fp+'*');
                    $("#product_variations_"+product_id).html('');
                    $("#p_product_variations_"+product_id).hide();                    
               
                }
                else
                {
                    $( "#store-section-add-to-cart_"+product_id).show();
                    //if all good now just update the price to user
                    $("#store_price_"+product_id).html(returnObject.obj.price);
                    $('.store-product-message_'+product_id).html('');
                    //after attributes change we want to clear the list
                    $("#product_variations_"+product_id).html('');
                    $("#p_product_variations_"+product_id).hide();
                    update_currency_symbol('#store_price_'+product_id);

                }

                //do whatever we need to after ajax call
                nc_aj_after_call();    
        });

        event.preventDefault();
    }); 

    /** 
     * Variance select changed, update price
     * No option data is being sent, just the variance id
     */
    $(document).on('change', '#eav_variances_id', function(event) 
    {
        
        var product_id  = $(this).attr('product_id');
        $('.store-product-message_'+product_id).html('');

        variance_option = $(this).val();

        postto = SITE_URL +  NC_ROUTE + '/eavtest/productvariance/'+variance_option;

        $.ajax({
            url: postto,
            type: 'POST'
        }).done(function(data) 
        {
            var returnObject = jQuery.parseJSON(data);
            if(returnObject.status=='error')
            {
                //alert('Unable to update price.');
                ajax_error('Unable to update price.');

                $("#product_variations_"+product_id).html('');
                $("#p_product_variations_"+product_id).hide();                
            }
            if(returnObject.status=='success')
            {
                $("#store_price_"+product_id).html(returnObject.price);
                update_currency_symbol('#store_price_'+product_id); 
            } 

            //do whatever we need to after ajax call
            nc_aj_after_call();

        });  

        event.preventDefault();
    });  


    /** 
     * Add item to cart
     */
    $(document).on('click', '.add_to_cart_btn', function(event) 
    {

        $("#product_variations_"+product_id).html("");
        
        var formIDClean = $(this).closest('form').attr('id');
        var form_id  = '#' +  formIDClean;
        var product_id  =  $(this).attr('product_id');

        $('.store-product-message_'+product_id).html('');

        postto = SITE_URL +  NC_ROUTE + '/eavcart/add';

        var xx;
        var alertUser=false;
        var normalPost=false;
        

        if(formDataSupported()==true)
        {
            var formElement = document.getElementById(formIDClean);
            var odata = new FormData(formElement);  
            xx = {
                url: postto,
                data: odata, //$form.serialize(),
                type: 'POST',
                processData: false,  // tell jQuery not to process the data
                contentType: false   // tell jQuery not to set contentType            
            };           
        }  
        else
        {
            var $form = $(form_id);
            xx = {
                url: postto,
                data: $form.serialize(),
                type: 'POST',       
            };  
            $(form_id).each(function(){
                var input = $(this).find(':input') //<-- Should return all input elements in that specific form.
                if(input.attr('type')=='file') {
                    alertUser = true;
                    //normalPost=true;
                }
            });               
        }  
     
        if(normalPost==true) {
            $(form_id).submit();
        }
        else {

            $.ajax(xx).done(function(data)   {

                    var returnObject = jQuery.parseJSON(data);

                    if(returnObject.status=='error')
                    {
                        ajax_error(returnObject.message);
                    }
                    else
                    {
                        if(returnObject.mode=='add_success')
                        {

                            if(alertUser==true) {
                                var msg = 'Item has been added however file uploading is restricted on your browser, please use a modern browser.';
                                ajax_notice(msg);
                            }

                            $("#product_variations_"+product_id).html('');
                            update_cart_info(returnObject.cost,returnObject.qty);
                            ajax_notice('Item added to cart.');
                            $('.store-product-message_'+product_id).html('Item added to cart.');

                            $("#product_variations_"+product_id).html('');
                            $("#p_product_variations_"+product_id).hide();

                        }                 
                        else
                        {
                            if(alertUser==true) {
                                //normalPost=true;
                            } 

                            //alert(returnObject.message);
                            //if all good now just update the price to user
                            $("#product_variations_"+product_id).html(returnObject.html);
                            $("#p_product_variations_"+product_id).show();
                            //ajax_notice('Please select an variation.');
                            $("#eav_variances_id").chosen({width: '100%'});
                
                        }

                        //do whatever we need to after ajax call
                        nc_aj_after_call();                    
                        
                    }
            });   
        }

        event.preventDefault();
    }); 



    /** 
     * Remoce item from cart
     */
    $(document).on('click', '.ajax_store_remove_cart_item', function(event)  {     

        rowid = $(this).attr('rowid');
        postto = SITE_URL +  NC_ROUTE + '/cart/delete/' + rowid;
        table_tr = 'tr#' + rowid;

        $.post( postto ).done(function(data)
        {
            var returnObject = jQuery.parseJSON(data);
            if(returnObject.status=='error')
            {
                ajax_error(returnObject.message);
            }
            else
            {
                update_cart_info(returnObject.cost,returnObject.qty);              
                $(table_tr).fadeOut('slow');
                ajax_success(returnObject.message);

                if(returnObject.qty == 0)
                {
                    //redirect back to the cart page (same page) so the UI picks up new layout file
                    setTimeout(function(){ window.location.replace( SITE_URL +  NC_ROUTE + '/cart/'); }, 1000);
                }
            }

            //do whatever we need to after ajax call
            nc_aj_after_call();              

        });

        event.preventDefault();
    });
    

    //lets do this all page loads
    update_currency_symbols();

});


function update_currency_symbol(el)
{
    $(el).formatCurrency(); 
}
function update_currency_symbols()
{
    $('.store-cart-subtotal').formatCurrency();
    $('.store-cart-line-price').formatCurrency();
    $('.store-cart-line-was-price').formatCurrency();
    $('.store-cart-line-subtotal').formatCurrency();  
    $('.store-minicart-total').formatCurrency();  
    $('.store-checkout-shipping-cost').formatCurrency();  
    $('.store-checkout-order-total').formatCurrency();  
    $('.store-checkout-shipping-item-cost').formatCurrency();  
    $('.store-checkout-review-item-subtotal').formatCurrency();  
    $('.store-checkout-review-item-price').formatCurrency();  
    $('.store-checkout-grand-total').formatCurrency();     
}
/*
function after_cart_update()
{
    $('.store-cart-line-price').formatCurrency();
}
*/
function getMiniCartLineItem(name,slug,qty,sub)
{
    //return "hello there";
    var str = "";
    str += "<li>";
    str += "            <div>";
    str += "                <a href='store/products/product/"+slug+"'>"+name+"</a>";
    str += "                <span class='price'>";
    str += "                    <span class='amount'>"+qty+" x <span class='store-minicart-total'>"+sub+"</span></span>";
    str += "                </span>";
    str += "            </div>";
    str += "        </li>";

    return str;

}


function formDataSupported() {
    if( window.FormData === undefined ) {
        return false;
    }
    return true;
}


    /*always do these before ajax calls*/
    function nc_aj_before_call()  {

    }
    
    /*always do these after ajax calls*/
    function nc_aj_after_call() {

        //always update any currencies
        update_currency_symbols();

    }



    /*
     * We want to show the current total items in the cart
     */
    function update_cart_info(cost,qty)  {

        /*
        name = 'some product';
        slug = 'some_product';
        qty = '77';
        sub = '44444';
        */
        //str = getMiniCartLineItem(name,slug,qty,sub);

        //alert(str);


        $(".store-cart-subtotal").html(cost);
        $("#minicart_total").html(cost);
        //$('#minicart_total').formatCurrency();


        $("#minicart_item_count").html(qty);    
        //$("#dropdown-cart ul").append(str);       
    }




    function ajax_error(str) {
        display_flash_message( str, 'error' );
    }

    function ajax_success(str) {
        display_flash_message( str, 'success' );
    }

    function ajax_notice(str) {
        display_flash_message( str, 'warning' );
    }    
  
    function display_flash_message( str, type ) {
      
        messagebar = _getMessageStart(type) + str + '</div>';

        $("#store_message_area").append(messagebar);
        
        $("#" + uuid).delay(5000).fadeOut('slow');
    }

    /** 
     * type = warning|error|success
     */
    function _getMessageStart(type) {
        uuid = guid();
        return "<div id='"+uuid+"' class='alert alert-"+type+"'><button class='close' data-dismiss='alert'>×</button>";
    }

    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
                 .toString(16)
                 .substring(1);
    };

    function guid() {
      var div = '-';
      var str = '';
      for(i=1;i<4;i++) {
        str+=s4()+ div;
      }
      return str+=s4();
    }
