/*
 * Simple swap function
 * Can swap any 2 elements in Jquery
 * Using a.swap(b);
 */
$.fn.swap = function(other) {
    $(this).replaceWith($(other).after($(this).clone(true)));
};


jQuery(function($){

	/*
	 * Generate Slug for Products,  Categories, Brands etc..
	 */
	oxy.generate_slug('input[name="name"]', 'input[name="slug"]');


});