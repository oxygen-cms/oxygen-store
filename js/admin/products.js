
function setfield(i,idstring)
{

	var id = "#store_col_" + idstring + "_" + i;

	var ct = parseInt(  $(id).attr("status") );

	ct = (ct==1)?0:1;

	var url_array = [];

	url_array[1] = 'store/admin/products/setfield/'+i+'/searchable/' + ct;	
	url_array[2] = 'store/admin/products/setfield/'+i+'/public/' + ct;
	url_array[3] = 'store/admin/products/setfield/'+i+'/featured/' + ct;
	
	the_url = url_array[idstring];

	$.post( the_url ).done(function(data)
	{
		var obj = jQuery.parseJSON(data);

		switch(obj.status)
		{
			case 'success':
				if(obj.prop==0)
				{

                	$(id).attr("class", 'tooltip-s fa fa-times text-danger');
					$(id).attr("status", 0);
					break;
				}
				else if(obj.prop==1)
				{			
                	$(id).attr("class", 'tooltip-s fa fa-check text-danger');
					$(id).attr("status", 1);
					break;
				}
			case 'error':
			default:
				alert('Oops, something went wrong. Try refreshing the page');
                $(id).attr("class", 'icon-warning');
				$(id).attr("status", 0);
				break;
		}

	});
}

/**	
 * Hide/Show filter section
 *
 */
function toggle_filter()
{
	var e = document.getElementById('hideable_filters');
    var o = document.getElementById('flink');

	if (e.style.display =='none')
    {
        e.style.display = 'block';
    }
    else
    {
        e.style.display = 'none';
    }
}
