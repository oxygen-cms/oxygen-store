<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */


# Common namespace will reside in the Core Front and Admin
$lang['store:common:payment_cancelled']        				= 'Payment Cancelled';
$lang['store:common:order_does_not_exist']     				= 'Invalid Order ID.';
$lang['store:common:order_has_been_paid']      				= 'This Order has been paid for.';
$lang['store:common:cart_updated']             				= 'Cart has been updated';
$lang['store:common:create']                                = 'Create New';
$lang['store:common:add']                                 	= 'Add';
$lang['store:common:edit']                                  = 'Edit';
$lang['store:common:delete']                                = 'Delete';
$lang['store:common:product_not_found']            			= 'Unable to locate product';


# OLD LANG FOR BACKWARDS COMPATIBILITY


# Payments
$lang['store:payments:payment_cancelled']        = 'Payment Cancelled';
$lang['store:payments:order_does_not_exist']     = 'Invalid Order ID.';
$lang['store:payments:order_has_been_paid']      = 'This Order has been paid for.';


# Field labels
$lang['store:checkout:shipment']                 = 'Shipping Method';
$lang['store:checkout:order_has_been_placed']    = 'Your order has been placed';




# Cart Messages
$lang['store:cart:unknown']                      = 'Error : Unknown Product or action';
$lang['store:cart:order_has_been_placed']        = 'Your order is placed';
$lang['store:cart:dropped']                      = 'our cart has been emptied.';
$lang['store:cart:updated']                      = 'updated';
$lang['store:cart:item_added']                   = 'Item Added to Cart';
$lang['store:cart:variant_invalid']              = 'Variant or Product invalid.';
$lang['store:cart:item_removed']                 = 'Item has been removed';
$lang['store:cart:not_in_cart']                  = 'Item not in cart.';
$lang['store:cart:must_login_first']             = 'You must login before using the cart';
$lang['store:cart:product_not_available']        = 'product_not_available';
$lang['store:cart:product_not_found']            = 'Unable to locate product';
$lang['store:cart:item_not_added']               = 'item_not_added';



// Address field labels
$lang['store:address:first_name']         = "First Name";
$lang['store:address:last_name']          = "Last Name";
$lang['store:address:company']            = "Company";
$lang['store:address:email']              = "Email";
$lang['store:address:phone']              = "Phone";
$lang['store:address:address1']           = "Address 1";
$lang['store:address:address2']           = "Address 2";
$lang['store:address:city']               = "City";
$lang['store:address:state']              = "State";
$lang['store:address:country']            = "Country";
$lang['store:address:zip']                = "Zip";

$lang['store:fields:address:first_name']         = "First Name";
$lang['store:fields:address:last_name']          = "Last Name";
$lang['store:fields:address:company']            = "Company";
$lang['store:fields:address:email']              = "Email";
$lang['store:fields:address:phone']              = "Phone";
$lang['store:fields:address:address1']           = "Address 1";
$lang['store:fields:address:address2']           = "Address 2";
$lang['store:fields:address:city']               = "City";
$lang['store:fields:address:state']              = "State";
$lang['store:fields:address:country']            = "Country";
$lang['store:fields:address:zip']                = "Zip";


