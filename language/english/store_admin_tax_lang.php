<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['store:tax:create']                    =  'Create Tax Class';
$lang['store:tax:rate']                    =  'Rate';
$lang['store:tax:title']                    =  'Tax Classes';
$lang['store:tax:no_data']                    =  'there are currently no <em>Custom Tax Groups</em>.';
$lang['store:tax:no_data_create']            =  "You can <a href='".NC_ADMIN_ROUTE."/tax/create/'>create</a> one now";
