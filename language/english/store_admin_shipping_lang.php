<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:shipping:title']                =   'Shipping Options';
$lang['store:shipping:installed']            =   'Installed Options';
$lang['store:shipping:available']            =   'Available Options';
$lang['store:shipping:name']                 =   'Name';
$lang['store:shipping:image']                =   'Image';
$lang['store:shipping:description']          =   'Description';
$lang['store:shipping:actions']              =   'Actions';
$lang['store:shipping:details']              =   'Details';
$lang['store:shipping:disable']              =   'Disable';
$lang['store:shipping:uninstall']            =   'Uninstall';
$lang['store:shipping:enable']               =   'Enable';
$lang['store:shipping:edit']                 =   'Edit';
$lang['store:shipping:edit_title']           =   'Edit Shipping Option';
$lang['store:shipping:field_title']          =   'Title';
$lang['store:shipping:id']                   =   'ID';
$lang['store:shipping:code']                 =   'Code';
$lang['store:shipping:no_installed']         =   "There are NO shipping options installed.";
$lang['store:shipping:delete_error']         =   "Unable to remove/uninstall Shipping";
$lang['store:shipping:region']               =   'Region';


//
// Shipping Zones
//
$lang['store:shipping:zones:create']         = 'New Zone';
$lang['store:shipping:zones:create_country']         = 'New Country';
$lang['store:shipping:zones:create_state']         = 'New State';
$lang['store:shipping:zones:create']         = 'New Zone';
$lang['store:shipping:zones:master']         = 'Master Countries List';
$lang['store:shipping:zones:showall']        = 'Y (All)';
$lang['store:shipping:zones:showactive']     = 'Y (Active)';