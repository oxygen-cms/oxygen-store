<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:reports:title']		=	'Reports';
$lang['store:reports:no_data']		=	'No data available';


$lang['store:reports:description']		=	'Description';
$lang['store:reports:type']		=	'Type';
$lang['store:reports:name']		=	'Name';




$lang['storedt_reports:admin:menu']		=	'Reports';
$lang['storedt_reports:global:back']       =   'Back';
$lang['storedt_reports:admin:export']      =   'Export';