<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['store:features:title']                    =  'Store Features';
$lang['store:features:description']                    =  'You must have all sub-systems installed before installing features';


$lang['store:features:system']                    =  'System';
$lang['store:features:version']                    =  'Version';
$lang['store:features:requires']                    =  'Requires';
$lang['store:features:status']                    =  'Status';
$lang['store:features:action']                    =  'Action';
