<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

$lang['store:packages:create']                       =   'New Package';
$lang['store:packages:create_group']                 =   'New Package Group';
$lang['store:packages:title']                        =   'Packages';
$lang['store:packages:name']                         =   'Name';
$lang['store:packages:height']                       =   'Height';
$lang['store:packages:width']                        =   'Width';
$lang['store:packages:length']                       =   'Length';
$lang['store:packages:weight']                       =   'Weight';
$lang['store:packages:max_weight']                   =   'Max Weight';
$lang['store:packages:curr_weight']                  =   'Current Weight';
$lang['store:packages:inner_outer_height']           =   'Height (inner/outer)';
$lang['store:packages:inner_outer_width']            =   'Width (inner/outer)';
$lang['store:packages:inner_outer_length']           =   'Length/Depth (inner/outer)';
$lang['store:packages:empty_max_weight']             =   'Weight (empty/max)';
$lang['store:packages:new']                          =   'Create';
$lang['store:packages:actions']                      =   'Actions';
$lang['store:packages:edit']                         =   'Edit';
$lang['store:packages:copy']                         =   'Copy';
$lang['store:packages:copy_edit']                    =   'Copy and Edit';
$lang['store:packages:delete']                       =   'Delete';

$lang['store:packages:no_packages_here']             =   "No packages here - <a href='".NC_ADMIN_ROUTE."/packages/create'>Create</a> one now.";
$lang['store:packages:no_packagegroup_here']         =   "No Packages Groups here - <a href='".NC_ADMIN_ROUTE."/packages_groups/create'>Create</a> one now.";