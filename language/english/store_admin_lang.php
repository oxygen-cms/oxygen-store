<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

//
// common admin stuff
//


$lang['store:admin:shipping_zones']                          =   'Zones';
$lang['store:admin:shop_setup_menu']                         =   'Store Setup';
$lang['store:admin:products_types']                          =   'Product Types';
$lang['store:admin:store_health']                            =   'Store Health';
$lang['store:admin:store_features']                          =   'Store Features';
$lang['store:admin:store_subsystems']                        =   'Store Subsystems';
$lang['store:admin:shop']                                    =   'Store';
$lang['store:admin:shop_admin']                              =   'Store Settings';
$lang['store:admin:feedback']                                =   'Enjoy using Store! Send us your feedback here <a href="mailto:feedback@oxygen-cms.com">feedback@oxygen-cms.com</a>';
$lang['store:admin:home']                               	 =   'Store Home';
$lang['store:admin:dashboard']                               =   'Dashboard';
$lang['store:admin:orders']                                  =   'Orders';
$lang['store:admin:dailydeals']                              =   'Daily Deals';
$lang['store:admin:options']                                 =   'Options';
$lang['store:admin:products']                                =   'Products';
$lang['store:admin:affiliates']                              =   'Affiliates';
$lang['store:admin:affiliates_users']                        =   'Affiliate Users';
$lang['store:admin:categories']                              =   'Categories';
$lang['store:admin:brands']                                  =   'Brands';
$lang['store:admin:gateways']                                =   'Payment Gateways';
$lang['store:admin:shipping']                                =   'Shipping Options';
$lang['store:admin:blacklist']                               =   'Blacklist';
$lang['store:admin:charts']                                  =   'Charts';
$lang['store:admin:analytics']                               =   'Analytics';
$lang['store:admin:manage']                                  =   'Manage Store';
$lang['store:admin:view_shop']                               =   'Goto Shop';
$lang['store:admin:packages']                                =   'Packages';
$lang['store:admin:packages_groups']                         =   'Package Groups';
$lang['store:admin:shipping_countries']                      =   'Zones &amp; Countries';
$lang['store:admin:customers']                               =   'Customers';
$lang['store:admin:shipping_packages']                       = 	'Shipping Packages';
$lang['store:admin:sandbox']                               	=   'Sandbox';
$lang['store:admin:reports']                               	=   'Reports';
$lang['store:admin:carts']                               	=   'Shopping Carts';
$lang['store:admin:apis']                               	=   'API Keys';

$lang['store:admin:remove']                               	=   'Remove';
$lang['store:admin:uninstall']                              =   'Uninstall';
$lang['store:admin:uninstall_systems']                      =   'Uninstall Systems ONLY';
$lang['store:admin:uninstall_features']                     =   'Uninstall Features ONLY';
$lang['store:admin:uninstall_module']                       =   'Uninstall entire module';


$lang['store:admin:coupons']                       			= 	'Coupons';
$lang['store:admin:workflows']                       		=  	'Workflows';
$lang['store:admin:tax']                       				=  	'Configure Tax';
$lang['store:admin:coupons:create']                       	=  	'Create Coupon';
$lang['store:admin:tax:create']                       		=  	'Create Tax Class';
$lang['store:admin:workflows:create']                       	=  	'Create Workflows';

// Other common admin lang
$lang['store:admin:menu']               						=	'Store';
$lang['store:admin:id']                 						=   'ID';
$lang['store:admin:slug']               						=   'Slug';
$lang['store:admin:actions']               					=   'Actions';


$lang['store:admin:view']                 					=   'View';
$lang['store:admin:edit']               						=   'Edit';
$lang['store:admin:delete']               					=   'Delete';
$lang['store:admin:name']               						=   'Name';


$lang['store:admin:enable']               					=   'Enable';
$lang['store:admin:disable']               					=   'Disable';


$lang['store:admin:created_by']                  =  'Created by';
$lang['store:admin:date_created']                =  'Date created';
$lang['store:admin:date_updated']                =  'Date updated';


//common features
$lang['store:common:create']               					=   'Create';
