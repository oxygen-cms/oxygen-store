<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:pt:create']                   		  =  'New Type';
$lang['store:pt:create_product']                   =  'New Product';
$lang['store:pt:create_attribute']                 =  'New Attribute';

$lang['store:variances:menu']                      =  'Variances';
$lang['store:variances:id']                        =  'ID';
$lang['store:variances:new']                       =  'New';
$lang['store:variances:edit']                      =  'Edit';
$lang['store:variances:name']                      =  'Name';
$lang['store:variances:variance']                  =  'Variance';
$lang['store:variances:variances']                 =  'Variances';
$lang['store:variances:no_data']                   =  'There are currently no variances here.';
$lang['store:variances:variances_placeholder']     =  'Enter variance name here';


$lang['store:variances:standard']                  =  'Standard';
$lang['store:variances:created']                   =  'Created';


$lang['store:variances:no_delete_core']               =  'Can not edit/delete the default variant';

$lang['store:variances:required']                  	=  'Required';
$lang['store:variances:a_description']               =  'A Variance is a label that uniquely differentiates each variation.';
