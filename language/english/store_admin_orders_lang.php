<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

# Admin/Orders
$lang['store:orders:item']                                    = 'Item';
$lang['store:orders:image']                                    = 'Image';
$lang['store:orders:title']                                  = 'Orders';
$lang['store:orders:actions']                                = 'Actions';
$lang['store:orders:account']  								= 'Account';
$lang['store:orders:ago']  									= 'Ago';
$lang['store:orders:billing']                                = 'Billing';
$lang['store:orders:shipping']                               = 'Shipping';
$lang['store:orders:compose']                                = 'Compose';
$lang['store:orders:customer']                               = 'Customer';
$lang['store:orders:date']                                   = 'Date';
$lang['store:orders:date_order_placed']                      = 'Date order placed';
$lang['store:orders:details']                                = 'Details';
$lang['store:orders:download']                               = 'Download';
$lang['store:orders:filter']                                 = 'Filter';
$lang['store:orders:from']                                   = 'From';
$lang['store:orders:ip_address']                             = 'Ip address';
$lang['store:orders:item_code']                              = 'Code';
$lang['store:orders:items']                                  = 'Items';
$lang['store:orders:items_amount']                           = 'Items amount';
$lang['store:orders:loading_messages']                       = 'Loading messages';
$lang['store:orders:notes']                                  = 'Notes';
$lang['store:orders:order_id']                               = 'Order id';
$lang['store:orders:id']                               = 'Order id #';
$lang['store:orders:order_id_description']                   = 'Order id description';
$lang['store:orders:order_status']                           = 'Order status';
$lang['store:orders:status']                           = 'Status';
$lang['store:orders:pmt_status']  							= 'Pmt status';
$lang['store:orders:order_total']                            = 'Order total';
$lang['store:orders:payment_type']                           = 'Payment type';
$lang['store:orders:price_base']                             = 'Base';
$lang['store:orders:progress_status']                        = 'Progress status';
$lang['store:orders:qty']                                    = 'Qty';
$lang['store:orders:reason']                                 = 'Reason';
$lang['store:orders:shipping_amount']                        = 'Shipping amount';
$lang['store:orders:shipping_method']                        = 'Shipping method';
$lang['store:orders:subtotal']                               = 'Subtotal';
$lang['store:orders:total']                                  = 'Total';
$lang['store:orders:transactions']                           = 'Transactions';
$lang['store:orders:user']                                   = 'User';
$lang['store:orders:view']                                   = 'View';
$lang['store:orders:view_message']                           = 'View message';
$lang['store:orders:send']                                   = 'Send';
$lang['store:orders:save']                                   = 'Save';
$lang['store:orders:email']                                  = 'Email';
$lang['store:orders:first_name']                             = 'First Name';
$lang['store:orders:last_name']                              = 'Surname';
$lang['store:orders:company']                                = 'Company';
$lang['store:orders:address']                                = 'Address';
$lang['store:orders:city']                                   = 'City';
$lang['store:orders:state']                                  = 'State';
$lang['store:orders:country']                                = 'Country';
$lang['store:orders:zip']                                    = 'Zip/Postcode';
$lang['store:orders:phone']                                  = 'Phone';
$lang['store:orders:price']                                  = 'Price';
$lang['store:orders:no_orders_to_display']                    = 'There are no orders here to display';
$lang['store:orders:no_data']                                  = 'No results found here, try a different filter.';
$lang['store:orders:deleted']                                  = 'Deleted';
$lang['store:orders:identification']                                  = 'Identification';
$lang['store:orders:discounts']                                  = 'Discounts';
$lang['store:orders:total_tax']                                  = 'Total Tax';
$lang['store:orders:deleted']                                  = 'Deleted';
$lang['store:orders:pmt_shipping']                                  = 'Payment &amp; Shipping';
$lang['store:orders:payment_method']                                  = 'Payment Method:';
$lang['store:orders:shipping_options']                                  = 'Shipping Option(s):';
