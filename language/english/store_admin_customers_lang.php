<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

//
// common admin stuff
//
$lang['store:customers:customer']           		=   'Customer';
$lang['store:customers:customers']           		=   'Customers';
$lang['store:customers:active_customers']           =   'Active Customers';
$lang['store:customers:edit_profile']           	=   'Edit Profile';
$lang['store:customers:general_info']           	=   'General';
$lang['store:customers:client_order']           	=   'Client Orders';



$lang['store:customers:deleted']           			=   'Deleted';
$lang['store:customers:manage']           			=   'Manage';
$lang['store:customers:user_id']           			=   'User ID';