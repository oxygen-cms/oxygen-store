<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

# Required
$lang['store:admin:menu']	        =  'Affiliates';

$lang['store:admin:id']            =  'ID';
$lang['store:admin:new']           =  'New';
$lang['store:admin:edit']          =  'Edit';
$lang['store:admin:view']          =  'View';
$lang['store:admin:name']          =  'Name';
$lang['store:code']          		=  'Code';
$lang['store:admin:affiliate']     =  'affiliate';
$lang['store:admin:affiliates']    =  'affiliates';
$lang['store:admin:no_data']       =  'There are currently no affiliates here.';
$lang['store:admin:affiliates_placeholder']     =  'Enter affiliates name here';