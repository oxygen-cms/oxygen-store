<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */


$lang['store:products:products_types']           		 = 'View Product Types';
$lang['store:products:variance']                      = 'Variance';
$lang['store:products:add_variance']                  = 'Add Variance';
$lang['store:products:edit_variance']                 = 'Edit Variance';
$lang['store:products:active']                 		 = 'Active';
$lang['store:products:variation_name']                 = 'Variation name';



$lang['store:products:actions']                      = 'Actions';
$lang['store:products:add']                          = 'Add';
$lang['store:products:base_price']                   = 'Base price';
$lang['store:products:click_to_change']              = 'Click to change';
$lang['store:products:code']                         = 'Code';
$lang['store:products:cancel']                       = 'Cancel';
$lang['store:products:customer_view']                = 'Customer View';
$lang['store:products:view']                			= 'View';
$lang['store:products:delete']                		= 'Delete';
$lang['store:products:code_description']             = 'Code description';
$lang['store:products:create']                       = 'Create / Add product';
$lang['store:products:default_price']                = 'Default Price';
$lang['store:products:created']                      = 'Created';
$lang['store:products:updated']                      = 'Updated';
$lang['store:products:date_created']                 = 'Created';
$lang['store:products:date_updated']                 = 'Updated';
$lang['store:products:description']                  = 'Description';
$lang['store:products:default']                      = 'Default';
$lang['store:products:depth']                        = 'Depth';
$lang['store:products:edit']                         = 'Edit';
$lang['store:products:featured']                     = 'Featured';
$lang['store:products:filter']                       = 'Filter';
$lang['store:products:height']                       = 'Height';
$lang['store:products:height_description']           = 'Height description';
$lang['store:products:id']                           = 'ID';
$lang['store:products:id_descending']                = 'ID descending';
$lang['store:products:items_per_page']               = 'Items per page';
$lang['store:products:keywords']                     = 'Keywords';
$lang['store:products:mid_discount']                 = 'Mid discount';
$lang['store:products:min_purchase_req']             = 'Min purchase req';
$lang['store:products:min_qty']                      = 'Min qty';
$lang['store:products:name']                         = 'Name';
$lang['store:products:name_descending']              = 'Name descending';
$lang['store:products:name_description']             = 'Name description';
$lang['store:products:order_by']                     = 'Order by';
$lang['store:products:price']                        = 'Price';
$lang['store:products:price_tab_description']        = 'Price tab description';
$lang['store:products:products']                     = 'Products';
$lang['store:products:rrp']                          = 'RRP';
$lang['store:products:save']                         = 'Save';
$lang['store:products:slug']                         = 'Slug';
$lang['store:products:save_and_exit']                = 'Save and exit';
$lang['store:products:search']                       = 'Search';
$lang['store:products:searchable']                   = 'Searchable';
$lang['store:products:select_all']                   = 'Select all';
$lang['store:products:select_none']                  = 'Select none';
$lang['store:products:set_as_cover']                 = 'Set as cover';
$lang['store:products:standard']                     = 'Standard';
$lang['store:products:start']                        = 'Start';
$lang['store:products:true_id']                      = 'True id';
$lang['store:products:view_as_admin']                = 'View as administrator';
$lang['store:products:view_as_customer']             = 'View as customer';
$lang['store:products:visibility']                   = 'Visibility';
$lang['store:products:weight']                       = 'Weight';
$lang['store:products:width']                        = 'Width';
$lang['store:products:yes']                          = 'Yes';
$lang['store:products:no']                           = 'No';
$lang['store:products:duplicate_success']            = 'Product duplicated.';
$lang['store:products:duplicate_failed']             = 'Product was unable to be duplicated.';
$lang['store:products:delete_success']               = 'Product deleted.';
$lang['store:products:delete_failed']                = 'Product failed to delete.';
$lang['store:products:cant_find']                	 = 'Unable to locate the product you were looking for.';


$lang['store:products:no_products_to_display']       = "There are no products in this store, <a href='".NC_ADMIN_ROUTE."/product/create'>create</a> one now.";
$lang['store:products:product']                           = 'Product';

$lang['store:products:variations']                           = 'Variations';




$lang['store:products:length']                           = 'Length';
$lang['store:products:shipping_zone']                           = 'Shipping Zone';
$lang['store:products:package_class']                           = 'Package class/Group';
$lang['store:products:base_price']                           = 'Base price';
$lang['store:products:shipping']                           = 'Shipping';
$lang['store:products:discountable']                           = 'Discountable';
$lang['store:products:pricing']                           = 'Pricing';

$lang['store:products:weight']                           = 'Weight';

$lang['store:products:more']                           = 'More';







