<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:my:test']                           = 'TEST';
$lang['store:my:address_created_success']        = 'Address was created';
$lang['store:my:address_deleted_success']        = 'Address was removed';
$lang['store:my:user_not_authenticated']         = 'User not authenticated';
$lang['store:my:order_not_found']                = 'Order not found';
$lang['store:my:wishlist']                       = 'Wishlist';
$lang['store:my:already_in_wishlist']            = 'Item already in Wishlist';
$lang['store:my:product_not_found']              = 'Product not found';

$lang['store:my:product_unavailable']            = 'Product unavailable';
$lang['store:my:wishlist:delete_success']        = 'Item removed from wishlist';
$lang['store:my:wishlist:delete_error']          = 'Unable to remove from wishlist';