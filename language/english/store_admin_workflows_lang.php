<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:workflows:title']                    =  'Workflow Management';
$lang['store:workflows:workflow']                    =  'Workflow';
$lang['store:workflows:workflows']                    =  'Workflows';
$lang['store:workflows:progress']                    =  'Progress';
$lang['store:workflows:create']                    =  'Create Workflow';
$lang['store:workflows:no_data']                    =  'There are no current workflows';


$lang['store:workflows:save_exit']                    =  'Save &amp; Exit';

$lang['store:workflows:cancel']                    =  'Cancel';


