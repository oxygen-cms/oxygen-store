<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
# Dashboard
$lang['store:dashboard:total_products']                      =   'Total SHOP products';
$lang['store:dashboard:total_products_live']                 =   'Total Active Products (Selling)';
$lang['store:dashboard:total_products_hidden']               =   'Total Hidden Products (Not selling)';
$lang['store:dashboard:total_products_live_featured']        =   'Total Live & Featured';
$lang['store:dashboard:total_products_deleted']              =   'Total Deleted Products';
$lang['store:dashboard:total_shipping_methods']              =   'Active Shipping Options';
$lang['store:dashboard:total_gateway_methods']               =   'Active Gateway Options';

# Admin/Dashboard
$lang['store:dashboard:as_csv']                              = 'As csv';
$lang['store:dashboard:as_json']                             = 'As json';
$lang['store:dashboard:as_xml']                              = 'As xml';
$lang['store:dashboard:catalogue']                           = 'Catalogue';
$lang['store:dashboard:city']                                = 'City';
$lang['store:dashboard:clear_all_cache']                     = 'Clear all cache';
$lang['store:dashboard:clear_categories_cache']              = 'Clear categories cache';
$lang['store:dashboard:clear_product_cache']                 = 'Clear product cache';
$lang['store:dashboard:customer']                            = 'Customer';
$lang['store:dashboard:day']                                 = 'Day';
$lang['store:dashboard:low_level']                           = 'Low level';
$lang['store:dashboard:low_stock']                           = 'Low stock';
$lang['store:dashboard:low_stock_level_ok']                  = 'Low stock level ok';
$lang['store:dashboard:metric']                              = 'Metric';
$lang['store:dashboard:month']                               = 'Month';
$lang['store:dashboard:notification_date']                   = 'Notification date';
$lang['store:dashboard:out_of_stock']                        = 'Out of stock';
$lang['store:dashboard:product']                             = 'Product';
$lang['store:dashboard:recent_orders']                       = 'Recent orders';
$lang['store:dashboard:total']                               = 'Total';
$lang['store:dashboard:total_brands']                        = 'Total brands';
$lang['store:dashboard:total_categories']                    = 'Total categories';
$lang['store:dashboard:total_offline_products']              = 'Total offline products';
$lang['store:dashboard:total_online_products']               = 'Total online products';
$lang['store:dashboard:total_products']                      = 'Total products';
$lang['store:dashboard:week']                                = 'Week';
$lang['store:dashboard:overview']                            = 'Overview';
$lang['store:dashboard:orders_7days']                        = 'Orders (7 Days)';
