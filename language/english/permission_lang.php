<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:role_admin_store_subsystems']	= 'Manage Features and Subsystems (Developer)<br>';
$lang['store:role_admin_checkout']          = 'Manage Checkout options : Shipping, Packages, Payment Gateways and Countries (Developer)<br>';
$lang['store:role_admin_workflows']			= 'Manage Workflows (Developer)<br><br><br>';


$lang['store:role_store_administrator'] 	= 'Store Administrator (Can open and close store)<br>';
$lang['store:role_admin_r_catalogue_edit'] 	= 'Products &plus; Catalogue<br>';

$lang['store:role_admin_affiliates']    	= 'Affiliates (create and manage) [FEATURE]';
$lang['store:role_admin_reports']           = 'View Reports<br>';
$lang['store:role_admin_orders']            = 'Manage and View Orders<br>'; //allow to set status


$lang['store:role_admin_tax'] 				= 'Manage Tax groups<br><br>';

