<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
$lang['store:gateways:title']                        =   'Payment Gateways';
$lang['store:gateways:title_options']                =   'Payment Gateways (Payment Options)';
$lang['store:gateways:form_title']                   =   'Gateway Information';


$lang['store:gateways:installed']                    =   'Installed Options';
$lang['store:gateways:available']                    =   'Available Options';
$lang['store:gateways:name']                         =   'Name';
$lang['store:gateways:image']                        =   'Image';
$lang['store:gateways:description']                  =   'Description';
$lang['store:gateways:actions']                      =   'Actions';
$lang['store:gateways:details']                      =   'Details';
$lang['store:gateways:disable']                      =   'Disable';
$lang['store:gateways:uninstall']                    =   'Uninstall';
$lang['store:gateways:enable']                       =   'Enable';
$lang['store:gateways:edit']                         =   'Edit';
$lang['store:gateways:edit_title']                   =   'Edit Gateway Option';
$lang['store:gateways:field_title']                  =   'Title';
$lang['store:gateways:id']                           =   'ID';
$lang['store:gateways:code']                         =   'Code';
$lang['store:gateways:no_installed_gateways']        =   'There are no installed Gateways';
$lang['store:gateways:delete_error']                 =   'Unable to delete Gateway';

$lang['store:gateways:password']                     =   'Password';
$lang['store:gateways:username']                     =   'Username';
$lang['store:gateways:api_signature']                =   'Api Signature';
$lang['store:gateways:test_mode']                    =   'Test Mode';
$lang['store:gateways:options']                    =   'Options';

