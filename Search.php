<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search_Store
{
    protected $ci;

    protected static $results;
    protected static $added;

    public function __construct()
    {
        $this->ci =& get_instance();
        Search::register('admin_search', array($this, 'admin_search'));
    }

    public function admin_search($query_string='')
    {
        self::$added    = [];       
        self::$results  =
        [
            'total-results'=>0,
            'results'=> []
        ];   

        $terms = explode ( ' ', $query_string );

        foreach($terms as $term) {
            $this->list_all_products($term);
        }

        return self::$results;
    }

    private function list_all_products($term) {

        $products = [];


        $query = [];
        $query['search'] = $term;
        $query['f_filter'] = 'all';
        $query['status'] = 'active';

        $query['order_by_order'] = 'desc';
        $query['order_by'] = 'id';

        $this->ci->load->model('store/admin/products_admin_filter_m');

        $products = $this->ci->products_admin_filter_m->filter($query,5);


        foreach($products as $product) 
        {

            if(isset(self::$added[$product->id])) {
                //pass this product is already added
                continue; 
            }

            //add the index
            self::$added[$product->id]= $product->id;

            $this->_add_result($product);
        }   
    }

    private function _add_result($product)
    {
        self::$results['total-results'] = self::$results['total-results'] + 1;
        self::$results['results'][] = 
        [
            'title'=>$product->name,
            'module'  => 'Store',
            'icon'=> 'fa fa-shopping-cart',
            'description'=>$product->name,
            'url'=>site_url('store/products/product/'.$product->id),
            'admin_url'=>site_url('admin/store/product/edit/'.$product->id),
        ];
    }


}
/* End of file Search.php */