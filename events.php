<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Events_Store
{

    static $common_payload_called;


    /**
     * Get the CI instance into this object
     *
     * @param unknown_type $var
     */
    public function __get($var)
    {
        if (isset(get_instance()->$var))
        {
            return get_instance()->$var;
        }
    }




    /**
     * Register event triggers
     */
    public function __construct()
    {   
        // Register events
        // System Events
        //Events::register('admin_controller', array($this, 'evt_admin_controller'));
        Events::register('public_controller', array($this, 'evt_public_controller'));
        Events::register('post_user_register', array($this, 'evt_post_user_register'));
        Events::register('post_user_activation', array($this, 'evt_post_user_activation'));
        Events::register('post_user_login', array($this, 'evt_user_login'));
        Events::register('post_admin_login', array($this, 'evt_admin_login'));
        Events::register('admin_display_dashboard', [$this, 'storevt_dashboard'] );

        //
        // Store Events
        //

        // Store Admin Events
        Events::register('STOREVT_RegisterModule', array($this, 'storevt_register_module'));
        Events::register('STOREVT_DeRegisterModule', array($this, 'storevt_de_register_module'));

        //Store Public events
        Events::register('STOREVT_OrderPlaced', array($this, 'storevt_order_lodged'));
        Events::register('STOREVT_gateway_callback', [$this, 'storevt_gateway_callback']);
        Events::register('STOREVT_order_paid', [$this, 'storevt_order_paid'] );
        Events::register('STOREVT_SendOrderInvoice', array($this, 'storevt_order_resend_invoice'));
        Events::register('STOREVT_ShopPublicController', array($this, 'storevt_ShopPublicController'));
        Events::register('STOREVT_ShopAdminController', array($this, 'storevt_ShopAdminController'));
        Events::register('STOREVT_AdminProductListGetFilters', array($this, 'storevt_AdminProductListGetFilters'));
        Events::register('STOREVT_AdminProductGet', [$this, 'storevt_admin_product_get']);
        Events::register('maintenance_request', array($this, 'maintenance_request'));
    }

    /**
     * Collect data for the (CMS) dashboard
     */
    public function storevt_dashboard()
    {

        //temp add
        //Asset::add_path('tasks', APPPATH.'modules/tasks/' );

        $this->load->model('store/orders_m');
        $this->load->model('store/statistics_m');       
        $this->load->model('store/admin/orders_admin_m');       
        $this->load->model('store/admin/products_admin_m');     
        $this->lang->load('store/store_admin_dashboard');

        // Collect 5 most recent orders
        $limit = 5;
        $data = (object) [];

        $data->revenue_today = $this->statistics_m->getStoreRevenue(1);
        $data->revenue_week = $this->statistics_m->getStoreRevenue(7);
        $data->revenue_monthly = $this->statistics_m->getStoreRevenue(30);
        $data->revenue_anual = $this->statistics_m->getStoreRevenue(365);

        $data->store_dash_aobc =  $this->statistics_m->getAverageOrdersByCustomers(365);
        

        $data->order_items = $this->orders_admin_m->get_most_recent($limit);
        //$data->order_items = $this->orders_admin_m->where('nct_orders.deleted',NULL)->limit($max)->offset(0)->order_by('order_date','desc')->get_all();

        $data->cat = $this->statistics_m->get_catalogue_data();

        $data->most_viewed =  $this->statistics_m->_get_most_viewed(5);

        //TODO: Use better chart api, also improve the data
        $rows = $this->db->query("select order_date, count(id) as `Val` from ".$this->db->dbprefix('storedt_orders')." group by DATE(FROM_UNIXTIME(order_date)) LIMIT 4");
        
        $data->SalesRecords = ($rows) ? $rows->result() : [] ;

        $data->recent_users = $this->statistics_m->get_recent_members(6);

        $data->average_order = $this->statistics_m->monthlyAverageSale();
        $data->active_carts = $this->statistics_m->activeCarts();
        $data->file_count = $this->statistics_m->countFiles();


    
        $this->template->set($data);
        //$this->template->append_js('tasks::dashboard_widget.js');

        //$this->template->set('todo_tasks',$todo_tasks);
        
    }

    //request a list of tasks to display in maintenance module
       
    public function maintenance_request($object_list_array)
    {

       $object_list_array->items[] = ['module'=>'store','name'=>'Clear Deleted EAVs','action'=>'admin/store/product_maintenance/clean_eav_products', 'button_text'=>'Clear','description'=>''];
       $object_list_array->items[] = ['module'=>'store','name'=>'Check for Unzoned products','action'=>'admin/store/product_maintenance/check_zones', 'button_text'=>'Check','description'=>''];
       $object_list_array->items[] = ['module'=>'store','name'=>'Clean deleted variations','action'=>'admin/store/product_maintenance/clean_variations', 'button_text'=>'Clean','description'=>''];

       $object_list_array->items[] = ['module'=>'store','name'=>'Ability to Assign packages','action'=>'admin/store/checklist/test_shipping_boxes', 'button_text'=>'Check','description'=>''];
       $object_list_array->items[] = ['module'=>'store','name'=>'Ability to Checkout','action'=>'admin/store/checklist/test_checkout_ability', 'button_text'=>'Check','description'=>''];
       $object_list_array->items[] = ['module'=>'store','name'=>'Check zones','action'=>'admin/store/checklist/test_all_zones_accountability', 'button_text'=>'Clean','description'=>''];     
       
    } 

    /**
     * This is where core/built in features can show tabs for the prouct view
     *
     * @param  [type] $product [description]
     * @return [type]          [description]
     */
    public function storevt_admin_product_get($product){ }



    /**
     * Get filters for the admin products display
     */
    public function storevt_AdminProductListGetFilters($filters)
    {
        $all = $this->db->get('storedt_products_types')->result();
        $filters->modules["All Products"]['store,all|0']     = 'All products';
        $filters->modules["All Products"]['store,price|0']   = 'With price records';
        $filters->modules["All Products"]['store,noprice|0'] = 'Without price records';
        
        foreach($all as $type)
            $filters->modules["Product Type"]["store,producttype|{$type->id}"] = $type->name;
    }

    /**
     * Registers a Module with SHOP
     *
     * Expected Array format:
     *
     *   $array_data = array(
     *            'name'=> 'Categories',
     *            'namespace'=>'store_categories',
     *            'product-tab'=> true|false,
     *   );
     */
    public function storevt_register_module($module_data=[])
    {
        $this->_register_module($module_data,'install');
    }

    /**
     * De-register a shop module, usually for uninstall
     */
    public function storevt_de_register_module($module_data=[])
    {
        $this->_register_module($module_data,'uninstall');
    }


    /**
     * helpers for register/deregister
     */
    private function _register_module($module_data=[],$method='install')
    {
        $this->load->model('store/modules_m');
        $this->modules_m->$method($module_data);
    }



    /**
     * Event for user login
     */
    public function evt_user_login()
    {
        $this->storevt_ShopPublicController();
        $this->load->library('store/workflow_library');
        $this->workflow_library->user_login();
    }

    /**
     * Login strait to shop dashboard
     *
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function evt_admin_login($data=NULL)
    {
        $this->load->library('store/workflow_library');
        $this->workflow_library->admin_login($data);
    }

    /**
     * if redir is set redir to that page,
     * therwise redirect to checkout
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function evt_post_user_register($id)
    {
        $this->load->library('store/workflow_library');
        $this->workflow_library->user_registered($id);
    }

    /**
     * Auto login after succesfull activation
     */
    public function evt_post_user_activation($id)
    {   
        $this->load->library('store/workflow_library');
        $this->workflow_library->user_account_activate($id);
    }

    /**
     * we need to see if the last part of store install is done, if not force ui to install
     */
    private function checkInstall() 
    {
        
        if($this->uri->uri_string == 'admin/store/subsystems') {
            return;
        }

        $row = $this->db->where('driver','system_z_admin_layer')->get('storedt_systems')->row();
        if($row) {
            if($row->installed==1) {
                return;
            }
        }

        $this->session->set_flashdata('notice','Please install the store subsystems before accessing store modules.');
        redirect('admin/store/subsystems');
    }


    /**
     * Common shop admin controller event
     */
    public function storevt_ShopAdminController($data=[])
    {
        $this->checkInstall();
        $this->storevt_ShopPublicController();
        $this->load->helper('store/store_admin');
        $this->load->helper('dropmenu');
        $this->template->enable_parser(true);
    }

    /**
     * Common public shop controller event
     */
    public function storevt_ShopPublicController($data=[])
    {
        $this->_common_payload();
    }


    /**
     * Standard public controller
     */
    public function evt_public_controller($input=[])
    {
        $this->_common_payload();
        $this->_call_store_features_events();         
    }


    /**
     * Required on all pages, standard, admin and shop controllers
     */
    private function _common_payload($is_admin=false)
    {

        //do not want to recvall this stuff
        if(self::$common_payload_called)
        {
            return;
        }

        $this->load->library('store/storecore_library'); 
        Asset::add_path('store', STOREMOD_INSTALL_PATH ); 

        $this->load->library('store/mycart'); 
        $this->load->library('store/Toolbox/Nc_tools');   
        $this->load->helper('store/store'); 
        $this->lang->load('store/store');


        self::$common_payload_called = TRUE;


        //exit
        if($is_admin) return;

        $js = PHP_EOL;
        $js.= '<!-- Globals -->'.PHP_EOL;
        $js.= '<script type="text/javascript">'.PHP_EOL;
        $js.= '    var APPPATH_URI                 = "'.APPPATH_URI.'";'.PHP_EOL;
        $js.= '    var SITE_URL                    = "'.rtrim(site_url(), '/').'/";'.PHP_EOL;
        $js.= '    var BASE_URL                    = "'.BASE_URL.'";'.PHP_EOL;
        $js.= '    var BASE_URI                    = "'.BASE_URI.'";'.PHP_EOL;
        $js.= '    var STORE_ID                    = "'.$this->_get_hash().'";'.PHP_EOL;
        $js.= '    var X_URI                       = "'.rtrim(site_url(), '/').'/'.NC_ROUTE.'";'.PHP_EOL;
        $js.= '    var NC_ROUTE                    = "'.NC_ROUTE.'";'.PHP_EOL;

        $js.= '</script>';

        $this->template->append_metadata($js);
    }


    private function _get_hash()
    {
        return md5(
            site_url().
            $this->session->userdata('http_server').
            $this->input->server('SERVER_NAME').
            $this->input->server('SERVER_ADDR').
            $this->input->server('SERVER_SIGNATURE')
            );      
    }





    /**
     * Send Admin and User Email notification that order has been placed
     * @param  array   $email_variables [description]
     * @param  boolean $is_guest        [description]
     * @return [type]                   [description]
     */
    public function storevt_order_lodged( $email_variables=[], $is_guest = false )
    {
        $this->load->library('store/email_library');
        $admin_email = Settings::get('storst_admin_order_notify_email');
        //$email_variables =  $this->email_library->prepareOrderLodgedEmail( $email_variables );

        //invoice
        $invoice_vars =  $this->email_library->prepareOrderInvoiceEmail( $email_variables['order_id'] );
        $this->email_library->sendEmail( $invoice_vars, 'storemt_order_invoice', $invoice_vars['email'] );


        //$this->email_library->sendEmail( $email_variables, 'storemt_user_order_notification', $email_variables['email'] );
        if($admin_email != '') 
        {
             $this->email_library->sendEmail( $invoice_vars, 'storemt_order_invoice', $admin_email );
            //$this->email_library->sendEmail( $email_variables, 'storemt_admin_order_notification' , $admin_email );
         }

        //additionally, if affiliates is installed check that the user is part of a affiliate campaign and log the order
        if(system_installed('feature_affiliates')) 
        {
            $this->load->library('store/features/feature_affiliates');
            
            $args = [
                'type' => 'order_placed',
                'order_id' => $email_variables['order_id'],
            ];
                 
            $this->feature_affiliates->event_main($args);
        }

        // Send Invoice
        //$this->storevt_order_resend_invoice( $email_variables['order_id'] );

    }

    /**
     * Resend invoice event
     */
    public function storevt_order_resend_invoice( $order_id )
    {
        $this->load->library('store/email_library');
        $vars =  $this->email_library->prepareOrderInvoiceEmail( $order_id );
        $this->email_library->sendEmail( $vars, 'storemt_order_invoice', $vars['email'] );
    }

    // Send Admin and User Email notification that order has been placed
    public function storevt_order_paid($id)
    {
        $this->load->library('store/email_library');
        $email_variables =  $this->email_library->prepareOrderPaidEmail( $id );
        $admin_email = Settings::get('storst_admin_pmt_notify_email');
        $this->email_library->sendEmail( $email_variables, 'storemt_user_order_paid_notification' , $email_variables['email'] );
        if($admin_email != '')
            $this->email_library->sendEmail( $email_variables, 'storemt_admin_order_paid' , $admin_email );
    }

    // Send User email notification thah payment was received
    public function storevt_gateway_callback($input=[])
    {
        return;
    }


    /**
     * Call the commen_events from the features file
     */
    private function _call_store_features_events()
    {
        $this->load->model('store/systems_m');

        $installed_features = $this->systems_m->get_installed_features();
        
        $args = null;

        foreach($installed_features as $module)
        {
            $this->load->library('store/features/'.$module->driver);
            
            $this->{$module->driver}->event_common($args);
        }
    }

}
/* End of file events.php */