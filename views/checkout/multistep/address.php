{{x:js file='countries'}}

<div class="content container">

    {{if existing_addresses}}
     <form name="form1" action="{{x:uri}}/checkout/address/" method="POST">
        <article class="container inforow"> 
            <div class="col-md-12">
                    <fieldset>
                        <h3>Select an existing address</h3>
                        <table>
                                <tr>
                                    <th>
                                        Billing|Shipping
                                    </th> 
                                    <th>
                                        Address
                                    </th>  
                                    <th>
                                        City    
                                    </th>  
                                    <th>
                                       State 
                                    </th>                                                                         
                                    <th>
                                        Postcode 
                                    </th> 
                                    <th>
                                        Country
                                    </th> 
                                </tr>                            
                            {{existing_addresses}}
                                <tr>
                                    <td>
                                        <input type="radio" name="billing_address_id" value="{{id}}">
                                    </td> 
                                    <td>
                                        {{address1}}
                                        {{ if address2 }}
                                            <br>{{address2}}
                                        {{ endif }}
                                    </td>  
                                    <td>
                                        {{city}}    
                                    </td>  
                                    <td>
                                        {{state}}  
                                    </td>                                                                         
                                    <td>
                                        {{zip}} 
                                    </td> 
                                    <td>
                                        {{ country_label  }}  
                                    </td> 
                                    <td>
                                         <a href="{{x:uri}}/my/addresses/delete/{{id}}"><img src="http://chino.aisconverse.com/images/close.png" alt=""></a>
                                    </td>                                     
                                </tr>
                            {{/existing_addresses}}
                        </table>
                    </fieldset>  

                    <input type="hidden" name="selection" value="existing">
                    <br>
                    <input type="hidden" name="same_as_billing" value='1'>

                        <div class="form-group">
                            Agree to Terms and Conditions 
                            <span class="required">*</span>
                            <input type="checkbox" name="useragreement" value="1"> 
                        </div>                      

                    <button class="btn btn-default" name='submit' type='submit'>
                        Use above adress for Billing+Shipping
                    </button>
                    OR
            </div>
        </article>
    </form>
    {{endif}}

    <form name="form1" action="{{x:uri}}/checkout/address/" method="POST">
        <div class="col-md-14"> 
                <article class="container inforow">
                    <div class="col-sm-6">
                        <div class="ship-header clearfix">
                            <h4 class="pull-left">Create Billing address</h4>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" placeholder="First name *" name="first_name" value="{{first_name}}">
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" placeholder="Last name *" name="last_name" value="{{last_name}}">
                            </div>
                        </div>
                        <div class="form-group">
                             <input class="form-control" type="text" placeholder="Company name" name="company" value="{{company}}">
                        </div>
                    
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Address *" name="address1" value="{{address1}}">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Appartment, suite, unit *" name="address2" value="{{address2}}">
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="city" value="{{city}}" placeholder="Town / City *">
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="zip" value="{{zip}}" placeholder="Postcode *">
                            </div>
                        </div>                        
                        <div class="form-group">
                            {{x:countries class='nc_country_select' name='country' link='billing' }}
                        </div>                        
                        <div class="form-group">
                             {{x:states class='nc_states_select' name='state' init='yes' link='billing' }}
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <input  name="email" value="{{email}}" class="form-control" type="email" placeholder="Email *">
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" placeholder="Phone *" name="phone" value="{{phone}}">
                            </div>
                        </div>
                        <div class="form-group">
                            Agree to Terms and Conditions 
                            <span class="required">*</span>
                            <input type="checkbox" name="useragreement" value="1"> 
                        </div>                                                
                    </div>
                    <div class="col-sm-6">
                        <div class="ship-header clearfix">
                            <h4 class="pull-left">Create Shipping Address</h4>
                            <div class="checkbox">
                                <label data-toggle="collapse" data-target="#shipto">
                                    <input type="checkbox" name='same_as_billing' value='1' checked>
                                    Ship to billing address?
                                </label>
                            </div>
                        </div>
                        <div id="shipto" class="collapse clearfix">

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" placeholder="First name *" name="shipping_first_name" value="{{first_name}}">
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" placeholder="Last name *" name="shipping_last_name" value="{{last_name}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                     <input class="form-control" type="text" placeholder="Company name" name="shipping_company" value="{{company}}">
                                </div>
                            
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Address *" name="shipping_address1" value="{{address1}}">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Appartment, suite, unit *" name="shipping_address2" value="{{address2}}">
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="shipping_city" value="{{city}}" placeholder="Town / City *">
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="shipping_zip" value="{{zip}}" placeholder="Postcode *">
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    {{x:countries class='nc_country_select' name='shipping_country' link='shipping' }}
                                </div>                        
                                <div class="form-group">
                                     {{x:states class='nc_states_select' name='shipping_state' init='yes' link='shipping'}}
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <input  name="shipping_email" value="{{email}}" class="form-control" type="email" placeholder="Email *">
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" placeholder="Phone *" name="shipping_phone" value="{{phone}}">
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                            <textarea name='instruction' class="form-control" placeholder="Notes about yout order"></textarea>
                        </div>
                    </div>
                </article>

                <div class="col-sm-12">
                    <div class="ship-header clearfix">
                        <div class="buttons"> 
                            <a class="btn btn-default" href='{{x:uri}}/cart'>
                                {{# helper:lang line="store:messages:checkout:back_to_cart" #}}Back to Cart</a> 
                                or 
                                <button class="btn btn-default" name='selection' value='new' type='submit'>
                                    Continue
                                </button>
                                
                        </div> 
                    </div> 
                </div>   
        </div>
    </form> 

</div>
