
<div class="content container">
    <form action="{{x:uri}}/checkout/gatreview" method="POST">
        <script>
        $(function() 
        {
            $(document).on('click', '.shipobj', function(event) {
                    var ship_cost = $(this).attr('data-ship');
                    var order_total = $('#order_total').attr('data-val');
                    var grand = parseFloat(order_total) + parseFloat(ship_cost);
                    //$('.shiphand_cost_div').html(ship_cost);
                    $('.store-checkout-shipping-cost').html(ship_cost);
                    //$('.order_total_div').html(grand);
                    $('.store-checkout-grand-total').html(grand);

                    

                    update_currency_symbols();
                    //event.preventDefault();
            });
        });
        </script>
    <!--
    Gateways + Shipping
    -->

        <article class="container ">
            <div class="col-sm-14">
                <div class="col-sm-6 col-sm-offset-1">
                    <h4 id="page_title">Delivery option</h4>                
                    <div class="panel-group" id="delivery">                      
                        {{shipments}}
                        <div class="panel clearfix">
                            <div class="radio">
                                <label class='shipobj' data-ship="{{shipping_cost.cost}}" data-toggle="collapse" data-target="#delivery-{{id}}" data-parent="#delivery">
                                    <input type="radio" value="{{id}}" name="shipment_id" checked=checked>
                                    {{title}}
                                    <span class="store-checkout-shipping-item-cost">{{shipping_cost.cost}}</span>
                                </label>
                            </div>
                        </div>                       
                        {{/shipments}}                
                    </div>
                </div> 

                <div class="col-sm-5">
                    <h4 id="page_title">Payment method</h4>
                    <div class="panel-group" id="payment">
                        
                        {{gateways}}
                        <div class="panel clearfix">
                            <div class="radio">
                                <label data-target="#payment-{{id}}" data-parent="#payment">
                                    <input type="radio" value="{{id}}" name="gateway_id" checked>
                                    {{title}}
                                </label>
                            </div>
                        </div>                       
                        {{/gateways}}                 
                    </div>
                </div>  
            </div>   
                            
        </article>   
    <!--
    END Gateways
    -->




    <!--
    Cart Items
    -->
        <article class="container">
            <div class="table-responsive">
                <div class="col-sm-12">

                    <h4>Review</h4>
       
                </div>
                <table class="table shop_table cart text-center">
                    <thead>
                    <tr>
                        <th></th>
                        <th class="text-left">Product</th>
                        <th class="text-center">price</th>
                        <th class="text-center">QUANTITY</th>
                        <th class="text-center">total</th>
                    </tr>
                    </thead>
                    <tbody>
                        {{cart}}
                        <tr>
                            <td class="product-thumbnail text-left">
                                 {{products:cover product_id='{{productid}}' }}
                            </td>  
                            <td class="product-name text-left"> {{products:link product_id=productid text=name}} <br> {{variant_name}}</td>
                            <td class="product-price">{{price}}</td>
                            <td class="product-quantity text-primary">{{qty}}</td>
                            <td class="product-subtotal">{{subtotal}}</td>
                        </tr>
                        {{/cart}} 
                    <tr>
                        <td></td>
                        <td class="align-right"></td>
                        <td class="align-right"></td>
                    </tr>
                
                    </tbody>
                </table>
            </div>
        </article>


    <!--
    Summary of cost
    -->
        <article class="container no-padding-top">
            <div class="coupon bg-info text-left clearfix inforow m-center">
                <div class="col-sm-7 no-padding">
                    <h4>shipping information</h4>
                    <address>
                        <p>
                        {{shipping_address.first_name}} {{shipping_address.last_name}},<br>
                        {{shipping_address.address1}} <br>
                        {{ if shipping_address.address2 }}
                            {{shipping_address.address2}},<br> 
                        {{ endif }}
                        {{shipping_address.city}},
                        {{shipping_address.state}},
                        {{shipping_address.country}}
                        &nbsp; {{shipping_address.zip}}
                        </p>
                    </address>    
                </div>
                <div class="col-sm-5">
                    <table class="table cart-total">
                        <tr>
                            <div style='display:none;' id='order_total' data-val="{{order_total_clean}}"></div>
                            <th>Items Total</th>
                            <td class="text-primary" ><span class='store-checkout-order-total'>{{order_total}}</span></td>
                        </tr>                        
                        <tr>
                            <th>Shipping and Handling</th>
                            <td class="text-primary"><span class='shiphand_cost_div'><span class='store-checkout-shipping-cost'>0</span></span></td>
                        </tr>
                        <tr style='border-top:#777 solid thin'>
                            <th>Grand Total</th>
                            <td class="text-primary"> <span class='order_total_div' ><span class='store-checkout-grand-total'>{{order_total_clean}}</span></span></td>
                        </tr>                        
                    </table>
                </div>
            </div>
        </article>
    <!--
    Continue Buttons
    -->
        <article class="container no-padding-top">
            <div class="col-sm-12">
                <a class="btn btn-default" href='{{x:uri}}/cart'>
                    Back to Cart
                </a>&nbsp; or &nbsp;
                <input class="btn btn-default" type='submit' name='submit' value='continue'>
            </div>
        </article>        
    </form>
</div>
