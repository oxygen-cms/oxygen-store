<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="row">
	    <div class="col-xs-12">
	        <div class="box box-solid">
	            <div class="box-header">
					<h4 class="box-title">
						<?php echo lang('store:manage:title'); ?>
					</h4>
					<div class="pull-right">
						
					</div>
	            </div>
	            <div class="box-body">
					<?php foreach ($thesettings as $settings): ?>
						<?php  $form = $this->settings->form_control($settings);?>
							<div class='box-body'>
								<h4>
									<?php echo $settings->title;?><br>
									<small><?php echo $settings->description;?></small>
								</h4>
								<div>
									<?php echo $form;?>
								</div>
							</div>
							<hr/>
					<?php endforeach;?>
				</div>
	            <div class="box-footer">
					<button class="btn blue" value="save_exit" name="btnAction" type="submit">
						<span><?php echo lang('store:manage:save_exit');?></span>
					</button>
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
	            </div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>