<?php

	$items = array();

	//function dropdownMenuStandard($url, $						divider=false, $label, $delete=false, $icon='edit')
	if(group_has_role('store', 'admin_r_catalogue_edit'))
		$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/product/edit/{$id}", false, lang('store:products:edit'), false, "edit");

	$items[] = dropdownMenuStandard(NC_ROUTE."/products/product/{$id}", false,  lang('store:products:customer_view'), false, "eye-open","new");


	if(group_has_role('store', 'admin_r_catalogue_edit'))
	{
		$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/product/duplicate/{$id}/list", true,  "Copy", false, "copy","","");
		$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/product/duplicate/{$id}/edit", false,  "Copy and Edit", false, "copy");
	}

	$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/products", 	true,  "Refresh", false, "refresh");

	if(group_has_role('store', 'admin_r_catalogue_edit'))
		$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/product/delete/{$id}", 	true,  lang('store:products:delete'), true, "minus");

	echo dropdownMenuListSplit($items,'Edit',NC_ADMIN_ROUTE."/product/edit/{$id}");



