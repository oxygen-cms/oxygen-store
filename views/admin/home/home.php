<div class="row">

    <div class="col-xs-12">


        <div class="box box-solid">
                <div class="box-header">
                  	<h3 class="box-title">Manage <?php echo lang('store:products:products');?></h3>
                </div>


                <div class="box-body">

			    	<?php echo form_open_multipart('admin/store/home') ?>

									<?php $config_wysiwyg = 'wysiwyg-simple';?>
									<?php //$config_wysiwyg = 'wysiwyg-advanced';?>

									<?php echo form_hidden('id',$id);?>
						              <div class="nav-tabs-custom">
							                <ul class="nav nav-tabs">
								                  <li class="active"><a href="#tab_1" data-toggle="tab">Content</a></li>
								                  <li class="pull-right"><a href="#tab_2" data-toggle="tab"><i class="fa fa-gear"></i> Options</a></li>
								                  <li class="pull-right"><a href="#tab_3" data-toggle="tab">Advanced</a></li>
							                </ul>
							                <div class="tab-content">
							                  <div class="tab-pane active" id="tab_1">
							                    <?php echo form_textarea('content',$content,"class='".$config_wysiwyg."'");?>
							                  </div><!-- /.tab-pane -->
							                  <div class="tab-pane" id="tab_2">
													<fieldset>
														<ul>
															<li>
																<label>Recently Added Products</label>
																<div class="form_inputs">
																	
																	<?php echo form_dropdown('recent',[1=>'Display',0=>'Hide'],$options->recent);?>
																</div>
															</li>
															<li>
																<label>Display Categories</label>
																<div class="form_inputs">
																	<?php echo form_dropdown('categories',[1=>'Display',0=>'Hide'],$options->categories);?>
																</div>
															</li>
															<li>
																<label>Display Cover Images</label>
																<div class="form_inputs">
																	<?php echo form_dropdown('coverimg',[1=>'Display',0=>'Hide'],$options->coverimg);?>
																</div>
															</li>						
														</ul>
													</fieldset>
							                  </div><!-- /.tab-pane -->
							                  <div class="tab-pane" id="tab_3">
												<label>Theme Layout</label>
												<?php echo form_dropdown('theme_layout',['store_home.html'=>'store_home.html','default.html'=>'default.html'],$theme_layout);?>						
							                  </div><!-- /.tab-pane -->
							                </div><!-- /.tab-content -->
						              </div><!-- nav-tabs-custom -->

						               <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>

			        <?php echo form_close() ?>

			    </div>


	</div>
</div>
</div>