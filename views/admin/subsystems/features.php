<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header">
              	<h3 class="box-title">
              		<?php echo lang('store:features:title');?>
              	</h3>
            </div>
            <div class="box-body">
				<?php echo lang('store:features:description');?>
				<table class='table'>
					<thead>
						<tr>
							<th></th>
							<th><?php echo lang('store:features:system');?></th>
							<th><?php echo lang('store:features:version');?></th>
							<th><?php echo lang('store:features:requires');?></th>
							<th><?php echo lang('store:features:status');?></th>
							<th><span style="float:right;"><?php echo lang('store:features:action');?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($subsystems as $key => $value) : ?>
								<tr class="">
									<td></td>
									<td>
										<?php echo $value->title; ?>
										<br><br>
										<small>
										<i style='color:#777'>
											<?php echo $value->description; ?>
										</i>
										</small>
									</td>
									<td><?php //echo $value->version; ?></td>
									<td><span style='color:#777'><?php echo sih_slug2title($value->require); ?></span></td>
									<td>
											<?php echo yesNoBOOL($value->installed, 'string', '<span class="label bg-blue">Installed</span>', '' ) ; ?>
											<?php if($value->installed==1): ?>	
											<?php endif;?>	
																					
									</td>
									<td>
										<span style="float:right;">

											<?php if($value->installed==0): ?>
													<a class='btn btn-flat bg-blue edit_button blue' href='{{x:uri x='ADMIN'}}/features/add/<?php echo $value->driver; ?>'>Add</a>
											<?php else:?>
													<a class='btn btn-flat bg-red alert_button red confirm' href='{{x:uri x='ADMIN'}}/features/remove/<?php echo $value->driver; ?>'>Remove</a>
											<?php endif;?>

										</span>
									</td>
								</tr>
						<?php endforeach;?>
					</tbody>
				</table>
				<div class="buttons">
				</div>
			</div>
		</div>
	</div>
</div>
<?php if (isset($pagination)): ?>
	<?php echo $pagination; ?>
<?php endif; ?>