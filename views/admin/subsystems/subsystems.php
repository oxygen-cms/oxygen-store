 <div class="box box-default">


	<h4>SubSystems</h4>


<section class="item">
	<div class="content">
		<fieldset id="">
			<div class="item one_full" style="float:left;">
			
			</div>
		</fieldset>		
		<br>	
			<table table='class'>
				<thead>
					<tr>
						<th></th>
						<th>System</th>
						<th>Version</th>
						<th>Requires</th>
						<th>Status/Health</th>
						<th><span style="float:right;">Action</span></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($subsystems as $key => $value) : ?>
							<tr class="">
								<td></td>
								<td>
									<?php echo $value->title; ?>
									<br><br>
									<small>
									<i style='color:#777'>
										<?php echo $value->description; ?>
									</i>
									</small>
								</td>
								<td><?php //echo $value->version; ?></td>
								<td><span style='color:#777'><?php echo sih_slug2title($value->require); ?></span></td>
								<td>
										<?php echo yesNoBOOL($value->installed, 'string', '<span class="label bg-blue">Installed</span>', '' ) ; ?>
										<?php if($value->installed==1): ?>	
										<?php endif;?>	
																				
								</td>
								<td>
									<span style="float:right;">

										<?php if($value->installed==0): ?>
												<a class='btn btn-flat bg-blue edit_button' href='{{x:uri x='ADMIN'}}/subsystems/add/<?php echo $value->driver; ?>'>Install</a>
										<?php else:?>
												<a class='btn btn-flat bg-red alert_button confirm' href='{{x:uri x='ADMIN'}}/subsystems/remove/<?php echo $value->driver; ?>'>Uninstall</a>
										<?php endif;?>

									</span>
								</td>
							</tr>
					<?php endforeach;?>
				</tbody>
			</table>

	</div>

<?php if (isset($pagination)): ?>
	<?php echo $pagination; ?>
<?php endif; ?>


</div>