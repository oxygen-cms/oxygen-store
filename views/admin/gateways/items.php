<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
                <div class="box-header">
                  <h3 class="box-title"><?php echo lang('store:gateways:title');?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php if (!empty($installed)): ?>
								<table class='table'>
									<thead>
										<tr>
											<th><?php echo lang('store:gateways:name'); ?></th>
											<th><?php echo lang('store:gateways:image'); ?></th>
											<th><?php echo lang('store:gateways:description'); ?></th>
											<th>Enabled</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($installed as $item): ?>
											<tr>
												<td><?php echo $item->title; ?></td>
												<td><?php echo $item->image ? img($item->image) : ''; ?></td>
												<td><?php echo $item->description; ?></td>
												<td><?php echo yesNoBOOL($item->enabled,'string','<i class="fa fa-check text-primary"></i>','<i class="fa fa-times text-danger"></i>'); ?></td>
												<td class="">
													<span style="float:right">
														<?php if ($item->enabled):?>
															<a class='btn btn-flat bg-red' href='{{x:uri x="ADMIN"}}/gateways/enable/<?php echo $item->id;?>/0'><?php echo lang('store:admin:disable');?></a>
														<?php else:?>
															<a class='btn btn-flat btn-default' href='{{x:uri x="ADMIN"}}/gateways/enable/<?php echo $item->id;?>/1'><?php echo lang('store:admin:enable');?></a>
														<?php endif;?>
														<a class='btn btn-flat bg-blue' href='{{x:uri x='ADMIN'}}/gateways/edit/<?=$item->id;?>'><?= lang('store:admin:edit');?></a>
													</span>													
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5">
												<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
											</td>
										</tr>
									</tfoot>
								</table>
					<?php else: ?>
						<div class="no_data"><?php echo lang('store:gateways:no_installed_gateways'); ?></div>
					<?php endif; ?>	
                    <div>
                    	{{pagination:links}}</th>
                    </div> 
                </div>
        </div>
    </div>
</div>