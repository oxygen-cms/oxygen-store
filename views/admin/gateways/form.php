<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-header">
					<h3 class="box-title">
						<?php echo lang('store:gateways:title'); ?> : <?php echo $gateway->title;?>
					</h3>
				</div><!-- /.box-header -->
				<div class='box-body'>
					<?php echo form_hidden('id', $gateway->id); ?>
					<label for="name">
						<?php echo lang('store:admin:name'); ?><span>*</span>
					</label>
					<?php echo form_input('title', set_value('name', $gateway->title), 'class="form-control width-15"'); ?>
					<label for="description">
						<?php echo lang('store:gateways:description'); ?><span>*</span>
					</label>
					<div class="input">
						<?php echo form_textarea('description', set_value('description', $gateway->description), 'class="form-control width-15"'); ?>
					</div>
					<?php $this->load->view('admin/merchant/'.$gateway->slug.'/form'); ?>
				</div>
				<div class="box-footer">
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
				</div><!-- /.box-header -->
			</div>
		</div>
	</div>
<?php echo form_close(); ?>