<div class="content">
<div class="row">

	    <div class="col-md-12">
	        <h2>Timeline</h2>      
	    </div>
		<div class="col-md-12">

	
					<ul class="timeline">

						
					    <!-- timeline time label -->
					    <li class="time-label">
					        <span class="bg-red">
					              {{time}} 
					        </span>
					    </li>


					    {{timelineData}}
					    <!-- timeline item -->
					    <li>
					        <!-- timeline icon -->
					        <i class="{{icon}} bg-{{color}}"> </i>
					        <div class="timeline-item">
					            <span class="time"><i class="fa fa-clock-o"></i> {{time}}</span>
					            <h3 class="timeline-header">{{title}}</h3>
					            <div class="timeline-body">
					               {{body}}
					            </div>
					        </div>
					    </li>

					    {{/timelineData}}

					    <!-- END timeline item -->
						<li>
							<i class="fa fa-clock-o bg-gray"></i>
						</li>		
					</ul>
								
				
		</div>
</div>
</div>