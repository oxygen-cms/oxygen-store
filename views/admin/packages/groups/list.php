<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">Package Groups</h3>
			</div>
			<div class="box-body">
				<?php if (!empty($packages_groups)): ?>
					<table id="standard_data_table" class="table table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th><?php echo lang('store:packages:name'); ?></th>
								<th>Created By</th>
								<th>Created</th>
								<th>Default Group</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($packages_groups as $item): ?>
								<tr>
									<td><?php echo $item->id; ?></td>
									<td><?php echo $item->name; ?></td>
									<td><?php echo user_displayname($item->created_by, true); ?></td>
									<td><?php echo nc_format_date($item->created,''); ?></td>
									<td><?php echo yesNoBOOL($item->default, 'string', '<span class="label label-success">DEFAULT</span>',''); ?></td>
									<td class="">
										<span style="float:right;">
											<?php
												$items = array();
												$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages_groups/package_list/'.$item->id), false, 'Packages', false, "edit");																			
												$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages_groups/edit/'.$item->id), false, lang('store:packages:edit'), false, "edit");																	
												$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages_groups/duplicate/'.$item->id), false, lang('store:packages:copy'), false, "edit");
												$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages_groups/delete/' . $item->id), true,  lang('store:packages:delete'), true, "minus");
												echo dropdownMenuListSplit($items,'Actions');
											?>
										</span>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
								</td>
							</tr>
						</tfoot>
					</table>
				<?php else: ?>
					<div class="no_data">
						<?php echo lang('store:packages:no_packagegroup_here');?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>