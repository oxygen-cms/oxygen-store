<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">Add New
					<span class='text-success'>
						
					</span>
				</h3>
			</div>
			<div class="box-body">

								<span>
									<?php
										$items = array();
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/envc6'), false, 'C6 Envelope', false);	
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/envc5'), false, 'C5 Envelope', false);	
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/envc4'), false, 'C4 Envelope', false);															
										echo dropdownMenuListSplit($items,'Envelopes');
									?>
								</span>	


								<span>
									<?php
										$items = array();
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/sat5'), false, '500g Satchel', false);
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/sat3k'), false, '3KG Satchel', false);
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/sat5k'), false, '5KG Satchel', false);																
										echo dropdownMenuListSplit($items,'Satchels');
									?>
								</span>	
						
								<span>
									<?php
										$items = array();
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/bx1'), false, 'BX1', false);
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/bx2'), false, 'BX2', false);
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/bx2'), false, 'BX4', false);																
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/envc6b'), false, 'C6 Box', false);	
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/envc5b'), false, 'C5 Box', false);	
										$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/quick_add/'.$id.'/envc4b'), false, 'C4 Box', false);											
										echo dropdownMenuListSplit($items,'Boxes');
									?>
								</span>
			</div>



		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">Packages defined in 
					<span class='text-success'>
						<?php echo $name; ?> (<?php echo $id; ?>)
					</span>
				</h3>
			</div>
			<div class="box-body">
				<?php if(isset($packages)): ?>
					<div class="item form_inputs">
								
							<div class='table-responsive'>
								<table class='table'>
									<thead>
										<tr>
											<th>ID</th>
											<th><?php echo lang('store:packages:name'); ?></th>
											<th><?php echo lang('store:packages:inner_outer_height'); ?></th>
											<th><?php echo lang('store:packages:inner_outer_width'); ?></th>
											<th><?php echo lang('store:packages:inner_outer_length'); ?></th>
											<th><?php echo lang('store:packages:empty_max_weight'); ?></th>
											<th>Package Type / Code</th>
											<th style='min-width:130px'></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($packages as $item): ?>
											<tr>
												<td><?php echo $item->id; ?></td>
												<td><?php echo $item->name; ?></td>
												<td><?php echo $item->height .'/'. '<strong>' .$item->outer_height .'</strong>'; ?> [cm]</td>
												<td><?php echo $item->width.'/'. '<strong>'. $item->outer_width.'</strong>'; ?> [cm]</td>
												<td><?php echo $item->length.'/'. '<strong>'. $item->outer_length.'</strong>'; ?> [cm]</td>
												<td><?php echo $item->cur_weight.' / '. '<strong>'.$item->max_weight.'</strong>'; ?> [kg]</td>
												<td>
													<?php echo ($item->package_type=='')?'<span class="label label-default">Auto</span>': '<span class="label bg-orange">'.ucfirst($item->package_type).'</span>'; ?> 
													/ 
													<?php echo ($item->code=='')?'<span class="label label-default">No Code</span>':'<span class="label label-success">'.$item->code.'</span>'; ?></td>

												<td class="">
														<span style="float:right;">
															<?php
																$items = array();
																$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/edit/'.$item->id), false, lang('store:packages:edit'), false, "edit");																	
																$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/duplicate/'.$item->id), false, lang('store:packages:copy'), false, "edit");
																$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/duplicate/'.$item->id.'/edit'), false, lang('store:packages:copy_edit'), false, "edit");
																$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/delete/' . $item->id), true,  lang('store:packages:delete'), true, "minus");
																echo dropdownMenuListSplit($items,'Actions');
															?>
														</span>

												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
					</div>
				<?php endif; ?>
			</div>



		</div>
	</div>
</div>

