<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">
					<?php if(isset($id)): ?>
						<?php echo lang('store:packages:edit');?> <?php echo $name;?> (<?php echo $id;?>)
					<?php else: ?>
						<?php echo lang('store:packages:new');?> Package Group
					<?php endif; ?>
				</h3>
			</div>
			<div class="box-body">
				<?php if(isset($id)): ?>
					<?php echo form_open(NC_ADMIN_ROUTE.'/packages_groups/edit/'.$id); ?>
				<?php else: ?>
					<?php echo form_open(NC_ADMIN_ROUTE.'/packages_groups/create/'); ?>
				<?php endif; ?>

					<input type='hidden' name='is_digital' value='0'>
					<input type='hidden' name='is_shipping' value='1'>
					<table class='table'>
						<tr>
							<td>
								<?php echo lang('store:packages:name');?><span>*</span>
							</td>
							<td>
								<?php echo form_input('name', set_value('name', $name), 'class="form-control" id="name"  placeholder="Enter package name here"'); ?>
							</td>
						</tr>
						<tr>
							<td>
								Is Default
							</td>
							<td>
								<?php echo form_dropdown('default', array(0=>'No',1=>'Yes'),  $default,'class="form-control"' ); ?>
							</td>
						</tr>
					</table>								

					<div class="buttons">
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save'))); ?>
						<a href="{{x:uri x='ADMIN'}}/packages_groups/" class='btn gray'>Cancel</a>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>