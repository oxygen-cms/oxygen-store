
<div class="row">

    <div class="col-xs-12">

        <h3><?php echo lang('store:packages:title'); ?></h3>

        <div class="box">
                <div class="box-body">


									<?php if (!empty($packages)): ?>
										<table id="standard_data_table" class="table table-bordered">
											<thead>
												<tr>
													<th><?php echo lang('store:packages:name'); ?></th>
													<th><?php echo lang('store:packages:inner_outer_height'); ?></th>
													<th><?php echo lang('store:packages:inner_outer_width'); ?></th>
													<th><?php echo lang('store:packages:inner_outer_length'); ?>(Depth or Thickness)</th>
													<th><?php echo lang('store:packages:empty_max_weight'); ?></th>
													<th>Code</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($packages as $item): ?>
													<tr>
														<td><?php echo $item->name; ?></td>
														<td><?php echo $item->height .'/'. $item->outer_height; ?> [cm]</td>
														<td><?php echo $item->width.'/'. $item->outer_width; ?> [cm]</td>
														<td><?php echo $item->length.'/'. $item->outer_length; ?> [cm]</td>
														<td><?php echo $item->cur_weight.' / '.$item->max_weight; ?> [kg]</td>
														<td><?php echo $item->code; ?></td>

														<td class="">
																<span style="float:right;">
																	<?php
																		$items = array();
																		$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/edit/'.$item->id), false, lang('store:packages:edit'), false, "edit");																	
																		$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/duplicate/'.$item->id), false, lang('store:packages:copy'), false, "edit");
																		$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/duplicate/'.$item->id.'/edit'), false, lang('store:packages:copy_edit'), false, "edit");
																		$items[] = dropdownMenuStandard(site_url(NC_ADMIN_ROUTE.'/packages/delete/' . $item->id), true,  lang('store:packages:delete'), true, "minus");
																		echo dropdownMenuListSplit($items,'Actions');
																	?>
																</span>

														</td>
													</tr>
												<?php endforeach; ?>
											</tbody>
											<tfoot>
												<tr>
													<td colspan="7">
														<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
													</td>
												</tr>
											</tfoot>
										</table>
									<?php else: ?>
										<div class="no_data"><?php echo lang('store:packages:no_packages_here');?></div>
									<?php endif; ?>

									<?php echo form_close(); ?>
				




				</div>

			</div>
	</div>

</div>