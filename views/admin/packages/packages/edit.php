<script>

	function reverseAll() {
	    toMM($('#height'),$('.mmHeight'));  
	    toMM($('#width'),$('.mmWidth'));
	    toMM($('#length'),$('.mmLength')); 

	    toMM($('#outer_height'),$('.mmOuterHeight'));
	    toMM($('#outer_width'),$('.mmOuterWidth'));
	    toMM($('#outer_length'),$('.mmOuterLength')); 

	    toGM($('#max_weight'),$('.gmMaxWeight'));
	    toGM($('#cur_weight'),$('.gmCurWeight')); 	
	}
	function toMM(from,to) {
		if (from.val()*10 != 0) {
			to.val(from.val()*10);
		}
		else {
			to.val(0);
		}		
	}

	function toCM(from,to) {
		if (from.val()/10 != 0) {
			to.val(from.val()/10);
		}
		else {
			to.val(0);
		}		
	}

	function toKG(from,to) {
		if (from.val()/1000 != 0) {
			to.val(from.val()/1000);
		}
		else {
			to.val(0);
		}
	}
	function toGM(from,to) {
		if (from.val()*1000 != 0) {
			to.val(from.val()*1000);
		}
		else {
			to.val(0);
		}
	}
	$(function() {

		reverseAll();

	    $(document).on('change', '.mmHeight', function(event) 
	    {
	        toCM($(this),$('#height'));
	    });      
	    $(document).on('change', '.mmWidth', function(event) 
	    {
	        toCM($(this),$('#width'));
	    });  
	    $(document).on('change', '.mmLength', function(event) 
	    {
	        toCM($(this),$('#length'));
	    }); 
	    $(document).on('change', '.mmOuterHeight', function(event) 
	    {
	        toCM($(this),$('#outer_height'));
	    }); 
	    $(document).on('change', '.mmOuterWidth', function(event) 
	    {
	        toCM($(this),$('#outer_width'));
	    }); 
	    $(document).on('change', '.mmOuterLength', function(event) 
	    {
	        toCM($(this),$('#outer_length'));
	    }); 	    	
	    $(document).on('change', '.gmMaxWeight', function(event) 
	    {
	        toKG($(this),$('#max_weight'));
	    }); 	  
	    $(document).on('change', '.gmCurWeight', function(event) 
	    {
	        toKG($(this),$('#cur_weight'));
	    }); 
	});
</script>
<div class="row">
    <div class="col-xs-12">

		<?php if(isset($id)): ?>
			<?php echo form_open(NC_ADMIN_ROUTE.'/packages/edit/'.$id); ?>
		<?php else: ?>
			<?php echo form_open(NC_ADMIN_ROUTE.'/packages/create/'); ?>
		<?php endif; ?>

        <div class="box box-solid">
        	<div class="box-header">
				<h3 class="box-title">
					<?php if(isset($id)): ?>
						<?php echo lang('store:packages:edit');?>
					<?php else: ?>
						<?php echo lang('store:packages:new');?>
					<?php endif; ?>        			
        		</h3>
        	</div>
            <div class="box-body">

		        <div class="nav-tabs-custom">
		                <ul class="nav nav-tabs">
		                	<li class="active">
		                		<a data-toggle="tab" href="#detail-tab"><i class="fa fa-bars"></i> 
		                			Details
		                		</a>
		                	</li>
		                	<li class="">
		                		<a data-toggle="tab" href="#dimensions-tab"><i class="fa fa-arrows-alt"></i> 
		                			Dimensions
		                		</a>
		                	</li>
		                	<li class="">
		                		<a data-toggle="tab" href="#weight-tab"><i class="fa fa-cubes"></i> 
		                			Weight
		                		</a>
		                	</li>
		                	<li class="">
		                		<a data-toggle="tab" href="#cost-tab"><i class="fa fa-dollar"></i> 
		                			Cost and Rules
		                		</a>
		                	</li>				                					                				                	
		                	<li class="">
		                		<a data-toggle="tab" href="#other-tab"><i class="fa fa-asterisk"></i> 
		                			Other
		                		</a>
		                	</li>			                	
		                </ul>
		                <div class="tab-content">

						 	<div id="detail-tab" class="tab-pane active">

								<table class='table'>
									<tr>
										<td width="20%"><?php echo lang('store:packages:name');?><span>*</span></td>
										<td><?php echo form_input('name', set_value('name', $name), 'class="form-control" id="name"  placeholder="Package Name"'); ?></td>
									</tr>
									<tr>
										<td width="20%">Code<span></span></td>
										<td><?php echo form_input('code', set_value('code', $code), 'class="form-control" id="code"  placeholder="Enter package code here"'); ?></td>
									</tr>
									<tr>
										<td width="20%">Package Class/Group</td>
										<td>
											<?php if(isset($id)): ?>
												<input type='hidden' name='pkg_group_id' value='<?php echo $pkg_group_id; ?>'>
												<?php echo $available_groups[$pkg_group_id]; ?>					
											<?php else: ?>
												<i>You must assign a package to a group. i.e Envelopes,Letters,Parcels etc..</i>
												<?php echo form_dropdown('pkg_group_id', $available_groups,set_value('pkg_group_id',(isset($pkg_group_id))?$pkg_group_id:NULL) ); ?>
											<?php endif; ?>											
										</td>
									</tr>																			
								</table>
			                </div>
						 	<div id="dimensions-tab" class="tab-pane">

								<h4>Inner Dimensions</h4>
								<table class='table'>
									<tr>
										<td width="20%">Inner Height</td>
										<td>
												<?php if($metrictype=='cm'):?>
													<?php echo form_input('height', set_value('height', $height), 'class="form-control" id="height"  placeholder="0"'); ?> cm.
												<?php else:?>
													<input type='hidden' name='height' value='<?php echo $height;?>'id='height'>
													<input type='text' class='mmHeight'/> mm.
												<?php endif;?>
										</td>
									</tr>
									<tr>
										<td width="20%">Inner Width</td>
										<td>
												<?php if($metrictype=='cm'):?>
													<?php echo form_input('width', set_value('width', $width), 'class="form-control" id="width"  placeholder="0"'); ?> cm.
												<?php else:?>
													<input type='hidden' name='width' value='<?php echo $width;?>'id='width'>
													<input type='text' class='mmWidth'/> mm.
												<?php endif;?>
										</td>
									</tr>	
									<tr>
										<td width="20%">Inner Length(Depth)</td>
										<td>
												<?php if($metrictype=='cm'):?>
													<?php echo form_input('length', set_value('length', $length), 'class="form-control" id="length"  placeholder="0"'); ?> cm.
												<?php else:?>
													<input type='hidden' name='length' value='<?php echo $length;?>'id='length'>
													<input type='text' class='mmLength'/> mm.
												<?php endif;?>
												<span>
													
												</span>													
										</td>
									</tr>	
									<tr>
										<td width="20%"></td>
										<td>
											<a href='#' class='btn small green boxduplicate'>Outer is Same as Inner</a>
										</td>
									</tr>																																													
								</table>

								<h4>Outer Dimensions</h4>
								<table class='table'>
									<tr>
										<td width="20%">Outer Height <?php if($metrictype=='cm'):?>cm.<?php else:?>mm.<?php endif;?></td>
										<td>
											<?php if($metrictype=='cm'):?>
												<?php echo form_input('outer_height', set_value('outer_height', $outer_height), 'class="form-control" id="outer_height"  placeholder="0"'); ?>
											<?php else:?>
												<input type='hidden' name='outer_height' value='<?php echo $outer_height;?>'id='outer_height'>
												<input type='text' class='mmOuterHeight'/>
											<?php endif;?>	
										</td>
									</tr>
									<tr>
										<td width="20%">Outer Width</td>
										<td>
											<?php if($metrictype=='cm'):?>
												<?php echo form_input('outer_width', set_value('outer_width', $outer_width), 'class="form-control" id="outer_width"  placeholder="0"'); ?> cm.
											<?php else:?>
												<input type='hidden' name='outer_width' value='<?php echo $outer_width;?>'id='outer_width'>
												<input type='text' class='mmOuterWidth'/> mm.
											<?php endif;?>	
										</td>
									</tr>	
									<tr>
										<td width="20%">Outer Length(Depth)</td>
										<td>
											<?php if($metrictype=='cm'):?>
												<?php echo form_input('outer_length', set_value('outer_length', $outer_length), 'class="form-control" id="outer_length"  placeholder="0"'); ?> cm.
											<?php else:?>
												<input type='hidden' name='outer_length' value='<?php echo $outer_length;?>'id='outer_length'>
												<input type='text' class='mmOuterLength'/> mm.
											<?php endif;?>		
										</td>
									</tr>																																														
								</table>			
																		
			                </div>	
						 	<div id="weight-tab" class="tab-pane">
						 			<div class="col-sm-12">
						 			<div class="col-sm-6">

											<h3>Weight</h3>

											<label for="name">Max Weight<span>*</span></label>
										
											<?php if($metrictype=='cm'):?>
												<?php echo form_input('max_weight', set_value('max_weight', $max_weight), 'class="form-control" id="max_weight"  placeholder="0"'); ?> Kg.
											<?php else:?>
												<input type='hidden' name='max_weight' value='<?php echo $max_weight;?>'id='max_weight'>
												<input type='text' class='form-control gmMaxWeight'/> grams.
											<?php endif;?>										
											
											<label for="name">Package Weight (Empty weight)<span>*</span></label>
											
											<?php if($metrictype=='cm'):?>
												<?php echo form_input('cur_weight', set_value('cur_weight', $cur_weight), 'class="form-control" id="cur_weight"  placeholder="0.200"'); ?> Kg.
											<?php else:?>
												<input type='hidden' name='cur_weight' value='<?php echo $cur_weight;?>'id='cur_weight'>
												<input type='text' class='form-control gmCurWeight'/> grams.
											<?php endif;?>									
											
									</div>
									</div>
			                </div>
			                <div id="cost-tab" class="tab-pane">

								<?php if(isset($id)): ?>
									<fieldset>
										<h3>Additional Information</h3>
										<label for="name">Package Cost<span>*</span> Cost to buyer for the package </label>
										<div class="input">
											<?php echo form_input('addi_cost_unit', set_value('addi_cost_unit', $addi_cost_unit),'class="form-control" id="addi_cost_unit"  placeholder="0"'); ?> 
										</div>

										<label for="name">Maximum Items Allowed<span>*</span> The Max number of items allowed in box, '0' for infinite </label>
										<div class="input">
											<?php echo form_input('max_items', set_value('max_items', $max_items),'class="form-control" id="max_items"  placeholder="999999"'); ?>
										</div>
										<label for="name">Packing Method<span>*</span> Select the type of packing method to be used</label>
										<div class="input">
											<?php 
										        $packing_method_list = $this->config->item('admin/packages/packing_method');
											?>
											<?php echo form_dropdown('packing_method', $packing_method_list,set_value('packing_method',(isset($packing_method))?$packing_method:1),'class="form-control"' ); ?>
										</div>

										<label for="name">Package Type<span>*</span> Letter or package ?</label>
										<div class="input">
										
											<?php 
												$package_type_default ='dynamic';
										        $package_types = $this->config->item('core/packages/types');
											?>
											<?php echo form_dropdown('package_type', $package_types,set_value('package_type',(isset($package_type))?$package_type:$package_type_default),'class="form-control"'  ); ?>
										</div>
									</fieldset>		
								<?php endif; ?>       
			                </div>					                			                
						 	<div id="other-tab" class="tab-pane">
								<?php if(isset($id)): ?>
									<ul>
										<li class="">
											<label for="name">
												Display Mode:
											</label>
											<div class="input">
												<?php if($metrictype=='cm'): ?>
													<a class='btn blue' href='admin/store/packages/edit/<?php echo $id;?>/mm'>Switch to mm and grams</a>
												<?php else: ?>
													<a class='btn blue' href='admin/store/packages/edit/<?php echo $id;?>/cm'>Switch to cm and kg</a>
												<?php endif ?>
											</div>
										</li>								
										<li class="">
											<label for="name">
												Created By:
											</label>
											<div class="input">
												<?php echo user_displayname($created_by, true); ?>
											</div>
										</li>
										<li class="">
											<label for="name">
												Date Created:
											</label>
											<div class="input">
												<?php echo $created; ?>
											</div>
										</li>
										<li class="">
											<label for="name">
												Date Updated:
											</label>
											<div class="input">
												<?php echo $updated; ?>
											</div>
										</li>
									</ul>
								<?php endif; ?>
			                </div>
		              	</div>
		        </div>
			</div>
			<div class="box-footer">
	
					<input type='hidden' name='is_digital' value='0'>
					<input type='hidden' name='is_shipping' value='1'>

					<?php if(isset($id)): ?>
						<div class="buttons">
							<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save','save_exit'))); ?>
							<a class='btn btn-flat btn-default' href='admin/store/packages_groups/package_list/<?php echo $pkg_group_id;?>'>Cancel</a>
						</div>
					<?php else: ?>
						<div class="buttons">
							<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save','save_exit', 'cancel'))); ?>
						</div>
					<?php endif; ?>			
			</div>
		</div>

		<?php echo form_close(); ?>
	</div>
</div>