

			<?php if (!empty($uninstalled)): ?>

				<table id="standard_data_table" class="table table-bordered">
					<thead>
						<tr>
							<th style='width:20%'><?php echo lang('store:shipping:name'); ?></th>
							<th style='width:20%'><?php echo lang('store:shipping:image'); ?></th>
							<th style='width:30%'><?php echo lang('store:shipping:description'); ?></th>
							<th style='width:5%'></th>
							<th style='width:5%'></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($uninstalled as $item): ?>
							<tr>
								<td><?php echo $item->name; ?></td>
								<td><?php echo $item->image ? img($item->image) : ''; ?></td>
								<td><?php echo $item->description; ?></td>
								<td></td>
								<td></td>
								<td>
									<?php echo anchor(NC_ADMIN_ROUTE.'/shipping/install/' . $item->slug, lang('global:install'), 'class="button"'); ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4">
								<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
							</td>
						</tr>
					</tfoot>
				</table>

			<?php else: ?>
				<div class="no_data"><?php echo lang('store:shipping:no_data'); ?></div>
			<?php endif; ?>