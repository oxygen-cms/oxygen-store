
		<?php if (!empty($installed)): ?>
					<table id="standard_data_table" class="table table-bordered">
						<thead>
		
							<tr>
								<th style='width:20%'><?php echo lang('store:shipping:name'); ?></th>
								<th style='width:20%'><?php echo lang('store:shipping:image'); ?></th>
								<th style='width:30%'><?php echo lang('store:shipping:description'); ?></th>
								<th style='width:5%'>Int. Shipping</th>
								<th style='width:5%'>Enabled</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($installed as $item): ?>
								<tr>
									<td><?php echo $item->title; ?></td>
									<td><?php echo $item->image ? img($item->image) : ''; ?></td>
									<td><?php echo wordwrap($item->description, 50, "<br>\n"); ?></td>
									<td><?php echo yesNoBOOL($item->international_shipping); ?></td>
									<td><?php echo yesNoBOOL($item->enabled); ?></td>
									<td class="">
										<span style="float:right">
												<?php
													$items = array();
													$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/shipping/edit/{$item->id}", false, lang('store:shipping:edit'), false, "edit");
													if ($item->enabled){
														$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/shipping/disable/{$item->id}", false, lang('store:shipping:disable'), false, "edit");
													}
													else {
														$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/shipping/enable/{$item->id}", false, lang('store:shipping:enable'), false, "edit");
													}							
													$items[] = dropdownMenuStandard(NC_ADMIN_ROUTE."/shipping/uninstall/{$item->id}", true,  lang('store:shipping:uninstall'), true, "minus");
													echo dropdownMenuListSplit($items,'Edit',NC_ADMIN_ROUTE."/shipping/edit/{$item->id}");
														
												?>

											</span>
										</span>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
								</td>
							</tr>
						</tfoot>
					</table>

		<?php else: ?>
					<div class="no_data"><?php echo lang('store:shipping:no_installed'); ?></div>
		<?php endif; ?>

