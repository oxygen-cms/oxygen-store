
	 			<?php echo form_hidden('id', $id); ?>


				

				<label for="name"><?php echo lang('store:shipping:name') ; ?><span></span></label>
				<div class="input"><?php echo $name; ?></div>

				<label for="name"><?php echo lang('store:shipping:field_title') ; ?> <span>*</span></label>
				<div class="input"><?php echo form_input('title', set_value('name', $title), 'class="width-15"'); ?></div>

				<label for="name"><?php echo lang('store:shipping:enable') ; ?> <span></span></label>
				<div class="input"><?php echo form_dropdown('enabled', array('0'=>'Disabled','1'=>'Enabled'), set_value('enabled', $enabled), 'class="width-15"'); ?></div>

				<label for="description"><?php echo lang('store:shipping:description') ; ?><span>*</span></label>
				<div class="input"><?php echo form_input('description', set_value('description', $description)); ?></div>

				<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save_exit','save', 'cancel'))); ?>

