
<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
			  <h3 class="box-title">
				Installed Shipping Options
			  </h3>
			</div>
			<div class="box-body">
				<h3>Installed</h3>
				<?php echo $this->load->view('admin/shipping/options/partials/installed');?>
			</div>
		</div>
	</div>
</div>


<div class="row">

	<div class="col-xs-12">

		<div class="box box-solid">

			<div class="box-header">
				  <h3 class="box-title">
						Available Shipping Options
				  </h3>
			</div>

			<div class="box-body">
				<?php echo $this->load->view('admin/shipping/options/partials/available');?>
			</div>
		</div>

	</div>

</div>