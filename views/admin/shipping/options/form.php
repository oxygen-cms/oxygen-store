<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<div class="row">
    <div class="col-xs-12">

    	<div class="box box-primary">

            <div class='box-header'>

                <h4><?php echo lang('store:shipping:edit_title') ; ?></h4>

            </div>

    		<div class='box-body'>

					<div class="col-md-6">
    					<?php $this->load->view('admin/shipping/options/partials/details_tab'); ?>
				    </div>

					<div class="col-md-6">
    					<?php $this->load->file($form); ?>
				    </div><!-- /.tab-content -->
   			</div>

   		</div>
  	</div>
</div>
<?php echo form_close(); ?>