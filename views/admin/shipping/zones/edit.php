<?php $route = (isset($id))?'edit/'.$id:'create/';?>
<?php echo form_open_multipart(NC_ADMIN_ROUTE.'/shipping_zones/'.$route);  ?>
<div class="row">
    <div class="col-xs-12 col-md-6  col-lg-3">
        <div class="box box-solid">

                <div class="box-header">
					<h3 class="box-title">
						<?php if(isset($id)): ?>
							Edit
						<?php else: ?>
							Create 
						<?php endif; ?>
						Shipping Zone
					</h3>
                </div><!-- /.box-header -->

                <div class="box-body">


					<label for="name">Name<span>*</span></label>
					<div class="input">
						<?php echo form_input('name', set_value('name', $name), 'class="form-control" id="name"  placeholder="Enter zone name here"'); ?>
					</div>

					<label for="default">Is Default<span></span></label>
					<div class="input">
						<?php echo form_dropdown('default', array(0=>'No',1=>'Yes'),  $default ,'class="form-control"'); ?> 
					</div>
					
					<br>

					<div class="buttons">
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save'))); ?>
						<a href='{{x:uri x='ADMIN'}}/shipping_zones/' class='btn btn-flat btn-default'>Cancel</a>
					</div>
				</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>