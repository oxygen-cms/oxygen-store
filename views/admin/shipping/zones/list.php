<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">Shipping Zones</h3>
			</div>
			<div class="box-body">
				<?php if (!empty($zones)): ?>
					<table id="standard_data_table" class="table table-bordered">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Default</th>
									<th>
										<span style='float:right'>
											Actions
										</span>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($zones as $zone): ?>
									<tr>
										<td><?php echo $zone->id; ?></td>
										<td><?php echo $zone->name; ?></td>
										<td><?php echo yesNoBOOL($zone->default, 'string', '<span style="color:green">Yes</span>','<span style="color:gray">No</span>'); ?></td>

										
										<td class="actions">
												<a class='btn btn-flat btn-sm bg-orange' href="{{x:uri x='ADMIN'}}/shipping_zones/countries/<?php echo $zone->id;?>">Countries</a>
												<a class='btn btn-flat btn-sm bg-blue' href="{{x:uri x='ADMIN'}}/shipping_zones/edit/<?php echo $zone->id;?>">Edit</a>
												<a class='btn btn-flat btn-sm bg-red confirm' href="{{x:uri x='ADMIN'}}/shipping_zones/delete/<?php echo $zone->id;?>">Delete</a>
										</td>
										

									</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5">
										<div class="inner"></div>
									</td>
								</tr>
							</tfoot>
					</table>
				<?php else: ?>
					<div class="no_data">No Shipping Zones to display.</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>