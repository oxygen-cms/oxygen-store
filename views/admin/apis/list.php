<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">
					API Keys
				</h3>
			</div>
			<div class="box-body">

				<a class='btn btn-primary btn-flat' href='{{x:uri x='ADMIN'}}/apis/create'>Create New</a>

				<table class='table'>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Key</th>
						<th>Total Req</th>
						<th>Curr Req</th>
						<th>Max Allowed</th>
						<th class='actions'><?php echo lang('store:admin:actions');?></th>
					</tr>
					{{keys}}
						<tr>
							<td>{{id}}</td>
							<td>{{name}}</td>
							<td>{{key}}</td>
							<td>{{tot_request}}</td>
							<td>{{tot_curr_request}}</td>
							<td>{{max_allowed}}</td>
							<td>
								<span style='float:right'>
									<a class='btn btn-flat btn-sm btn-primary' href='{{x:uri x='ADMIN'}}/apis/edit/{{id}}'>Edit</a>
									<a class='btn btn-flat btn-sm btn-primary' href='{{x:uri x='ADMIN'}}/apis/view/{{id}}'>View Request</a>							
									<a class='btn btn-flat btn-sm btn-danger confirm' href='{{x:uri x='ADMIN'}}/apis/delete/{{id}}'>Delete</a>
								</span>
							</td>
						</tr>
					{{/keys}}
				</table>
			</div>
		</div>
	</div>
</div>




		


		{{pagination:links}}
	
	</div>

</section>