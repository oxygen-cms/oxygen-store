<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">
					Request for: {{keydata.name}}
				</h3>
			</div>
			<div class="box-body">
	
				<table class='table'>
						<tr>
							<th>ID</th>
							<th>Endpoint ID</th>
							<th>Date</th>
							<th class='actions'></th>
						</tr>
					
					{{items}}
						<tr>
							<td>{{id}}</td>
							<td>{{endpoint}}</td>
							<td>{{date}}</td>
							<td>
								<span style='float:right'>
								</span>
							</td>
						</tr>
					{{/items}}
				</table>

				{{pagination:links}}
			</div>
		</div>
	</div>
</div>
