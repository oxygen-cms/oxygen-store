<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">
					Create API Key
				</h3>
			</div>
			<div class="box-body">

				<a class='btn btn-primary btn-flat' href='{{x:uri x='ADMIN'}}/apis/create'>Create New</a>

				<table class='table'>
					<tr>
						<td>
							<label>Name:<label>
						</td>
						<td>
							<input type='text' name='name' value='{{name}}'>
						</td>
					</tr>	
					<tr>
						<td>
							<label>Max Allowed:<label>
						</td>
						<td>						
							<select name='max_allowed'>
								<option value='100'>100</option>
								<option value='100000'>100,000</option>	
								<option value='500000'>500,000</option>	
								<option value='1000000'>1,000,000</option>																	
							</select>
						</td>
					</tr>			
					<tr>
						<td>
							<label>Enabled:<label>
						</td>
						<td>							
							<select name='enabled'>
								<option value='0'>No</option>
								<option value='1'>Yes</option>					
							</select>
						</td>
					</tr>			
					<tr>
						<td colspan='2'>	
							<button class='btn btn-primary btn-flat'>Save</button>				
							<a href="{{x:uri x='ADMIN'}}/apis" class='btn btn-default btn-flat'>Cancel</a>
						</td>
					</tr>				
				</table>
			</div>
		</div>
	</div>
</div>
<?php echo form_close();?>