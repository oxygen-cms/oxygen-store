<div class="row">

	<div class="box">

		<div class='' id="filters_group" style="">

			<div class="col-md-12">

				<fieldset id="filters" style="">
					<?php echo form_open(NC_ADMIN_ROUTE.'/products/callback'); ?>

					
						<div class="col-md-3">
								<fieldset id="" style="">
									<?php echo form_input('f_keyword_search',  $f_keyword_search, 'placeholder="Search" style=""'); ?>
									<a title='Clear Search' style='' class='btn btn-sm bg-green' href='{{x:uri x='ADMIN'}}/products/filter/clear'>Reset</a>	
									<?php echo $filter_type; ?>
								</fieldset>
						</div>
								
					    <div class="col-md-3"  id="hideable_filters" style="">
					        	<fieldset id="" style="">

									<?php echo form_hidden('f_module', $module_details['slug']); ?>		
						
												<label>
													<?php echo lang('store:products:order_by'); ?>
												</label>
												<div class="input">

													<?php echo form_dropdown('f_order_by',  
															array(
																'id'=> 'ID',
																'name'=> 'Name',
																'slug'=> 'Slug',
																'views'=> 'View Count',
																'created'=> 'Date Created',
																'updated'=> 'Date Last Updated',
																'ordering_count' => 'Custom Order',
																'created_by' => 'User that Created Product',
																),$f_order_by ); ?>
												</div>
										
												<label>
													Order Direction
												</label>
												<div class="input">

													<?php echo form_dropdown('f_order_by_dir',  
															array(
																'asc'=> 'Ascending',
																'desc'=> 'Decending',
																),$f_order_by_dir ); ?>
												</div>
									
												<label>
													<?php echo lang('store:products:items_per_page'); ?>
												</label>
												<div class="input">
													<?php echo form_dropdown('f_items_per_page', array(5=>"5", 10=>"10", 20=>"20", 50=>"50", 100=>"100", 200=>"200"), $f_items_per_page); ?>
												</div>
								</fieldset>
						</div>

					    <div class="col-md-3" >
				     
				        	<fieldset id="" style="">
							
									<label>
										Enabled On/Off
									</label>
									<div class="input">
										<?php echo form_dropdown('f_visibility',  array('all' => lang('global:select-all'),'on'=> 'On / Visible','off'=> 'Off / InVisible'), $f_visibility ); ?>
									</div>
					
									<label>
										Featured
									</label>
									<div class="input">
										<?php echo form_dropdown('f_featured',  array('all'=> lang('global:select-all'),'yes'=> 'Yes','no'=> 'No'), $f_featured ); ?>
									</div>
					
									<label>
										Status
									</label>
									<div class="input">
										<?php echo form_dropdown('f_status',  array('all'=> lang('global:select-all'),'active'=> 'Active','deleted'=> 'Deleted'), $f_status ); ?>
									</div>

								</fieldset>
						</div> 

				
							
					<?php echo form_close(); ?>	
				</fieldset>

			</div>

		</div>

	</div>
</div>	
