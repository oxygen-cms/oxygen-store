		<fieldset>
					<ul>
						<li class="">
							<h3>ID :  {{id}} </h3>
						</li>
						<li>
							<h3>Product Type<span>: {{type_slug}}</span></h3>
							<div class="input">

								Order of preference for layout file:

								<code>store_product_{{slug}}.html</code>
								<code>store_product_type_{{type_slug}}.html</code>								
								<code>store.html</code>
								<code>default.html</code>

							</div>
						</li>	
					
					</ul>
		</fieldset>