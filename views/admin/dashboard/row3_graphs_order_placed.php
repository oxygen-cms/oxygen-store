	<div class="content no-padding">
		<div class="col-sm-12">

			<!-- Area chart -->
			<div class="box box-solid">

				<div class="box-header with-border">
					<i class="fa fa-bar-chart-o"></i>
					<h3 class="box-title">Quarterly Trend (Orders Placed)</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						<a href="{{url:site}}admin/dashboard/toggle/<?php echo $id;?>" class="btn btn-box-tool" ><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="box-body">
					<div id="area-chart" style="height: 338px;" class="full-width-chart"></div>
				</div>

			</div>
		</div>
	</div>

<script>

		$(window).load(function ()
		{

			$.ajax({
					//url: SITE_URL+'store/admin/stats/1',
					url: SITE_URL+'store/admin/stats/qtrend/90',
					dataType: 'json',
					success: function(json){

						$.plot("#area-chart", [json], {

						  grid: {
							borderWidth: 0
						  },
						  series: {
							shadowSize: 0, // Drawing is faster without shadows
							color: "#00c0ef"
						  },
						  lines: {
							fill: true //Converts the line chart to area chart
						  },
						  yaxis: {
							show: false
						  },
						  xaxis: {
							show: false
						  }
						});	
					}
			});
			/*
			var bar_data = {
			  data: [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]],
			  color: "#3c8dbc"
			};     */   
			$.ajax({
				//url: SITE_URL+'store/admin/stats/1',
				url: SITE_URL+'store/admin/stats/msales/180',
				dataType: 'json',
				success: function(bar_data){

					$.plot("#bar-chart", [bar_data], {
					  grid: {
						borderWidth: 1,
						borderColor: "#f3f3f3",
						tickColor: "#f3f3f3"
					  },
					  series: {
						bars: {
						  show: true,
						  barWidth: 0.5,
						  align: "center"
						}
					  },
					  xaxis: {
						mode: "categories",
						tickLength: 0
					  }
					}); 
				}
			});
			//end

			/*
			var bar_data = {
			  data: [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]],
			  color: "#3c8dbc"
			};     */   
			$.ajax({
				//url: SITE_URL+'store/admin/stats/1',
				url: SITE_URL+'store/admin/stats/mostviewed/3',
				dataType: 'json',
				success: function(donutData){

				  $.plot("#donut-chart", donutData, {
					series: {
					  pie: {
						show: true,
						radius: 1,
						innerRadius: 0.5,
						label: {
						  show: true,
						  radius: 2 / 3,
						  formatter: labelFormatter,
						  threshold: 0.1
						}

					  }
					},
					legend: {
					  show: false
					}
				  });
				  //end 
				}
			});
			//end


		
	 
			$.ajax({
				url: SITE_URL+'store/admin/stats/orders/31',
				dataType: 'json',
				success: function(result){
					$.plot($('#line-chart'), [result], {
						grid: {
						  hoverable: true,
						  borderColor: "#f3f3f3",
						  borderWidth: 1,
						  tickColor: "#f3f3f3"
						},
						series: {
						  shadowSize: 0,
						  lines: {
							show: true
						  },
						  points: {
							show: true
						  }
						},
						lines: {
						  fill: false,
						  color: ["#3c8dbc", "#f56954"]
						},
						yaxis: {
						  show: true,
						},
						xaxis: {
						  show: true
						}
					}
					);

				}
			});
	  

			$("#line-chart").bind("plothover", function (event, pos, item) {

			  if (item) {
				var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

				$("#line-chart-tooltip").html(item.series.label + " of " + x + " = " + y)
						.css({top: item.pageY + 5, left: item.pageX + 5})
						.fadeIn(200);
			  } else {
				$("#line-chart-tooltip").hide();
			  }

			}); 

			//Initialize tooltip on hover
			$("<div class='tooltip-inner' id='line-chart-tooltip'></div>").css({
			  position: "absolute",
			  display: "none",
			  opacity: 0.8
			}).appendTo("body");


		});




		/* END BAR CHART */
	  /*
	   * Custom Label formatter
	   * ----------------------
	   */
	  function labelFormatter(label, series) {
		return "<div style='font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;'>"
				+ label
				+ "<br>"
				+ Math.round(series.percent) + "%</div>";
	  }
</script>