
	<div class="content no-padding">
		<div class="col-sm-12">
			<div class="box box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bar-chart-o"></i>
					<h3 class="box-title">
						Most Viewed Products
					</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<a href="{{url:site}}admin/dashboard/toggle/<?php echo $id;?>" class="btn btn-box-tool" >
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="box-body">
					<div id="donut-chart" style="height: 300px;"></div>
				</div>
			</div>
		</div>
	</div>
