          <div class="row">
            <div class="col-xs-12">

              <div class="box box-primary">
                <div class="box-header">

			            <h4 class="box-title">
			              	Sandbox Area
			            </h4>


                  <div class="box-tools">

	                    <div class="input-group">
		                      
		                      	<button class="btn btn-sm btn-default">
			                      	<div class="input-group-btn">
			                        	<i class="fa fa-search"></i>
			                      	</div>
		                      	</button>
	                    </div>
					
                  </div>

                </div><!-- /.box-header -->
                <div class="box-body">



						<a href='admin/store/sandbox/shipping' class='btn orange'>Shipping[Cart]</a>

						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('cancel'))); ?>

						<?php echo $output;?>

				</div>
			</div>
		</div>
	</div>

		
