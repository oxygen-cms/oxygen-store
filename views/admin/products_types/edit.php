<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<div class="row">
    <div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">
					Edit Product Type
				</h3>
				<div class="box-tools">
				</div>
			</div>
			<div class="box-body">
				<?php
				if($has_active)
				{
					echo "You have active records";
				}
				?>
				<table class='table'>
					<tr>
						<td>
							Name
						</td>
						<td>
							<?php echo form_input('name', set_value('name', $type->name)); ?>
						</td>
					</tr>
					<tr>
						<td>
							Default
						</td>							
						<td>
							<?php echo form_dropdown('default', array(0=>'No',1=>'Yes'),  $type->default ); ?> 
						</td>
					</tr>
					<tr>
						<td>
							Slug
						</td>
						<td>
							<?php echo $type->slug; ?>
						</td>
					</tr>
					<tr>
						<td>
							View Products By Type
						</td>
						<td>
							<a target='new' class="" href="shop/products/type/<?php echo $type->slug; ?>">View {{url:site}}shop/products/type/<?php echo $type->slug; ?></a>
						</td>
					</tr>
					<tr>
						<td>
							Custom View File
						</td>
						<td>
							<code>store_product_type_<?php echo $type->slug; ?>.html</code>
						</td>
					</tr>

					<?php if((count($available_types) > 0) AND ($has_products==false)): ?>
					<tr>
						<td>
							Attributes
						</td>
						<td>	
							<?php echo form_multiselect('properties[]', $available_types, $set_types) ?>
						</td>
					</tr>
					<?php endif; ?>	
					<?php  if((count($available_types) > 0) AND ($has_products==true)): ?>
					<tr>
						<td>
							 Attributes
						</td>
						<td>	
					 		<?php foreach($set_types as $ttpe): ?>
								<?php echo '<input type="hidden" name="properties[]" value="'.$ttpe.'">'.$available_types[$ttpe]; ?>
							<?php endforeach; ?>
						</td>
					</tr>
					<?php endif; ?>	
				</table>
				<div class="buttons">
					<button class="btn btn-sm btn-flat bg-blue" value="save_exit" name="btnAction" type="submit">Save and Exit</button>					
					<button class="btn btn-sm btn-flat bg-blue" value="save" name="btnAction" type="submit">Save</button>
					<a class="btn btn-default btn-sm btn-flat" href="admin/store/products_types">Cancel</a>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>