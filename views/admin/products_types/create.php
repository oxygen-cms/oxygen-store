<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<div class="row">
    <div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h3 class="box-title">
					Create A Product Type
				</h3>
				<div class="box-tools">

				</div>
			</div>

			<div class="box-body">

				<table class='table'>
					<tr>
						<td>
							Name
						</td>
						<td>
							<?php echo form_input('name', set_value('name', $name)); ?>
						</td>						
					</tr>
					<tr>
						<td>
							Is Default
						</td>
						<td>
							<?php echo form_dropdown('default', array(0=>'No',1=>'Yes'),  $default ,'class="form-control"'); ?> 
						</td>						
					</tr>					
				</table>

				<div class="buttons">
					<button class="btn blue" value="save" name="btnAction" type="submit">Create</button>
					<a class="btn btn-default btn-sm btn-flat" href="{{x:uri x='ADMIN'}}/products_types">Cancel</a>
				</div>					

			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>