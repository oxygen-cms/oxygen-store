<div class="row">

    <div class="col-xs-12">

        <div class="box box-solid">

                <div class="box-header">
	                  <h3 class="box-title">
							Product Types
	                  </h3>
                </div><!-- /.box-header -->

                <div class="box-body">

					<table class='table'>
						<thead>
							<tr>
								<th><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
								<th>ID</th>
								<th>Name</th>
								<th>Custom Layout</th>
								<th>Created</th>
								<th>Is Default</th>
								<th style="width: 120px"></th>
							</tr>
						</thead>
						<tbody>
							<?php if (count($entries ) >0): ?>
							<?php
								foreach ($entries AS $type): ?>
											<tr class="<?php echo alternator('even', ''); ?>">
												<td><input type="checkbox" name="action_to[]" value="<?php echo $type['id']; ?>"  /></td>
												<td><?php echo $type['id']; ?></td>
												<td><?php echo $type['name']; ?></td>
												<td><span style='font-family:courier'><?php echo 'store_type_'.$type['slug'].'.html';?></span></td>
												<td><?php echo nc_format_date($type['created']); ?></td>
												<td><?php echo yesNoBOOL($type['default'], 'string', '<span style="color:green">Yes</span>','<span style="color:gray">No</span>'); ?></td>

												<td>
													<span style="float:right;">
																<?php
																	$items = array();
																	$items[] = dropdownMenuStandard(site_url('admin/store/products_types/edit/' . $type['id']), FALSE, lang('store:admin:edit'), FALSE, "edit");
																	$items[] = dropdownMenuStandard(site_url('admin/store/product_type_fields/fields/' . $type['slug']), FALSE, 'Edit Fields', FALSE, "edit");
																	$items[] = dropdownMenuStandard(site_url('admin/store/products_types/delete/' . $type['id']), 	TRUE,  lang('store:admin:delete'), TRUE, "minus");
																	echo dropdownMenuListSplit($items,'Edit',site_url('admin/store/products_types/edit/' . $type['id']));
																?>
													</span>
												</td>
											</tr>
							<?php endforeach; ?>
							<?php else: ?>
											<tr>
												<td colspan="9">
												No recoreds found
												</td>
											</tr>
							<?php endif; ?>

						</tbody>
						<tfoot>
							<tr>
								<td colspan="8"><div style="float:right;"></div></td>
							</tr>
						</tfoot>
					</table>


				</div>
		</div>


	<?php if (isset($pagination)): ?>
		<?php echo $pagination; ?>
	<?php endif; ?>

	</div>
</div>
