
	<?php echo form_open_multipart($this->uri->uri_string()); ?>
	<?php echo form_hidden('id', set_value('id', $id)); ?>


	<div class='col-sm-6'>

		<div class="form-group">
				<label class="control-label" for="name">
					<?php echo lang('store:admin:name');?>
				</label>
				<?php echo form_input('name', set_value('name', $name),'class="form-control"' ); ?> 
		</div>

		<div class="form-group">
				<label class="control-label" for="rate">
					<?php echo lang('store:tax:rate');?> %
					<small>	
							<br>The tax rate should be in whole number format (integer)
							For 10% enter the value of `10`. For half a percent type `0.5`
					</small>					
				</label>
				<?php echo form_input('rate', set_value('discount_pcent', $rate),'class="form-control"'); ?>
		</div>

		<div class="form-group">
				<label class="control-label" for="default">
					Is Default tax ?
				</label>
				<?php echo form_dropdown('default', [0=>'No',1=>'Yes'],  $default,'class="form-control"' ); ?>
		</div>


		<div class="buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
		</div>

	</div>

<?php form_close(); ?>
