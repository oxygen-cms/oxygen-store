
<div class="row">

	<div class="col-xs-12">

		<div class="box box-solid">

				<div class="box-header">

					<h4 class="box-title">
						<?php echo lang('store:workflows:title');?>
					</h4>
				</div>

				<div class="box-body">

						<?php echo form_open(NC_ADMIN_ROUTE.'/workflows/delete'); ?>

							<?php if (empty($workflows)): ?>
								<div class="no_data">
									<p>
										<?php echo lang('store:workflows:workflows');?>
									</p>
										<?php echo lang('store:workflows:no_data');?>
									<br><br><br>
									<p>
									<small>
										<?php echo lang('store:admin:feedback');?>
									</small>
									</p>
								</div>
							<?php else: ?>
								<table class='table' id=''>
									<thead>
										<tr>
											<th width="5%"></th>
											<th width="15%"><?php echo lang('store:admin:id');?></th>
											<th width="35%"><?php echo lang('store:workflows:workflow');?></th>
											<th width="15%"><?php echo lang('store:workflows:progress');?></th>
											<th width="5%">Default</th>
											<th></th>
										</tr>
									</thead>
									<tbody>

										<?php
											foreach ($workflows AS $workflow): ?>
												<tr>
													<td><a class='handle'></a> <input type="checkbox" name="action_to[]" value="<?php echo $workflow->id; ?>"  /></td>
													<td><?php echo $workflow->id; ?></td>
													<td><?php echo $workflow->name; ?></td>
													<td><?php echo $workflow->pcent; ?> %</td>
													<td><?php echo $workflow->is_placed; ?></td>
													<td>
														<a href="{{x:uri x='ADMIN'}}/workflows/edit/<?php echo $workflow->id;?>" class='btn btn-flat bg-green'><?php echo lang('store:admin:edit');?></a>
														<?php if($workflow->core == 0):?>
															<a href="{{x:uri x='ADMIN'}}/workflows/delete/<?php echo $workflow->id;?>" class='btn btn-flat btn-danger confirm'><?php echo lang('store:admin:delete');?></a>
														<?php endif;?>
													</td>
												</tr>
											<?php endforeach; ?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5"><div style="float:right;"></div></td>
										</tr>
									</tfoot>
								</table>
								<div class="buttons">
									<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
								</div>
							<?php endif; ?>

						<?php echo form_close(); ?>
					<?php if (isset($pagination)): ?>
						<?php echo $pagination; ?>
					<?php endif; ?>						
				</div>
		</div>
	</div>
</div>