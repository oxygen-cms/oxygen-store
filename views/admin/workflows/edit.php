
<div class="row">

	<div class="col-xs-12">

		<div class="box box-solid">

				<div class="box-header">

					<h4 class="box-title">
						<?php if (isset($id) AND $id > 0): ?>
							<?php echo sprintf('Edit', $name); ?>
						<?php else: ?>
							New
						<?php endif; ?>
					</h4>
				</div>

				<div class="box-body">

				
						<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
						<?php if (isset($id) AND $id > 0): ?>
							<?php echo form_hidden('id', $id); ?>
							<input type="hidden" name="cid" id="cid" value="<?php echo $id; ?>" >
						<?php endif; ?>

							<table class='table'>	
								<tr>
									<td>
										<label for="name">Name<span>*</span></label>
									</td>
									<td>
										<?php echo form_input('name', set_value('name', $name), 'id="name" '); ?>
									</td>
								</tr>																			
								<tr>
									<td>
										<?php echo lang('store:workflows:progress');?>
									</td>
									<td>
										<?php echo form_input('pcent', set_value('pcent', $pcent), 'id="name" '); ?>
									</td>
								</tr>
								<tr>
									<td>
										<label for="name">Default<span>*</span></label>
									</td>
									<td>
										<?php echo form_dropdown('is_placed', [0=>'No', 1=>'Yes'] , $is_placed, 'id="is_placed" '); ?>
									</td>
								</tr>										
							</table>


							<div class="buttons">
									<button class="btn blue" value="save_exit" name="btnAction" type="submit">
										<span><?php echo lang('store:workflows:save_exit');?></span>
									</button>

									<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save'))); ?>
									<a href="{{x:uri x='ADMIN'}}/workflows/" class="btn gray"><?php echo lang('store:workflows:cancel');?></a>
							</div>
						<?php echo form_close(); ?>
				</div>



		</div>
	</div>
</div>
