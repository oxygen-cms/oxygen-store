
<div class="row">

	<div class="col-xs-12">

		<div class="box box-solid">

				<div class="box-header">

					<h4 class="box-title">
						Create new Workflow
					</h4>
				</div>

				<div class="box-body">

						<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>

								<table class='table'>	
									<tr>
										<td>
											<label for="name">Name<span>*</span></label>
										</td>
										<td>
											<?php echo form_input('name', set_value('name', $name), 'id="name" '); ?>
										</td>
									</tr>																			

								</table>

								<div class="buttons">
										<button class="btn blue" value="save_exit" name="btnAction" type="submit">
											<span>Save</span>
										</button>

										<a href="{{x:uri x='ADMIN'}}/workflows/" class="btn gray">Cancel</a>
								</div>

						<?php echo form_close(); ?>
				</div>
		</div>
	</div>

</div>