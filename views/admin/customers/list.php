<div class="row">
    <div class="col-xs-12">
		<div class="box box-solid">
			<?php echo form_open(NC_ADMIN_ROUTE.'/customers/filter') ?>
			<?php echo form_hidden('f_module', $module_details['slug']) ?>  
			<?php echo form_hidden('auto_filter_enabled', 'disabled') ?>  
			<div class="box-header">
				<h3 class="box-title">
					<?php echo lang('store:customers:active_customers');?>
				</h3>
				<div class="box-tools">
					<div class="input-group">
						<input type="text" placeholder="Search" style="width: 150px;" class="form-control input-sm pull-right" name="f_name" value="<?php echo $str_search;?>">
						<div class="input-group-btn">
							<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div><!-- /.box-header -->
			<?php echo form_close() ?>
			<div class="box-body table-responsive">
				<table class="table table-hover">
					<thead>
					    <tr>
							<th><?php echo lang('store:customers:user_id');?></th>
							<th><?php echo lang('store:admin:name');?></th>
							<th class=''></th>
					    </tr>                  		
					</thead>
					<tbody>
						<?php foreach($customers as $cust):?>
							<tr>
								<td><?php echo $cust->user_id;?></td>
								<td><?php echo $cust->first_name;?></td>
								<td><?php echo $cust->last_name;?></td>
								<td>
									<span style='float:right'>
										<a href='{{x:uri x='ADMIN'}}/customers/orders/<?php echo $cust->user_id;?>' class='btn btn-flat bg-green'><?php echo lang('store:admin:orders');?></a>
										<a href='{{x:uri x='ADMIN'}}/customers/addresses/<?php echo $cust->user_id;?>' class='btn btn-flat bg-blue'>Addresses</a>
									</span>
								</td>
							</tr>
						<?php endforeach;?>
					
					</tbody>
				</table>
				{{pagination:links}}
			</div>
		</div>
    </div>
</div>