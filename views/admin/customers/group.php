<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<div class="row">

    <div class="col-xs-12">

		<div class="box box-solid">

			<div class="box-header">
				<h3 class="box-title">
					<?php echo lang('store:customers:customer');?> : Users Group Level : <?php echo $user->first_name.' '.$user->last_name;?>
				</h3>
			</div>

			<div class="box-body">

				<fieldset>

						<div class="input">
							<?php echo form_dropdown('group_id',$cms_user_groups,set_value( 'group_id', $user->group_id ));?>
							<input type='hidden' name='user_id' value='<?php echo $user->user_id;?>'>
						</div>

						<br>

						<button class="btn blue" value="Update" name="btnAction" type="submit">
								<span>Update &amp; Exit</span>
						</button>



						<a class='btn gray' href='{{x:uri x='ADMIN'}}/customers/'>Cancel</a>

				</fieldset>
			</div>
		</div>
	</div>
</div>

<?php echo form_close(); ?>