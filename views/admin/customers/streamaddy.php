<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header">
	            <h4 class="box-title">
	            	Edit <?php echo $user->first_name.' '. $user->last_name;?> address #id={{address_id}}
	            </h4>
				<div class="pull-right">
				</div>
            </div>
            <div class="box-body">
				<?php echo $tabbed_html;?>
			</div>
		</div>
	</div>
</div>
