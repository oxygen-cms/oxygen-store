
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header">
	            <h4 class="box-title">
	            	<a href='{{x:uri x='ADMIN'}}/customers/'>[Back &larr;] </a> <?php echo lang('store:customers:client_order');?>: {{customer.last_name}}, {{customer.first_name}}
	            </h4>
				<div class="pull-right">
				</div>
            </div>
            <div class="box-body">
            	<input type='hidden' name='auto_filter_enabled' value='disabled'>
				<?php echo form_open(NC_ADMIN_ROUTE.'/customers'); ?>

				<table class='table'>
					{{orders}}
						<tr>
							<td>{{id}}</td>
							<td>{{ helper:date format="d/m/Y" timestamp=order_date }}</td>
							<td>{{status}}</td>
							<td>{{total_amount_order_wt}}</td>
							<td>{{if deleted==''}}{{else}}
								<span class='stags red'>
									<?php echo lang('store:customers:deleted');?>
								</span>
								{{endif}}
							</td>
							<td>
								<span style='float:right'>
									<a href='{{x:uri x='ADMIN'}}/orders/order/{{id}}/0' class='btn btn-flat btn-default as_modal'>Preview</a>
									<a href='{{x:uri x='ADMIN'}}/orders/order/{{id}}' class='btn btn-flat bg-blue'>View</a>
								</span>
							</td>
						</tr>
					{{/orders}}
				</table>
				{{pagination:links}}
				<?php echo form_close(); ?>

				<br>
				<a class='btn blue' href='{{x:uri x='ADMIN'}}/customers/'>Back to Customers</a>
			</div>
		</div>
	</div>
</div>