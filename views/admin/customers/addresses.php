<div class='row'>
	<div class="col-sm-12">
		<div class="box box-solid">
		    <div class="box-header with-border">
				<h3 class="box-title">
					<a href='{{x:uri x='ADMIN'}}/customers/'>[Back &larr;] </a> 
					<?php echo lang('store:customers:client_order');?>: {{customer.last_name}}, {{customer.first_name}}
				</h3>
				<div class="box-tools">
				</div>
		    </div>
		    <div class="box-body">

				<?php echo form_open("{{x:uri x='ADMIN'}}/customers"); ?>
				<table class='table'>
						<tr>
							<th>ID</th>
							<th>Last Updated</th>
							<th>Name</th>
							<th>Email</th>
							<th>Address</th>
							<th>State</th>
							<th>Zip/Postcode</th>
							<th>Country</th>
							<th>Billing</th>
							<th>Shipping</th>
							<th>
							</th>
						</tr>				
					{{addresses}}
						<tr>
							<td>{{id}}</td>
							<td>{{ helper:date format="m/d/Y" timestamp=updated }}</td>
							<td>{{last_name}}, {{first_name}}</td>
							<td>{{email}}</td>
							<td>{{address1}} {{address2}}</td>					
							<td>{{state}}</td>
							<td>{{zip}}</td>
							<td>{{country}}</td>
							<td>{{billing}}</td>
							<td>{{shipping}}</td>
							<td>
								<span style='float:right'>
									<a class='btn btn-sm btn-flat bg-blue' href='admin/store/customers/edit_user_addy/{{id}}/{{user_id}}'>Edit Address</a>
								</span>
							</td>
						</tr>
					{{/addresses}}
				</table>
				{{pagination:links}}
				<?php echo form_close(); ?>
				<br><p>
				<a class='btn btn-flat bg-blue' href='{{x:uri x='ADMIN'}}/customers/'>Back to Customers</a> | 
				<a class='btn btn-flat bg-orange' href='{{x:uri x='ADMIN'}}/customers/orders/{{customer.id}}/'>Orders by Customer</a>


		   	</div>
		</div>
	</div>
</div>



