<div class='row'>
	<div class="col-sm-12">
		<div class="box box-solid">
		    <div class="box-header with-border">
				<h3 class="box-title">
					<?php echo "Customer : " . $user->display_name. " | UserID: {$customer_id} "; ?>
				</h3>
				<div class="box-tools">
				</div>
		    </div>
		    <div class="box-body">

				<a class='btn bg-green btn-flat small' href='admin/store/customers/addresses/<?php echo $customer_id;?>'>
					Customer addresses
				</a>

				<fieldset>
					<?php echo $tabbed_html;?>
				</fieldset>

			</div>
		</div>
	</div>
</div>