<div class="row">
	<div class="col-xs-12">
	    <div class="box box-solid">
	        <div class="box-header">
	            <h4 class="box-title">
	            	<?php echo lang('store:admin:affiliates');?>
	            </h4>
				<div class="box-tools">
					<a href="admin/store/affiliates/create" title="" class='btn btn-sm btn-primary'>New</a>
				</div>
	        </div>
	        <div class="box-body">
				<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
					<?php if (empty($affiliates)): ?>
						<div class="no_data">
							<p><?php echo lang('store:admin:no_data');?></p>
							<?php echo lang('no_items'); ?>
						</div>
					<?php else: ?>
						<table class='table'>
							<thead>
								<tr>
									<th style="width: 20px"><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
									<th style="width: 20px"><?php echo lang('store:admin:id');?></th>
									<th style="width: 200px"><?php echo lang('store:admin:affiliates');?></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
									foreach ($affiliates AS $type): ?>

												<tr class="">

													<td><input type="checkbox" name="action_to[]" value="<?php echo $type->id; ?>"  /></td>
													<td><?php echo $type->id; ?></td>
													<td><?php echo $type->name; ?></td>
													<td>
														<span style="float:right;">

															<a class="btn btn-flat btn-primary " href="<?php echo site_url('admin/store/affiliates/view/' . $type->id); ?>">
																<?php echo lang('store:admin:view');?>
															</a>
										
									
															<a class="btn btn-flat btn-primary " href="<?php echo site_url('admin/store/affiliates/edit/' . $type->id); ?>">
																<?php echo lang('store:admin:edit');?>
															</a>
											

															<a class="btn btn-flat btn-danger confirm" href="<?php echo site_url('admin/store/affiliates/delete/' . $type->id); ?>"><?php echo lang('store:common:delete');?></a></li>
															
														</span>
													</td>
												</tr>


								<?php endforeach; ?>

							</tbody>
							<tfoot>
								<tr>
									<td colspan="8"><div style="float:right;"></div></td>
								</tr>
							</tfoot>
						</table>
						<div class="buttons">
							<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
						</div>
					<?php endif; ?>
					<?php if (isset($pagination)): ?>
						<?php echo $pagination; ?>
					<?php endif; ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>