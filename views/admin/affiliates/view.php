<style>
	textarea.st_affiliate_ta {
		border:4px solid #eee;
		clear:both;
		margin:5px;
		width:100%;

	}
</style>
<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h4 class="box-title">
					<?php echo "View {$type->name}"; ?>
				</h4>
				<div class="box-tools">
					<a href="admin/store/affiliates/create" title="" class='btn btn-sm btn-primary'>New</a>
				</div>
			</div>
			<div class="box-body">


				<table class='table'>
					<tr>
						<td>
							<?php echo lang('store:common:name');?>
						</td>
						<td>
							<?php echo $type->name;?>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo lang('store:code');?>
						</td>
						<td>
							<?php echo $type->code;?>
						</td>
					</tr>
					<tr>
						<td>
							Example Usage
						</td>
						<td>
							{{url:site}}?QUANTAM=<?php echo $type->code;?>
						</td>
					</tr>					
					<tr>
						<td>
							
						</td>
						<td>
							
						</td>
					</tr>	
					<tr>
						<td>
							
						</td>
						<td>
							
						</td>
					</tr>
				</table>	

				<form action='admin/store/affiliates/generate/' method='get'>
					<input type='hidden' name='client_code' value='<?php echo $type->code;?>'>
					<h4>Generate a link</h4>
					<table class='table'>				
						<tr>
							<td>
								Enter a link here: 
							</td>
							<td>
								<input class="form-control" type='text' id='url' name='url' placeholder='{{url:site}}' value='{{url:site}}'>
							</td>
						</tr>	
						<tr>
							<td>
								Display Text for link
							</td>
							<td>
								<input class="form-control" id='text' type='text' name='text_to_display' placeholder='Click me'>
							</td>
						</tr>
					</table>	
				</form>
	
				Be sure to test your links before mailing it to your affiliate or affiliate group.

				<a class='btn btn-flat btn-sm bg-orange getlink'>View Link</a>

				<div id="LinkArea">

				</div>

				<div class="buttons">
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array( 'cancel'))); ?>
				</div>

			</div>
		</div>
	</div>
</div>


<script>
	$(document).on('click', '.getlink', function(event)
	{
		var postto = "admin/store/affiliates/generate/";
		var _url = $('#url').val();
		var _text = $('#text').val();
		var _code = '<?php echo $type->code;?>';
		var senddata = {url:_url, text:_text,code:_code };

		$.post(postto,senddata).done(function(data)
		{
				$('#LinkArea').html(data);
		});
		// Prevent Navigation
		event.preventDefault();
	});
</script>