<div class="row">
	<div class="col-xs-12">
	    <div class="box box-solid">
	        <div class="box-header">
	            <h4 class="box-title">
	            	Affiliates : New
	            </h4>
				<div class="box-tools">
					<a href="admin/store/affiliates/create" title="" class='btn btn-sm btn-primary'>New</a>
				</div>
	        </div>
	        <div class="box-body">
	        	<?php echo form_open_multipart('store/admin/affiliates/create/'); ?>
		        	<table class="table">
		        		<tr>
		        			<td width="10%">
		        				<?php echo lang('store:admin:name');?>
		        			</td>
		        			<td>
		        				<?php echo form_input('name', set_value('name', ''), 'class="form-control" id="name"  placeholder="'.lang('store:admin:affiliates_placeholder').'"'); ?>
		        			</td>	        			
		        		</tr>
		        	</table>
					<br>
					<div class="buttons">
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>