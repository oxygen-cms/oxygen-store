<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-header">
				<h4 class="box-title">
					<?php echo sprintf(lang('store:common:edit'), $type->name); ?>
				</h4>
				<div class="box-tools">
					<a href="admin/store/affiliates/create" title="" class='btn btn-sm btn-primary'>New</a>
				</div>
			</div>
			<div class="box-body">

				<?php echo form_open_multipart("store/admin/affiliates/edit/{$id}"); ?>

					<?php if (isset($id) AND $id > 0): ?>

						<?php echo form_hidden('id', $id); ?>

					<?php endif; ?>


					<table class='table'>
						<tr>
							<td>
								Name:
							</td>
							<td>
								<?php echo form_input('name', set_value('name', $type->name), 'class="form-control" id="name" '); ?>
							</td>							
						</tr>
					</table>
					<br>

					<div class="buttons">
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save_exit','save', 'cancel'))); ?>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>