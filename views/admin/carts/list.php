<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header">
	            <h4 class="box-title">
	            	Active Shopping carts
	            </h4>
				<div class="pull-right">
					
				</div>
            </div>
            <div class="box-body">
				<table class='table'>
						<tr>
							<th>USER-ID</th>
							<th>User name</th>
							<th>Items QTY</th>
							<th>Total Value</th>
							<th>Last Seen</th>
							<th>First Seen</th>
							<th class='actions'><?php echo lang('store:admin:actions');?></th>
						</tr>
					{{carts}}
						<tr>
							<td>{{user_id}}</td>
							<td>{{username}}</td>
							<td>{{qty}}</td>
							<td>{{value}}</td>
							<td>{{date}}</td>
							<td>{{oldest}}</td>
							<td>
								<span style='float:right'>
									<a class='button btn btn-flat btn-sm btn-primary as_modal' href='{{x:uri x='ADMIN'}}/carts/view/{{user_id}}'>Items</a>
									<a class='button btn btn-flat btn-sm btn-danger delete confirm' href='{{x:uri x='ADMIN'}}/carts/delete/{{user_id}}'>Delete</a>
								</span>
							</td>
						</tr>
					{{/carts}}
				</table>
				{{pagination:links}}
			</div>
		</div>
	</div>
</div>
