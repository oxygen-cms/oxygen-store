	<div class='' id="filters_group" style="">
		<div class="oxygen_hidden_filters">

					<div class="col-md-6" style="">
							<?php echo form_hidden('f_module', $module_details['slug']); ?>		
							<label>
								Filter
							</label>
							<div class="input">
								<?php echo $filter_type; ?>
							</div>

							<label>
								<?php echo lang('store:products:order_by'); ?>
							</label>
							<div class="input">

								<?php echo form_dropdown('f_order_by',  
										array(
											'id'=> 'ID',
											'name'=> 'Name',
											'slug'=> 'Slug',
											'views'=> 'View Count',
											'created'=> 'Date Created',
											'updated'=> 'Date Last Updated',
											'ordering_count' => 'Custom Order',
											'created_by' => 'User that Created Product',
											),$f_order_by, 'class="form-control"' ); ?>
							</div>
					
							<label>
								Order Direction
							</label>
							<div class="input">

								<?php echo form_dropdown('f_order_by_dir',  
										array(
											'asc'=> 'Ascending',
											'desc'=> 'Decending',
											),$f_order_by_dir , 'class="form-control"'); ?>
							</div>
				
							<label>
								<?php echo lang('store:products:items_per_page'); ?>
							</label>
							<div class="input">
								<?php echo form_dropdown('f_items_per_page', array(5=>"5", 10=>"10", 20=>"20", 50=>"50", 100=>"100", 200=>"200"), $f_items_per_page, 'class="form-control"'); ?>
							</div>
					</div>

		    		<div class="col-md-6" >
						<label>
							Enabled On/Off
						</label>
						<div class="input">
							<?php echo form_dropdown('f_visibility',  array('all' => lang('global:select-all'),'on'=> 'On / Visible','off'=> 'Off / InVisible'), $f_visibility, 'class="form-control"' ); ?>
						</div>
		
						<label>
							Featured
						</label>
						<div class="input">
							<?php echo form_dropdown('f_featured',  array('all'=> lang('global:select-all'),'yes'=> 'Yes','no'=> 'No'), $f_featured, 'class="form-control"' ); ?>
						</div>
		
						<label>
							Status
						</label>
						<div class="input">
							<?php echo form_dropdown('f_status',  array('all'=> lang('global:select-all'),'active'=> 'Active','deleted'=> 'Deleted'), $f_status, 'class="form-control"' ); ?>
						</div>

						<label>
							Clear Filter
						</label>
						<div class="input">
							<a title='Clear Search' class='btn btn-flat btn-sm bg-green' href='{{x:uri x='ADMIN'}}/products/filter/clear'>Reset</a>	
						</div>						
					</div> 
		</div>
	</div>