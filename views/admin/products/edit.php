<?php if( $this->config->item('admin/product/show_infobar') ) : ?> 
    <?php //$this->load->view('admin/products/partials/infobar'); ?>
<?php endif;?>

<div class='row'>       
    <div class="col-lg-2 col-md-12 col-sm-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{name}}
                </h3>

            </div>     
            <div class="box-body">
                    <div id='cover_container' class="image" style="border:1px solid #aaa">
                        <?php echo "".nc_product_cover( $id,true, 'prod_cover','[No Cover Image]' ).""; ?>
                    </div>   
                               
                    <p>
                        <span class='label label-default'><?php echo lang('store:products:date_created');?>:<?php echo nc_format_date($created,'hms'); ?></span>
                        <br>
                        <span class='label label-primary'><?php echo lang('store:products:date_updated');?>:<?php echo nc_format_date($updated,'hms'); ?></span>
                        <br>
                        <?php if($deleted != NULL):?>
                            <span class='label label-warning'>Deleted:<?php echo nc_format_date($deleted,'hms'); ?></span>         
                        <?php endif;?>                    
                    </p>
                 
                    <div>
                        <span style="">
                            <a title='Previous Product' href="{{x:uri x='ADMIN'}}/product/autosel/{{userDisplayMode}}/prev/{{id}}" class='btn-flat btn btn-default tooltip-s'>&larr;</a>
                            <a title='Next Product' href="{{x:uri x='ADMIN'}}/product/autosel/{{userDisplayMode}}/next/{{id}}" class='btn-flat btn btn-default tooltip-s'>&rarr;</a>
                        </span>                  
                    </div>
            </div>
            <br>
        </div>
        <?php if($deleted == NULL):?>
            <?php if($this->method=='edit'):?>
                <a class="btn btn-block btn-primary" href="{{x:uri x='ADMIN'}}/product/view/{{id}}">Switch to View Mode</a>
                <a class="btn btn-block btn-danger confirm" href="{{x:uri x='ADMIN'}}/product/delete/{{id}}">Delete</a>
            <?php else:?>
                <a class="btn btn-block btn-primary" href="{{x:uri x='ADMIN'}}/product/edit/{{id}}">Switch to Edit Mode</a>
            <?php endif;?>  
        <?php endif;?>    

            <a class="btn btn-block btn-default" target ='new' href="{{x:uri}}/products/product/{{slug}}">View as User</a>
            <a class="btn btn-block btn-default" target ='new' href="{{x:uri}}/productsadmin/product/{{slug}}/1">View as Admin</a>
        <br>        
    </div>
    <div class="col-lg-10 col-md-12 col-sm-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Product : <span id='title_product_name'>{{name}}</span>
                </h3>
                <div class="box-tools">
                   
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
  
                    <div class="border">

                        <input type="hidden" name="static_product_id" id="static_product_id" data-pid="{{id}}" />
                        <input type="hidden" name="id" id="id" value="{{id}}" />



                        <div class="nav-tabs-custom">

                                <ul class="nav nav-tabs">


                                    <li class="active"><a href="#product-tab" data-toggle="tab"><?php echo lang('store:products:product');?></a></li>
                                      
                                    <?php if (isset($product_extra_form)):?>
                                        <?php if ($product_extra_form != 'no'):?>
                                            <li><a href="#product-type-fields-tab" data-toggle="tab">Extra Fields</a></li>
                                        <?php endif;?> 
                                    <?php endif;?>    

                                     <li><a href="#price-tab" data-toggle="tab"><?php echo lang('store:products:variations');?></a></li>    

                                        <?php
                                            foreach($module_tabs as $module) {
                                                echo "<li><a data-toggle='tab'  href='#".strtolower($module->namespace)."-tab'>{$module->name}</a></li>";
                                            }
                                        ?>
                                    <?php if(system_installed('feature_affiliates')):?>
                                        <li><a href="#affiliates-tab" data-toggle="tab">Affiliates</a></li> 
                                     <?php endif;?>

                                    {{if show_product_other_tab}}
                                        <li><a href="#extra-tab" data-toggle="tab">Other</a></li> 
                                    {{endif}}

                                                              
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane active" id="product-tab">
                                            <?php $this->load->view("admin/products/partials/{$mode}/product"); ?>
                                    </div>
                              
                                    <?php if (isset($product_extra_form)):?>
                                        <?php if ($product_extra_form != 'no'):?>
                                      
                                          <div class="tab-pane" id="product-type-fields-tab">
                                                <?php $this->load->view("admin/products/partials/{$mode}/product_extra"); ?>
                                          </div>
                                       
                                        <?php endif;?> 
                                    <?php endif;?> 


                               
                                    <div class="tab-pane" id="price-tab">

                                        <?php $this->load->view("admin/products/partials/{$mode}/price"); ?>

                                    </div>
                                  
                                    <?php foreach($module_tabs as $module) :?>
                                         
                                              <div class="tab-pane" id="<?php echo strtolower($module->namespace); ?>-tab">
                                                    <?php echo $this->load->view(strtolower($module->namespace).'/admin/products/partials/tab','',true);?>
                                              </div>
                                           
                                    <?php endforeach;  ?>


                                    <?php if(system_installed('feature_affiliates')):?>
                                       
                                          <div class="tab-pane" id="affiliates-tab">
                                              <?php $this->load->view("admin/affiliates/product_tab"); ?>
                                          </div>
                                     
                                    <?php endif;?>

                                    {{if show_product_other_tab}}
                                      
                                          <div class="tab-pane" id="extra-tab">
                                           <?php $this->load->view("admin/products/partials/{$mode}/extra"); ?>
                                          </div>
                                        
                                    {{endif}}
                                </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<!--- END-->
<script>

    /*  
     * Build the price_record line (variance)0 for the variation tab.
     *
     */
    function pr_get_str(thevariance) {

        var has_attributes = {{has_attributes}};

        if(thevariance.name =='') {
            thevariance.name = "[SET A NAME]";
        }    
        if(thevariance.sku =='') {
            thevariance.sku = "[SET A CODE]";
        }

        thevariance.sk_on_hand = 'N/A'; 

        var str  ="<tr pr-id='"+thevariance.id+"'>";
        str += "<td><a var-id='"+thevariance.id+"' class='btn btn-link as_modal' href='{{x:uri x='ADMIN'}}/product/variant/"+thevariance.id+"'>"+thevariance.id+"</a></td>";
        str += "<td> <a href='{{x:uri x='ADMIN'}}/variances/edit/"+ thevariance.id +"' class='as_modal'>"+ thevariance.name +"</a></td>";
        str += "<td> <a href='{{x:uri x='ADMIN'}}/variances/edit/"+ thevariance.id +"' class='as_modal'>"+ thevariance.sku +"</a></td>";
        str += "<td><a>"+ thevariance.sk_on_hand +"</a></td>";

        <?php if($base_amount_pricing == true ):?>
            str += "<td> <a href='{{x:uri x='ADMIN'}}/variances/price/get/"+ thevariance.id +"' class='as_modal tooltip-s' title='Price'> <?php echo nc_currency_symbol();?> "+ thevariance.base +" </a></td>";     
        <?php endif;?>
        
        str += "<td> <a href='{{x:uri x='ADMIN'}}/variances/price/get/"+ thevariance.id +"' class='as_modal tooltip-s' title='Price'> <?php echo nc_currency_symbol();?> "+ thevariance.price +" </a></td>";

        str += "<td> <a class='call_toggle_pr ' func='edit_available' href='{{x:uri x='ADMIN'}}/variances/toggle_value/"+ thevariance.id +"'>"+ thevariance.available +"</a></td>";
        str += "<td> <a class='call_toggle_pr ' func='edit_discountable' href='{{x:uri x='ADMIN'}}/variances/toggle_value/"+ thevariance.id +"'>"+ thevariance.discountable +"</a></td>";
        str += "<td> <a class='call_toggle_pr ' func='toggle_shippable' href='{{x:uri x='ADMIN'}}/variances/toggle_value/"+ thevariance.id +"'>"+ thevariance.is_shippable +"</a></td>";

        str += "<td>";
        str += "        <span style='float:right'>";
        str += "            <a class='btn btn-link as_modal' href='{{url:site}}admin/store/variances/shipping/get/"+ thevariance.id +"'><i class='fa fa-briefcase'></i></a>";

        str += "            <a href='{{x:uri x='ADMIN'}}/variances/duplicate_aj/"+ thevariance.id +"' class='copyVariantAJ tooltip-s' title='Copy'><i class='fa fa-copy'></i></a>";

        //make a dom el with a value that can be read here!
        if(has_attributes==1){
            str += "            <a href='{{x:uri x='ADMIN'}}/attributes/ajax_get/<?php echo $id;?>/"+ thevariance.id +"' class='as_modal btn btn-link tooltip-s' title='Attributes'><i class='fa fa-star'></i></a>";     
        }

        str += "            <a pr-id='"+ thevariance.id +"' href='#' class='delPriceRecord btn btn-link text-danger delete_button tooltip-s' title='Delete'>&times;</a></td>";
        str += "        </span>";           
        str += "</tr>";

        return str;     
    }



    /*
     * Add a price record(variation) to 
     * the table of variations
     */
    function addPriceRecord(obj) {

        $('table.prices_list tbody').append( formatPriceRecord( obj ) );
    }

    function updatePriceRecord(obj) {

        $('table.prices_list tbody tr[pr-id="'+obj.id+'"]').replaceWith( formatPriceRecord(obj) );
    }

    function getSymbolClass(i) {

        return (i == 1)? '<i class="call_toggle_pr fa fa-check"></i>' : '<i class="call_toggle_pr fa fa-times text-danger"></i>';  

    }

    function formatPriceRecord(obj) {

        obj.available =(obj.available == 1)? '<i class="call_toggle_pr fa fa-check"></i>' : '<i class="call_toggle_pr fa fa-times text-danger"></i>';  
        obj.discountable =(obj.discountable == 1)? getSymbolClass(1) : getSymbolClass(0);            
        obj.is_shippable =(obj.is_shippable == 1)? getSymbolClass(1) : getSymbolClass(0);     
        return pr_get_str(obj) ;
    }


    function validate(h,w,l,weight) {

        if(h<=0) return false;

        if(w<=0) return false;

        if(l<=0) return false;

        return true;
    }

    function get_Senddata() {

        /* The record to update*/
        var var_name = $("input[name='var_name']").val();
        var var_sku = $("input[name='var_sku']").val();

        return {
                name:var_name,
                sku:var_sku,
              };

    }

    function close_all_variats() {

        $('.vcal').html('<h2>Variant Details { [None Selected] } </h2><br>Please select a variation.');
    }

    $(function() {

        $(document).on('click', '.call_toggle_pr', function(event) {
            var url = $(this).attr("href");
            var _func = $(this).attr("func");

            var senddata = { dummy:'dummy', func:_func };
            $.post(url, senddata ).done(function(data)
            {
                var obj = jQuery.parseJSON(data);
                $('.VarianceContext').html(obj.message);
                if(obj.status == 'success')
                {
                    close_all_variats();
                    updatePriceRecord(obj.record);
                    tooltip_reset();
                }
            });
            event.preventDefault();
        });

        $(document).on('click', '.copyVariantAJ', function(event) {
                var url = $(this).attr('href');
                $.post( url ).done(function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    if(obj.status == 'success')
                    {
                        close_all_variats();
                        addPriceRecord(obj.record);
                        tooltip_reset();
                    }
                });
                event.preventDefault();
        }); 

        $(document).on('click', '.editVariantName', function(event) {

                var var_id = $("input[name='editvariance_id']").val();
                var var_name = $("input[name='var_name']").val();

                var url = "<?php echo NC_ADMIN_ROUTE;?>/variances/set_name/"+var_id;

                var senddata = {
                                id:var_id,
                                name:var_name,
                              };

                $.post(url, senddata ).done(function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    $('.VarianceContext').html(obj.message);
                    if(obj.status == 'success')
                    {
                        close_all_variats();
                        updatePriceRecord(obj.record);
                        tooltip_reset();
                    }
                });

                // Prevent Navigation
                event.preventDefault();
        });



        $(document).on('click', '.view_variance_button', function(event) {

                var var_id = $(this).attr('var-id');
                var url = $(this).attr('href');

                var ContentArea = '#VarianceContentArea_List';
                //clear all first
                close_all_variats();
                //$(ContentArea).html('cool');
                $(ContentArea).load(url);
                //$(ContentArea2).load(url2);

                // Prevent Navigation
                event.preventDefault();
        });


        $(document).on('click', '.clear_all_variance_data', function(event) {
                //clear all first
                close_all_variats();
                // Prevent Navigation
                event.preventDefault();
        });


        $(document).on('click', '.editPriceData', function(event) {

                var var_id = $("input[name='editvariance_id']").val();

                var new_discountable = $("select[name='discountable']").val();
                var new_price = $("input[name='price']").val();
                var new_base = $("input[name='base']").val();
                var new_rrp = $("input[name='rrp']").val();
                var min_q = $("input[name='min_qty']").val();
                
                var url = "<?php echo NC_ADMIN_ROUTE;?>/variances/price/update/"+var_id;

                var senddata = {
                                discountable:new_discountable,
                                price:new_price,
                                base:new_base,
                                rrp:new_rrp,
                                min_qty:min_q
                              };

                $.post(url, senddata ).done(function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    $('.VarianceContext').html(obj.message);

                    if(obj.status == 'success')
                    {
                        close_all_variats();
                        updatePriceRecord(obj.record);
                        tooltip_reset();
                    }

                });

                // Prevent Navigation
                event.preventDefault();
        });

        $(document).on('click', '.editShippingData', function(event) {

                var var_id = $("input[name='editvariance_id']").val();
                var select_pkg_group_id = $("select[name='pkg_group_id']").val();
                var zid = $("select[name='zone_id']").val();
                var _can_force_pack = $("select[name='can_force_pack']").val();
                var input_height = $("input[name='height']").val();
                var input_width = $("input[name='width']").val();
                var input_length = $("input[name='length']").val();
                var input_weight = $("input[name='weight']").val();

                if(validate(input_height,input_width,input_length,input_weight)===false)
                {
                    alert("There is an error with the Height, Width, Weight, please check your data.");
                    return;
                }

                var url = "<?php echo NC_ADMIN_ROUTE;?>/variances/shipping/update/"+var_id;

                var senddata = {
                                pkg_group_id:select_pkg_group_id,
                                height:input_height,
                                width:input_width,
                                length:input_length,
                                weight:input_weight,
                                zone_id:zid,
                                can_force_pack:_can_force_pack
                              };

                $.post(url, senddata ).done(function(data)
                {
                    var obj = jQuery.parseJSON(data);
                    //send message regardless of status
                    $('.VarianceContext').html(obj.message);

                    if(obj.status == 'success')
                    {
                        //no need to display/update visuals
                    }   

                });

                // Prevent Navigation
                event.preventDefault();
        });

        $(document).on('click', '.editVarianceRecord', function(event) {

                var var_id = $("input[name='editvariance_id']").val();

                /* The record to update*/
                var senddata =  get_Senddata();
                senddata.id = var_id;
                senddata.product_id = <?php echo $id;?>;

                var url = "<?php echo NC_ADMIN_ROUTE;?>/variances/edit/"+var_id;

                $.post(url, senddata ).done(function(data)
                {
                    var obj = jQuery.parseJSON(data);

                    //send message regardless of status
                    $('.VarianceContext').html(obj.message);

                    if(obj.status == 'success')
                    {
                        close_all_variats();
                        updatePriceRecord(obj.record);
                        tooltip_reset();
                    }

                });

                // Prevent Navigation
                event.preventDefault();
        });

        $(document).on('click', '.addVarianceRecord', function(event) {

                var senddata =  get_Senddata();

                var url = "<?php echo NC_ADMIN_ROUTE;?>/variances/create/<?php echo $id;?>/";

                $.post(url, senddata ).done(function(data)
                {

                    var obj = jQuery.parseJSON(data);

                    $('.VarianceContext').html(obj.message);

                    if(obj.status == 'success')
                    {
                        close_all_variats();
                        addPriceRecord(obj.record);
                        tooltip_reset();
                    }

                });

                // Prevent Navigation
                event.preventDefault();
        });

        $(document).on('click', '.delPriceRecord', function(event) {
                
                var message = "Are you sure you want to delete this.";
                var record_id = $(this).attr('pr-id');
                var item = $('table.prices_list tbody tr[pr-id="'+record_id+'"]');
                var url = "<?php echo NC_ADMIN_ROUTE;?>/variances/delete/" + record_id;

                bootbox.confirm(message, function(result) 
                {
                    if(result==true) {

                        $.post(url).done(function(data)
                        {
                            close_all_variats();
                            var obj = jQuery.parseJSON(data);

                            if(obj.status == 'success')
                            {
                                item.ncRemoveLine(3000);
                            }
                            else
                            {
                                alert(obj.status);
                            }

                            tooltip_reset();
                        });
                    }
                });


                // Prevent Navigation
                event.preventDefault();

        });
    });
</script>