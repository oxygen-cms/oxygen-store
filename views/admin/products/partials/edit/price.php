
<div class="row">

    <div class="col-md-10">



    <?php if( $this->config->item('admin/product/show_infobar') ) : ?> 
       
    <?php endif;?>

			<a 
				class="btn btn-flat bg-olive as_modal" 
				href="{{x:uri x='ADMIN'}}/variances/create/<?php echo $id;?>">
				Add variance
			</a>

			<br>
			<br>

			<table class="prices_list table table-condensed">
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>SKU</th>
					<th>On Hand</th>
					{{if base_amount_pricing == true }}
						 <th>Base</th>
					{{endif}}
					<th>Price</th>
					<th class='tooltip-s' title='Click to change'>Active</th>
					<th class='tooltip-s' title='Click to change'>Discountable</th>
					<th class='tooltip-s' title='Click to change'>Shippable</th>
					<th class=''>Actions</th>
				</tr>

				<?php foreach($prices AS $price_record) : ?>
					<tr pr-id="<?php echo $price_record->id; ?>">
						<td>
							<!--a var-id="<?php echo $price_record->id; ?>" class='button view_variance_button'  href="{{x:uri x='ADMIN'}}/product/variant/<?php echo $price_record->id;?>"><?php echo $price_record->id; ?></a-->
							<a var-id="<?php echo $price_record->id; ?>" class='button as_modal'  href="{{x:uri x='ADMIN'}}/product/variant/<?php echo $price_record->id;?>"><?php echo $price_record->id; ?></a>
						</td>	
						<td>
							<?php $price_record->name = (trim($price_record->name)=='')? '[SET A NAME]' :$price_record->name ; ?>
							<?php echo "<a href='{{x:uri x='ADMIN'}}/variances/edit/{$price_record->id}' class='as_modal'>{$price_record->name}</a>";?>
						</td>												
						<td>
							<?php $price_record->sku = (trim($price_record->sku)=='')? '[SET A CODE]' :$price_record->sku ; ?>
							<?php $price_record->sk_on_hand =  'N/A'; ?>
							<?php echo "<a href='{{x:uri x='ADMIN'}}/variances/edit/{$price_record->id}' class='as_modal'>{$price_record->sku}</a>";?>
						</td>		
						<td><?php echo "<a>{$price_record->sk_on_hand}</a></td>";?>
						{{if base_amount_pricing == true }}
							<td><?php echo "<a href='{{x:uri x='ADMIN'}}/variances/price/get/{$price_record->id}' class='as_modal'>".nc_currency_symbol()." {$price_record->base}</a></td>";?>			
						{{endif}}

						<td><?php echo "<a href='{{x:uri x='ADMIN'}}/variances/price/get/{$price_record->id}' class='as_modal'>".nc_currency_symbol()." {$price_record->price}</a></td>";?>
						
						<td>																																	
							<a class='call_toggle_pr ' func='edit_available' href="{{x:uri x='ADMIN'}}/variances/toggle_value/<?php echo $price_record->id;?>"><?php echo yesNoBOOL($price_record->available,'string','<i class="call_toggle_pr fa fa-check"></i>','<i class="call_toggle_pr fa fa-times text-danger"></i>'); ?></a>
						</td>
						<td>
							<a class='call_toggle_pr ' func='edit_discountable' href="{{x:uri x='ADMIN'}}/variances/toggle_value/<?php echo $price_record->id;?>"><?php echo yesNoBOOL($price_record->discountable,'string','<i class="call_toggle_pr fa fa-check"></i>','<i class="call_toggle_pr fa fa-times text-danger"></i>'); ?></a>
						</td>	
						<td>
							<a class='call_toggle_pr ' func='toggle_shippable' href="{{x:uri x='ADMIN'}}/variances/toggle_value/<?php echo $price_record->id;?>"><?php echo yesNoBOOL($price_record->is_shippable,'string','<i class="call_toggle_pr fa fa-check"></i>','<i class="call_toggle_pr fa fa-times text-danger"></i>'); ?></a>
						</td>														
						<td>
							<span style='float:right'>
								<a class='as_modal btn btn-link' href='{{url:site}}admin/store/variances/shipping/get/<?php echo $price_record->id;?>'><i class='fa fa-briefcase'></i></a>
								<a href="{{x:uri x='ADMIN'}}/variances/duplicate_aj/<?php echo $price_record->id;?>" class='copyVariantAJ tooltip-s btn btn-link' title='Copy'><i class='fa fa-copy'></i></a>	

								{{if has_attributes}}
									<a href="{{x:uri x='ADMIN'}}/attributes/ajax_get/{{id}}/<?php echo $price_record->id;?>" class='btn btn-link as_modal tooltip-s' title='Attributes'><i class='fa fa-star'></i></a>								
								{{endif}}
							
								<a pr-id='<?php echo $price_record->id;?>' href="#" class='delPriceRecord btn btn-link text-danger delete_button tooltip-s' title='Delete'>&times;</i></a>
							</span>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		
    
    </div>

</div>