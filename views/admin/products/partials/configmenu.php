<?php if($deleted == NULL):?>
      <li class="pull-right dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-gear"></i>
            </a>
            <ul class="dropdown-menu">
				<?php if($this->method=='edit'):?>
					<li class=''><a class="" href="{{x:uri x='ADMIN'}}/product/view/{{id}}">Switch to View Mode</a></li>
					<li class='text-danger'><a class="confirm text-danger" href="{{x:uri x='ADMIN'}}/product/delete/{{id}}">Delete</a></li>
				<?php else:?>
					<li class=''><a class="" href="{{x:uri x='ADMIN'}}/product/edit/{{id}}">Switch to Edit Mode</a></li>
				<?php endif;?>	
            </ul>
     </li>
<?php endif;?> 						   