
<div class="row">

    <div class="col-xs-12">
        
        <div class="box box-solid">
				
				<fieldset id="filters" style="">
					<?php echo form_open(NC_ADMIN_ROUTE.'/products/callback'); ?>
		                <div class="box-header">
					            <h3 class="box-title">
					              	<?php echo lang('store:products:products');?>
					            </h3>
								<div class="box-tools">
				                    <div class="input-group">
					                      <?php echo form_input('f_keyword_search',  $f_keyword_search, 'style="width: 150px;" class="form-control  input-sm pull-right" placeholder="Search" style=""'); ?>
					                      <div class="input-group-btn">
					                        <a class="btn btn-sm btn-default filterToggleBtn">
					                        	<i class="fa fa-filter"></i>
					                        </a>
					                      </div>
				                    </div>
				                </div>
					            <?php $this->load->view('admin/products/filter'); ?>
		                </div><!-- /.box-header -->
	                <?php echo form_close(); ?>	
                </fieldset>

                <div class="box-body">

					<?php if ($products) : ?>

						<?php echo form_open(NC_ADMIN_ROUTE.'/products/action'); ?>

							<table class="table">
              					<thead>
									<tr>
										<th class=""><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')); ?></th>
										<th class=""><?php echo lang('store:products:id');?></th>
										<th class=""><?php echo lang('store:products:image');?></th>
										<th class=""><?php echo lang('store:products:name');?></th>
										<?php 
										if ($this->config->item('admin/show_products_views_field'))
										{
											echo '<th>Views</th>';
										}
										?>	
										<?php 
										if ($this->config->item('admin/show_products_featured_field'))
										{
											echo "<th class=''><span class='tooltip-s'  title='Is this product featured ?'>Featured</span></th>"; 
										}
										?>
										<th class=""><span class='tooltip-s' title='Can users find this product in search results ?'><?php echo lang('store:products:searchable');?></span></th>
										<th class=""><span class='tooltip-s' title='Is this product currently available on the site ?'>Enabled</span></th>
										<th class=""><span class='tooltip-s' title='Is the product active or deleted from history ?'></span></th>
										<th></th>
									</tr>
								</thead>
								<tbody id="filter-stage">
								</tbody>
							</table>

						<?php echo form_close(); ?>
					<?php else : ?>
						
							No results found here, try a different filter or create a new product.
				
					<?php endif; ?>
				</div>

		</div>

	</div>
</div>