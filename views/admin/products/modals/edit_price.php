
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
			   	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title">
	              		Edit Price
	            </h4>
            </div>
            <div class="modal-body">

				<div class="content VarianceContext">
						<input type='hidden' name='editvariance_id' value='<?php echo $variance->id;?>'>
						<fieldset>

									<div class="input">

										<h4>Pricing</h4>

										<table>

											<tr>
												<td>
													<label for="">Discountable <span></span><br></label>
													<?php echo form_dropdown('discountable',array(0=>'Not Allowed',1=>'Allowed'),set_value('discountable', (isset($variance))?$variance->discountable:NULL)); ?>
												</td>
												<td>
													Min Cart QTY<br>
													<?php echo form_input('min_qty', (isset($variance))?$variance->min_qty:0, "placeholder='0'"); ?>								
												</td>
											</tr>

											<tr>									
												<td>
													<label for="">Price <?php echo nc_currency_symbol();?> <span>required</span><br></label>
													<?php echo form_input('price', (isset($variance))?$variance->price:NULL, "placeholder='0.00'"); ?>
												</td>
												<td>
												</td>									
											</tr>
											<tr>
												<td>
													<label for="rrp">RRP (Recomended retail Price) <?php echo nc_currency_symbol();?> <span></span><br></label>
													<?php echo form_input('rrp', (isset($variance))?$variance->rrp:NULL, "placeholder='0.00'"); ?>
												</td>
												<td>
												</td>									
											</tr>

											<tr>									
												<td>
													{{if base_amount_pricing }}										
														<label for="">Base Amount <?php echo nc_currency_symbol();?> <span>Single fee</span><br></label>
														<?php echo form_input('base', (isset($variance))?$variance->base:NULL, "placeholder='0.00'"); ?>
													{{else}}
														<input type='hidden' name='base' value='0'>
													{{endif}}
												</td>
												<td>

												</td>									
											</tr>
										</table>

									</div>
						</fieldset>
				</div>			
                
            </div>

            <div class="modal-footer">
            		<a class="editPriceData btn btn-flat btn-primary" data-dismiss="modal" style='cursor:pointer'>Save/Confirm</a>
            </div>

        </div>
      </div> 














			