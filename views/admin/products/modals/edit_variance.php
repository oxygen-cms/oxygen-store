

<!-- Bootstrap Modal content-->
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              	<h4 class="modal-title">
            	  <?php echo lang('store:products:edit_variance');?>
          		</h4>
            </div>
            <div class="modal-body">
            		<div class="content VarianceContext">
						<input type='hidden' name='editvariance_id' value='<?php echo $variance->id;?>'>
						<table class='table'>
							<tr>
								<td>
									<label for=""><?php echo lang('store:products:variation_name');?><br></label>
									<?php echo form_input('var_name',(isset($variance))?$variance->name:'Standard'); ?>
								</td>
								<td>
									<label for="">SKU Code<br></label>
									<?php echo form_input('var_sku',(isset($variance))?$variance->sku:''); ?>

									<div style='display:none'>
										<label for=""><?php echo lang('store:products:active');?> <span></span><br></label>
										<?php echo form_dropdown('available',array(0=>'Disabled',1=>'Enabled'),(isset($variance))?$variance->available:1); ?>
									</div>
								</td>									
							</tr>
						</table>					
					</div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-flat btn-default pull-left" data-dismiss="modal">Cancel</button>
              <button type="button" class="editVarianceRecord btn btn-flat bg-blue btn-default pull-left" data-dismiss="modal">Save</button>
            </div>
        </div>
      </div>   