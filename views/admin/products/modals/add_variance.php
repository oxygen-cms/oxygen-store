
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
			   	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="modal-title">
	              		Add Variance
	            </h4>
            </div>
            <div class="modal-body">

					<div class="content VarianceContext">
								<fieldset>
										<div class="input">
											<h4><?php echo lang('store:products:add_variance');?></h4>
											<table>
												<tr>
													<td>
														<label for=""><?php echo lang('store:products:variation_name');?><br></label>
														<?php echo form_input('var_name',(isset($variance))?$variance->name:'Standard'); ?>
													</td>
													<td>
														<label for="">SKU Code<br></label>
														<?php echo form_input('var_sku',(isset($variance))?$variance->sku:''); ?>

														<div style='display:none'>
															<label for=""><?php echo lang('store:products:active');?> <span></span><br></label>
															<?php echo form_dropdown('available',array(0=>'Disabled',1=>'Enabled'),(isset($variance))?$variance->available:1); ?>
														</div>
													</td>									
												</tr>
											</table>

										</div>

										<div class="input">
										</div>

								</fieldset>
					</div>

						
                
            </div>

            <div class="modal-footer">


            		<button type="button" class="btn btn-flat btn-default" data-dismiss="modal" aria-label="Close">Cancel</button>

            		<a class="addVarianceRecord btn btn-flat btn-primary" data-dismiss="modal" style='cursor:pointer'><?php echo lang('store:products:add');?></a>
			
              
            </div>

        </div>
      </div> 







