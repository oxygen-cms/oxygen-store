
<!-- Bootstrap Modal content-->
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              	<h4 class="modal-title">
            	  <?php echo lang('store:products:edit_variance');?>
          		</h4>
            </div>
            <div class="modal-body">
            		<div class="content VarianceContext">
						<input type='hidden' name='editvariance_id' value='<?php echo $variance->id;?>'>

							<table class='table'>
								<tr>
									<td>
										<label for="">Package Class/Group <span>*</span></label><br>
										<?php echo form_dropdown('pkg_group_id', $available_groups,set_value('pkg_group_id',(isset($variance))?$variance->pkg_group_id:NULL) ); ?>
									</td>
									<td>
										<label for="">Shipping Zone <span></span></label><br>
										<?php echo form_dropdown('zone_id', array(0=>'Default') + $shipping_zones ,set_value('zone_id',(isset($variance))?$variance->zone_id:$default_id) ); ?>
									</td>
								</tr>
								<tr>
									<td>
										<label for="rrp">Height <span></span></label><br>
										<?php echo form_input('height', (isset($variance))?$variance->height:0.0, "placeholder='0.0'"); ?> cm
									</td>
									<td>
										<label for="rrp">Weight <span></span></label><br>
										<?php echo form_input('weight', (isset($variance))?$variance->weight:0.0, "placeholder='0.0'"); ?> Kg
									</td>
								</tr>
								<tr>
									<td>
										<label for="rrp">Width <span></span></label><br>
										<?php echo form_input('width', (isset($variance))?$variance->width:0.0, "placeholder='0.0'"); ?> cm
									</td>
									<td>
										<label for="rrp">Force Pack ? <span></span></label><br>
										<?php echo form_dropdown('can_force_pack', array(0=>'No',1=>'Yes') ,set_value('can_force_pack',(isset($variance))?$variance->can_force_pack:0) ); ?>										
									</td>									
								</tr>
								<tr>
									<td>
										<label for="rrp">Length(Depth or Thickness)<span></span><br></label>
										<?php echo form_input('length', (isset($variance))?$variance->length:0.0, "placeholder='0.0'"); ?> cm
									</td>
									<td>
									</td>
								</tr>
							</table>					
					</div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-flat btn-default pull-left" data-dismiss="modal">Cancel</button>
              <button type="button" class="editShippingData btn btn-flat bg-blue btn-default pull-left" data-dismiss="modal">Save</button>
            </div>
        </div>
      </div>   
