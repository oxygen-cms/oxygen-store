
<!-- Bootstrap Modal content-->
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            		<div class="content VarianceContext">
						<input type='hidden' name='editvariance_id' value='<?php echo $variance->id;?>'>
						<label for=""><?php echo lang('store:products:variation_name');?></span><br></label>
						<?php echo form_input('var_name',(isset($variance))?$variance->name:'Standard'); ?>						
					</div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-flat btn-default pull-left" data-dismiss="modal">Cancel</button>
              <button type="button" class="editVariantName btn btn-flat bg-blue btn-default pull-left" data-dismiss="modal">Save</button>
            </div>
        </div>
      </div>   