<div class="row">

    <div class="col-xs-12">

        <div class="box box-primary">

                <div class="box-header">
	                  <h3 class="box-title">
							Attributes
	                  </h3>
                </div><!-- /.box-header -->

                <div class="box-body">

						<table class='table'>
							<thead>
								<tr>
									<th><input type="checkbox" name="action_to_all" value="" class="check-all" /></th>
									<th>ID</th>
									<th>Name</th>
									<th style="width: 120px"></th>
								</tr>
							</thead>
							<tbody>
								<?php if (count($attributes ) >0): ?>
								<?php
									foreach ($attributes AS $attribute): ?>
												<tr class="<?php echo alternator('even', ''); ?>">
													<td><input type="checkbox" name="action_to[]" value="<?php echo $attribute->id; ?>"  /></td>
													<td><?php echo $attribute->id; ?></td>
													<td><?php echo $attribute->name; ?></td>
													<td>
														<span style="float:right;">
																	<?php
																		$items = array();
																		$items[] = dropdownMenuStandard(site_url('admin/store/attributes/edit/' . $attribute->id), FALSE, lang('store:admin:edit'), FALSE, "edit");
																		$items[] = dropdownMenuStandard(site_url('admin/store/attributes/delete/' . $attribute->id), 	TRUE,  lang('store:admin:delete'), TRUE, "minus");
																		echo dropdownMenuListSplit($items,'Edit',site_url('admin/store/attributes/edit/' . $attribute->id));
																	?>
														</span>
													</td>
												</tr>
								<?php endforeach; ?>
								<?php else: ?>
										<tr>
											<td colspan="4">
											No recoreds found
											</td>
										</tr>
								<?php endif; ?>

							</tbody>

						</table>
				</div>

                <div class="box-footer">
	                  <?php $this->load->view('admin/partials/pagination'); ?>
                </div><!-- /.box-header -->

		</div>
	</div>
</div>



