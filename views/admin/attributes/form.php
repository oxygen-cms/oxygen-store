<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
<?php 
if(isset($id))
{
	echo form_hidden('id', set_value('id', $id)); 
}
?>
<?php  $label =  (isset($id)) ? 'Save' : 'Create' ; ?>

<div class="row">

    <div class="col-xs-12">

        <div class="box box-primary">

                <div class="box-header">
	                  <h3 class="box-title">
							Create/Edit An Attribute
	                  </h3>
                </div><!-- /.box-header -->

                <div class="box-body">

						<fieldset>

								<label for="name">Name<span>*</span>
									<small></small>
								</label>
								<div class="input" ><?php echo form_input('name', set_value('name', $name)); ?></div>

						</fieldset>
				</div>


				<div class="box-footer">
					<button class="btn btn-flat btn-sm bg-blue" value="save" name="btnAction" type="submit"><?php echo $label;?></button>
					<button class="btn btn-flat btn-sm bg-blue" value="save_create" name="btnAction" type="submit"><?php  echo $label;?> &amp; Add Another</button>
					<a class="btn btn-flat btn-sm bg-gray cancel" href="admin/store/attributes">Cancel</a>
				</div>	

			</div>
		</div>
	</div>


<?php echo form_close(); ?>
