<?php 
	$paid_status = ($order->order->paid_date == NULL)? 'unpaid' : 'paid';
	$status_color = Helper_order_status_color($order->order->status_id); 
	$paid_color = Helper_order_paid_status_color($paid_status);
?>

<div class='row'>
		<div class="col-sm-12">
		  	<div class="box box-solid">
			    <div class="box-body">
				              <!-- Custom Tabs -->
				              <div class="nav-tabs-custom">
				                <ul class="nav nav-tabs">
				                  <?php $this->load->view('admin/orders/partials/tabs_min'); ?>
				                </ul>
				                <div class="tab-content">

									 	<div id="order-tab" class="tab-pane active">
											<?php $this->load->view('admin/orders/partials/details'); ?>
						                </div><!-- /.tab-pane -->

										<!-- always display billing-->
										<div id="billing-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/billing'); ?>
										</div>
										<?php if($order->shipping_address_id != $order->billing_address_id AND $order->shipping_address_id != NULL):?>
											<div id="delivery-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/shipping'); ?>
											</div>
										<?php endif;?>


										<?php if( $this->config->item('admin/orders/show_items_tab') ) : ?> 
											<div id="contents-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/items'); ?>
											</div>
										<?php endif;?>


										<?php if( $this->config->item('admin/orders/show_txn_tab') ) : ?> 
											<div id="transactions-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/transactions'); ?>
											</div>
										<?php endif;?>

										
			
				              	</div>
				        </div>
				</div> 
			</div>

		</div>	

</div>	