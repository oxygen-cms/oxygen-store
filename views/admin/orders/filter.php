<div class="box box-solid">

	<div class="box-header">
		<h3 class="box-title">
			Filter
		</h3>
	</div>

	<div class="box-body">

		<ul class='form-list'>
			<li>
				<label>
					Filter Type
				</label>
				
					<?php echo form_dropdown('f_filter', $filters , $namespace ,"class='form-control'"); ?>
				
			</li>																	
		</ul>	
		<ul class='form-list'>
			<li>
				<label>
					Status
				</label>
				<div class="input">
					<?php echo form_dropdown('f_status',  array('all'=> lang('global:select-all'),'active'=> 'Active','deleted'=> 'Deleted'), $f_status,"class='form-control'" ); ?>
				</div>
			</li>
			<li>
				<label>
					Payment Status
				</label>
				<div class="input">
					<?php echo form_dropdown('f_payment_status', array( 'all'=> lang('global:select-all'),'paid'  => 'Paid','unpaid' => 'UnPaid', ),$f_payment_status ,"class='form-control'"); ?>
				</div>
			</li>	
			<li>
				<label>
					Progress Status
				</label>
				<div class="input">
					<?php echo form_dropdown('f_order_status',$order_workflows,$f_order_status,"class='form-control'");?>
				</div>
			</li>	
		</ul>
	</div>
</div>

<div class="box box-solid">

	<div class="box-header">
		<h3 class="box-title">
			Display
		</h3>
	</div>

	<div class="box-body">

		<ul class='form-list'>				
			<li>
				<label>
					By
				</label>
				<div class="input">
							<?php echo form_dropdown('f_order_by',  
								array(
									'id'=> 'ID',
									'user_id'=> 'Group by User',
									'status'=> 'Status',
									'updated'=> 'Order Actioned',
									'created'=> 'Date Placed',
									'paid_date'=> 'Date Paid',
									'total_totals' => 'Total Order value',
									'total_shipping' => 'Total Shipping value',
									'total_discount' => 'Total Discounted',
									'count_items' => 'Total Items Purchased',
								),$f_order_by,"class='form-control'" ); ?>
				</div>
			</li>
			<li>
				<label>
					Direction
				</label>
				<div class="input">
						<?php echo form_dropdown('f_order_by_dir',  array('desc'=> 'Descending','asc'=> 'Ascending',),$f_order_by_dir,"class='form-control'" ); ?>
				</div>
			</li>	
			<li>
				<label>
					Items per Page
				</label>
				<div class="input">
					<?php echo form_dropdown('f_display_count', array( 5  => '5', 10 => '10',	 20 => '20', 50 => '50', 100 => '100', 150 => '150', 200 => '200', ),$f_display_count,"class='form-control'" ); ?>
				</div>
			</li>												
		</ul>	

	</div>
</div>