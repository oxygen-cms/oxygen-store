
	<fieldset>
				<table class='table'>
					<thead>
						<tr>
							<th><?php echo lang('store:orders:image'); ?></th>
							<th><?php echo lang('store:orders:item'); ?></th>
							<th>Variation</th>
							<th><?php echo lang('store:orders:qty'); ?></th>
						</tr>
					</thead>
					<tbody>
							{{order.contents}}
							<tr>
								<td rowspan="2">
					                {{store_gallery:cover product_id="{{id}}" x="" }}
					                        <img src="{{ url:site }}files/thumb/{{file_id}}/120"  alt="{{alt}}" />
					                {{/store_gallery:cover }}
								</td>
								<td>
									<a target='new' class='' href='{{x:uri x='ADMIN'}}/product/view/{{id}}'><i style='color:#f00'  class='icon-external-link'></i> {{name}} </a>
								</td>
								<td>
								
									<a target='new'class='as_modal' href='{{x:uri x="ADMIN"}}/product/variant/{{variant_id}}'> 
										{{products:variant id=variant_id }} 
									</a>							
								</td>
								<td>{{qty}}</td>
							</tr>
							<tr>
								<td colspan="8">{{options}}</td>
							</tr>
						{{/order.contents}}
					</tbody>
				</table>
			</fieldset>