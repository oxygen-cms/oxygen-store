<script>
	console.log('loading billing tab');
</script>

			<fieldset>
				<table class='table'>
					<tr>
						<td><?php echo lang('store:orders:email'); ?></td>
						<td><?php echo $order->invoice->email; ?></td>
					</tr>
					<tr>
						<td><?php echo lang('store:orders:first_name'); ?></td>
						<td><?php echo $order->invoice->first_name; ?></td>
					</tr>
					<tr>
						<td><?php echo lang('store:orders:last_name'); ?></td>
						<td><?php echo $order->invoice->last_name; ?></td>
					</tr>
					<?php if ($order->invoice->company != ""): ?>
					<tr>
						<td><?php echo lang('store:orders:company'); ?></td>
						<td><?php echo $order->invoice->company; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><?php echo lang('store:orders:address'); ?></td>
						<td><?php echo $order->invoice->address1; ?>,<?php echo $order->invoice->address2; ?></td>
					</tr>
					<tr>
						<td><?php echo lang('store:orders:city'); ?></td>
						<td><?php echo $order->invoice->city; ?></td>
					</tr>
					<?php if ($order->invoice->state != ""): ?>
					<tr>
						<td><?php echo lang('store:orders:state'); ?></td>
						<td><?php echo $order->invoice->state; ?></td>
					</tr>
					<?php endif; ?>
					<?php if ($order->invoice->country != ""): ?>
					<tr>
						<td><?php echo lang('store:orders:country'); ?></td>
						<td><?php echo $order->invoice->country; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><?php echo lang('store:orders:zip'); ?></td>
						<td><?php echo $order->invoice->zip; ?></td>
					</tr>			
					<tr>
						<td><?php echo lang('store:orders:phone'); ?></td>
						<td><?php echo $order->invoice->phone; ?></td>
					</tr>	
					<tr>
						<td>Instruction</td>
						<td><?php echo $order->invoice->instruction; ?></td>
					</tr>						
				</table>
			</fieldset>


			<?php if($show_actions) : ?>
				<?php if ($order->customer->id > 0): ?>
				<a class='btn btn-block btn-flat bg-orange' href='{{x:uri x='ADMIN'}}/customers/addresses/{{order.order.user_id}}'>View Other Addresses</a>
				<?php endif;?>
			<?php endif;?>

