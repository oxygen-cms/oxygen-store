<script>
	console.log('loading details tab');
</script>
<style>
	table.darker td
	{
		color:#555;
	}
</style>

<fieldset>
		<table class='darker table'>
			<tbody>
				<tr>
					<td colspan='2'>
						<h3><?php echo lang('store:orders:identification'); ?></h3>
					</td>
				</tr>
				<tr>
					<td>Order #</td>
					<td>{{order.order.id}}</td>
				</tr>
				<tr>
					<?php if ($order->customer->id > 0): ?>
						<td>
							<?php echo lang('store:orders:customer'); ?>:
						</td>							
						<td>
							<?php echo anchor('user/' . $order->customer->id, $order->customer->display_name, array('class'=>'btn btn-sm btn-flat btn-default')); ?>

							| <?php echo anchor('admin/users/edit/' . $order->customer->id, 'Edit', array('class'=>'btn btn-link')); ?>
						</td>
					<?php else: ?>
						<td>
							Bill-to Name:
						</td>							
						<td>
							<?php echo $order->invoice->first_name . ' ' . $order->invoice->last_name.' ['.$order->invoice->email.']'; ?> (Not registered)
						</td>
					<?php endif; ?>
				</tr>
				<tr>
					<td>
						User Group
					</td>
					<td>
						<?php if ($order->customer->id > 0): ?>
							<?php echo user_displaygroup($order->customer->user_id); ?>
						<?php else: ?>
							GUEST (Not registered) 
						<?php endif;?>
					</td>
				</tr>					
				<tr>
					<td>
						<?php echo lang('store:orders:ip_address'); ?>
					</td>
					<td>
						 <strong>{{order.order.ip_address}}</strong>
					</td>
				</tr>



				<?php // Start $this->config->item('admin/orders/show_totals_on_details')  ?>
				<?php if( $this->config->item('admin/orders/show_totals_on_details') ) : ?> 
						<tr>
							<td colspan='2'>
								<h3>Totals</h3>
							</td>
						</tr>

								<?php if( $this->config->item('admin/orders/show_points') ) : ?> 
								<tr>
									<td>
										User accumulated points
									</td>
									<td>
										<span style='color:#447'><?php echo $order->order->total_points; ?> (PTS)</span>
									</td>
								</tr>
								<?php endif;?>
						<tr>
							<td>
								<?php echo lang('store:orders:shipping_amount'); ?>
							</td>
							<td>
								<span style='font-family:courier'><?php echo nc_format_price($order->order->total_shipping); ?></span>
							</td>
						</tr>					
						<tr>
							<td>
								Total <?php echo lang('store:orders:discounts'); ?></label>
							</td>
							<td>
								<span style='font-family:courier'><?php echo nc_format_price($order->order->total_discount); ?></span>
							</td>
						</tr>
						<tr>
							<td>
								<?php echo lang('store:orders:total_tax'); ?>
							</td>
							<td>
								<span style='font-family:courier'><?php echo nc_format_price($order->order->total_tax ); ?></span>
							</td>
						</tr>

						<tr>
							<td>
								Order Subtotal (tax excl)
							</td>
							<td>
								<span style='font-family:courier'><?php echo nc_format_price( $order->order->total_subtotal ); ?></span>							
							</td>
						</tr>
						<tr>
							<td>
								Order total (tax inc)
							</td>
							<td>
								<strong style='font-family:courier'><?php echo nc_format_price($order->order->total_totals); ?></strong>
							</td>
						</tr>
				<?php endif;?>
				<?php // End $this->config->item('admin/orders/show_totals_on_details')  ?>



				<?php if( $this->config->item('admin/order/details/show_checkoutmethod') ) : ?>
					<tr>
						<td colspan='2'>
							<h3><?php echo lang('store:orders:pmt_shipping'); ?></h3>
						</td>
					</tr>
					<tr>
						<td>
							<?php echo lang('store:orders:date_order_placed'); ?>
						</td>
						<td>
							 <strong> <?php echo date('d / M / Y ', $order->order->order_date); ?> </strong> @ <?php echo date('H:i:s',$order->order->order_date) ;?> <small><em>{ <?php echo timespan($order->order->order_date); ?> <?php echo lang('store:orders:ago'); ?> }</em></small>
						</td>
					</tr>
						<?php if(isset( $order->paid_date)) : ?>
						<tr>
							<td>
								Paid Date
							</td>
							<td>
								 <strong> <?php echo date('d / M / Y ', $order->order->paid_date); ?> </strong> @ <?php echo date('H:i:s',$order->paid_date) ;?> <small><em>{ <?php echo timespan($order->paid_date); ?> <?php echo lang('store:orders:ago'); ?> }</em></small>
							</td>
						</tr>
						<?php endif; ?>
					<tr>
						<td>
							<?php echo lang('store:orders:payment_method'); ?>
						</td>
						<td>
							{{order.payments.link}}
						</td>
					</tr>
					<tr>
						<td>
							<?php echo lang('store:orders:shipping_options'); ?> 
						</td>
						<td>
							{{order.shipping_method.link}}
						</td>
					</tr>
				<?php endif;?>					
			</tbody>
		</table>
</fieldset>