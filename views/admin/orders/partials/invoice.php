<style>

.invoice_table *:not(i):not(span) {font-family:courier;}
.invoice_table thead * {text-decoration: underline;color:#333;}
.invoice_table tbody * {color:#333;}
.invoice_table tfoot * {font-weight:bold;color:#000;}
</style>


	<div class='table-responsive'>
		<table class='table invoice_table'>
			<thead>
				<tr>
					<td>Title</td>
					<td>QTY</td>
					<td>Original Price</td>
					<td>Price</td>
					{{if base_amount_pricing === true }}
						<td>Base</td>
					{{ endif }}
					<td>Tax</td>				
					<td>Discount</td>
					<td>SubTotal</td>
					<td>Total (Tax Inc)</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach($order->invoice_items as $inv_item):?>
					<tr>
						<td>
							<?php echo $inv_item->title;?>
						</td>
						<td><?php echo $inv_item->qty;?></td>
						<td>{{store:currency}} <?php echo $inv_item->orprice;?></td>
						<td>{{store:currency}} <?php echo $inv_item->price;?></td>
						{{if base_amount_pricing === true }}
							<td>{{store:currency}} <?php echo $inv_item->base;?></td>
						{{ endif }}					
						<td>{{store:currency}} <?php echo $inv_item->tax;?></td>
						<td class='tooltip-s' title='<?php echo $inv_item->discount_message;?>'>
							<?php if($inv_item->discount > 0):?>
								<i class='fa fa-hand-o-down'> </i>
								<span>
									<?php echo $inv_item->discount;?>
								</span>
							<?php else:?>
							--
							<?php endif;?>
						</td>	
						<td>{{store:currency}} <?php echo $inv_item->subtotal;?></td>
						<td>{{store:currency}} <?php echo $inv_item->total;?></td>
					</tr>
				<?php endforeach;?>
				<!-- totals row-->
			</tbody>
			<tfoot style='font-weight:bold'>
					<tr>
						<td>TOTALS</td>
						<td></td>
						<td></td>
						<td></td>
						{{if base_amount_pricing === true }}
							<td></td>
						{{ endif }}						
						<td><?php echo nc_format_price( $order->order->total_tax ); ?></td>
						<td><?php echo nc_format_price( $order->order->total_discount); ?></td>
						<td><?php echo nc_format_price( $order->order->total_subtotal); ?></td>
						<td><?php echo nc_format_price( $order->order->total_totals); ?></td>
					</tr>
			</tfoot>
		</table>
	</div>


	<?php if($show_actions) : ?>
		<?php if($order->order->deleted==NULL):?>
			<a class='btn btn-block bg-orange confirm' href='{{x:uri x='ADMIN'}}/orders/reinvoice/{{order.order.id}}' title='Are you sure ?'>Send Invoice</a>
		<?php endif;?>
	<?php endif;?>
