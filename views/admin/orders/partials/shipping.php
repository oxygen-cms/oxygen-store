
			<fieldset>
				<table class='table'>
					<tr>
						<td><?php echo lang('store:orders:email'); ?></td>
						<td><?php echo $shipping_address->email; ?></td>
					</tr>
					<tr>
						<td><?php echo lang('store:orders:first_name'); ?></td>
						<td><?php echo $shipping_address->first_name; ?></td>
					</tr>
					<tr>
						<td><?php echo lang('store:orders:last_name'); ?></td>
						<td><?php echo $shipping_address->last_name; ?></td>
					</tr>
					<?php if ($shipping_address->company != ""): ?>
					<tr>
						<td><?php echo lang('store:orders:company'); ?></td>
						<td><?php echo $shipping_address->company; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><?php echo lang('store:orders:address'); ?></td>
						<td><?php echo $shipping_address->address1; ?>,<?php echo $shipping_address->address2; ?></td>
					</tr>
					<tr>
						<td><?php echo lang('store:orders:city'); ?></td>
						<td><?php echo $shipping_address->city; ?></td>
					</tr>
					<?php if ($shipping_address->state != ""): ?>
					<tr>
						<td><?php echo lang('store:orders:state'); ?></td>
						<td><?php echo $shipping_address->state; ?></td>
					</tr>
					<?php endif; ?>
					<?php if ($shipping_address->country != ""): ?>
					<tr>
						<td><?php echo lang('store:orders:country'); ?></td>
						<td><?php echo $shipping_address->country; ?></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><?php echo lang('store:orders:zip'); ?></td>
						<td><?php echo $shipping_address->zip; ?></td>
					</tr>			
					<tr>
						<td><?php echo lang('store:orders:phone'); ?></td>
						<td><?php echo $shipping_address->phone; ?></td>
					</tr>	
					<tr>
						<td>Instruction</td>
						<td><?php echo $order->invoice->instruction; ?></td>
					</tr>						
				</table>
			</fieldset>	

			<?php if($show_actions) : ?>
				<a class='btn btn-block btn-flat bg-orange' href='{{x:uri x='ADMIN'}}/customers/addresses/{{order.order.user_id}}'>View Other Addresses</a>
			<?php endif;?>
			