

			<li class="active"><a data-toggle="tab" href="#order-tab"><i class="fa fa-user"></i> <?php echo lang('store:orders:details'); ?></a></li>

			<?php if($order->shipping_address_id == $order->billing_address_id):?>
				<li class=""><a data-toggle="tab" href="#billing-tab"><i class="fa fa-map-marker"></i> Billing + Shipping Address</a></li>
			<?php else:?>
				<li class=""><a data-toggle="tab" href="#billing-tab"><i class="fa fa-map-marker"></i> Billing Address</a></li>
				<?php if($order->shipping_address_id !== NULL):?>
					<li class=""><a data-toggle="tab" href="#delivery-tab"><i class="fa fa-map-marker"></i> Shipping Address</a></li>
				<?php endif;?>
			<?php endif;?>


			
			<?php if( $this->config->item('admin/orders/show_items_tab') ) : ?> 
				<li class=""><a data-toggle="tab" href="#contents-tab"><i class="fa fa-dropbox"></i> <?php echo lang('store:orders:items'); ?></a></li>
			<?php endif;?>



            <li class=""><a data-toggle="tab" href="#invoice-tab"><i class="fa fa-list-alt"></i> Invoice</a></li>
			

			<?php if( $this->config->item('admin/orders/show_txn_tab') ) : ?> 
				<li class=""><a data-toggle="tab" href="#transactions-tab"><i class="fa fa-list-alt"></i> <?php echo lang('store:orders:transactions'); ?></a></li>
			<?php endif;?>
 

			<?php if( $this->config->item('admin/orders/show_notes_tab') ) : ?> 
				<li class=""><a data-toggle="tab" href="#notes-tab"><i class="fa fa-comment-o"></i> <?php echo lang('store:orders:notes'); ?></a></li>
			<?php endif;?>
