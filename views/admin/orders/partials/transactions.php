	<div style="overflow-y:scroll;max-height:300px;">
		<fieldset>
			<table class='table'>
				<thead class=''>
					<tr>
						<th><?php echo lang('store:status:status'); ?></th>
						<th><?php echo lang('store:orders:reason'); ?></th>
						<th><?php echo lang('store:status:received'); ?></th>
						<th><?php echo lang('store:status:refunded'); ?></th>
						<th><?php echo lang('store:orders:user'); ?></th>
						<th><?php echo lang('store:orders:date'); ?></th>
						<th><?php echo lang('store:common:action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($order->transactions as $item): ?>
						<tr>
							<td><a class='status_img_icon status_img_<?php echo $item->status; ?>'> </a></td>
							<td><?php echo $item->reason; ?></td>
							<td><?php echo ($item->amount != 0) ? nc_format_price($item->amount) : ' - '  ; ?></td>
							<td><?php echo ($item->refund != 0) ? nc_format_price($item->refund) : ' - '  ; ?></td>
							<td><?php echo $item->user; ?></td>
							<td><?php echo date('Y-m-d H:i:s', $item->timestamp); ?></td>
							<td><a class='fa fa-list-alt as_modal' href='{{x:uri x="ADMIN"}}/orders/viewtx/<?php echo $item->id;?>' > </a></td>

						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</fieldset>
	</div>