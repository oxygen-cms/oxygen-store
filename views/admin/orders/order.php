<?php 
	$paid_status = ($order->order->paid_date == NULL)? 'unpaid' : 'paid';
	$status_color = Helper_order_status_color($order->order->status_id); 
	$paid_color = Helper_order_paid_status_color($paid_status);
?>

<div class='row'>

		<div class="col-lg-3 col-md-12 col-sm-12">
			<div class="box box-solid">
				<div class="box-header with-border">
				  <h3 class="box-title"><?php echo lang('store:orders:account'); ?></em>: <?php echo  $order->customer->display_name; ?>  --  [ORDER #: <?php echo $order->order->id; ?> ]</h3>
					<div class="box-tools">
						<?php if( $this->config->item('admin/orders/show_info_status') ) :
								echo '<span class="pull-right">'.gravatar_alt( $order->invoice->email,['size'=>'30px']).'</span>';
						endif; ?>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="progress">
						<div style="width: <?php echo $order->percent_value;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $order->percent_value;?>" role="progressbar" class="progress-bar progress-bar-aqua">
							<span class="sr-only"><?php echo $order->percent_value;?>% Complete</span><?php echo $order->percent_value;?>% Complete</span>
						</div>
					</div>
					<table class='table'>
						<tr>
							<td>Order #</td>
							<td>{{order.order.id}}</td>
						</tr>
						<tr>
							<td><?php echo lang('store:orders:customer'); ?></td>
							<td>{{order.customer.display_name}}</td>
						</tr>	
						<tr>
							<td>Order total</td>
							<td><?php echo nc_format_price($order->order->total_totals); ?></td>
						</tr>
						<tr>
							<td><?php echo lang('store:orders:date_order_placed'); ?></td>
							<td><?php echo date('d / M / Y ', $order->order->order_date); ?></td>
						</tr>												
					</table>
				</div>
			</div>
			<div class="box box-solid">
				<div class="box-header with-border">
				  <h3 class="box-title">Status</h3>
				</div><!-- /.box-header -->
				<div class="box-body">

					<table class='table'>
						<tr>
							<td>
								Status
							</td>
							<td>
								<?php echo ($order->order->deleted != NULL) ? '<span class="label bg-red">Order Deleted</span>':'<span class="label bg-gray"> Active</span>' ; ?>
							</td>				    		
						</tr>				    	
						<tr>
							<td>
								Progress Status
							</td>
							<td>
								<?php echo '<span class="label bg-'.$status_color.'">'.(($order->order->current_status) ? $order->order->current_status->name :'Not Set').'</span>';?>
							</td>				    		
						</tr>
						<tr>
							<td>
								Payment Status
							</td>
							<td>
								<?php echo '<span class="label bg-'.$paid_color.'">'.ucfirst($paid_status).'</span>';?>
							</td>				    		
						</tr>
					
					</table>
										
					<br>

						<?php if($order->order->deleted==NULL):?>


							<label>Change Status</label>
							<p>
								You can change the status of this order by selecting a new status and press Confirm
							</p>
							<div>
								<form action="{{x:uri x='ADMIN'}}/orders/setstatus/{{order.order.id}}/{{order.order.status_id}}" method="post">
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

									<?php echo form_dropdown('order_status',$order->order_workflows, $order->status_id,'class="form-control"');?>

									<br>
									<span class=''>
										<button type="submit" class="btn btn-primary btn-md confirm" title='Please confirm that you want to change the order status.'><span>Change Status</span></button>
									</span>

								</form>
							</div>

						<?php endif;?>
				</div>
			</div>

			<?php if($order->customer->id > 0): ?>
				<a class='btn btn-block bg-green' href='{{x:uri x='ADMIN'}}/customers/orders/{{order.customer.id}}'>All orders by customer</a>
			<?php endif;?>
			
			<br>
			<?php 
				/**
				 * Can only do actions on orders not deleted
				 *
				 */
				if($order->order->deleted==NULL):?>

					<?php if($order->paid_date == NULL):?>
						<a class='btn btn-block bg-green confirm' href='{{x:uri x='ADMIN'}}/orders/mapaid/{{order.order.id}}' title='Are you sure you want to mark this order as PAID ?'>Mark as PAID</a>
						<br>
					<?php endif;?>

					<a class='btn btn-block  btn-danger delete confirm' href='{{x:uri x='ADMIN'}}/orders/delete/{{order.order.id}}' title='Are you sure ?'>Delete Order</a>

					<label>If you delete an order you will not be able to un-delete it or re-open it. In most cases a cancel or close order is most suitable</label>
				<?php else:?>
					<label>You can not action a deleted order.</label>
				<?php endif;?>
		</div>

		<div class="col-lg-9 col-md-12 col-sm-12">

			<div class="box box-solid">
				<div class="box-body">
					<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
							  <?php $this->load->view('admin/orders/partials/tabs'); ?>
							</ul>
							<div class="tab-content">


									<div id="order-tab" class="tab-pane active">
										<?php $this->load->view('admin/orders/partials/details'); ?>
									</div><!-- /.tab-pane -->

									<!-- always display billing-->
									<div id="billing-tab" class="tab-pane">
										<?php $this->load->view('admin/orders/partials/billing'); ?>
									</div>
									<?php if($order->shipping_address_id != $order->billing_address_id AND $order->shipping_address_id != NULL):?>
										<div id="delivery-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/shipping'); ?>
										</div>
									<?php endif;?>


									<?php if( $this->config->item('admin/orders/show_items_tab') ) : ?> 
										<div id="contents-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/items'); ?>
										</div>
									<?php endif;?>


									<div id="invoice-tab" class="tab-pane">
										<?php $this->load->view('admin/orders/partials/invoice'); ?>
									</div>

									<?php if( $this->config->item('admin/orders/show_txn_tab') ) : ?> 
										<div id="transactions-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/transactions'); ?>
										</div>
									<?php endif;?>


									<?php if( $this->config->item('admin/orders/show_notes_tab') ) : ?> 
										<div id="notes-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/notes'); ?>
										</div>
									<?php endif;?>
							</div>
					</div>
				</div> 
			</div>
		</div>	

</div>	