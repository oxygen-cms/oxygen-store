<fieldset id="filters" style="">
	<?php echo form_open(NC_ADMIN_ROUTE.'/orders/callback'); ?>
		<?php echo form_hidden('f_module', $module_details['slug']); ?>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-xs-12">
				<?php $this->load->view('admin/orders/filter'); ?>	
			</div>
			<div class="col-lg-9 col-md-9 col-xs-12">
				<div class="box box-solid">
					<div class="box-header">
						<h3 class="box-title">
							<?php echo lang('store:orders:title');?>
						</h3>
						<div class="box-tools">
							<?php echo form_input('f_keyword_search',  '', 'style="width: 150px;" placeholder="Enter Order #" class="form-control input-sm pull-right'); ?>
						</div>
						<div class="input-group">
							<div class="input-group-btn">
							</div>
						</div>			
					</div>
					<div class="box-body">
						<table class="table">
							<thead>
								<tr>
									<th style="width:5%">ID</th>
									<th style="width:5%"></th>
									<th style="min-width:20%"><?php echo lang('store:orders:customer');?></th>
									<th style="width:15%"><?php echo lang('store:orders:date');?></th>
									<th style="width:10%"><?php echo lang('store:orders:total');?></th>
									<th style=""><?php echo lang('store:orders:status');?></th>
									<th></th>
								</tr>
							</thead>
							<tbody id="filter-stage">				
								<?php $this->load->view('admin/orders/line_item'); ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	<?php echo form_close(); ?>	
</fieldset>