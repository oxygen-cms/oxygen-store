<?php 
	$paid_status = ($order->paid_date == NULL)? 'unpaid' : 'paid';
	$status_color = Helper_order_status_color($order->status_id); 
	$paid_color = Helper_order_paid_status_color($paid_status); 
	$percent_value = 5;
?>

<div class='row'>

		<div class="col-md-6">
		  <div class="box box-solid">
			    <div class="box-header with-border">
			      <h3 class="box-title"><?php echo lang('store:orders:account'); ?></em>: <?php echo  $customer->display_name; ?>  --  [ORDER #: <?php echo $order->id; ?> ]</h3>
			    </div><!-- /.box-header -->
			    <div class="box-body">
					<p>
						<code>Order Progress : </code>
					</p>

					<div class="progress">
					<div style="width: <?php echo $percent_value;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $percent_value;?>" role="progressbar" class="progress-bar progress-bar-aqua">
						<span class="sr-only"><?php echo $percent_value;?>% Complete</span><?php echo $percent_value;?>% Complete</span>
					</div>
					</div>
			    </div>
		  </div>
		</div>


		<div class="col-md-4">
		  <div class="box box-solid">
			    <div class="box-header with-border">
			      <h3 class="box-title">Status</h3>
			    </div><!-- /.box-header -->
			    <div class="box-body">
			    	<?php
					if( $this->config->item('admin/orders/show_info_status') ) :
						echo '<span class="pull-right">'.gravatar( $order->invoice->email).'</span>';
					endif;
					echo 'Order Status : <span class="label bg-'.$status_color.'">'.(($order->current_status) ? $order->current_status->name :'Not Set').'</span><br>';
					echo 'Payment Status : <span class="label bg-'.$paid_color.'">'.$paid_status.'</span>';
					if($order->deleted != NULL): echo '<br><span class="label bg-red">Order Deleted</span>';endif; 

					?>


			    </div>
		  </div>
		</div>


		<div class="col-md-10">
		  <div class="box box-solid">
			    <div class="box-body">




				              <!-- Custom Tabs -->
				              <div class="nav-tabs-custom">
				                <ul class="nav nav-tabs">
				                  <?php $this->load->view('admin/orders/partials/tabs'); ?>
				                </ul>
				                <div class="tab-content">


									 	<div id="order-tab" class="tab-pane active">
											<?php $this->load->view('admin/orders/partials/details'); ?>
						                </div><!-- /.tab-pane -->

										<!-- always display billing-->
										<div id="billing-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/billing'); ?>
										</div>
										<?php if($order->shipping_address_id != $order->billing_address_id AND $order->shipping_address_id != NULL):?>
											<div id="delivery-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/shipping'); ?>
											</div>
										<?php endif;?>


										<?php if( $this->config->item('admin/orders/show_items_tab') ) : ?> 
											<div id="contents-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/items'); ?>
											</div>
										<?php endif;?>


										<div id="invoice-tab" class="tab-pane">
											<?php $this->load->view('admin/orders/partials/invoice'); ?>
										</div>

										<?php if( $this->config->item('admin/orders/show_txn_tab') ) : ?> 
											<div id="transactions-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/transactions'); ?>
											</div>
										<?php endif;?>


										<?php if( $this->config->item('admin/orders/show_notes_tab') ) : ?> 
											<div id="notes-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/notes'); ?>
											</div>
										<?php endif;?>
										
										<?php if($show_actions): ?>
											<div id="actions-tab" class="tab-pane">
												<?php $this->load->view('admin/orders/partials/actions'); ?>
											</div>
										<?php endif;?>
						
								
					

				              </div><!-- nav-tabs-custom -->
				            </div>
				</div> 


			</div>
		</div>	

</div>	