	<?php foreach ($orders as $order) : ?>
		<tr>
			<td>
			<?php echo anchor(NC_ADMIN_ROUTE.'/orders/order/' . $order->id, '#'. (0 + $order->id) ); ?>
			
			</td>
			<td  style="width:35px" >
				<a href="{{x:uri x='ADMIN'}}/orders/order/<?php echo $order->id; ?>">
					<?php echo gravatar_alt($order->customer_email,['size'=>'30']);?>
				</a>
			</td>
			<td>
				<?php echo anchor(NC_ADMIN_ROUTE.'/orders/order/' . $order->id, $order->customer_name, array('class'=>'',  'title' => lang('store:common:view') ) ); ?>
			</td>
			<td class="">
				<?php echo format_date($order->order_date); ?>
			</td>
			<td class="">
				<?php echo nc_format_price($order->total_totals); ?>
			</td>
			<td>
				<?php $clastorst_storename = $order->paid_date == null ? 'success' : 'info' ; ?>
				<div class='label label-<?php echo $clastorst_storename;?>'><?php echo ($order->paid_date==NULL)? 'Un Paid' : 'Paid';?></div>
			</td>
			<td>
				<?php $clastorst_storename = Helper_order_status_color($order->status_id); ?>
				<div class='badge bg-<?php echo $clastorst_storename;?>' title='<?php echo strtoupper($order->status_name);?>'><?php echo strtoupper($order->status_name);?></div>
				<?php if($order->deleted!=NULL):?>
					<span class='label label-danger'><?php echo lang('store:orders:deleted');?></span>
				<?php endif;?>

			</td>
			<td>
				<span style="float:right;">
						<?php if(($order->paid_date==NULL)&& ($order->deleted==NULL)):?>
							<a href="<?php echo NC_ADMIN_ROUTE.'/orders/reinvoice/' . $order->id;?>" class="btn btn-sm btn-flat bg-orange"> Send Invoice</a>
						<?php endif;?>
						<a href="<?php echo NC_ADMIN_ROUTE.'/orders/order/' . $order->id;?>" class="btn btn-sm btn-flat bg-blue"> <?php echo lang('store:orders:view'); ?></a>
				</span>
			</td>
		</tr>
	<?php endforeach;?>
	<tr>
		<td colspan='10'>			
			<div class="inner" style="float:right;">
				<?php $this->load->view('admin/partials/pagination'); ?>
			</div>		
		</td>						
	</tr>
	<tr>
		<td colspan='10'>Showing <?php echo count($orders);?> results</td>					
	</tr>	