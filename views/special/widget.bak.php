
<?php
class PluginWidget {

	public function form() {
		if($enabled) {
			$str = "
					{{plugin:of_some_description}}
						{{id}}
					{{/plugin:of_some_description}}
				";
			return $str;
		} else {
			return '';
		}

	}

}