<div class="row">

    <div class="col-xs-12">

        
        <div class="box box-primary">
				
            <div class="box-header">
	   
		            <h4 class="box-title">
		              	{{title}}
		            </h4>

					<div class="pull-right"></div>
            </div>
            <!-- /.box-header -->


            <div class="box-body">


				{{body}}
			</div>
		</div>
	</div>
</div>