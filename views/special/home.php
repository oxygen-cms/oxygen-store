<div class="row">

    <div class="col-xs-12">

        
        <div class="box box-primary">
                
            <div class="box-header">
       
                    <h4 class="box-title">
                        Store
                    </h4>

                    <div class="pull-right"></div>
            </div>
            <!-- /.box-header -->


            <div class="box-body">


                    {{content}}

                    {{if options.recent == '1' }}
                        <!-- Show recent products here-->
                    {{endif}}

                    {{if options.coverimg == '1' }}
                        
                    {{endif}}

                    {{if options.categories == '1' }}
                            <div class="clearfix">
                                <div class="category-list">
                                    <div class="row">
                                        <article class="container text-center products">
                                        {{ store_categories:parents }}
                                            <ul>                        
                            
                                                <li class="col-sm-4 product">
                                                    <a href="{{x:uri}}/categories/category/{{slug}}">
                                                        <img class="img-responsive" src='{{url:site}}files/large/{{file_id}}'>
                                                    <h6><a href="{{x:uri}}/categories/category/{{slug}}"><h4>{{ name }}</h4></a></h6>
                                                </li>                                   

                                            </ul>
                                        {{ /store_categories:parents }}
                                        </article>
                                    </div>                                                     
                                </div>     
                            </div> 
                    {{endif}}                                 
            </div>

            <div class="box-footer">
       
                 
            </div>            

        </div>
    </div>
</div>

