<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Home extends Public_Controller
{


	public function __construct()
	{
		parent::__construct();
	}

	/**
	 */
	public function index()
	{

		$this->load->model('store/store_home_m');

		$row = $this->db->where('page','home')->order_by('id','desc')->get('storedt_store_home')->row();

		//hmm, we dont have a page
		if($row==null) {
			//lets create
			$this->store_home_m->init();
			$row = $this->db->where('page','home')->order_by('id','desc')->get('storedt_store_home')->row();
		}

		$row->options = json_decode($row->options);

		//set the layout
		$this->setLayoutForShop($row->theme_layout);

		//set the view
		$this->template
				->title($this->module_details['name'])
				->enable_parser(true)	
				->build('special/home', $row);				
	}

	private function setLayoutForShop($preffered='store_home.html')
	{
		if($this->template->layout_exists($preffered))
		{
			$this->template->set_layout($preffered);
		}		
		elseif($this->template->layout_exists('store_home.html'))
		{
			$this->template->set_layout('store_home.html');
		}
		elseif($this->template->layout_exists('store.html'))
		{
			$this->template->set_layout('store.html');
		}	
	
	}

}