<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */


class Eavtest extends Public_Controller
{


	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();	
		Events::trigger('STOREVT_ShopPublicController');
	}

	/**
	 * Interface only:the traditional way, product_id is actually variance_id
	 */
	public function product($product_id = 0)
	{

		//$this->return_status('error','Failed to find');die;
		$input = $this->input->post();

		$qty = 1;

		//get product id
		if(isset($input['form_eav_product_id']))
		{
			$product_id = (int) $input['form_eav_product_id'];

			//remove this key
			unset($input['form_eav_product_id']);

		}


		//get product id
		if(isset($input['pid']))
		{
			unset($input['pid']);
		}





		$this->posted = json_encode($input);


		//
		// Load custom libraries
		//
		$this->load->library('store/Toolbox/Nc_string'); 


		//
		// Do a pre check to see if there are attributes, if not, just add default first
		//
		if($this->_has_attributes($product_id)==false)
		{
			if($variant = $this->db->where('product_id',$product_id)->where('deleted',NULL)->where('available',1)->get('storedt_products_variances')->row())
			{
				// 1. Get product from Database (incl basic checks) return error code if fails
				$product = $this->fetchProduct( $variant->product_id );

				$this->return_status('success','Found without options',$data->product);
				die;
			}

		}


		//
		// Builds the list of option ID data
		//
		$options = $this->_seed_plantation($input);



		//
		// Prune - remove anomolies
		// Harvest - Collect info on the options
		//
		$nceavarray = $this->_prune_and_harvest($product_id, $options);



		//
		// If we dont have any, it will fail the test
		//
		if( $variant = $this->_pick( $nceavarray ) )
		{
			$this->return_status('success','We found it!',$variant);die;
		}	


		$this->return_status('error','Failed to find [a]',$this->posted);
		
	}

	public function productvariance($variance_id = 0)
	{

		if($variant = $this->db->where('id',$variance_id)->where('deleted',NULL)->where('available',1)->get('storedt_products_variances')->row())
		{
		
			$retarray = [
							'status'=>'success',
							'message'=>'',
							'obj'=>null,
							'price'=> $variant->price ,
							'base'=>  $variant->base ,
						];

			$retobject = (object) $retarray;

			echo json_encode($retobject);die;

		}
		else
		{
			$retarray = [
							'status'=>'error',
							'message'=>'',
							'obj'=>null,
							'price'=>0,
							'base'=>0 ,							
						];

			$retobject = (object) $retarray;

			echo json_encode($retobject);die;
		}
		
	}
	

	private function return_status($status='success',$message='',$object=null)
	{
		$retarray = [
						'status'=>$status,
						'message'=>$message,
						'obj'=>$object,
					];

		$retobject = (object) $retarray;

		echo json_encode($retobject);die;
	}





	private function _has_attributes($product_id)
	{

		if($this->db->where('e_product',$product_id)->where('e_variance',NULL)->get('storedt_e_attributes')->row())
		{
			return true;
		}

		return false;
	}


	/**
	 * Build the list of ID's for the options requested.
	 * We will use this data to filter the variations down to 
	 * the customers expectations.
	 *
	 *
	 */
	private function _seed_plantation($input)
	{

		$options = [];

		foreach($input as $key=>$value)
		{
			$string = new NCString($key);
			if($string->startsWith('form_eav_'))
			{
				if($string->rightOf('form_eav_')->isNumeric())
				{
					if($intval = $string->rightOf('form_eav_')->toNCFloat()->toInt())
					{
						$options[$intval] = $value;
					}
				}
			}
		}
		return $options;
	}




	/**
	 * Combines both prune and harvest into a single function
	 *
	 *
	 * Lets make sure that the set of requested 
	 * options are all from the same stem.
	 *
	 * What we need to do is remove anything from other
	 * products
	 *
	 */
	private function _prune_and_harvest( $product_id, $options )
	{
		$_stem = NULL;

		$array = new NCEAVArray();


		// Get all the variances in a NCEAVArray
		$rows = $this->db->where('e_variance IS NOT NULL', null, false)->where('e_product', $product_id)->get('storedt_e_attributes')->result();

		$this->posted = 'Product ID' + json_encode($product_id);

		//var_dump($rows);die;

		// Build te NCAray and trim nulls
		$array = $array->pushall($rows)->noNullValues();



		$new = [];
		foreach($options as $id=>$value)
		{
			$row = $this->db->where('id',$id)->get('storedt_e_attributes')->row();
			$new[$row->e_label] = $value;
		}


		foreach($new as $key=>$value)
		{
			$array = $array->variancesWith($key,$value);
		}

	
		return $array;
	}	




	/**
	 * Pick will select the individual SKU/Product variant that...
	 * 1, matches the user request, 
	 * 2, is available for picking (availability ect...)
	 */
	private function _pick( $nceavarray )
	{

		// All variances that remain are matching of the users request.
		// so we need to see which variances are valid by the admin/office
		// lets get all the variances that remain..

		$variants = [];
		foreach( $nceavarray->iterator()  as $i )
		{
			$variants[$i->getVarianceID()] = $i->getVarianceID();
		}

		// Dont just get the first of the same attributes.
		// Actually get the one with the lowest price as the user sees this first
		$possibilities = [];
		foreach($variants as $variant)
		{
			if($prodvar = $this->db
				->where('id',$variant)
				->where('deleted',NULL)
				->where('available',1)
				->get('storedt_products_variances')->row())
			{
				$possibilities[$variant] = $variant;
			}
		}

		if(!count($possibilities))
		{
			return false;
		}


		if($prodvar = $this->db->where_in('id',$possibilities)->order_by('price','asc')->get('storedt_products_variances')->row())
		{
			return $prodvar;
		}

		//out of luck.. no results found
		return false;
	}


	
}

