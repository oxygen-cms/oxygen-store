<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
require_once(dirname(__FILE__) . '/mybase_controller.php');
class My_portal extends MyBase_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->template
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(Settings::get('storst_storename'), '/'.NC_ROUTE);
	}


	/**
	 *
	 *
	 * This will display a list of orders the customer
	 * has placed with the shop.
	 *
	 */
	public function index()
	{
		$en_wl = Settings::get('shop_my_wishlist_enabled');

		// Display the page
		$this->template
			->set_breadcrumb('My')
			->set('wishlist_enabled',$en_wl)
			->title( Settings::get('storst_storename') )
			->build('store/my/dashboard');
	}

}