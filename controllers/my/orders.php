<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
require_once(dirname(__FILE__) . '/mybase_controller.php');
class Orders extends MyBase_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('store/orders_m');
		$this->template
			->set_breadcrumb('Home', '/')
			->set_breadcrumb( Settings::get('storst_storename') , '/'.NC_ROUTE)
			->set_breadcrumb('My', '/'.NC_ROUTE.'/my');
	}


	/**
	 *
	 *
	 * This will display a list of orders the customer
	 * has placed with the shop.
	 *
	 */
	public function index()
	{

		$data = new ViewObject();

		$data->items = $this->orders_m->order_by('id','desc')->get_all_by_user($this->current_user->id);

		// Display the page
		$this->template
			->set_breadcrumb('Orders')
			->title( Settings::get('storst_storename'), 'MY Orders')
			->build('store/my/orders', $data);
	}


	/**
	 *
	 *
	 * Show the main dashboard menu and also display some usefull summary information about
	 * their transactions ect.
	 * field invoice is deprecated
	 */
	public function order($id)
	{
		$data = new ViewObject();

		$this->load->model('store/addresses_m');

		// Retrieve the order
		$data->order = $this->orders_m->where('user_id', $this->current_user->id)->get($id);
		if (!$data->order )
		{
			$this->session->set_flashdata('error', lang('store:my:order_not_found'));
			redirect( NC_ROUTE.'/my/orders');
		}

		$data->shipping = $this->addresses_m->get($data->order->shipping_address_id);
		$data->billing = $data->invoice = $this->addresses_m->get($data->order->billing_address_id);
		$data->transactions = $this->db->where('order_id', $id)->get('storedt_transactions')->result();
		$data->contents = $this->orders_m->get_order_items($data->order->id);

		$this->template
				->set_breadcrumb('Orders',NC_ROUTE.'/my/orders')
				->set_breadcrumb("Order [{$id}]")
				->title(Settings::get('storst_storename'),'MY Order')
				->build('store/my/order', $data);
	}


}