<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Countries extends Public_Controller
{

	protected $section = 'api';

	public function __construct()
	{
		parent::__construct();

        // call event for extention module integration
        Events::trigger('STOREVT_ShopPublicController');

	}

	public function index()
	{

	}

	
	/**
	 * [CurrentUserCanExpress description]
	 * @param [type] $country_id 	The ID of the Country
	 * @param string $method The valid method to choose, states,add,remove ect..
	 */
	public function country($country_id,$method='states')
	{
		
		switch ($method) 
		{
			case 'states':
				$response = $this->states($country_id);
				break;
			
			default:
				# code...
				break;
		}

		echo json_encode($response);die;
	}

	private function states($country_id)
	{
		$response = [];
		$response['status'] = JSONStatus::Success;
		$response['action'] = 'store/api/countries/country/<ID>/states';		
		$response['message'] = '';
		$response['html'] = $this->build_options($country_id);

		return $response;
	}

	private function build_options($country_id)
	{
		
		if($country = $this->db->where('code2',$country_id)->get('storedt_countries')->row())
		{
			if($states = $this->db->where('country_id',$country->id)->order_by('name','asc')->select('name,code')->get('storedt_states')->result())
			{
				$html = '';
				//$html .= "<select name='state' class=''><option value='---'>--Please select a state--</option>";
				$html .= "<option value='---'>--Please select a state--</option>";
				foreach ($states as $state)
				{
					$html .= "<option value='{$state->code}'>{$state->name}</option>";
				}
				//$html.='</select>';
				$html.='';
				
				return $html;	
			}
			else
			{
				return ''; //'<input type="text" name="state" placeholder="Enter State">';
			}
			//return $states;


		}
		return '';

	}

}
