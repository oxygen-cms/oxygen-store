<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */ 
class Api_core extends Public_Controller 
{

	protected $section = 'api';
	protected $subsection = 'public';

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopPublicController');
        $this->load->library('store/Toolbox/Nc_status');
        $this->load->library('store/Toolbox/Nc_string');  

        get_instance()->current_user = $this->current_user = $this->ion_auth->get_user();

        /**
         * Check to see if the system is instaled
         */
        if(! (system_installed('feature_api')) AND ($this->db->table_exists('storedt_api_keys') ))
        {
        	echo "Sorry.. the system is unavailable at thi time.";die;
        }
        
        
        /**
         * Track user request usage
         * @var boolean
         */
        $this->track_request =true;

        /**
         * Note that track-request needs to be enabled for this to work
         * @var boolean
         */
        $this->log_responses = true;     

	}

	protected function do_auth()
	{
		$x = $this->input->post();
		$r = $this->ion_auth->login($x['email'], $x['password']);
		if($r)
		{
			$user = $this->ion_auth->get_user_by_email($x['email']);

			$this->session->set_userdata('user_id',$user->id);
		 	$this->ion_auth->force_login($user->id, 1);
		}

		return $r;
	}

	/**
	 * public test of Auth
	 */
	public function auth()
	{
		$r = $this->do_auth();
		$x['status'] = $r;
		echo json_encode($x);die;
	}

	public function user()
	{
		//$user = get_instance()->current_user = $this->current_user = $this->ion_auth->get_user( $this->session->userdata('user_id') ); // $this->current_user ? $this->current_user : $this->ion_auth->get_user();

		$user = $this->current_user ? $this->current_user : $this->ion_auth->get_user();

		echo json_encode($user);die;
	}	


	/**
	 * Public handler
	 * @return [type] [description]
	 */
	public function index()
	{
		if($input = $this->input->post())
		{
			var_dump($input);
		}
		die;
	}



	/**
	 * All incoming request should be routed/extend this.
	 * 
	 * @param  [type] $key    [description]
	 * @return [type]         [description]
	 */
	protected function req($endpoint,$key)
	{
		if($row = $this->_validate_key($endpoint,$key))
		{
			if($this->track_request) 
				$this->_logrequest($row);

			return true;
		}
		else
		{
			// Just die now, no need to bubble
			$array = ['status'=>false,'message'=>'Sorry, you do not have access.'];
			echo json_encode($array);die;
		}
	}


	/**
	 * Send the data
	 * @param  [type] $endpoint [description]
	 * @param  [type] $status   [description]
	 * @param  [type] $message  [description]
	 * @param  [type] $result   [description]
	 * @return [type]           [description]
	 */
	protected function send($endpoint,$status,$message,$result)
	{
		$return = [];
		$return['status'] = $status;
		$return['message'] = $message;
		$return['result'] = $result;

		$response = json_encode($return);

		if(($this->log_responses)AND($this->track_request)) 
			$this->log($endpoint,$response);

		die($response);		
		//die('<code>'.$response.'</code>');		
	}


	/**
	 * Validates a key and acccess rights
	 * 
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	private function _validate_key($endpoint,$key)
	{
		if($row = $this->db->where('key',strtoupper($key))->get('storedt_api_keys')->row())
		{
			if($row->max_allowed > $row->tot_curr_requests)
			{
				$this->key_id = $row->id;
				if($row->enabled)
				{
					return $row;
				}
			}
		}
		return false;
	}


	private function _logrequest($keyrow)
	{
		$data =  ['tot_requests'=>($keyrow->tot_requests + 1),'tot_curr_requests'=>($keyrow->tot_curr_requests + 1)];

		$this->db
			->where('key',strtoupper($keyrow->key))
			->update('storedt_api_keys', $data );
	}

	/**
	 * Log information in database
	 * 
	 * @param  [type] $endpoint [description]
	 * @param  [type] $response [description]
	 * @return [type]           [description]
	 */
	private function log($endpoint,$response)
	{
		$data =
		[
			'key_id'	=> $this->key_id,
			'endpoint'	=> $endpoint,
			'date'		=> date('Y-m-d H:m:s'),
			'result'	=> $response
		];
		$this->db->insert('storedt_api_requests',$data);
	}

}
