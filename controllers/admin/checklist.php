<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Checklist extends Admin_Controller
{

	protected $section = 'checklist';
	private $data;

	private $reportNames = array();

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');		
		$this->data = new StdClass;
        //$this->lang->load('maintenance');        
	}


	/**
	 * List all items
	 */
	public function index()
	{
		$this->template->title($this->module_details['name'])
			->build('admin/checklist/checklist');		
	}


    public function test_shipping_boxes()
    {

    	$this->load->model('store/admin/products_admin_m');
    	$this->load->model('store/admin/products_variances_admin_m');
    	$this->load->library('store/packages_library');
    	
		$variances = $this->products_variances_admin_m->where('deleted',NULL)->get_all();

    	$items = array();

    	foreach($variances as $variance)
    	{
    		if($row = nc_sim_cart_item( $variance ))
    		{
     			$items[] = $row;
    		}
    	}

		$redir_options=array(
				'redir'=>'admin/maintenance/checklist',
				'mode'=>'admin',
				);

		$this->packages_library->pack( $items , $redir_options );


		//if you get here then we should all be good
		$this->session->set_flashdata(JSONStatus::Success,'Packing success !');
		redirect($redir_options['redir']);
    }


    public function test_checkout_ability()
    {

    	$status = $this->checklist_item_a();
		if($status['status'] == 'success')
		{
			$status = $this->checklist_item_b();
			if($status['status'] == 'success')
			{
				$status = $this->checklist_item_c();
			}
		}

		$this->session->set_flashdata($status['status'],$status['message']);
		redirect('admin/maintenance');
    }



	public function test_product_type_assoc()
    {

		$message = 'All products have valid product types associated';
		$status = 'success';

    	$info = $this->db->where('deleted',NULL)->where('type_id',NULL)->get('storedt_products')->result();

    	if(count($info) > 0)
    	{	
    		$message = "There are {} products unassigned to a Product Type. You must contact your webde to fix this.";
    		$status = 'error';
		}

		$this->session->set_flashdata($status,$message);
		redirect('admin/maintenance');			
    }




    private function checklist_item_a()
    {
    	$status = 'error';
        if($this->db->where('enabled',1)->where('module_type', 'shipping')->get('storedt_checkout_options')->row())
    	{
	    	if($this->db->where('enabled',1)->where('module_type', 'gateway')->get('storedt_checkout_options')->row())
	    	{
	    		$status = 'success';
				$message = 'You can checkout just fine';
	    	}
	    	else
	    	{
	    		$message = 'You need to set up a gateway';
	    	}
    	}
    	else
    	{
    		$message = 'You do not have SHIPPING setup.';
    	}

    	return array('status'=>$status,'message'=>$message);
    }

    private function checklist_item_b()
    {
	    $status = 'error';

    	if(Settings::get('storst_open_status'))
    	{
	    	$status = 'success';
			$message = 'You can checkout just fine';
    	}
    	else
    	{
			$message = 'You need to Set your Open status to OPEN!';
    	}

    	return array('status'=>$status,'message'=>$message);
    }

    private function checklist_item_c()
    {
	    $status = 'success';
		$message = 'You can checkout just fine';
    	return array('status'=>$status,'message'=>$message);
    }    


    /**
     * This makes sure that all zones are existing in the zones table.
     * Some product_variances may have had zones asigned, if the zone no longer exist
     * we may experience some issues with checkout
     * @return [type] [description]
     */
    public function test_all_zones_accountability()
    {

    	//get all active / valid variances
    	$prods = $this->db->where('is_shippable',1)->where('deleted',NULL)->group_by('zone_id')->get('storedt_products_variances')->result();


    	foreach ($prods as $key => $prod) 
    	{
    		//dont calculate 0 or NULL
    		if($prod->zone_id==0) continue;
    		if($prod->zone_id==NULL) continue;

    		if($zone = $this->db->where('id',$prod->zone_id)->get('storedt_zones')->row())
    		{

    		}
    		else
    		{
    			//we have a problem
				$this->session->set_flashdata(JSONStatus::Error, $prod->name . " - PROD-ID( {$prod->id} ) Is not assigned to  a valid Zone. We recomend changing it to MCL.");   
				redirect('admin/maintenance'); 			
    		}

    	}


		$this->session->set_flashdata(JSONStatus::Success,'All products are zoned correctly');
		redirect('admin/maintenance'); 			

    }


}