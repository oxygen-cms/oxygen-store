<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Sandbox extends Admin_Controller
{
	// Set the section in the UI - Selected Menu
	protected $section = 'sandbox';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');
        
		$this->data = new ViewObject();

		role_or_die('store', 'store_administrator');
	}

	/**
	 * List all items:load the dashboard
	 */
	public function index()
	{


		$output ='View shipping or currect test';
		//$this->data->items = & $items;

		$this->template->title($this->module_details['name'])
			->set('output',$output)
			->build('admin/sandbox/main');
	
	}

	/**
	 * The currency sandbox should display 1500.50 for AU and US.
	 * For Eu it should display 1.500,50
	 * @return [type] [description]
	 */
	public function currency()
	{

		$this->load->library('store/Currency_library');

		$output = '';

		$output .= $this->currency_library->getCurrencySymbol();
		$output .= '1';
		$output .= $this->currency_library->getCurrencyThousandsSeperator();
		$output .= '500';
		$output .= $this->currency_library->getCurrencyDecimalSeperator();
		$output .= '50';

		$output .= $this->currency_library->getCountryISOCode();


		$this->template->title($this->module_details['name'])
			->set('output',$output)
			->build('admin/sandbox/main');
	}

	public function shipping()
	{

		$this->load->library('store/packages_library');

		$output = '';

		if($this->mycart->contents() != NULL) {
			
			$output = $this->packages_library->pack( $this->mycart->contents() );

			$output = "<br><h3>Cart Items</h3><br>".$this->packages_library->getTrace();
		}


		$this->template->title($this->module_details['name'])
			->set('output',$output)
			->build('admin/sandbox/main');

	}

	public function cart()
	{

		$items = $this->mycart->contents();
		$output = '<pre>'.print_r($items,true).'</pre>';
		$this->template->title($this->module_details['name'])
			->set('output', $output)
			->build('admin/sandbox/cart',$items);
	}	


	public function canvas()
	{
		$this->load->library('store/Toolbox/Nc_string');
		$this->load->library('store/Toolbox/Nc_status');

        $status = new NCMessageObject(true,'Error Message');
        if(!$status->setMessage('New Error'))
        {
        	echo "failed";
        }
        echo $status->set('jubbie','are kool').br();
        echo $status->asJson().br();
        echo $status->asJsonReturn();
        //var_dump( $status);

	}
}