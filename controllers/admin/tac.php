<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Tac extends Admin_Controller
{

	protected $section = 'dashboard';

	public function __construct()
	{
		parent::__construct();
		Events::trigger('STOREVT_ShopAdminController');
	}

	/**
	 */
	public function index()
	{
		
		
		//check for save
		if($input = $this->input->post()) {
			$this->_index_save($input);
		}

		//load after save, unless save redirects
		$this->_index_load();
	
	}

	private function _index_load()
	{
		$this->load->model('store/store_tac_m');

		$row = $this->db->where('page','tac')->order_by('id','desc')->get('storedt_store_home')->row();

		//hmm, we dont have a page
		if($row==null) {
			//lets create
			$this->store_tac_m->init();
			$row = $this->db->where('page','tac')->order_by('id','desc')->get('storedt_store_home')->row();
		}

		$row->options = json_decode($row->options);		

		$this->template
				->title($this->module_details['name'])
				->enable_parser(true)	
				->append_metadata($this->load->view('fragments/wysiwyg', $row, TRUE))	
				->build('admin/home/tac', $row);				
	}

	private function _index_save($input)
	{

		$this->load->model('store/store_tac_m');

		$row = $this->db->where('page','tac')->order_by('id','desc')->get('storedt_store_home')->row();
		//hmm, we dont have a page
		if($row==null) {
			//lets create
			$this->store_tac_m->init();
		}	

		$options = [];


		//dont double up
		unset($options['id']);
		unset($options['content']);
		unset($options['theme_layout']);
		unset($options['btnAction']);
		unset($input['btnAction']);

		//now assign to option
		$input['options']=$options;

		$this->store_tac_m->save($input['id'],$input);
	}	

	protected function truncate_if_exist($table_name)
	{
        if($this->db->table_exists($table_name) )
        {
            $this->db->truncate($table_name);
        }
	}	
}