<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Packages_groups extends Admin_Controller
{

	protected $section = 'packages'; //packages_groups';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');

		$this->data = new ViewObject();

		$this->load->library('form_validation');
		$this->load->model('store/admin/packages_groups_admin_m');
		$this->load->model('store/admin/packages_admin_m');
		$this->config->load('store/admin/'.NC_CONFIG);
		
		$this->lang->load('store/store_admin_packages');

        $this->template->append_js('store::admin/packages.js');

	}

	/**
	 * List all Installed and Not installed gateways
	 */
	public function index()
	{
		role_or_die('store', 'admin_checkout');
		$this->data->packages_groups = $this->packages_groups_admin_m->get_all_available();
		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/groups/list', $this->data);
	}

	/**
	 * Edit an existing package and validate
	 *
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function edit($id)
	{

		role_or_die('store', 'admin_checkout');

		$this->data->id = $id;

		if($input = $this->input->post())
		{
			$this->form_validation->set_rules($this->packages_groups_admin_m->_create_validation_rules);

			if( $this->form_validation->run() )
			{
				$this->data = $this->packages_groups_admin_m->save($id,$input);
				redirect(NC_ADMIN_ROUTE.'/packages_groups');
			}
			else
			{
				foreach ($this->packages_groups_admin_m->_create_validation_rules AS $rule)
					$this->data->{$rule['field']} = $this->input->post($rule['field']);
			}

		}
		else
		{
			$this->data = $this->packages_groups_admin_m->get($id);
		}

		//
		// Now get the packages for this group
		//
		$this->data->packages = $this->packages_admin_m->get_by_group_id($id);



		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/groups/edit',$this->data);
	}

	public function package_list($id)
	{

		role_or_die('store', 'admin_checkout');

		$this->data->id = $id;

		if($input = $this->input->post())
		{
			$this->form_validation->set_rules($this->packages_groups_admin_m->_create_validation_rules);

			if( $this->form_validation->run() )
			{
				$this->data = $this->packages_groups_admin_m->save($id,$input);
				redirect(NC_ADMIN_ROUTE.'/packages_groups');
			}
			else
			{
				foreach ($this->packages_groups_admin_m->_create_validation_rules AS $rule)
					$this->data->{$rule['field']} = $this->input->post($rule['field']);
			}

		}
		else
		{
			$this->data = $this->packages_groups_admin_m->get($id);
		}

		//
		// Now get the packages for this group
		//
		$this->data->packages = $this->packages_admin_m->get_by_group_id($id);

		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/groups/package_list',$this->data);
	}
	
	/**
	 * Create a new package
	 *
	 * On post back check and validate input
	 *
	 * @return [type] [description]
	 */
	public function create()
	{
		role_or_die('store', 'admin_checkout');

		$this->data = new ViewObject();

		if($input = $this->input->post())
		{
			$this->form_validation->set_rules($this->packages_groups_admin_m->_create_validation_rules);

			if( $this->form_validation->run() )
			{
				if($id = $this->packages_groups_admin_m->create($input))
				{
					$this->session->set_flashdata(JSONStatus::Success,'Package has been created.');
					redirect(NC_ADMIN_ROUTE.'/packages_groups');
				}
			}
		}

		foreach ($this->packages_groups_admin_m->_create_validation_rules AS $rule)
			$this->data->{$rule['field']} = $this->input->post($rule['field']);

		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/groups/edit',$this->data);
	}


	/**
	 * Duplicate the package group
	 */
	public function duplicate($id)
	{
		role_or_die('store', 'admin_checkout');


		if($new_id = $this->packages_groups_admin_m->duplicate($id))
		{
			//
			// Now copy all the packages in group to new group
			//
			$new_packages = $this->packages_admin_m->get_by_group_id($id);
			foreach($new_packages as $np)
			{
				$np->pkg_group_id = $new_id;
				$this->packages_admin_m->CreatePackage($np);
			}

			$this->session->set_flashdata(JSONStatus::Success,'Package has been copied.');
			redirect( NC_ADMIN_ROUTE. '/packages_groups/edit/'.$new_id);
		}

		$this->session->set_flashdata(JSONStatus::Error,'Failed to duplicate package..');

		redirect(NC_ADMIN_ROUTE.'/packages_groups');
	}

	/**
	 * Delete a package
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		role_or_die('store', 'admin_checkout');

		if($this->packages_groups_admin_m->delete($id))
			$this->session->set_flashdata(JSONStatus::Success,'Package Group has been removed.');
		else
			$this->session->set_flashdata(JSONStatus::Error,'You cant delete a Package Group if products or packages are assigned to it, or if is the last one.');

		redirect(NC_ADMIN_ROUTE.'/packages_groups');
	}
}