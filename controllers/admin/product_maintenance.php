<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class product_maintenance extends Admin_Controller
{

	protected $section = 'product_maintenance';
	private $data;

	private $reportNames = array();

	public function __construct()
	{
		parent::__construct();

		//$this->lang->load('maintenance');
		$this->refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'store';
		$this->data = new StdClass;
	}

	/**
	 * List all items
	 */
	public function index()
	{
		$this->template->title($this->module_details['name'])
			->build('admin/product_maintenance/maintenance');		
	}

	/**
	 * Removes e_attributes ||EAV table of variances that have been deleted
	 * The attributes are no longer required.
	 */
	public function clean_eav_products()
	{
		if(!$this->db->table_exists('storedt_products'))
		{
			redirect('admin/maintenance');	
		}
		$m = 'No records required to be cleaned';
		$mysold_array = array();
		
		$items = $this->db->where('deleted !=', 'NULL')->get('storedt_products')->result();

		if(count($items)>0)
		{
			foreach($items as $item)
			{
				$mysold_array[] = $item->id;
			}

			if($this->db->where_in('e_product', $mysold_array)->delete('storedt_e_attributes'))
			{	
				$m = 'Some records cleaned';			
			}
		}

		$this->session->set_flashdata('success',$m);
		redirect('admin/maintenance');		
	}


	/**
	 * This will remove any deleted variation that has NOT been sold.
	 */
	public function clean_variations($redir=TRUE)
	{
		//just check if the store is propperly installed
		if(!$this->db->table_exists('storedt_products'))
		{
			redirect('admin/maintenance');	
		}

		$mysold_array = array();
		$items = $this->db->group_by('variant_id')->get('storedt_order_items')->result();
		if(count($items)>0)
		{
			foreach($items as $item)
			{
				$mysold_array[] = $item->variant_id;
			}
			$this->db->where_not_in('id', $mysold_array);			
		}

		$this->db->where('deleted !=', 'NULL')->delete('storedt_products_variances');		
		
		$this->session->set_flashdata('success','Variations Cleaned : OK');
		redirect('admin/maintenance');				
	}

    public function check_zones()
    {

		//just check if the store is propperly installed
		if(!$this->db->table_exists('storedt_products'))
		{
			redirect('admin/maintenance');	
		}
		    	
    	$this->load->model('store/zones_m');

    	$info = $this->zones_m->info();

    	if(count($info) > 0)
    	{
			// Build Template
			$this->template
					->enable_parser(TRUE)
					->set('title','Products Variations without Zones')
					->set('count',count($info))					
					->set('info',$info)
					->build("admin/zone_info");
		}
		else
		{
			$this->session->set_flashdata('success','All variances have zones');
			redirect('admin/maintenance');			
		}
    }

}