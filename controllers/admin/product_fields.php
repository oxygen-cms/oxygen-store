<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Product_fields extends Admin_Controller
{

	protected $section = 'product_fields';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');

		$this->data = new stdClass();
	}

	public function index()
	{

		$production_mode = TRUE;
		$limit = 20;
		$buttons = array(
			array(
				'url'		=> 'admin/store/product_fields/edit/-assign_id-', 
				'label'		=> $this->lang->line('global:edit'),
				'locked'    => $production_mode // hide delete button for locked fields
			),
			array(
				'url'		=> 'admin/store/product_fields/delete/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> TRUE,
				'locked'    => $production_mode // hide delete button for locked fields
			),			
		);




		/*'public'*/
		//$ommit = ['id', 'slug', 'deleted', 'views', 'featured', 'searchable',  'name', 'type_id', 'type_slug'];
		$ommit = ['has_attributes'];
		//$ommit = [];

		$this->template->title('Product Fields');

		$this->streams->cp->assignments_table(
								'products',
								'nc_products',
								$limit,
								'admin/store/product_fields',
								TRUE,
								array('buttons' => $buttons),
								$ommit);
	}


	/**
	 * Create a new profile field
	 *
	 * @access 	public
	 * @return 	void
	 */
	public function create()
	{
		$extra['title'] 		= lang('streams:new_field');
		$extra['show_cancel'] 	= TRUE;
		$extra['cancel_uri'] 	= 'admin/store/product_fields/';


		$this->streams->cp->field_form('products', 'nc_products', 'new', 'admin/store/product_fields', null, array(), true, $extra);
	}

	// --------------------------------------------------------------------------

	/**
	 * Delete a profile field
	 *
	 * @access 	public
	 * @return 	void
	 */
	public function delete()
	{
		if ( ! $assign_id = $this->uri->segment(5))
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('user:profile_delete_failure'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('user:profile_delete_success'));			
		}
	
		redirect('admin/store/product_fields');
	}

	// --------------------------------------------------------------------------

	/**
	 * Edit a profile field
	 *
	 * @access 	public
	 * @return 	void
	 */
	public function edit()
	{
		if ( ! $assign_id = $this->uri->segment(5))
		{
			show_error(lang('streams:cannot_find_assign'));
		}

		$extra['title'] 		= lang('streams:edit_field');
		$extra['show_cancel'] 	= true;
		$extra['cancel_uri'] 	= 'admin/store/product_fields/';

		$this->streams->cp->field_form('countries', 'nc_zones', 'edit', 'admin/store/product_fields/', $assign_id, array(), true, $extra);
	}


	public function entries($type='list',$stream_id=0,$entry_id=0)
	{

		$id = $this->db->where('stream_slug','products')->where('stream_namespace','nc_products')->get('data_streams')->row();
		$id = $id->id;

		$this->load->driver('Streams');
		$this->load->config('streams/streams');
		$this->load->helper('streams/streams');

		$this->data->stream = new ViewObject();
		$this->data->stream->id = $this->data->stream_id = $id ;
		$this->data->stream->stream_name 		= 'products';
		$this->data->stream->stream_slug 		= 'products';
		$this->data->stream->stream_namespace 	= 'nc_products';

		switch ($type) 
		{
			case 'list':
				$this->entries_list();
				break;
			case 'edit':
				$this->entries_edit($entry_id);
				break;
			case 'delete':
				$this->entries_delete();
				break;				
			default:
				$this->entries_list();
				break;
		}
	}	


	/**
	 * List all the entries
	 */
	private function entries_list()
	{

		$pagination_uri = 'admin/store/product_fields/entries/'.$this->data->stream->id;
	
 		// -------------------------------------
		// Get Streams Entries Table
		// -------------------------------------

		$extra = array(
				'title'		=> $this->data->stream->stream_name,
				'buttons'	=> array(
					array(
						'label' 	=> lang('global:edit'),
						'url'		=> 'admin/store/product_fields/entries/edit/'.$this->data->stream->id.'/-entry_id-',
						'confirm'	=> false
					),
					array(
						'label' 	=> lang('global:delete'),
						'url'		=> 'admin/store/product_fields/entries/delete/'.$this->data->stream->id.'/-entry_id-',
						'confirm'	=> true
					)
				)
			);

		$this->streams->cp->entries_table(
								$this->data->stream->stream_slug,
								$this->data->stream->stream_namespace,
								Settings::get('records_per_page'),
								$pagination_uri,
								true,
								$extra);	
	}	

	private function entries_edit($entry_id)
	{
		$this->data->stream->stream_slug 		= 'storedt_products';
		$extra = array(
			'return' 			=> 'admin/store/product_fields/entries/',
			'success_message' 	=> $this->lang->line('streams:edit_entry_success'),
			'failure_message'	=> $this->lang->line('streams:edit_entry_error')
		);
		// Title
		$extra['title'] = $this->data->stream->stream_name;
		if ( ! $entry_id)
		{
			show_error('streams:invalid_row');
		}
		$this->streams->cp->entry_form($this->data->stream, 'nc_products', 'edit', $entry_id, true, $extra);
	}

	private function entries_delete()
	{
	}	

}