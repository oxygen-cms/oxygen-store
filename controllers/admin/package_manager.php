<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Package_manager extends Admin_Controller
{

	protected $section = 'package_manager';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');

		$this->data = new ViewObject();

		$this->load->library('form_validation');
		$this->load->model('store/packages_m');

		$this->lang->load('store/store_admin_packages');
;
	}

	public function index()
	{
		role_or_die('store', 'admin_checkout');
		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/index');
	}

}