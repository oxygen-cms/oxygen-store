<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Manage extends Admin_Controller
{
	// Set the section in the UI - Selected Menu
	protected $section = 'manage';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');

		$this->data = new ViewObject();
		role_or_die('store', 'store_administrator');
		$this->lang->load('store/store_admin_manage');
	}

	public function index()
	{

		if($input = $this->input->post())
		{
			if($this->input->post('btnAction'))
			{
				//update the settings
				$this->save($input, $this->input->post('btnAction'));
				redirect(NC_ADMIN_ROUTE.'/manage');
			}
		}

		$this->load->model('settings/settings_m');
		$arr = $this->settings_m->where('module','store')->where('is_gui',false)->order_by('order','asc')->get_all();

		$this->template->title($this->module_details['name'])
			->set('thesettings',$arr)
			->build('admin/manage/main');
	}

	private function save($input, $_action ='save')
	{
		$input = $this->input->post();
		unset($input['btnAction']);

		foreach($input as $key => $value)
		{
			Settings::set($key,$value);
		}

		$this->session->set_flashdata(JSONStatus::Success,'Settings saved');

		if($_action =='save_exit')
		{
			redirect(NC_ADMIN_ROUTE.'/');
		}
	}

}