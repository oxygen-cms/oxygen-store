<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Home extends Admin_Controller
{

	protected $section = 'dashboard';

	public function __construct()
	{
		parent::__construct();
		Events::trigger('STOREVT_ShopAdminController');
	}

	/**
	 */
	public function index()
	{
		
		//check for save
		if($input = $this->input->post()) {
			$this->_index_save($input);
		}
		else {
			//
		}

		//load after save, unless save redirects
		$this->_index_load();
	
	}

	private function _index_load()
	{
		

		$this->load->model('store/store_home_m');

		$row = $this->db->where('page','home')->order_by('id','desc')->get('storedt_store_home')->row();

		//hmm, we dont have a page
		if($row==null) {
			//lets create
			$this->store_home_m->init();
			$row = $this->db->where('page','home')->order_by('id','desc')->get('storedt_store_home')->row();
		}

		$row->options = json_decode($row->options);		

		$this->template
				->title($this->module_details['name'])
				->enable_parser(true)	
				->append_metadata($this->load->view('fragments/wysiwyg',null, TRUE))	
				->build('admin/home/home', $row);				
	}

	private function _index_save($input)
	{

		$this->load->model('store/store_home_m');

		$row = $this->db->where('page','home')->order_by('id','desc')->get('storedt_store_home')->row();
		//hmm, we dont have a page
		if($row==null) {
			//lets create
			$this->store_home_m->init();
		}	

		//prepare options
		$ignore_data = ['updated','created','content','id','theme_layout','options'];
		$options = [];

		foreach($input as $key=>$value) {
			if(isset($ignore_data[$key])) {
				//lets ignor it
			}
			else {
				$options[$key]=$value;
			}
		}

		//truncate table
		//comment out next line if you want to keep records of data.
		//$this->truncate_if_exist('storedt_store_home');


		//dont double up
		unset($options['id']);
		unset($options['content']);
		unset($options['theme_layout']);
		unset($options['btnAction']);
		unset($input['btnAction']);

		//now assign to option
		$input['options']=$options;

		$this->store_home_m->save($input['id'],$input);
	}	

	protected function truncate_if_exist($table_name)
	{
        if($this->db->table_exists($table_name) )
        {
            $this->db->truncate($table_name);
        }
	}	
}