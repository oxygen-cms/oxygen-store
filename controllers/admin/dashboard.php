<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Dashboard extends Admin_Controller
{

	protected $section = 'dashboard';

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');

		$this->data = new ViewObject();

	}


	public function index()
	{
		redirect(NC_ADMIN_ROUTE.'/home');
	}

	/**
	 * api for charting graphs
	 */
    public function stats($chart='orders', $limit = 7)
    {
    	$this->load->model('store/statistics_m');
        if ($this->input->is_ajax_request())
        {
            $this->data = $this->statistics_m->get_period($limit, $chart );
            echo json_encode($this->data);die;
        }
        //no?... get outta here
        redirect(NC_ROUTE);
    }


}