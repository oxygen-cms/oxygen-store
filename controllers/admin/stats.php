<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Stats extends Admin_Controller
{

	protected $section = 'dashboard';

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');
		$this->data = new ViewObject();
	}


	public function index()
	{

	}

	/**
	 * api for charting graphs
	 */
    public function qtrend($days=90)
    {
        
       //if ($this->input->is_ajax_request())  {
        	//$res = [ ['1','50'], ['2','2'] ];

        	$this->load->model('store/statistics_m');
            $data = $this->statistics_m->quartlyTrend($days);
            echo json_encode($data);die;
        //

        //no?... get outta here
        redirect(NC_ROUTE);
    }

    //monthly
    public function msales($days=90)
    {
        
        //if($this->input->is_ajax_request())  {
            $this->load->model('store/statistics_m');
            $data = $this->statistics_m->monthlySales($days);
            echo json_encode($data);die;
        //}

        //no?... get outta here
        redirect(NC_ROUTE);
    }

    //most viewed
    public function mostviewed($days=3)
    {
        
        if($this->input->is_ajax_request())  {
            $this->load->model('store/statistics_m');
            $data = $this->statistics_m->mostViewed($days);
            echo json_encode($data);die;
        }

        //no?... get outta here
        redirect(NC_ROUTE);
    }

    public function orders($chart='orders', $limit = 7)
    {

        $this->load->model('store/statistics_m');
        if ($this->input->is_ajax_request())
        {
            $this->data = $this->statistics_m->get_period($limit, $chart );
            echo json_encode($this->data);die;
        }
        //no?... get outta here
        redirect(NC_ROUTE);
    }   


    public function avg_order() {
        
        $this->load->model('store/statistics_m');
        if ($this->input->is_ajax_request()) {
            $this->data = $this->statistics_m->monthlyAverageSale();
            echo json_encode($this->data);die;
        }
        //no?... get outta here
        redirect(NC_ROUTE);

    } 


    public function test() {

        $this->load->model('store/statistics_m');

        echo $this->data = $this->statistics_m->activeCarts();

    }
}