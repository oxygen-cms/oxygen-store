<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Attributes extends Admin_Controller
{

	protected $section = 'attributes';

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');

		//$this->input->is_ajax_request() OR redirect(NC_ADMIN_ROUTE);
		$this->lang->load('store/store_admin_products_types');		
		$this->load->model('store/admin/attributes_m');
		$this->load->library('store/eav_library');
		$this->data = new ViewObject();
	}


	public function ajax_get($product_id, $variance_id)
	{
		$this->load->model('store/e_attributes_m');
		$this->load->model('store/admin/products_variances_admin_m');
		$this->load->model('store/admin/products_admin_m');
		$this->data->variance = $this->products_variances_admin_m->get($variance_id);
		$this->data->forms = $this->e_attributes_m->get_by_variance_id($variance_id);
		$this->data->product = $this->products_admin_m->get($product_id);

		// Ajax response
		if($this->input->is_ajax_request()) $this->template->set_layout(false);		

		$has_attr = (count($this->data->forms))?true:false;
		
		$this->template
				->enable_parser(true)		
				->set('has_attr', $has_attr)
				->build('admin/products/modals/edit_attributes',$this->data);
	}

	public function post_save($product_id,$variance_id)
	{
		$this->load->model('store/admin/products_variances_admin_m');
		$this->load->model('store/e_attributes_m');

		$input = $this->input->post();

		$len = strlen ('e_attribute_id_');

		$varaince_id = 0;

		$tmp_name = '';
		$first = true;
		
		foreach ($input as $key => $value) 
		{
			$v = substr($key, 0, $len );
			$_pre_id = substr($key, $len );

			if($v == 'e_attribute_id_')
			{
				$id = (int) $_pre_id;
				if($id > 0)
				{
					$attr = $this->e_attributes_m->get($id);
					$tmp_name .= (($first)?'':'&') . $attr->e_label . '=' . $value ;
					$this->e_attributes_m->set_value($id, $value);
					$first = false;
				}
			}
		}

		//now rename the variance
		if(isset($input['rename']))
		{
			$update = ['name'=>$tmp_name,'id'=>$input['variance_id'] ];
			$this->products_variances_admin_m->edit($product_id , $input['variance_id'], $update);	
		}

		redirect(NC_ADMIN_ROUTE.'/product/edit/'.$product_id.'#price-tab');
	}

	public function ajax_set($product_id, $variance_id)
	{
		//update
		$this->load->model('store/admin/products_variances_admin_m');
		$returnArray = $this->products_variances_admin_m->edit_shipping( $variance_id , $this->input->post() );
		echo json_encode($returnArray);die;	
	}


	/**
	 * product field maintenance
	 *
	 */

	public function _manage_setup()
	{
        
		$this->lang->load('store/store_admin_products_types');		
		$this->load->model('store/admin/attributes_m');
		$this->load->library('store/eav_library');
		$this->data = new ViewObject();
	}


	public function index()
	{
		$this->filter(0);
	}

	public function filter($offset=0)
	{
		$this->_manage_setup();		
		$limit = 10;

		$total_items =  $this->attributes_m->count_all();

		$this->data->pagination = create_pagination('admin/store/attributes/filter', $total_items, $limit, 5);

		$this->data->attributes =  $this->attributes_m->limit($this->data->pagination['limit'])->offset($this->data->pagination['offset'])->get_all();  

        $this->template
					->enable_parser(TRUE)
					->build('store/admin/attributes/list', $this->data);		
	}

	public function create()
	{

		$this->_manage_setup();
		
		if($input = $this->input->post())
		{
			$id = $this->attributes_m->create($input);

			if( ($input['btnAction'] =='save_create') AND ($id) )
			{
				$this->session->set_flashdata(JSONStatus::Success, "Attribute `{$input['name']}` Created");
				redirect('admin/store/attributes/create');
			}
			redirect('admin/store/attributes/');
		}

		$this->template
				->set('name','')
				->enable_parser(TRUE)
				->title($this->module_details['name'])
				->build('store/admin/attributes/form');	
	}

	public function edit($id)
	{
		$this->_manage_setup();
		if($input = $this->input->post())
		{
			$id = $this->attributes_m->edit($input);

			if( ($input['btnAction'] =='save_create') AND ($id) )
			{
				$this->session->set_flashdata(JSONStatus::Success, "Attribute `{$id}` - `{$input['name']}` Updated");
				redirect('admin/store/attributes/create');
			}

			redirect('admin/store/attributes/');
		}
		$att =$this->attributes_m->get($id);
		$this->template
				->set('name',$att->name)
				->set('id',$att->id)
				->enable_parser(TRUE)
				->title($this->module_details['name'])
				->build('store/admin/attributes/form');	
	}

	public function delete($id)
	{
		$this->_manage_setup();
		$this->attributes_m->delete($id);
		redirect('admin/store/attributes/');		
	}

	public function install_attribute_set($type='clothing')
	{
		$this->_manage_setup();

		$this->eav_library->install_attribute_set($type);
		redirect('admin/store/attributes/');
	}


}