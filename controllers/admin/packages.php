<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Packages extends Admin_Controller
{

	protected $section = 'packages';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');
		

		$this->data = new ViewObject();

		$this->load->library('form_validation');
		$this->load->model('store/admin/packages_admin_m');
		$this->lang->load('store/store_admin_packages');
		$this->config->load('store/admin/'.NC_CONFIG);

        $this->template->append_js('store::admin/packages.js');
	}

	/**
	 * List all Installed and Not installed gateways
	 */
	public function index()
	{
        //
        // We are no longer supporting package list
        // redirect rto groups
        //redirect(NC_ADMIN_ROUTE.'/packages_groups');
        //
        // end

		role_or_die('store', 'admin_checkout');
		$this->data->packages = $this->packages_admin_m->get_all_available();
		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/packages/list', $this->data);
	}

	/**
	 * Edit an existing package and validate
	 *
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function edit($id,$metrictype='cm')
	{

		role_or_die('store', 'admin_checkout');

		$this->data->id = $id;

		if($input = $this->input->post())
		{
			$do_redirect = false;


			if(isset($input['btnAction'])) {
				if($input['btnAction']=='save_exit') {
					$do_redirect = true;
				}
			}
			$this->form_validation->set_rules($this->packages_admin_m->_create_validation_rules);

			if( $this->form_validation->run() )
			{
				if($this->data = $this->packages_admin_m->save($id,$input)) {
					//set flash message
					$this->session->set_flashdata('success','Package saved.');

					if($do_redirect ) {
						//package_list
						//edit 'group edit'
						redirect(NC_ADMIN_ROUTE.'/packages_groups/package_list/'.$input['pkg_group_id']);
					}
					//else
					redirect(NC_ADMIN_ROUTE.'/packages/edit/'.$id);
				}
				else {
					$this->session->set_flashdata('error','Package did not save.');
				}

			}
			else
			{
				foreach ($this->packages_admin_m->_create_validation_rules AS $rule)
					$this->data->{$rule['field']} = $this->input->post($rule['field']);
			}
		}
		else
		{
			$this->data = $this->packages_admin_m->get($id);
		}

		//we need to get a list of packages
		$this->load->model('store/admin/packages_groups_admin_m');
		$this->data->available_groups = $this->packages_groups_admin_m->get_for_admin();

		$this->template
				->set('metrictype',$metrictype)
				->title($this->module_details['name'])
				->build('admin/packages/packages/edit',$this->data);
	}

	/**
	 * Create a new package
	 *
	 * On post back check and validate input
	 *
	 * @return [type] [description]
	 */
	public function create()
	{
		role_or_die('store', 'admin_checkout');

		$this->checkForPackageGroups();
		$this->data = new ViewObject();

		if($input = $this->input->post())
		{
			$this->form_validation->set_rules($this->packages_admin_m->_create_validation_rules);

			if( $this->form_validation->run() )
			{
				if($this->packages_admin_m->create($input))
				{
					$this->session->set_flashdata(JSONStatus::Success,'Package has been created.');
					redirect(NC_ADMIN_ROUTE.'/packages');
				}
			}
		}

		//we need to get a list of packages
		$this->load->model('store/admin/packages_groups_admin_m');
		$this->data->available_groups = $this->packages_groups_admin_m->get_for_admin();

		foreach ($this->packages_admin_m->_create_validation_rules AS $rule)
			$this->data->{$rule['field']} = $this->input->post($rule['field']);

		$this->template
				->title($this->module_details['name'])
				->build('admin/packages/packages/edit',$this->data);
	}

	public function quick_add($group_id,$code='bx1')
	{

		role_or_die('store', 'admin_checkout');

		$this->checkForPackageGroups();

		$input = $this->boxList[$code];



		$to_insert = 
		[
			'name' 		  		=> $input['name'],
			'code' 		  		=> '',
			'pkg_group_id' 		=> $group_id,
			'height' 		  	=> $input['height'],
			'width' 		  	=> $input['width'],
			'length' 		  	=> $input['length'],
			'outer_height' 		=> $input['height'],
			'outer_width' 		=> $input['width'],
			'outer_length' 		=> $input['length'],
			'max_weight' 		=> $input['max_weight'],
			'cur_weight' 		=> $input['cur_weight'],
            'created_by'    	=> $this->current_user->id,
            'created'       	=> date("Y-m-d H:i:s"),
            'updated'       	=> date("Y-m-d H:i:s"),
			'deleted' 		  	=> NULL,
			'core'				=> 0,
            'addi_cost_unit'	=> $input['addi_cost_unit'], 
            'max_items'			=> $input['max_items'], 

		];



		if($this->packages_admin_m->create($to_insert))
		{
			$this->session->set_flashdata(JSONStatus::Success,'Package has been created.');
		}
		else
		{
			$this->session->set_flashdata(JSONStatus::Error,'Package has NOT been created.');
		}

		redirect(NC_ADMIN_ROUTE.'/packages_groups/package_list/'.$group_id);
	
		
	}
	public function duplicate($id,$mode='copy')
	{
		role_or_die('store', 'admin_checkout');


		if($new_id = $this->packages_admin_m->duplicate($id))
		{
			$this->session->set_flashdata(JSONStatus::Success,'Package has been copied.');

			if($mode=='edit')
				redirect(NC_ADMIN_ROUTE.'/packages/edit/'.$new_id);

			if($package = $this->packages_admin_m->get($id))
			{
				redirect(NC_ADMIN_ROUTE.'/packages_groups/package_list/'.$package->pkg_group_id);
			}
		}

		$this->session->set_flashdata(JSONStatus::Error,'Failed to duplicate package..');

		redirect(NC_ADMIN_ROUTE.'/packages_groups');
	}

	/**
	 * Delete a package
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id)
	{
		role_or_die('store', 'admin_checkout');

		if($package = $this->packages_admin_m->get($id))
		{
			$pkg_group = $package->pkg_group_id;

			if($this->packages_admin_m->delete($id))
				$this->session->set_flashdata(JSONStatus::Success,'Package has been removed.');
			else
				$this->session->set_flashdata(JSONStatus::Error,'Unable to delete package.');

			redirect(NC_ADMIN_ROUTE.'/packages_groups/package_list/'.$package->pkg_group_id);

		}
		else
		{
			redirect(NC_ADMIN_ROUTE.'/packages_groups/');
		}

	}

	/**
	 * [checkForPackageGroups description]
	 * @return [type] [description]
	 */
	private function checkForPackageGroups()
	{
		$this->load->model('store/admin/packages_groups_admin_m');
		$this->data->available_groups = $this->packages_groups_admin_m->get_for_admin();

		if(count($this->data->available_groups) <= 0)
		{
			$this->session->set_flashdata(JSONStatus::Error, "You must first create a package group.");
			redirect(NC_ADMIN_ROUTE.'/packages');
		}
	}




	private $boxList = [

		//'bx1' => [Width: 220mm   Depth: 160mm   Height: 77mm']
		'bx1' => 
				[
					'name'=>'BX1',
					'width'=>22.0,
					'length'=>16,
					'height'=>7.7,
					'max_weight'=>15,
					'cur_weight'=>0.250,
					'max_items'=>0,	
					'addi_cost_unit'=>0,				
				],

		//Width: 310mm   Depth: 225mm   Height: 102mm
		'bx2' => 
				[
					'name'=>'BX2',
					'width'=>31.0,
					'length'=>22.5,
					'height'=>10.2,
					'max_weight'=>15,
					'cur_weight'=>0.350,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],

		//Width: 430mm   Depth: 305mm   Height: 140mm
		'bx4' => 
				[
					'name'=>'BX4',
					'width'=>43.0,
					'length'=>30.5,
					'height'=>14.0,
					'max_weight'=>15,
					'cur_weight'=>0.350,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	
		'sat5' => 
				[
					'name'=>'500g (AustPost) Satchel',
					'width'=>35.5,
					'length'=>3,
					'height'=>22.0,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	
		'sat3k' => 
				[
					'name'=>'3KG (AusPost) Satchel',
					'width'=>40.5,
					'length'=>3,
					'height'=>31.0,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	
		'sat5k' => 
				[
					'name'=>'5KG (AusPost) Satchel',
					'width'=>51.0,
					'length'=>3,
					'height'=>43.5,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	
		//229 x 324 mm				
		'envc4' => 
				[
					'name'=>'C5 Envelope',
					'width'=>22.9,
					'length'=>2,
					'height'=>32.4,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],					
		//500g and 20mm ,162 x 229 mm
		'envc5' => 
				[
					'name'=>'C5 Envelope',
					'width'=>22.9,
					'length'=>2,
					'height'=>16.2,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	



		//114 x 162 mm
		'envc6' => 
				[
					'name'=>'C5 Envelope',
					'width'=>11.4,
					'length'=>2,
					'height'=>16.2,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	


		//229 x 324 mm				
		'envc4b' => 
				[
					'name'=>'C5 Box',
					'width'=>22.9,
					'length'=>4,
					'height'=>32.4,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],					
		//500g and 20mm ,162 x 229 mm
		'envc5b' => 
				[
					'name'=>'C5 Box',
					'width'=>22.9,
					'length'=>4,
					'height'=>16.2,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],	



		//114 x 162 mm
		'envc6b' => 
				[
					'name'=>'C5 Box',
					'width'=>11.4,
					'length'=>4,
					'height'=>16.2,
					'max_weight'=>0.5,
					'cur_weight'=>0.01,
					'max_items'=>0,	
					'addi_cost_unit'=>0,											
				],				
	];


}