<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Products_types extends Admin_Controller
{

	protected $section = 'producttypes';
	private $data;

	public function __construct()
	{

		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');
        

		// load lang file
		$this->lang->load('store/store_admin_products_types');

		$this->refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'store';

		$this->data = new StdClass;

		$this->load->model('store/admin/products_types_admin_m');
		$this->load->library('store/eav_library');
		$this->load->driver('Streams');

        $this->template->append_js('store::admin/products_types.js');	                    
	}

	/**
	 * List all items
	 */
	public function index()
	{

		$params = array(
		    'stream'    => 'products_types',
		    'namespace' => 'nc_products_types'
		);

		$data = $this->streams->entries->get_entries($params);
		$this->template->title($this->module_details['name'])			
				->build('store/admin/products_types/list', $data);		

	}

	/**
	 * Create a new Brand
	 */
	public function create()
	{
        $this->load->driver('Streams');  
		if($input = $this->input->post())
		{
			if($id = $this->products_types_admin_m->create($input))
			{
				$x = $this->products_types_admin_m->get($id);
				//var_dump($x->slug);die;
				//create the stream with the slug

				$this->streams->streams->add_stream($x->name, 'product_type_'.$x->slug, 'nc_products', 'storedt_', '');

				redirect('admin/store/products_types/edit/'.$id);
			}
			//else
		}

		$this->template
				->set('name','')
				->set('default',0)
				->enable_parser(TRUE)
				->title($this->module_details['name'])
				->build('store/admin/products_types/create');		
	}


	public function edit($id)
	{
		$this->load->model('store/admin/attributes_m');

		$has_active = FALSE;

		if($row = $this->db->where('type_id',$id)->get('storedt_products')->row())
		{
			$has_active = true;			
		}
		
		$has_products = false;
		if($row = $this->db->where('deleted',NULL)->where('type_id',$id)->get('storedt_products')->row())
		{
			$has_products = true;			
		}


		if($input = $this->input->post())
		{
			$redir = 'admin/store/products_types/edit/'.$id;

			if(isset($input['btnAction']))
			{
				if($input['btnAction']=='save_exit')
				{
					$redir = 'admin/store/products_types/';
				}				
			}


			$props = (isset($input['properties']))? $input['properties'] : [] ;
			$att_props =  [];
			foreach($props as $key => $p)
			{
				$att_props[] = $this->attributes_m->get($p);
			}

			$input['properties'] = $att_props;
			$status = $this->products_types_admin_m->edit($id,$input);
			redirect($redir);
		}

		$type = $this->products_types_admin_m->get($id);

		$this->load->model('store/admin/attributes_m');

		$available_types = $this->attributes_m->get_dropdown_array();

		$set_types = $this->products_types_admin_m->deserialize_properties($type->properties);



		
		//var_dump($set_types );die;

		//echo form_multiselect('properties[]', $available_types, set_value('properties[]',$set_types) ) ;die;

		$this->template
				->set('available_types',$available_types)
				->set('set_types',$set_types)
				->set('has_active',$has_active)
				->set('has_products',$has_products)
				->enable_parser(TRUE)
				->set('type',$type)
				->title($this->module_details['name'])
				
				->build('store/admin/products_types/edit');	
	}


	public function delete($id)
	{

        $this->load->driver('Streams');  


		$this->load->model('store/admin/products_types_admin_m');

		if( $p_type = $this->products_types_admin_m->get($id) )
		{

			if( $this->products_types_admin_m->delete($id) )
			{

				$this->session->set_flashdata(JSONStatus::Success, "Type removed");

				$stream_slug = 'product_type_'.$p_type->slug;
				if($this->db->table_exists($stream_slug))
				{	
					$this->streams->streams->delete_stream($stream_slug, 'nc_products');
				}
			}
			else
			{
				$this->session->set_flashdata(JSONStatus::Error, "Could not delete this type");
			}
		}
		//$this->session->set_flashdata(JSONStatus::Success, "Removed product type.");
		//$this->streams->entries->delete_entry($id, 'products_types', 'nc_products_types');
		redirect('admin/store/products_types');
	}
}