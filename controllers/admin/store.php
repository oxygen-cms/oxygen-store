<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Store extends Admin_Controller
{

	protected $section = '';

	public function __construct()
	{
		parent::__construct();
        redirect(NC_ADMIN_ROUTE.'/home');
	}

	/**
	 */
	public function index()
	{

	}
	

}