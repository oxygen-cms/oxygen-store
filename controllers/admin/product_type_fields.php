<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Product_type_fields extends Admin_Controller
{

	protected $section = 'product_type_fields';
	private $data;

	public function __construct()
	{
		parent::__construct();
        Events::trigger('STOREVT_ShopAdminController');
        



		$this->data = new stdClass();
	}

	public function index()
	{

		$production_mode = TRUE;
		$limit = 20;
		$buttons = array(
			array(
				'url'		=> 'admin/store/product_type_fields/edit/-assign_id-', 
				'label'		=> $this->lang->line('global:edit'),
				'locked'    => $production_mode // hide delete button for locked fields
			),
			array(
				'url'		=> 'admin/store/product_type_fields/delete/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> TRUE,
				'locked'    => $production_mode // hide delete button for locked fields
			),			
		);

		if($type_slug = $this->uri->segment(5))
		{
			$stream_slug = 'product_type_'.$type_slug;

			/*'public'*/
			//$ommit = array('id', 'slug', 'deleted', 'views', 'featured', 'searchable',  'name', 'type_id', 'type_slug');
			$ommit = array();

			$this->template->title('Product Fields');

			$this->streams->cp->assignments_table(
									'products',
									'nc_products',
									$limit,
									'admin/store/product_type_fields',
									TRUE,
									array('buttons' => $buttons),
									$ommit);
		}
		else
		{
			//luist all the available product types
			echo "You must select a Product Type";die;
		}
	}


	public function fields($type_slug)
	{

		$url = 'admin/store/product_type_fields/create/'.$type_slug;
		$url2 ='admin/store/product_type_fields/fields/'.$type_slug;
			
		$shortcuts = 
		[
			['name' => 'global:add', 'uri' => $url,'class' => '', 'label'=>'' ]
		];
		add_template_section($this,'product_type_fields','Fields',$url2,$shortcuts);

		//luist all the available product types
		$stream_slug = 'product_type_'.$type_slug;

		$production_mode = TRUE;
		$limit = 20;
		$buttons = array(
			array(
				'url'		=> 'admin/store/product_type_fields/edit/-assign_id-', 
				'label'		=> $this->lang->line('global:edit'),
				'locked'    => $production_mode // hide delete button for locked fields
			),
			array(
				'url'		=> 'admin/store/product_type_fields/delete/-assign_id-',
				'label'		=> $this->lang->line('global:delete'),
				'confirm'	=> TRUE,
				'locked'    => $production_mode // hide delete button for locked fields
			),			
		);

		$ommit = array();

		$this->template->title('Product Fields');

		$this->streams->cp->assignments_table(
								$stream_slug,
								'nc_products',
								$limit,
								'admin/store/product_type_fields/fields'.$type_slug,
								TRUE,
								array('buttons' => $buttons),
								$ommit);
	}


	/**
	 * Create a new profile field
	 *
	 * @access 	public
	 * @return 	void
	 */
	public function create($type_slug)
	{
	
		$stream_slug = 'product_type_'.$type_slug;

		$extra['title'] 		= lang('streams:new_field');
		$extra['show_cancel'] 	= TRUE;
		$extra['cancel_uri'] 	= 'admin/store/products_types_fields/fields/'.$type_slug;


		$success_redirect ='admin/store/product_type_fields/fields/'.$type_slug;


		$this->streams->cp->field_form($stream_slug, 'nc_products', 'new', $success_redirect, null, [], true, $extra);
	}

	// --------------------------------------------------------------------------

	/**
	 * Delete a profile field
	 *
	 * @access 	public
	 * @return 	void
	 */
	public function delete()
	{
		if ( ! $assign_id = $this->uri->segment(5))
		{
			show_error(lang('streams:cannot_find_assign'));
		}
	
		// Tear down the assignment
		if ( ! $this->streams->cp->teardown_assignment_field($assign_id))
		{
		    $this->session->set_flashdata('notice', lang('user:profile_delete_failure'));
		}
		else
		{
		    $this->session->set_flashdata('success', lang('user:profile_delete_success'));			
		}
	
		redirect('admin/store/products_types_fields');
	}

	// --------------------------------------------------------------------------

	/**
	 * Edit a profile field
	 *
	 * @access 	public
	 * @return 	void
	 */
	public function edit()
	{
		if ( ! $assign_id = $this->uri->segment(5))
		{
			show_error(lang('streams:cannot_find_assign'));
		}

		$extra['title'] 		= lang('streams:edit_field');
		$extra['show_cancel'] 	= TRUE;
		$extra['cancel_uri'] 	= 'admin/store/products_types_fields/';

		$this->streams->cp->field_form('countries', 'nc_zones', 'edit', 'admin/store/products_types_fields/', $assign_id, [], TRUE, $extra);
	}

}