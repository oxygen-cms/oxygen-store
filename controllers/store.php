<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Store extends Public_Controller
{
	private $data;

	public function __construct()
	{
		parent::__construct();	
		Events::trigger('STOREVT_ShopPublicController');

		$this->data = new ViewObject();
		$this->template->set_breadcrumb('Home', '/');
	}


	/**
	 * The Store home pae is driven by pages module, you need to set up a store page slug
	 * This wil
	 * @param  string $param [description]
	 * @return [type]        [description]
	 */
	public function index($param = '')
	{
		Settings::get('storst_open_status') OR redirect( NC_ROUTE . '/closed');

		$this->setLayoutForShop('store_home.html');
		
		//redirect(NC_ROUTE);

		$this->template
			//->title($title, 'Closed')
			->set('title','Store Home')
			->set('body','')
			//->set_breadcrumb($title.' [CLOSED]')
			->build('special/home');		
	}


	/*similar to shop home, first checks if shop is open, so it can redirect away*/
	public function closed($p='')
	{
 
		(! Settings::get('storst_open_status') ) OR redirect( NC_ROUTE );

		$this->setLayoutForShop('store_closed.html');

		$title = Settings::get('storst_storename');
		$body = Settings::get('storst_closed_reason');


		$this->template
			->title($title, 'Closed')
			->set('title',$title)
			->set('body',$body)
			->set_breadcrumb($title.' [CLOSED]')
			->build('special/closed', $this->data);
	}

	/**
	 * shop_home
	 */
	private function setLayoutForShop($preffered='store_home.html')
	{
		if($this->template->layout_exists($preffered))
		{
			$this->template->set_layout($preffered);
		}		
		elseif($this->template->layout_exists('store_home.html'))
		{
			$this->template->set_layout('store_home.html');
		}
		elseif($this->template->layout_exists('store.html'))
		{
			$this->template->set_layout('store.html');
		}	
	
	}


	/**
	 * The shop login function
	 *
	 * Point the login link to here site/shop/login and after you login you will be taken to wherever you came from.
	 *
	 * @return [type] [description]
	 */
	public function login()
	{
		// Get the page where we came from
		$url_redir = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : NC_ROUTE ;

		// Set the magic session value to redirect back
		$this->session->set_userdata('shop_force_redirect' , $url_redir );

		// Now go to login page
		redirect('users/login');
	}

}