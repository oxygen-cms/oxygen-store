<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
require_once(dirname(__FILE__) . '/shipping_options_m.php');
class Gateway_options_m extends Shipping_options_m 
{

	public $_type = 'gateway'; //shipping|gateway

	public function __construct()
	{
		parent::__construct();
	}

}