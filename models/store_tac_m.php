<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Store_tac_m extends MY_Model 
{


    public $_table = 'storedt_store_home';


    public function __construct()
    {
        parent::__construct();      
    }


    public function init()  {

        $input = 
        [
            'content'       => 'Your TaC',
            'options'       => '',
            'theme_layout'  => 'default.html'
        ];
        return $this->create($input);

    }

    public function create($input)  {

        $to_insert = 
        [
            'content'       => $input['content'],
            'page'          => 'tac',
            'created'       => date("Y-m-d H:i:s"),
            'options'       => json_encode(""),
            'theme_layout'  => 'default.html'
        ];

        if(isset($input['theme_layout'])) {
             $to_insert['theme_layout']= $input['theme_layout'];
        }        

            
        $this->db->trans_start();
        $input['id'] = $this->insert($to_insert);
        $this->db->trans_complete();

        return ($this->db->trans_status() === false) ? false : $input['id'];
    }


    public function save($id, $input)
    {
        $to_update = 
        [
            'content'       => $input['content'],
            'updated'       => date("Y-m-d H:i:s"),
        ];

        if(isset($input['theme_layout'])) {
             $to_update['theme_layout']= $input['theme_layout'];
        }      

        return $this->update($id, $to_update);
    }
}