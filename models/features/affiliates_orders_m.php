<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Affiliates_orders_m extends MY_Model
{

	public $_table = 'storedt_affiliates_orders';

	protected $_description_tags = '<b><div><strong><em><i><u><ul><ol><li><p><span><a><br><br>';

	public function __construct()
	{
		parent::__construct();
	}


	public function find($aff_id,$page='')
	{
		$row = $this->db
				->where('affiliate_id',$aff_id)
				->where('page',$page)
				->where('ip_address',$this->session->userdata('ip_address'))
				->where('date',date("Y-m-d 00:00:00"))
				->limit(1)
				->get($this->_table)
				->row();

		return ($row)?$row:false;
	}


	public function log($aff_id,$order_id='')
	{
		$to_insert = 
		[
            'affiliate_id'  => $aff_id,
            'order_id'      => $order_id,
            'time_placed'   => time(),
            'date_placed'   =>date("Y-m-d"),
		];

		if($id = $this->insert($to_insert))
		{
			return $id
		}

		return -1;
	}

}