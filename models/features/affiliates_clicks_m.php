<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Affiliates_clicks_m extends MY_Model
{

	public $_table = 'storedt_affiliates_clicks';

	protected $_description_tags = '<b><div><strong><em><i><u><ul><ol><li><p><span><a><br><br>';

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @deprecated
	 */
	public function exist($aff_id,$page='')
	{
		$items = $this
				->where('affiliate_id',$aff_id)
				->where('page',$page)
				->where('ip_address',$this->session->userdata('ip_address'))
				->where('date',date("Y-m-d 00:00:00"))
				->limit(1)
				->get_all();

		return (count($items))?true:false;
	}

	public function find($aff_id,$page='')
	{
		$row = $this->db
				->where('affiliate_id',$aff_id)
				->where('page',$page)
				->where('ip_address',$this->session->userdata('ip_address'))
				->where('date',date("Y-m-d 00:00:00"))
				->limit(1)
				->get($this->_table)
				->row();

		return ($row)?$row:false;
	}

	public function log($aff_id,$page='',$row)
	{

		$to_update = 
		[
			'affiliate_id' => $aff_id,
			'page' => $page,
			'ip_address' => $this->session->userdata('ip_address'),
			'date' => date("Y-m-d 00:00:00"), //time must be set to ZERo so we can identify a time cut off for the day
			'impressions' => $row->impressions + 1,
		];

		$id = $this->db->where('id',$row->id)->update($this->_table,$to_update);

		if($id)
		{
			return $id;
		}

		return -1;
	}
	public function log_new($aff_id,$page='')
	{

		$to_insert = 
		[
			'affiliate_id' => $aff_id,
			'page' => $page,
			'ip_address' => $this->session->userdata('ip_address'),
			'date' => date("Y-m-d 00:00:00"), //time must be set to ZERo so we can identify a time cut off for the day
			'impressions' => 1,
		];

		$id = $this->insert($to_insert);

		if($id)
		{
			return $id;
		}

		return -1;
	}





	private function getReturnObject()
	{
		$obj = array();
		$obj['status'] = false;
		$obj['message'] = 'No parameters set.';

		return $obj;
	}

	/**
	 * prepare the array so it can be used as a dropdown
	 */
	public function get_for_admin()
	{
		$return_array = array();
		$r = $this->where('deleted',NULL)->get_all();

		foreach($r as $key=>$value)
		{
			$return_array[$value->id]=$value->name;
		}

		return $return_array;

	}

}