<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Countries_m extends MY_Model
{

	public $_table = 'storedt_countries';


	public function __construct()
	{
		parent::__construct();
	}

	public function enable($id,$enable=1)
	{
		$edit['enabled'] = $enable;
		return $this->db->where('id',  $id )->update('storedt_countries', $edit);
	}
	public function disable($id)
	{
		$edit['enabled'] = 0;
		return $this->db->where('id',  $id )->update('storedt_countries', $edit);
	}	

	/*by regions*/
	public function enable_region($region = 'australia_and_oceania',$enable = true)
	{
		$edit['enabled'] = $enable;
		return $this->db->where('region',  $region )->update('storedt_countries', $edit);
	}
	public function clearall_regions()
	{
		return $this->db->update('storedt_countries', array('enabled'=>0) );
	}	


	public function get_region_dropdown($curr = NULL)
	{
		//get an array of regions
		$regions = $this->select('region')->distinct('region')->get_all();
		$oarray = [];
		foreach ($regions as $o) 
		{
			$oarray[$o->region]  = $o->region;
		}
		return form_dropdown('zone', $oarray);

	}
	public function get_regional_dropdown($curr = NULL)
	{
		$countries = $this->select('id,name,region')->get_all();
		$r=[];
		foreach($countries as $c)
		{
			$r[$c->region][$c->id]  = $c->name;
		}
		
		return form_dropdown('country_id',$r,$curr);
	}


	public function filter($limit=NULL,$offset=0, $only_active=false)
	{
		if($only_active)
		{
			$this->db->where('enabled',1);
		}
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		return $this->db->order_by('name')->offset($offset)->get('storedt_countries')->result();
	}
	
	public function filter_count($onlyactive=false)
	{
		if($onlyactive)
		{
			$this->db->where('enabled',1);
		}
				
		return $this->db->from('storedt_countries')->count_all_results();
	}	


}