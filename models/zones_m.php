<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Zones_m extends MY_Model
{

	public $_table = 'storedt_zones';


	public function __construct()
	{
		parent::__construct();
	}


	public function create($input)
	{

		if($input['default'] == 1)
		{
			$this->resetDefault();
		}

		$insert = array(
			'created'=>  date("Y-m-d H:i:s"), 
			'created_by' => $this->current_user->id, 
			'ordering_count'=>0,
			'name'=>$input['name'],
			'description'=>'',
			'default'	=> $input['default'],
		);
		
		return  $this->insert($insert);
	}
	


	public function edit($id,$input)
	{
		if($input['default'] == 1)
		{
			$this->resetDefault();
		}

		$update = array( 
			'name'=>$input['name'], 
			'default'=> $input['default'] 
		);

		return $this->update($id, $update);
	}

	public function resetDefault()
	{
		$this->db->update('storedt_zones', ['default'=>0] );
	}



	public function get_countries_by_zone($zone_id,$limit=10,$offset=0)
	{
		return	$this->db->where('zone_id',$zone_id)->limit($limit,$offset)->get('storedt_zones_countries')->result();
	}

	public function byzone_count($zone_id)
	{
		return $this->db->where('zone_id',$zone_id)->from('storedt_zones_countries')->count_all_results();
	}


	/**
	 * prepare the array so it can be used as a dropdown
	 */
	public function get_for_admin($add_mcl = true)
	{
		$return_array = [];
		$r = $this->get_all();

		if($add_mcl) 
			$return_array[0]='Default [MCL]';
		
		foreach($r as $key=>$value)
		{
			$return_array[$value->id]=$value->name;
		}
		return $return_array;
	}

	public function getDefaultID()
	{
		$row = $this->db->where('default',1)->get('storedt_zones')->row();
		if($row)
		{
			return $row->id;
		}
		return 0;
	}


	public function zone_a_country($zone_id,$country_id)
	{
		//only zone if it doesnt exist
		if(!( $this->db->where('zone_id',$zone_id)->where('country_id',$country_id)->get('storedt_zones_countries')->row()) )
		{
			if($arow = $this->db->where('id',$country_id)->get('storedt_countries')->row())
			{
				//not found
				$status = $this->db->insert('storedt_zones_countries', array('country_t'=>$arow->name, 'zone_id'=>$zone_id, 'country_id' =>$country_id, 'created' => date("Y-m-d H:i:s"),'created_by'  => $this->current_user->id ) );
				if($status)
				{
					return true;
				}
			}
		}

		return false;

	}

	public function info()
	{
		$p = [];
		$products = $this->db->where('deleted',NULL)->get('storedt_products')->result();

		if(count($products))
		{
			foreach($products as $prod)
			{
				$p[] = $prod->id;
			}

			$result = $this->db->where('zone_id <=',0)->where('deleted',NULL)->where_in('product_id',$p)->get('storedt_products_variances')->result();

			return $result;
		}

		return [];
	}

}