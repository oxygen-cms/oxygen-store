<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Statistics_m extends MY_Model
{

	public $_table = 'storedt_products';


	public function __construct()
	{
		parent::__construct();
	}

    /**
     * Get the total revenue of the store for the N number of days from now to in the past
     * return value should be a float.
     */
    public function getStoreRevenue($days=180)
    {

        $dates = [];

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('SUM(total_totals) AS total', false);
        $this->db->select("FROM_UNIXTIME(`order_date`, '%Y-%m-%d') AS date", false);
        $this->db->where('order_date >', time()-$period);
        $this->db->group_by('date', false);
        $result = $this->db->get('storedt_orders')->result();

        //var_dump($result);die;

        $grand = 0;

        foreach ($result as $item)
        {
            $grand += $item->total;
        }
        return $grand;
    }

    public function getAverageOrdersByCustomers($days=180) {

        //$days = 7;
        $dates = [];

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->where('order_date >', time()-$period);
        $this->db->where('paid_date >', time()-$period);
        $result1 = $this->db->get('storedt_orders')->row();

        $this->db->select('COUNT(DISTINCT(user_id)) AS total', false);
        $this->db->where('order_date >', time()-$period);
        $this->db->where('paid_date >', time()-$period);
        $result2 = $this->db->get('storedt_orders')->row();

        $total_orders_in_period = $result1->total;
        $total_customers_in_period = $result2->total;

        //protect div by ZERO
        if($total_orders_in_period == 0 OR $total_customers_in_period == 0)
            return '0 o/pc';

        return number_format( ($total_orders_in_period / $total_customers_in_period),3). ' o/pc';
    }

    /**
     * Get the total revenue of the store of all time.
     * return value should be a float.
     */
    public function getStoreTotalRevenue()
    {
        $dates = [];

        $this->db->select('SUM(total_totals) AS total', false);
        $result = $this->db->get('storedt_orders')->row();
        //var_dump($result->total);die;
        return $result->total;
    }


	public function get_catalogue_data()
	{
        $this->load->model('store/admin/products_admin_m');

		$data['total_products']               = $this->products_admin_m->count_all();
        $data['total_products_live']          = $this->db->where('deleted',NULL)->where('public',1)->count_all_results('storedt_products');
        $data['total_products_hidden']        = $this->db->where('deleted',NULL)->where('public',0)->count_all_results('storedt_products');
        $data['total_products_live_featured'] = $this->db->where('deleted',NULL)->where('public',1)->where('featured',1)->count_all_results('storedt_products');

        $data['total_products_deleted']       = $this->products_admin_m->count_by('deleted !=','``');

        $data['total_shipping_methods']       = $this->db->where('module_type','shipping')->where('enabled',1)->count_all_results('storedt_checkout_options');
        $data['total_gateway_methods']       = $this->db->where('module_type','gateway')->where('enabled',1)->count_all_results('storedt_checkout_options');

		return $data;
	}


    /**
     *
     * @param  integer $days  [description]
     * @param  string  $chart [order|users|income]
     * @return [type]         [description]
     */
    public function get_period($days = 5, $chart = 'orders')
    {

       $stats = [];

        switch ($chart)
        {

            case 'activity':
                $activity = $this->_get_recent_activity($days);
                $stats[] = ['label' => 'Session', 'data' => $activity];
                break;
            case 'income':
            case 'orders':
            	$orders = $this->_get_orders($days);
                $stats[] = ['label' => 'Orders', 'data' => $orders];
                break;
            case 'unpaid':
            	$orders = $this->_get_unpaid_orders($days);
                $stats[] = ['label' => 'Orders', 'data' => $orders];
                break;
            case 'newusers':
            	$users = $this->_get_new_users($days);
                $stats[] = ['label' => 'Users', 'data' => $users];
                break;
            case 'best':
                //days is actually # of products to get
                $best = $this->_get_best_sellers($days);
                $stats[] = ['label' => 'Sales', 'data' => $best];
                break;
            case 'bestsellers':
                //days is actually # of products to get
                $best = $this->_get_best_sellers($days);
                $stats[] = ['label' => 'Sales', 'data' => $best];
                break;


            case 'views':
                $views = $this->_get_most_viewed($days);
                $stats[] = ['label' => 'Views', 'data' => $views];
                break;
            case 'topclients':
                $top = $this->_get_top_clients($days);
                $stats[] = ['label' => 'Revenue', 'data' => $top];
                break;
            default:
                break;
        }

        return $stats;
    }

    private function _get_orders($days = 7)
    {
    	//$days = 7;
        $dates = [];

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`order_date`, '%Y-%m-%d') AS date", false);
        $this->db->where('order_date >', time()-$period);
        $this->db->group_by('date', false);
        $result = $this->db->get('storedt_orders')->result();

        $stats = [];

        for ($index = 0; $index < $days; $index++)
        {
            $timestamp = date('Y-m-d', time() - ($index * $day_seconds));
            $dates[$timestamp] = 0;
        }

        foreach ($result as $item)
        {
            $dates[$item->date] = $item->total;
        }

        $dates = array_reverse($dates);

        foreach ($dates as $key => $value)
        {
            $stats[] = array(strtotime($key)*1000, $value);
        }

        return $stats;
    }

    private function _get_unpaid_orders($days = 7)
    {
    	//$days = 7;
        $dates = [];

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`order_date`, '%Y-%m-%d') AS date", false);
        $this->db->where('order_date >', time()-$period);
		$this->db->where('pmt_status ', 'unpaid');

        $this->db->group_by('date', false);
        $result = $this->db->get('storedt_orders')->result();

        $stats = [];

        for ($index = 0; $index < $days; $index++)
         {
            $timestamp = date('Y-m-d', time() - ($index * $day_seconds));
            $dates[$timestamp] = 0;
        }

        foreach ($result as $item)
        {
            $dates[$item->date] = $item->total;
        }

        $dates = array_reverse($dates);

        foreach ($dates as $key => $value)
        {
            $stats[] = array(strtotime($key)*1000, $value);
        }

        return $stats;
    }

    private function _get_top_clients($clients = 7)
    {
        $this->db->select('user_id, sum(cost_items) as total');
        $this->db->group_by('user_id', false);
        $this->db->order_by('total desc', false);
        $this->db->limit($clients);

        $result = $this->db->get('storedt_orders')->result();

        $stats = [];
        foreach ($result as $item)
        {
            $user = $this->db->get_where('users_profiles',['user_id' =>$item->user_id])->result();
            $stats[] =  [$user[0]->first_name,$item->total];
        }
        return $stats;
    }

    public function get_recent_members($days=6) {

        $new_user_count =  $this->getCountOfNewUsers($days);
        $new_users = $this->getNewUsers(6); //7 days

        return ['count'=>$new_user_count,'period'=>$days,'users'=>$new_users];

    }

    public function getNewUsers($max=10) {
 
        $total_new = 0;
 
        $this->db->select('*');
        $this->db->order_by('created_on', 'desc');
        $this->db->limit($max);

        $users =  $this->db->get('users')->result();

        foreach($users as & $user) {
            $x = $this->db->select('display_name')->where('id',$user->id)->get('users_profiles')->row();
            $user->display_name = $x->display_name;
        }
        return $users;

    }    

    public function getCountOfNewUsers($days=6) {


        $dates = [];
        $total_new = 0;

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`created_on`, '%Y-%m-%d') AS date", false);
        $this->db->where('created_on >', time()-$period);
        $this->db->group_by('date', false);
        $result = $this->db->get('users')->result();

        foreach ($result as $item) {
            $total_new += intval($item->total);            
        }

        return $total_new;

    }


    private function _get_new_users($days = 7)
    {
    	//$days = 7;
        $dates = [];

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`created_on`, '%Y-%m-%d') AS date", false);
        $this->db->where('created_on >', time()-$period);
        $this->db->group_by('date', false);
        $result = $this->db->get('users')->result();

        $stats = [];

        for ($index = 0; $index < $days; $index++)
         {
            $timestamp = date('Y-m-d', time() - ($index * $day_seconds));
            $dates[$timestamp] = 0;
        }

        foreach ($result as $item)
        {
            $dates[$item->date] = $item->total;
        }

        $dates = array_reverse($dates);

        foreach ($dates as $key => $value)
        {
            $stats[] = array(strtotime($key)*1000, $value);
        }

        return $stats;
    }

    /**
     *
     * SELECT TOP(5) ProductID, SUM(Quantity) AS TotalQuantity
     * FROM order_items
     * GROUP BY ProductID
     * ORDER BY SUM(Quantity) DESC;
     *
     * @param  integer $product_count [description]
     * @return [type]                 [description]
     */
    private function _get_best_sellers($product_count = 5)
    {

        $this->db->select('title, product_id,sum(qty) as total_qty');
        $this->db->group_by('product_id', false);
        $this->db->order_by('sum(qty) desc', false);
        $this->db->limit($product_count);
        $result = $this->db->get('storedt_order_invoice')->result();


        $stats = [];
        foreach ($result as $item)
        {
            $stats[] =  [$item->title,$item->total_qty];
        }

        return $stats;
    }

    public function _get_most_viewed($product_count = 5)
    {

        $this->db->select('name, id, views');
        $this->db->where('deleted', NULL);
        $this->db->group_by('id', false);
        $this->db->order_by('views desc', false);
        $this->db->limit($product_count);
        $result = $this->db->get('storedt_products')->result();


        $stats = [];

        foreach ($result as $item)
        {
            $stats[] =  [$item->name,$item->views];
        }

        return $stats;
    }



    private function _get_recent_activity($days = 7)
    {
        //$days = 7;
        $dates = [];

        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`last_activity`, '%Y-%m-%d') AS date", false);
        $this->db->where('last_activity >', time()-$period);

        $this->db->group_by('date', false);
        $this->db->group_by('ip_address', false);

        $result = $this->db->get('sessions_log')->result();

        $stats = [];

        for ($index = 0; $index < $days; $index++)
         {
            $timestamp = date('Y-m-d', time() - ($index * $day_seconds));
            $dates[$timestamp] = 0;
        }

        foreach ($result as $item)
        {
            $dates[$item->date] = $item->total;
        }

        $dates = array_reverse($dates);

        foreach ($dates as $key => $value)
        {
            $stats[] = array(strtotime($key)*1000, $value);
        }

        return $stats;
    }


    public function _wip_quartlyTrend($days = 90)
    {
        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $now = time();
        $last = $now-$period;

        $info = $this->db->query("
                    SELECT
                        *, 
                        CASE WHEN last_3 > prev_3 THEN
                            'sales up'
                        ELSE
                            'sales down'
                        END AS sales_change,
                        (
                           CONCAT(((last_3 - prev_3) * 100) / last_3, '%')
                        ) AS precentage_change
                    FROM
                        (
                            SELECT
                                SUM(total_totals) AS last_3
                            FROM
                                default_storedt_orders
                            WHERE
                                paid_date > UNIX_TIMESTAMP(CURDATE() - INTERVAL 3 MONTH)
                        ) AS tb1,
                        (
                            SELECT
                                SUM(total_totals) AS prev_3
                            FROM
                                default_storedt_orders
                            WHERE
                                paid_date < UNIX_TIMESTAMP(CURDATE() - INTERVAL 3 MONTH)
                            AND paid_date > UNIX_TIMESTAMP(CURDATE() - INTERVAL 6 MONTH)
                        ) AS tb2")->result();


        dump($info);die;

        $info = $this->db->from('storedt_orders')->result();

           



        return [ ['1',$last_month_count->total], ['2',$this_month_count->total] ];

    } 
    /**
     * //$res = [ ['1','50'], ['2','2'] ];
     */
    public function quartlyTrend($days = 90)
    {
        $day_seconds = 86400;
        $period = $days * $day_seconds;

        $now = time();
        $last = $now-$period;

        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`order_date`, '%Y-%m-%d') AS date", false);
        $this->db->where('order_date >', $now-$period);
        $this->db->where('order_date <', $now);
        $last_month_count = $this->db->get('storedt_orders')->row();



        $this->db->select('COUNT(*) AS total', false);
        $this->db->select("FROM_UNIXTIME(`order_date`, '%Y-%m-%d') AS date", false);
        $this->db->where('order_date >', $last-$period);
        $this->db->where('order_date <', $last);
        $this_month_count = $this->db->get('storedt_orders')->row();

        return [ ['1',$last_month_count->total], ['2',$this_month_count->total] ];

    }    

    public function monthlySales($days = 180)
    {
        $day_seconds = 86400;
        $period = $days * $day_seconds;
        $now = time();

        $this->db->select('sum(total_totals) as total,order_date');
        $this->db->select("FROM_UNIXTIME(`order_date`, '%Y-%m-%d') AS date", false);
        $this->db->where('order_date >', $now-$period);
        $this->db->where('order_date <', $now);
        $this->db->group_by('YEAR(date), MONTH(date)', false);

        $results = $this->db->get('storedt_orders')->result();

        //dump($results);die;

        $fullarray=[];
        $retarray = [];

        foreach ($results as $key => $value) {
            $retarray[] = [$value->date,$value->total];
        }


         $fullarray['data'] = $retarray;
         $fullarray['color'] = "#3c8dbc";

        
        return $fullarray;

    }    

    public function monthlyAverageSale($days = 30)
    {
        $day_seconds = 86400;
        $period = $days * $day_seconds;
        $now = time();

        $this->db->select('avg(total_totals) as average');
        $this->db->where('order_date >', $now-$period);
        $this->db->where('order_date <', $now);

        $results = $this->db->get('storedt_orders')->row();

        return round($results->average,2);

    }   

    public function mostViewed($product_count = 3)
    {
        $colors = ["#3c8dbc","#0073b7","#00c0ef","#3c8dbc","#0073b7","#00c0ef","#3c8dbc","#0073b7","#00c0ef"];

        //max
        if($product_count>6)
            $product_count=6;

        /*
        var donutData = [
          {label: "Series2", data: 30, color: "#3c8dbc"},
          {label: "Series3", data: 20, color: "#0073b7"},
          {label: "Series4", data: 50, color: "#00c0ef"}
        ];
        */


        $this->db->select('name, id, views');
        $this->db->where('deleted', NULL);
        $this->db->group_by('id', false);
        $this->db->order_by('views desc', false);
        $this->db->limit($product_count);
        $result = $this->db->get('storedt_products')->result();


        $stats = [];
        $count=0;
        foreach ($result as $item)
        {
            $count++;
            $stats[] =  ['label'=>$item->name,'data'=>$item->views,'color'=>$colors[$count]];
        }

        return $stats;
    }


    public function activeCarts()
    {
        if($this->db->table_exists('storedt_carts')) {
            if($data = $this->db->select('count(*) as count')->group_by('user_id')->get('storedt_carts')->row()) {
                return $data->count;
            }
        }
        return 0;
    }

    public function countFiles() {
        if($this->db->table_exists('files')) {
            if($data = $this->db->select('count(*) as count')->group_by('user_id')->get('files')->row()) {
                return $data->count;
            }
        }
        return 0; 
    }

}