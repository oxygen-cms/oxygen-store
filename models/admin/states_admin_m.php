<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class States_admin_m extends MY_Model
{
	/**
	 * The default table for this model
	 * @var string
	 */
	public $_table = 'storedt_states';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('store/Toolbox/Nc_enums');
		$this->load->driver('Streams');
	}


    public function create($state_name,$country_id)
    {

        $to_insert = [
                'created_by'    => $this->current_user->id,
                'created'       => date("Y-m-d H:i:s"),
                'updated'       => date("Y-m-d H:i:s"),        
                'name'          => $state_name,
                'code'          => $state_name,
                'country_id'    => $country_id,            
        ];

        $state_id =  $this->insert($to_insert);

        return $state_id;
    }
}