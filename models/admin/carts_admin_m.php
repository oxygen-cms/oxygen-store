<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Carts_admin_m extends MY_Model
{
    public $_table = 'storedt_carts';

    public function __construct()
    {
        parent::__construct();
    }


    public function count_carts()
    {
    	return $this->db->group_by('user_id')->get('storedt_carts')->num_rows();
    }

    public function get_carts()
    {
    	$query = $this->db->select('storedt_carts.user_id,users.username')
				->select_sum('price','value')
				->select_sum('qty')
				->select_max('date')
				->select_min('date','oldest')
    			->from('users')
		    	->join('storedt_carts','storedt_carts.user_id = users.id')
		    	->group_by('user_id')
		    	->order_by('date','desc')
		    	->get()->result();

		return $query;
    }
    public function get_cart($user_id)
    {
        $query = $this->db->select('storedt_products.name,storedt_carts.*')
                ->where('user_id',$user_id)
                ->from('storedt_products')
                ->join('storedt_carts','storedt_carts.product_id = storedt_products.id')
                ->order_by('date','desc')
                ->get()->result();

        return $query;
    }

    public function delete($user_id)
    {
    	$this->db->where('user_id',$user_id)->delete('storedt_carts');
    	return true;
    }
}