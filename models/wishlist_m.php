<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Wishlist_m extends MY_Model
{

	/**
	 * @access public
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'storedt_wishlist';
		$this->primary_key = 'product_id';
	}

	/**
	 * @param $product
	 * @param $user_id
	 * @access public
	 */
	public function add($user_id, $product)
	{
		$input['user_id'] = $user_id;
		$input['product_id'] = $product->id;
		$input['price'] = $product->price;
		$input['date_added'] = date("Y-m-d H:i:s");
		$input['user_notified'] = 0;

		return $this->insert($input);
	}

	/**
	 * @param $product -
	 * @param $user_id
	 * @access public
	 *
	 * Note: changed to remove so that it doesnt conflict with MY_Model
	 */
	public function remove($user_id, $product_id)
	{
		$data = ['user_id' => $user_id, 'product_id' => $product_id];
		if ($this->delete_by($data))
		{
			return true;
		}
		return false;
	}

	/**
	 * Get all wishlist items
	 * @return [type] [description]
	 */
	public function get_all()
	{
		$this->db->select('storedt_products.*,storedt_wishlist.price');
		$this->db->join('storedt_products', 'storedt_products.id = storedt_wishlist.product_id', 'inner');
		return parent::get_all();
	}


	/**
	 * Checks wheather the item is already in the customers wishlist
	 *
	 * @param
	 * @param
	 * @access public
	 */
	public function item_exist($user_id = 0, $product_id = 0)
	{
		$data = ['user_id' => $user_id, 'product_id' => $product_id];
		if ($this->wishlist_m->get_by($data))
		{
			return true;
		}
		return false;
	}
}