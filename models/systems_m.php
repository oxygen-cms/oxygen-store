<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Systems_m extends MY_Model
{

    public $_table = 'storedt_systems';


    public function __construct()
    {
        parent::__construct();
    }

    public function get($driver)
    {   
        return $this->db
            ->select('*', false)
            ->where('driver',$driver)
            ->get($this->_table)
            ->row();
    }

    public function create($input)
    {
    }

    public function set_value($driver = '', $installed = 0)
    {
        return $this->db->update($this->_table, ['installed'=>$installed], ['driver' => $driver]);
    }
    public function set_driver_value($driver = '', $installed = 0)
    {
        return $this->db->where('driver',$driver)->update($this->_table, ['installed'=>$installed]);
    }

    public function get_installed_features()
    {
        return $this->where('installed',1)->where('system_type','feature')->get_all();
    }


    public function delete($id)
    {
        return false;
    }

}