<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

class Paypal_express_Gateway {


	public $title = 'Paypal Express';
	public $short_title = 'PayPal'; //short title is used for transaction line items - much better than storing full nae
	public $description = 'Process Payments via Paypal';
	public $author = 'Sal McDonald';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';



	public $fields = array(
		array(
			'field' => 'options[username]',
			'label' => 'Username',
			'rules' => 'trim|max_length[200]|required'
		),
		array(
			'field' => 'options[password]',
			'label' => 'Password',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'options[signature]',
			'label' => 'API Signature',
			'rules' => 'trim|max_length[200]|required'
		),
		array(
			'field' => 'options[test_mode]',
			'label' => 'Test Mode',
			'rules' => 'trim|max_length[100]|numeric'
		),
		array(
			'field' => 'options[auto]',
			'label' => 'Self submit',
			'rules' => 'trim|max_length[100]|numeric'
		),
	);


	public function __construct() {		}

		public function __get($var)
		{
				if (isset(get_instance()->$var))
				{
						return get_instance()->$var;
				}
		}

	/**
	 * Initialize the fields, and get redirect method !
	 * 
	 * @return [type] [description]
	 */
		public function pre_output()
		{
		}

		public function skip_confirmation()
		{
			if($this->options['auto']==1)
			{
				return true;
			}
			return false;
		}

		public function getPaymentDetails( $order_id )
		{
			if($order->user_id > 0) 
			{
				return '<a href="{{url:site}}store/payment/order/'.$order_id.'">Click here to pay via Paypal</a>';
			}
			return '';
		}
		
		/**
		 * Returns the pre-process params
		 * The input params are required
		 * 
		 * @param  [type] $billing_address [description]
		 * @param  [type] $payable_items   [description]
		 * @param  [type] $order           [description]
		 * @param  string $curr_code       [description]
		 * @return [type]                  [description]
		 */
	public function pre_process( & $order )
	{
		/*
			$order->billing_address;
			$order->payable_items;
			$order->country_currency_code;
		*/

		$payable_items = $this->_prep_items( $order );
	
				$params = array(
								'email'         => $order->billing_address->email,
								'amount'        => (float) $order->total_totals,
								'currency'      => $order->country_currency_code,
								'return_url'    => base_url() . "store/payment/callback/".  $order->id,
								'cancel_url'    => base_url() . "store/payment/cancel/".  $order->id,
								'phone'         => $order->billing_address->phone,
								'postcode'      => $order->billing_address->zip,
								'country'       => $order->billing_address->country,
								'city'          => $order->billing_address->city,
								'region'        => $order->billing_address->state,
								'address1'      => $order->billing_address->address1,
								'address2'      => $order->billing_address->address2,
								'name'          => $order->billing_address->first_name,
								'first_name'    => $order->billing_address->first_name, //test
								'last_name'     => $order->billing_address->last_name,   //test
								'items'         => $payable_items
				);

		$this->params = $params;

		return $params;
	}


	public function post_callback($response)
	{
		return NULL;
	}


	public function view_path()
	{
		return  'gateways/'.$this->slug.'/display';
	}


		/**
		 * prepare the order items for use by Paypal
		 * 
		 * @param  [type] $order [description]
		 * @return [type]        [description]
		 */
		private function _prep_items( $order )
		{
				$this->load->model('store/orders_m');
				$this->load->model('store/products_variances_m');


				//order_m should already be loaded
				$items = $this->orders_m->get_order_items( $order->id );


				$payable_items = [];

			 $base_charges =  
			 [
						'name'=> 'Base Charges',
						'desc'=> 'Administrative and Base charges',
						'amt' => 0,
						'qty' => 1,
			 ];

				//add the items
				foreach($items as $item)
				{
						$d = $this->products_variances_m->get($item->variant_id);
 
						 $payable_items[] =  array(
									'name'=> $item->name,
									'desc'=> '',/*$item->description,*/
									'amt' => (float) $d->price,
									'qty' => $item->qty,
								);

						 $base_charges['amt'] = ( (float) $base_charges['amt'] + (float) $d->base);
				}


				$payable_items[] = $base_charges;


				if( $order->total_shipping ) //If cost of shipping > 0
				{
						//add the shipping
						$payable_items[] =  array(

									'name'=>'Shipping and Handling',
									'desc'=> '',/*$item->description,*/
									'amt' => (float) $order->total_shipping,
									'qty' => 1,
						);

				}

				return $payable_items;
		}	

}