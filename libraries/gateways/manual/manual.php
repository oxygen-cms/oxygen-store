<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

class Manual_Gateway {


	public $title = 'Manual - Bank Deposit';
	public $short_title = 'Manual'; //short title is used for transaction line items - much better than storing full nae
	public $description = 'Bank Deposit';
	public $author = 'Sal McDonald';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';



	public $fields = array(
		array(
			'field' => 'description',
			'label' => 'Description',
			'rules' => 'trim|max_length[1000]'
		)
	);


	public function __construct() {		}


	public function pre_output()
	{
	
	}

    public function skip_confirmation()
    {
		return false;
    }

    public function getPaymentDetails($order_id)
    {
    	return $this->options->description;
    }

	/**
	 * Get the params to send
	 * @param  [type] $billing_address [description]
	 * @param  [type] $payable_items   [description]
	 * @param  [type] $order           [description]
	 * @param  string $curr_code       [description]
	 * @return [type]                  [description]
	 */
	public function pre_process( $order )
	{
		$this->params = [];		
		return  array();
	}


	public function post_callback($response)
	{
		return NULL;
	}



	public function view_path()
	{
		return 'gateways/'.$this->slug.'/display';
	}

}
