<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Reports_library extends ViewObject
{

	public function __construct()
	{
		log_message('debug', "Class Initialized");
	}

	/**
	 * also remove hidden reports
	 */
	public function get_all_report_types()
	{
		$data = new ViewObject();

		$order_types = [
				'prodsold'		=>	['list' => 'Products', 'visible' => true, 'path'=>'store', 'name' => 'Products by Sale Value' 			, 'description' => 'View listing of products most sold by Value'],
				'mostsoldp'		=>	['list' => 'Products', 'visible' => true, 'path'=>'store', 'name' => 'Products Item QTY Sold' 			, 'description' => 'View listing of products most sold by QTY'],
				'mostviewed'	=>	['list' => 'Products', 'visible' => true, 'path'=>'store', 'name' => 'Products Most viewed' 			, 'description' => 'List the most viewed products of ALL time.'],														
				'highorders'	=>	['list' => 'Orders'  , 'visible' => true, 'path'=>'store', 'name' => 'Highest valued Orders ' 			, 'description' => 'List the highest revenue orders of ALL time.'],	
				'bestcustomers'	=>	['list' => 'Customer', 'visible' => true, 'path'=>'store', 'name' => 'Best Customers ' 					, 'description' => 'List the highest spending customers.'],					
			];

		foreach ($order_types as $key => $value) 
		{
			if($value['visible']==false) unset($order_types[$key]);
		}

		$data->report_list = $order_types;

		//See if anyone has some reports for us
		Events::trigger('STOREVT_AdminReportListGet', $data);

		return $data->report_list ;
	}

}