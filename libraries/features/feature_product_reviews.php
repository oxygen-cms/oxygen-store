<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Feature_product_reviews extends ViewObject
{     
    public $title           = 'Product Reviews';
    public $driver          = 'feature_product_reviews';
    public $require         = 'system_z_admin_layer';
    public $description     = 'Allow your customers to write reviews';
    public $system_type     = 'feature'; 
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('store/Toolbox/Nc_status');  
    }


    public function install($installer=NULL)
    {
        if($installer->install_tables($this->module_tables))
        {
            Events::trigger("STOREVT_RegisterModule", $this->mod_details);
            return true;
        }

        return false;
    }

    public function uninstall($installer=NULL)
    {
        $installer->dbforge->drop_table('storedt_products_reviews');
        Events::trigger("STOREVT_DeRegisterModule", $this->mod_details);
        return true;
    }

    public function event_common()
    {
        
    }


    public function upgrade($installer,$old_version)
    {
        $ncmo = new NCMessageObject();
        return $ncmo;
    }

    protected $mod_details = [
                              'name'=> 'Product Reviews', 
                              'namespace'=>'store',
                              'path'=> 'features', 
                              'driver'=> 'feature_product_reviews',
                              'prod_tab_order'=> 0, 
                              'routes'=>
                                    [
                                        [
                                            'name'  => 'Product Reviews',
                                            'uri'   => '/reviews(/:any)?',
                                            'dest'  => 'store/features/reviews$1'
                                        ],
                                    ]
                                ];

    private $module_tables  =   [
            'storedt_products_reviews'     => 
            [
                'id'            => ['type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true, 'primary' =>true, 'key' => true],
                'product_id'    => ['type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'key' => true],
                'user_id'       => ['type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'key' => true],
                'flagged'       => ['type' => 'INT', 'constraint' => '1', 'unsigned' => true, 'default'=>0],
                'visible'       => ['type' => 'INT', 'constraint' => '1', 'unsigned' => true, 'default'=>1],
                'rating'        => ['type' => 'INT', 'constraint' => '1', 'unsigned' => true, 'default'=>0],
                'comment'       => ['type' => 'TEXT'],
                'reffered'      => ['type' => 'TEXT'],
                'date_reviewed' => ['type' => 'TIMESTAMP'],
            ],
        ];
}