<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Feature_affiliates extends ViewObject
{
    public $title           = 'Affiliates';
    public $driver          = 'feature_affiliates';
    public $require         = 'system_z_admin_layer';
    public $description     = 'Give Afiliates dedicated tracking links';
    public $system_type     = 'feature'; 

    public function __construct()
    {
        parent::__construct();
        $this->load->library('store/Toolbox/Nc_status');
    }

    public function install($installer=NULL)
    {
        $tables_installed = $installer->install_tables( $this->module_tables );

        if( $tables_installed  )
        {
            $this->add_menu_data();
            Events::trigger("STOREVT_RegisterModule", $this->mod_details);


            $this->load->library('maintenance/routes_lib');           
            Routes_lib::InstallModule('store_affiliates', $this->mod_routes );     




            return true;
        }
        return false;
    }

    public function uninstall($installer=NULL)
    {
        foreach($this->module_tables as $table_name => $table_data)
        {
            $this->dbforge->drop_table($table_name);

            $this->load->library('maintenance/routes_lib');
            Routes_lib::UninstallModule('store_affiliates');                 
        }
        $this->remove_menu_data();
        Events::trigger("STOREVT_DeRegisterModule", $this->mod_details);
        return true;
    }

    /**
     * Upgrade data 
     */
    public function upgrade($installer,$old_version)
    {
        $ncmo = new NCMessageObject();
        return $ncmo;
    }


    /**
     * Gets called/fired when any page is loaded
     * Alls the system to add assets
     */
    public function event_common($args=[]) 
    {
        if($code = $this->get_quantam_code())
        {   

            $this->load->model('store/features/affiliates_m');
            $this->load->model('store/features/affiliates_clicks_m');

            //set in session
            $this->session->set_userdata('QUANTAM', $code );

            //now we can do something
            if($affiliate = $this->affiliates_m->get_by('code', $code))
            {
                if($row =  $this->affiliates_clicks_m->find($affiliate->id, current_url()))
                {
                    //exist
                    $this->affiliates_clicks_m->log($affiliate->id, current_url(),$row );
                }
                else
                {
                    //new
                    $this->affiliates_clicks_m->log_new($affiliate->id, current_url() );
                }

            }
        } 
    }



    /**
     * Specific event just for this feature
     */
    public function event_main($args=[])
    {
        if($code = $this->get_quantam_code())
        {   
            if($args['type']=='order_placed') {

                $this->load->model('store/features/affiliates_m');

                if($affiliate = $this->affiliates_m->get_by('code', $code))
                {
                    $this->load->model('store/features/affiliates_orders_m');

                    $this->affiliates_orders_m->log($affiliate->id,$args['order_id']);
                }

            }
        }
    }

    private function get_quantam_code()
    {
        if($this->input->get('QUANTAM') || $this->session->userdata('QUANTAM'))
        {   
            if($this->input->get('QUANTAM')) {
                return $this->input->get('QUANTAM');
            }
            return $this->session->userdata('QUANTAM');
        }
        return false;
    }


    private function add_menu_data()
    {
        $data = [];
        $data[] = array(
            'label'         => 'Affiliates',
            'uri'           => NC_ADMIN_ROUTE.'/affiliates',
            'menu'          => 'lang:store:admin:shop_admin',
            'module'        => 'feature_affiliates',
            'order'         => 80,
            );

        $this->db->insert_batch('storedt_admin_menu', $data);
    }

    private function remove_menu_data()
    {
        $this->db->where('module','feature_affiliates')->delete('storedt_admin_menu');    
    }  


    //this module/extention requires MY/Customer
    protected $mod_details = [
                          'name'=> 'Affiliates', 
                          'namespace'=>'feature_affiliates',
                          'path'=> 'features', 
                          'driver'=> 'feature_affiliates',
                          'prod_tab_order'=> 0, 
                          'routes'=>
                                [
                                    [
                                        'name'  => 'Affiliates',
                                        'uri'   => '/affiliates(/:any)?',
                                        'dest'  => 'store/admin/affiliates$1'
                                    ],
                                ]
                            ];


    protected $mod_routes = 
                            [
                                [
                                    'name'  => 'Affiliates',
                                    'uri'   => '/affiliates(/:any)?',
                                    'dest'  => 'store/admin/affiliates$1'
                                ],
                            ];
                            


    protected $module_tables = 
    [
        'storedt_affiliates' => 
        [
            'id'            => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true, 'primary' => true),
            'name'          => array('type' => 'VARCHAR', 'constraint' => '100','default'=>''),
            'email'         => array('type' => 'VARCHAR', 'constraint' => '255','default'=>''),
            'code'          => array('type' => 'VARCHAR', 'constraint' => '255','default'=>''),
            'af_group'      => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>0), 
            'total_clicks'  => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>0),
            'enabled'       => array('type' => 'INT', 'constraint' => '1', 'unsigned' => true, 'default'=>1),
            'deleted'       => array('type' => 'DATETIME', 'null' => true, 'default' => NULL),
        ],
        'storedt_affiliates_clicks' => 
        [
            'id'            => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true, 'primary' => true),
            'affiliate_id'  => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>0),
            'page'          => array('type' => 'VARCHAR', 'constraint' => '500','default'=>''),
            'impressions'   => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>1),
            'info'          => array('type' => 'VARCHAR', 'constraint' => '2000','default'=>''),
            'ip_address'    => array('type' => 'VARCHAR', 'constraint' => '100','default'=>''),
            'date'          => array('type' => 'DATETIME', 'null' => true, 'default' => NULL),
        ],
        'storedt_affiliates_orders' => 
        [
            'id'            => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'auto_increment' => true, 'primary' => true),
            'affiliate_id'  => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>0),
            'order_id'      => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>0),
            'time_placed'   => array('type' => 'INT', 'constraint' => '11', 'unsigned' => true, 'default'=>0),
            'date_placed'   => array('type' => 'DATETIME', 'null' => true, 'default' => NULL),
        ],        
    ];

}