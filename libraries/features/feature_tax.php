<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Feature_tax extends ViewObject
{ 
    public $title           = 'Tax';
    public $driver          = 'feature_tax';
    public $require         = 'system_z_admin_layer';
    public $description     = 'Tax System - Install this if you want to apply tax to your products';
    public $system_type     = 'feature'; 
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('store/Toolbox/Nc_status');      
    }


    public function install($installer=NULL)
    {
        //menu
        $data = [];
        $data[] = array(
            'label'         => 'lang:store:admin:tax',
            'uri'           => NC_ADMIN_ROUTE.'/tax',
            'menu'          => 'lang:store:admin:shop_admin',
            'module'        => 'tax',
            'order'         => 90,
            );
        $this->db->insert_batch('storedt_admin_menu', $data);

        return true;
    }

    public function event_common()
    {
        
    }
    public function uninstall($installer=NULL)
    {
        //remove tax from the menu
        $this->db->where('module','tax')->delete('storedt_admin_menu');

        //delete the data data
        //$this->db->truncate('storedt_tax');

        //remove all tax records/assignments for the products
        //$this->db->update('storedt_products',array('tax_id'=>NULL));

        return true;
    }


    public function upgrade($installer,$old_version)
    {
        $ncmo = new NCMessageObject();
        return $ncmo;
    }
 

}