<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

use Store\Exceptions\OrderNotFoundException as OrderNotFoundException;
use Store\Exceptions\OrderRetrievalException as OrderRetrievalException;
class Orders_library extends ViewObject
{

	public function __construct($params = array())
	{
		log_message('debug', "Orders Library Class Initialized");
	}

	public function clear_admin_orders_filter()
	{
		$this->session->unset_userdata('display_orders_f_order_by_dir_filter');	
		$this->session->unset_userdata('display_orders_f_status_filter');		
		$this->session->unset_userdata('display_orders_f_keyword_search_filter');		
		$this->session->unset_userdata('display_orders_f_order_by_filter');	
		$this->session->unset_userdata('display_f_display_count_filter');		
		$this->session->unset_userdata('display_f_payment_status_filter');		
		$this->session->unset_userdata('display_f_order_status_filter');			
		$this->session->unset_userdata('display_f_filter_status_filter');	
	}


	public function get_admin_order($id)
	{

		
		$this->load->model('store/addresses_m');		
		$this->load->model('store/admin/orders_admin_m');		

		$this->data = new ViewObject();

		// Get the order
		if(!$this->data->order = $this->orders_admin_m->get($id))
		{
			throw new OrderNotFoundException('Order doesnt exist!',100);
		}



		// Order Contents
		$this->data->contents = $this->get_order_items($id);
		$this->data->invoice_items = $this->orders_admin_m->get_invoices_by_order($id);
		
		if( ! $this->data->contents)
		{
			//ok we have no contents but we shouldnt fail
			//throw new OrderNotFoundException('Order doesnt exist!',100);
			throw new Exception('No contents!',100);
		}

		if( ! $this->data->invoice_items )
		{
			//ok we have no contents but we shouldnt fail
			//throw new OrderNotFoundException('Order doesnt exist!',100);
			throw new OrderRetrievalException('No invoice Items on this order.',100);
		}


	 	if($this->data->order->has_shipping_address > 0)
	 	{
			$this->data->shipping_address = $this->_getAddressObject($this->data->order->shipping_address_id);
	 	}
	 	else
	 	{
	 		$this->data->shipping_address = $this->_getAddressObject($this->data->order->billing_address_id);
	 	}


	 	//if(($this->data->order->has_shipping_address > 0) AND ($this->data->order->shipping_address_id > 0))
	 	if($this->data->order->shipping_address_id > 0)
		{	
			// Shipping Method ID
			$this->data->shipping_method 	= $this->_getCheckoutMethodData('shipping', $this->data->order->shipping_id );
	 	}
	 	else
	 	{
			// Shipping Method ID
			$this->data->shipping_method 	= $this->no_ship();
	 	}					 	


		// Get Billing Address
		$this->data->invoice = $this->_getAddressObject($this->data->order->billing_address_id);
		if(!$this->data->invoice)
		{
			throw new OrderRetrievalException('point b',100);
		}

		//
		// get payments
		//
		$this->data->payments = $this->_getCheckoutMethodData('gateway', $this->data->order->gateway_id );
		if(!$this->data->payments)
		{
			throw new OrderRetrievalException('point c',100);
		}


		$this->data->transactions = $this->db->where('order_id', $id)->order_by('id desc')->get('storedt_transactions')->result();

		// Get All transaction history
		if(!$this->data->transactions)
		{
			$this->data->transactions = [];
			
			$this->data->transactions[] =(object)[
					'id' => 0,
					'order_id' => $id,
					'txn_id' => 'no-txn-'.$id,
					'status' => 'accepted',
					'reason' => '!! No Transactions were logged !!',
					'refund' => 0,
					'amount' => 0,
					'gateway' => 0,
					'user' => 'SYSTEM',
					'timestamp' => time(),
					'data' => json_encode([]),
		            'created'   => date("Y-m-d H:i:s"),
		            'updated'   => date("Y-m-d H:i:s"),	
		            'created_by'=> 0,						
			];

			//there should always be a transaction line, if not we have a real problem
			//throw new Exception('No transactions!',100);
		}
			




		//$this->data->notes = $this->db->where('order_id',$id)->order_by('id desc')->get('storedt_order_notes')->result();
		$this->data->notes = $this->db->where('order_id',$id)->order_by('id desc')->get('storedt_order_notes')->result();
		if(!$this->data->notes)
		{
			//throw new OrderRetrievalException('No notes',100);
			$this->data->notes = [];
		}

		// Get User Details
		if($this->data->order->user_id > 0) 
		{
			$this->data->customer = $this->orders_admin_m->get_user_data($this->data->order->user_id,  $this->data->invoice );
		}
		else
		{
			$this->data->customer = (object)[];

			$this->data->customer->id = 0;
			$this->data->customer->display_name = 'GUEST';
		}

		if($this->data->customer)
		{
				//Cleanup options
				$this->load->model('store/workflows_m');
				$this->data->order->current_status = $this->workflows_m->get( $this->data->order->status_id );
				$percent_value = ($this->data->order->current_status) ? $this->data->order->current_status->pcent : 0 ;


				$this->data->percent_value = $percent_value;
				$this->data->order_workflows = $this->workflows_m->form_select( [] , false );
				return $this->data;
		}
		else
		{
			throw new OrderNotFoundException('No valid User',100);
		}
	}

	public function get_admin_order_min($id)
	{
		
		$this->load->model('store/addresses_m');		
		$this->load->model('store/admin/orders_admin_m');		

		$this->data = new ViewObject();

		// Get the order
		if(!$this->data->order = $this->orders_admin_m->get($id))
		{
			throw new OrderNotFoundException('Order doesnt exist!',100);
		}

	 	if($this->data->order->shipping_address_id > 0)
		{	
			// Shipping Method ID
			$this->data->shipping_method 	= $this->_getCheckoutMethodData('shipping', $this->data->order->shipping_id );
	 	}
	 	else
	 	{
			// Shipping Method ID
			$this->data->shipping_method 	= $this->no_ship();
	 	}					 	

		// Get User Details
		if($this->data->customer = $this->orders_admin_m->get_user_data($this->data->order->user_id,  $this->data->invoice )) {
				//Cleanup options
				$this->load->model('store/workflows_m');
				$this->data->order->current_status = $this->workflows_m->get( $this->data->order->status_id );
				$percent_value = ($this->data->order->current_status) ? $this->data->order->current_status->pcent : 0 ;
				$this->data->percent_value = $percent_value;
				return $this->data;
		}
		else {
			throw new OrderNotFoundException('No valid User',100);
		}

	}

	private function no_ship()
	{
		$data = new ViewObject();

		$data->id = 0;
		$data->title = 'Shipping not required.';
		$data->slug = 'nsr';
		$data->module_type = 'shipping';
		$data->link = 'Shipping not required.';
		return $data;
	}

	public function _getAddressObject($id=0)
	{

		if($a = $this->addresses_m->get($id))
		{
			return $a;
		}

		return (object) [
			'id'=>0,
			'first_name'=>'',
			'last_name'=>'',
			'billing'=>'1',
			'address1'=>'',
			'address2'=>'',
			'phone'=>'',	
			'state'=>'',
			'zip'=>'',
			'country'=>'',	
			'email'=>'',
			'shipping'=>'1',
			'created'=>'',		
			'updated'=>'',		
			'deleted'=>'',		
			'company'=>'',	
			'instruction'=>'',	
			'user_id'=>'',
			'created_by'=>'',								
			];
	}


	private function get_order_items($order_id)
	{
		$items = $this->orders_admin_m->get_order_items($order_id);

		return $this->format_items( $items );
	}

	/**
	 * format order->items for display
	 */
	private function format_items($contents)
	{
		//var_dump($contents);die;
		foreach($contents as $key => $item)
		{

			$input = (array) json_decode($item->options);

			$str = '<br>';
			foreach ($input as $key2 => $value)
			{
				$str .= $key2 . ':' . $value . '<br>';
			}

			$contents[$key]->options = $str;

		}
		return $contents;
	}


	public function _getCheckoutMethodData($type='shipping',$id=0)
	{

		if($m = $this->db->where('module_type',$type)->where('id', $id)->get('storedt_checkout_options')->row() )
		{
			$type = ($type=='gateway')?'gateways':'shipping';
			$m->link = "<a href='./".NC_ADMIN_ROUTE."/{$type}/edit/{$id}' title='Click to view {$m->title}' class='tooltip-s nc_links'>{$m->title}</a>";
			return $m;
		}

		return (object) ['id'=>0,'slug'=>'','title'=>'Not Found','link'=>'Not Found'];
	}

}
// END Cart Class