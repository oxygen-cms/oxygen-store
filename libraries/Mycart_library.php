<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
require_once(dirname(__FILE__) . '/Mycart.php');
class Mycart_library extends Mycart  {



	public function __construct($params = [])
	{	
		parent::__construct($params);
	}

	public function prepareProductForCart($product, $variant,  $qty=1, $options=[])
	{

		$name =  convert_accented_characters( $product->name );

		$curr_qty = $this->getProductQtyByVariantID($variant->id);

		// Assign the Cart item array
		$data = [
					'id' 			=> $variant->id,
					'productid' 	=> $product->id,
					'tax_id' 		=> $product->tax_id,		//meta data used to calc totals later
					'variance' 		=> $variant->id,
					'pkg_group_id' 	=> $variant->pkg_group_id,
					'discountable' 	=> $variant->discountable,
					'points' 		=> $product->points,
					'slug' 			=> $product->slug,
					'qty' 			=> $qty,
					'new_qty' 		=> ($curr_qty + $qty), 					// here we want to add the qty requested, with any qty in the current cart, this will be used for calc discounts on the event fire
					'price' 		=> number_format($variant->price,2), 	// the price listed on the site - any discounts based on membership ?!
					'list_price' 	=> number_format($variant->price,2), 	// the price listed on the site
					'discount' 		=> 0, 									// any discounts applied o this product (this is just meta data)
					'base' 			=> number_format($variant->base,2),
					'name' 			=> $name,
					'variant_name' 	=> $variant->name,
					'options' 		=> $options,
					'discount_message' => '',
					'height' 		=> $variant->height, 		
					'width' 		=> $variant->width,
					'length' 		=> $variant->length,
					'weight' 		=> $variant->weight,
					'is_shippable'	=> $variant->is_shippable,
					'is_digital'	=> $variant->is_digital,
				];

		return $data;
	}


	public function requestQtyMinRequired( $variant, $qty = 1 )
	{
		return $this->_requestQtyMinRequiredByVariantID( $variant->id, $variant->min_qty, $qty );
	}


	private function _requestQtyMinRequiredByVariantID( $variant_id, $min_qty=0, $qty = 1 )
	{
		//if($variant->min_qty !== NULL)
		//{
			if($min_qty > 0)
			{
				//$minqty = (int) $variant->min_qty;

				$currQty = $this->getProductQtyByVariantID($variant_id);

				$tmpQty = $currQty + $qty;

				$qty = ($tmpQty < $minqty)?$minqty:$qty;
			}
		//}

		return $qty;
	}
	public function getProductQtyByRowID($row_id) {

		return $this->_itemIty($row_id, 'rowid');
	}


	public function getProductQtyByVariantID($variant_id) {

		return $this->_itemIty($variant_id, 'id');
	}


	private function _itemIty($val, $by = 'rowid') {
		$count = 0;
		if( $this->contents() != NULL )
		{
			foreach( $this->contents() as $item )
			{
				if(isset($item[$by]))
				{
					if ($item[$by] == $val)
					{
						$count += $item['qty'];
					}
				}
			}
		}
		return $count;
	}

}
// End of class