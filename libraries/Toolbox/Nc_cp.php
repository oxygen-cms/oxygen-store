<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
final class Nc_cp extends ViewObject
{

    public function __construct()
    {
		parent::__construct();
	}

	public function get_common_sections_menu()
	{
		$info = [];

        if(Settings::get('storst_storetype')=='standard')
        {		
	        $info['orders'] = array(
				'name' => 'store:admin:orders',
				'uri' => NC_ADMIN_ROUTE.'/orders',
				'shortcuts' => []
			);
    	}
		$info['products'] = array(
			'name' => 'store:admin:products',
			'uri' => NC_ADMIN_ROUTE.'/products',
			'shortcuts' => []
		);
		return $info;
	}
	
}