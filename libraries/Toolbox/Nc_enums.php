<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
final class Nc_enums
{
	const Yes  	= 1;
	const No  	= 0;
}
final class NCParamType
{
	const String  	= 'string';
	const Integer  	= 'integer';
	const Float 	= 'float';
}
final class UserActivationMode
{
	const ACTIVATE_BY_ADMIN  = 0;
	const ACTIVATE_BY_EMAIL  = 1;
	const ACTIVATE_INSTANTLY = 2;
}
final class ProductVisibility
{
	const Invisible 		= 0;
	const Visible 			= 1;
}
final class NCSections
{
	const Products 		= 'products';
	const Orders 		= 'orders';
	const System 		= '--SYSTEM--';	
	const Variance  	= 'variance';
}