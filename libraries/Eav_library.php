<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Eav_library extends ViewObject
{

	public function __construct()
	{
		parent::__construct();

	}


	/**
	 * install various attributes for types of industries, if the attribute exist do not install
	 */
	public function install_attribute_set($type='clothing')
	{
		/*
		switch ($type) 
		{
			case 'clothing':
				array('Height','Width');
				break;
			
			default:
				# code...
				break;
		}
		*/
	}

}