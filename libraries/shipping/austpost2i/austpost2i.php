<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
include_once('austpost2i_base.php');

//use Store\Shipping;
use Store\Shipping\AustpostMethodSelector as AustpostMethodSelector;
use Store\Shipping\Communicator as Communicator;
use Store\Shipping\AustPostServiceCode as ServiceCode;
use Store\Shipping\AustPostServiceOption as AustPostServiceOption;
use Store\Shipping\AustpostIntUrlBuilder as AustpostIntUrlBuilder;
use Store\Shipping\ShippingHelper as ShippingHelper;



class AustPost2i_ShippingMethod extends AustPostBase2i
{


	protected $selector=null;
	protected $communicator=null;

	public function __construct()
	{
		parent::__construct();

		$this->selector = new AustpostMethodSelector();
		$this->communicator = new Communicator();
		$this->serviceCode = new ServiceCode();
		$this->serviceOption = new AustPostServiceOption();
		//$this->urlBuilder = new AustpostUrlBuilder();

	}

		/*
			array (size=10)
	  'apikey' => string '79ba2fe8-5d59-4263-b87b-6828479e39ae' (length=36)
	  'distcode' => string '3000' (length=4)
	  'extracover' => string '0' (length=1)
	  'handlingfee' => string '0' (length=1)
	  'minfee' => string '0' (length=1)
	  'deliveryoption' => string 'regular' (length=7)
	  'usepackages' => string 'packages' (length=8)
	  'test' => boolean false
	  'api_key' => string '79ba2fe8-5d59-4263-b87b-6828479e39ae' (length=36)
	  'express' => boolean false*/

  /**
   * extracover needs to be implemented
   */

	/**
	 * Calculate the shipping for the given options
	 * 
	 * @param  [type] $options    [description]
	 * @param  [type] $items      [description]
	 * @param  Object  $to_address [description]
	 * @return [type]             [description]
	 */
	public function calc( $options, &$packaged_items, $to_address )
	{

		if(ShippingHelper::validateAUDestination($to_address)) {

			$options = $this->objectify($options);

			$this->urlBuilder = new AustpostIntUrlBuilder($to_address->country);

			$shipping_total = 0;


			//are we using packages or not ?
			$items = ($options->usepackages)?$packaged_items->packed_items:$packaged_items->unpacked_items;


			$this->urlBuilder->isTestMode($options->test);

			// Setup the communicator with common fields
			$this->communicator->setApi( $options->apikey );
			$this->communicator->setRedirectError($options->redirectTo);

			foreach($items as $item) {

				$this->urlBuilder->clear();

				// Make a wrapper for these two lines into a single call later
				$method = $this->selector->parcelOrPost($item);

				$this->urlBuilder->setMethod($method);

				if($options->express) {
					$this->urlBuilder->setServiceCode($this->serviceCode->getIntExpressAirmailPost());
				}
				else {
					$this->urlBuilder->setServiceCode($this->serviceCode->getIntAirmailPost());
				}

				// Required info
				$this->urlBuilder->addBox($item);		
		
				//Add any cost of the package
				ShippingHelper::addCostOfPackage( $shipping_total, $item );

				// Now call
				$response = $this->communicator->communicate($this->urlBuilder->get());

				if($response->error) {
					$this->session->set_flashdata('error',$response->message);
					redirect($options->redirectTo);
				}
				else {
					$shipping_total += $response->cost;
				}
			}


			$shippableObejct = ShippingHelper::getGlobalShippingObject($shipping_total);

			ShippingHelper::addHandling( $shippableObejct, $options->handlingfee );

			ShippingHelper::checkMinimum( $shippableObejct, $options->minfee );
			
		}
		else {
			//Not a AU state or location in Australia
			$shippableObejct = ShippingHelper::getGlobalShippingObject(0);
		}

		return $shippableObejct;


	}


	private function objectify($options) {

		$this->doubly($options,'express');
		$this->doubly($options,'test');
		$this->doubly($options,'registered');
		$this->doubly($options,'sod');
		
		return (object) $options;
	}
	private function doubly(&$options,$key) {

		if($options[$key]==$key) {
			$options[$key]=true;
		}
		else {
			$options[$key]=false;
		}
	}	
}


