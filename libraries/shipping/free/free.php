<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Free_ShippingMethod {

	public $name =  'Free';
	public $description = 'Free Shipping for All Items';
	public $author = 'inspiredgroup.com.au';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';
	public $tax_rate = 0.1; //10%
	public $international_shipping = false;
	public $fields = array(
		array(
			'field' => 'options[tax_rate]',
			'label' => 'The TAX rate to calculte shipping. Note that Tax is TI-Tax Inclusive.',
			'rules' => 'trim|max_length[5]|is_numeric'
		),		
	);


	public function __construct() {		}


	public function pre_save($input)
	{
		return $input;
	}

	public function pre_output()
	{
		$this->form_data = [];
	}

	public function calc( $options, $items, $to_address = [] )
	{
		$shippable = new ViewObject();
		$shippable->cost = 0;
		$shippable->tax  = 0;
		return $shippable;
	}

}
