<?php if(!(isset($options['amount'])) ): ?>
    <?php $options['amount']=0;?>
<?php endif; ?>
<?php if(!(isset($options['tax_rate'])) ): ?>
    <?php $options['tax_rate']=0.1;?>
<?php endif; ?>



              <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab"  href="#tab_1"><span>Details</span></a></li>
                </ul>
                <div class="tab-content">

	                  <div id="tab_1" class="tab-pane active">



							<ul>
								<li class="">
									<label>Tax Rate for this Shipping method ?</label>
									<div class="input">
										<?php 

											$values= [];
											for($i=0;$i<101;$i++)
											{
												if($i==10)
												{
													$values['0.1'] = $i . ' %';
													continue;
												}	

												if($i<10)
												{
													$values['0.0'.$i] = $i . ' %';
												}
												else if(($i>10) AND ($i<100))
												{
													$values['0.'.$i] = $i . ' %';
												}	
												else if($i == 100)
												{
													$values['1.0'] = $i . ' %';
												}
									
											}

										echo form_dropdown('options[tax_rate]', $values , $options['tax_rate']); ?>
									</div>
								</li>	
							</ul>

	                  </div>
	              </div>
	    </div>