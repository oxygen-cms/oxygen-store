<?php defined('BASEPATH') OR exit('No direct script access allowed');

//use Store\Shipping;
use Store\Shipping\AustpostMethod as AustpostMethod;


class AustPostBase2 
{

	// max letter size info before it becomes parcel : Weight:125g	W:260 x H:360 x L:20mm

	public function __construct()
	{
 		$this->load->library('session');
	}


	public $name = 'Aust Postage2';
	public $description = 'Aust Postage2';
	public $author = 'inspiredgroup.com.au';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';
	public $tax_rate = 0.1; //10%
	public $international_shipping = false;

	public $fields = 
	[
		[
			'field' => 'apikey',
			'label' => 'API Key',
			'rules' => 'trim|text',
		],
		[
			'field' => 'distcode',
			'label' => 'Distribution PostCode',
			'rules' => 'trim|text',
		],
		[
			'field' => 'extracover',
			'label' => 'Extra Cover',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'usepackages',
			'label' => 'Use Package or Items',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'test',
			'label' => 'Test Mode',
			'rules' => 'trim',
		],		
		[
			'field' => 'handlingfee',
			'label' => 'Handling Fee',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'mincharge',
			'label' => 'Minimum Charge',
			'rules' => 'trim|numeric',
		],	
		[
			'field' => 'redirectTo',
			'label' => 'Redirect To',
			'rules' => 'trim',
		],	
		[
			'field' => 'registered',
			'label' => 'Registered Post',
			'rules' => 'trim|numeric',
		],	
		[
			'field' => 'sod',
			'label' => 'Signature On Delivery',
			'rules' => 'trim|numeric',
		],	
		[
			'field' => 'enable_freeshipover',
			'label' => 'Enable Free Shipping Over',
			'rules' => 'trim|numeric',
		],			
		[
			'field' => 'freeshipover',
			'label' => 'Free Shipping Over',
			'rules' => 'trim|numeric',
		],				
	];

    public function __get($var)
    {
        if (isset(get_instance()->$var))
        {
            return get_instance()->$var;
        }
    }

	/**
	 * Admin form view before save just to be 
	 * able to convert some user input or preprocess info
	 */
	public function pre_save($input)
	{
		return $input;
	}


	/**
	 * Called just before edit view. pre-output formats data and makes sure default
	 * variables are available for display
	 * It also has access to the installed db data so it can format i required.
	 * 
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function pre_output()
	{
		$method = new AustpostMethod();

		return $this->form_data = 
		[
			 'test' => 
			 [
			 	'test'=>'Test Mode', 
			 	'public'=>'Public (Active)',
			 ],
			 'redirectTo' => 
			 [
			 	'store/cart'=>'Cart', 
			 	'store'=>'Store Home',
			 	'/'=>'Home Page',
			 ],
			 'registered' => 
			 [
			 	'registered'=>'Registered', 
			 	'default'=>'Default',
			 ],
			 'sod' => 
			 [
			 	'sod'=>'Signature On Delivery (Yes)', 
			 	'default'=>'Default',
			 ],
			 'enable_freeshipover' => 
			 [
			 	'0'=>'Disable', 
			 	'1'=>'Enable',
			 ],
	
			 		 
		];
	}	

}