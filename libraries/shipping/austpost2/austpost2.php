<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 *
 */
include_once('austpost2_base.php');

//use Store\Shipping;
use Store\Shipping\AustpostMethodSelector as AustpostMethodSelector;
use Store\Shipping\Communicator as Communicator;
use Store\Shipping\AustPostServiceCode as ServiceCode;
use Store\Shipping\AustPostServiceOption as AustPostServiceOption;
use Store\Shipping\AustpostUrlBuilder as AustpostUrlBuilder;
use Store\Shipping\ShippingHelper as ShippingHelper;


class AustPost2_ShippingMethod extends AustPostBase2
{

	protected $selector=null;
	protected $communicator=null;

	public function __construct()
	{
		parent::__construct();

		$this->selector = new AustpostMethodSelector();
		$this->communicator = new Communicator();
		$this->serviceCode = new ServiceCode();
		$this->serviceOption = new AustPostServiceOption();
	}

	public function calc( $options, $items, $to_address )
	{
		$cart_total = $this->mycart->total();

		if(isset($options['enable_freeshipover'])) {
			if($options['enable_freeshipover']=='1') {
				//check cart total, if over limit
				if(floatval($options['freeshipover'])>0) {
					if(floatval( $cart_total ) > floatval($options['freeshipover'])) {
						return ShippingHelper::getGlobalShippingObject(0);
					}
				}
			}
		}

		return $this->_calc( $options, $items, $to_address );
	}

	/**
	 * Calculate the shipping for the given options
	 * 
	 * @param  [type] $options    [description]
	 * @param  [type] $items      [description]
	 * @param  Object  $to_address [description]
	 * @return [type]             [description]
	 */
	public function _calc( $options, $packaged_items, $to_address )
	{



		if(ShippingHelper::validateAUDestination($to_address)) {



			$options = $this->objectify($options);

			$this->urlBuilder = new AustpostUrlBuilder($options->distcode,$to_address->zip);

			$shipping_total = 0;

			//are we using packages or not ?
			$items = ($options->usepackages)?$packaged_items->packed_items:$packaged_items->unpacked_items;


			$this->urlBuilder->isTestMode($options->test);

			// Setup the communicator with common fields
			$this->communicator->setApi( $options->apikey );
			$this->communicator->setRedirectError($options->redirectTo);



			foreach($items as $item) {

				$this->urlBuilder->clear();

				// Make a wrapper for these two lines into a single call later
				$method = $this->selector->parcelOrPost($item);


				//if($method->method != 'parcel') {
				//	echo $method->method;die;
				//}

				//false indicates standard and NOT express
				$this->serviceCode->initialize( $method, false );

				$this->urlBuilder->setMethod($method);

				$this->urlBuilder->setServiceCode($this->serviceCode->get());

				//optional extras
				if($options->registered) {
					$this->urlBuilder->setOptionCode($this->serviceOption->getRegisteredPostOption());
				}

				//Signature on delivery
				if($options->sod) {
					$this->urlBuilder->setOptionCode($this->serviceOption->getSignatureOnDelivery());
				}				

				// Extra cover
				if($options->extracover>0) {
					if($options->extracover<=5000) {
						$this->urlBuilder->setExtraCover($this->serviceOption->getExtraCover(),$options->extracover);
					}
				}

				// Required info
				$this->urlBuilder->addBox($item);	


				// Now call
				$response = $this->communicator->communicate($this->urlBuilder->get());




				if($response->error) {
					$this->session->set_flashdata('error',$response->message);
					redirect($options->redirectTo);
				}
				else {
					//Add any cost of the package
					ShippingHelper::addCostOfPackage( $shipping_total, $item );	
									
					$shipping_total += $response->cost;
				}
			}




			$shippableObejct = ShippingHelper::getGlobalShippingObject($shipping_total);

			ShippingHelper::addHandling( $shippableObejct, $options->handlingfee );

			ShippingHelper::checkMinimum( $shippableObejct, $options->minfee );
			
		}
		else {
			//Not a AU state or location in Australia
			$shippableObejct = ShippingHelper::getGlobalShippingObject(0);
		}

		return $shippableObejct;
	}


	private function objectify($options) {

		$this->doubly($options,'test');
		$this->doubly($options,'registered');
		$this->doubly($options,'sod');
		
		return (object) $options;
	}

	private function doubly(&$options,$key) {

		if($options[$key]==$key) {
			$options[$key]=true;
		}
		else {
			$options[$key]=false;
		}
	}	
}

