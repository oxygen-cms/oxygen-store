<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Flatrate_base {

	public $name =  'Flatrate';
	public $description = 'Flatrate for All Items';
	public $author = 'inspiredgroup.com.au';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';
	public $tax_rate = 0.1; //10%
	public $international_shipping = false;

	protected $_options;
	protected $_shippable_items;
	protected $_to_address;

	public $fields = array(
		array(
			'field' => 'options[amount]',
			'label' => 'Amount for shipping',
			'rules' => 'trim|max_length[5]|is_numeric'
		),
		array(
			'field' => 'options[tax_rate]',
			'label' => 'The TAX rate to calculte shipping. Note that Tax is TI-Tax Inclusive.',
			'rules' => 'trim|max_length[5]|is_numeric'
		),	
		array(
			'field' => 'options[flatratemethod]',
			'label' => 'The type of flat rate shipping',
			'rules' => 'trim'
		),				
	);

    public function __get($var)
    {
        if (isset(get_instance()->$var))
        {
            return get_instance()->$var;
        }
    }
	public function __construct() {
		$this->_options = [];
	}


	protected function _calc( $options, $items, $to_address = [] )
	{


		$this->_options = $options;

		$cost = 0;

		if(isset($this->_options['amount'])) {

			$this->_shippable_items = $this->_packItems($items);
			$this->_to_address = $to_address;

			switch($this->_options['flatratemethod']) {
				case 'peritem':
					$cost = $this->_calc_per_item();
					break;
				case 'byweight':
					$cost = $this->_calc_by_weight();
					break;
				case 'standard':
					$cost = (float) $options['amount'];
					break;	
				case 'packagecost':
					$cost = $this->_calc_by_packageCost();
					break;										
			}

		}

		return $cost;			
	}

	private function _calc_by_packageCost() {

		$c = 0;

		foreach($this->_shippable_items as $item) {

			//Add any cost of the package
			ShippingHelper::addCostOfPackage( $c, $item );

		}

		return $c;
	}

	private function _calc_per_item() {

		$pi  = floatval($this->_options['amount']);

		$cost = floatval($this->_options['handling']);

		$cost += ( count($shippable_items) * $pi );

		return $cost;
	}

	private function _calc_by_weight() {


		$pk  = floatval($this->_options['amount']);


		$total_weight = 0;


		$cost = floatval($this->_options['handling']);


		foreach ($shippable_items as $item)
		{
			$total_weight += $item['weight'];
		}


		$cost += ( $total_weight * $pk );

		return $cost;
	}

	private function _packItems($packaged_items) {
		

		$this->load->library('store/packages_library');

		// Define shippable boxes
		$_shippable_boxes = [];

		//just in case its not set
		if( ! isset($this->_options['usepackages']) ) 
		{
			$this->_options['usepackages'] = 'packages';
		}


		// Packages OR items
		if($this->_options['usepackages'] == 'packages') 
		{
			$_shippable_boxes = $packaged_items->packed_items;
		}
		else 
		{
			$_shippable_boxes = $packaged_items->unpacked_items;
		}	

		return $_shippable_boxes;	
	}	


	protected function _pre_output() {

		$tax_rates= [];
		
		for($i=0;$i<101;$i++)
		{
			if($i==10)
			{
				$tax_rates['0.1'] = $i . ' %';
				continue;
			}	

			if($i<10)
			{
				$tax_rates['0.0'.$i] = $i . ' %';
			}
			else if(($i>10) AND ($i<100))
			{
				$tax_rates['0.'.$i] = $i . ' %';
			}	
			else if($i == 100)
			{
				$tax_rates['1.0'] = $i . ' %';
			}

		}

		$this->form_data = 
		[
			'tax_rates'=>$tax_rates,
			'packages'=>
				[
					'packages'=>'Yes',
					'items'=>'No',
				],
			'method'=>
				[
					'peritem'=>'Per Item (Item or Package)',
					'byweight'=>'By Weight (KG)',
					'standard'=>'Standard (Flat fee)',
					'packagecost'=>'Cost of Packages Used'
				],				
		];

		return $this->form_data;
	}
}
