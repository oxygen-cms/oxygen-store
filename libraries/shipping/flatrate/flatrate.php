<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
include_once('flatrate_base.php');

class Flatrate_ShippingMethod extends Flatrate_base {



	public function __construct() {
		parent::__construct();
	}


	/**
	 * format the input before storing in db
	 * 
	 * @param  [type] $input [description]
	 * @return [type]        [description]
	 */
	public function pre_save($input)
	{
		return $input;
	}

	public function pre_output()
	{
		return $this->_pre_output();
	}

	public function calc( $options, $items, $to_address = [] )
	{
		$shippable = new ViewObject();
		$shippable->cost = $this->_calc( $options, $items, $to_address );
		$shippable->tax  = $shippable->cost * $options['tax_rate'];
		$shippable->packing_slip = '';
		return $shippable;
	}

}
