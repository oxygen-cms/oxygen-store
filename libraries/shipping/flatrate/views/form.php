<?php if(!(isset($options['amount'])) ): ?>
    <?php $options['amount']=0;?>
<?php endif; ?>
<?php if(!(isset($options['tax_rate'])) ): ?>
    <?php $options['tax_rate']=0.1;?>
<?php endif; ?>
<?php if(!(isset($options['usepackages'])) ): ?>
    <?php $options['usepackages']='packages';?>
<?php endif; ?>
<?php if(!(isset($options['flatratemethod'])) ): ?>
    <?php $options['flatratemethod']='standard';?>
<?php endif; ?>



              <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab"  href="#tab_1"><span>Details</span></a></li>
                </ul>
                <div class="tab-content">

	                  <div id="tab_1" class="tab-pane active">

							<ul>
								<li class="">
									<label>Amount</label>
									<div class="input">
										<?php echo form_input('options[amount]', set_value('options[amount]', $options['amount'])); ?>
									</div>
								</li>
								<li class="">
									<label>Tax Rate for this Shipping method ?</label>
									<div class="input">
										<?php echo form_dropdown('options[tax_rate]', $form_data['tax_rates'] , $options['tax_rate']); ?>
									</div>
								</li>	
								<li class="">
									<label>Use Packages</label>
									<div class="input">
										<?php echo form_dropdown('options[usepackages]', $form_data['packages'] , $options['usepackages']); ?>
									</div>
								</li>	
								<li class="">
									<label>Calculation Method</label>
									<div class="input">
										<?php echo form_dropdown('options[flatratemethod]', $form_data['method'] , $options['flatratemethod']); ?>
									</div>
								</li>	

							</ul>

	                  </div>
	              </div>
	    </div>

	