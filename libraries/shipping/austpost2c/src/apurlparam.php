<?php defined('BASEPATH') OR exit('No direct script access allowed');


if ( ! class_exists('APUrlParam')) 
{
    class APUrlParam
    {
        protected $params;
        protected $first;
        protected $test;

        const API_TEST         = 'https://auspost.com.au/api/';
        const API_PUBLIC       = 'https://auspost.com.au/api/';

        const DOMESTIC_PARCEL  = 'postage/parcel/domestic/calculate.json';
        const DOMESTIC_LETTER  = 'postage/letter/domestic/calculate.json';

        public function __construct($from_zip,$to_zip)
        {

            $this->first = true;
            $this->test = false;

            // required
            $this->add('from_postcode', $from_zip );
            $this->add('to_postcode', $to_zip );
        }

        public function setDeliveryMethod($method)
        {
            $this->add('service_code', $method );
        }
        public function addBox($item=array())
        {
            $item['weight'] = ($item['weight']==0)?0.01:$item['weight'];

            $this->add('weight', $item['weight'] );
            $this->add('width', $item['width']);
            $this->add('height', $item['height']);
            $this->add('length', $item['length']);
        }

        public function add($key,$value)
        {
            $this->params .= $this->first ? '?' : '&';
            $this->params .= "{$key}={$value}";
            $this->first = false;
        }

        public function get( $endpoint )
        {
            $api_root       = ($this->test) ? APUrlParam::API_TEST  : APUrlParam::API_PUBLIC  ;
            $api_endpoint   = ($this->test) ? APUrlParam::API_TEST  : APUrlParam::API_PUBLIC  ;

            return $api_root.$endpoint.$this->params;
        }

        /*
        public function getDomesticParcelURI()
        {
            $endpoint = self::DOMESTIC_PARCEL;
            $api_root       = ($this->test) ? APUrlParam::API_TEST  : APUrlParam::API_PUBLIC  ;
            $api_endpoint   = ($this->test) ? APUrlParam::API_TEST  : APUrlParam::API_PUBLIC  ;
            return $api_root.$endpoint.$this->params;
        }
        */
        public function getAustPostageURI($type)
        {
            $endpoint = $this->_endPoint($type);
            $api_root       = ($this->test) ? APUrlParam::API_TEST  : APUrlParam::API_PUBLIC  ;
            $api_endpoint   = ($this->test) ? APUrlParam::API_TEST  : APUrlParam::API_PUBLIC  ;
            return $api_root.$endpoint.$this->params;
        }
        private function _endPoint($type)
        {
            switch($type)
            {
                case AustPostServiceCode::AUS_LETTER_EXPRESS_SMALL:
                case AustPostServiceCode::AUS_LETTER_REGULAR_LARGE:
                case AustPostServiceCode::AUS_LETTER_REGULAR_SMALL:
                    return self::DOMESTIC_LETTER;
                    break;
                case AustPostServiceCode::AUS_PARCEL_REGULAR:
                default:
                    return self::DOMESTIC_PARCEL;
                    break; 
            }

            return self::DOMESTIC_PARCEL;
        }
        public function clear()
        {
            $this->first = true;
            $this->params = '';
        }

    }
}