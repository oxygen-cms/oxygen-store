<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
include_once('austpost2c_base.php');

//use Store\Shipping;
use Store\Shipping\AustpostMethodSelector as AustpostMethodSelector;
use Store\Shipping\Communicator as Communicator;
use Store\Shipping\AustPostServiceCode as ServiceCode;
use Store\Shipping\AustPostServiceOption as AustPostServiceOption;
use Store\Shipping\AustpostUrlBuilder as AustpostUrlBuilder;
use Store\Shipping\ShippingHelper as ShippingHelper;



class AustPost2c_ShippingMethod extends AustPostBase2c
{


	protected $selector=null;
	protected $communicator=null;

	public function __construct()
	{
		parent::__construct();

		$this->selector = new AustpostMethodSelector();
		$this->communicator = new Communicator();
		$this->serviceOption = new AustPostServiceOption();
	}


	/**
	 * Calculate the shipping for the given options
	 * 
	 * @param  [type] $options    [description]
	 * @param  [type] $items      [description]
	 * @param  Object  $to_address [description]
	 * @return [type]             [description]
	 */
	public function calc( $options, &$packaged_items, $to_address )
	{

		if(ShippingHelper::validateAUDestination($to_address)) {

			$options = $this->objectify($options);

			$this->urlBuilder = new AustpostUrlBuilder($options->distcode,$to_address->zip);

			$shipping_total = 0;


			//are we using packages or not ?
			$items = ($options->usepackages)?$packaged_items->packed_items:$packaged_items->unpacked_items;


			$this->urlBuilder->isTestMode($options->test);

			// Setup the communicator with common fields
			$this->communicator->setApi( $options->apikey );
			$this->communicator->setRedirectError($options->redirectTo);

			foreach($items as $item) {

				$this->urlBuilder->clear();

				// Make a wrapper for these two lines into a single call later
				$method = $this->selector->parcelOrPostCourier($item);

				//if method is error abort and do not use this method.

				$this->urlBuilder->setMethod($method);


	
				$this->urlBuilder->setServiceCode(ServiceCode::GetCourier($method));


				// Required info
				$this->urlBuilder->addBox($item);	

				//Add any cost of the package
				ShippingHelper::addCostOfPackage( $shipping_total, $item );
				
				// Now call
				$response = $this->communicator->communicate($this->urlBuilder->get());

				if($response->error==true) {
					$this->session->set_flashdata('error',$response->message);
					redirect($options->redirectTo);
				}
				else {
					$shipping_total += $response->cost;
				}
			}


			$shippableObejct = ShippingHelper::getGlobalShippingObject($shipping_total);

			ShippingHelper::addHandling( $shippableObejct, $options->handlingfee );

			ShippingHelper::checkMinimum( $shippableObejct, $options->minfee );
			
		}
		else {
			//Not a AU state or location in Australia
			$shippableObejct = ShippingHelper::getGlobalShippingObject(0);
		}

		return $shippableObejct;


	}


	private function objectify($options) {

		$this->doubly($options,'express');
		$this->doubly($options,'test');
		$this->doubly($options,'sod');
		
		return (object) $options;
	}
	private function doubly(&$options,$key) {

		if($options[$key]==$key) {
			$options[$key]=true;
		}
		else {
			$options[$key]=false;
		}
	}	
}


