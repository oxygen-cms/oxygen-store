
<?php if(!(isset($options['apikey'])) ): ?>
    <?php $options['apikey']='';?>
<?php endif; ?>

<?php if(!(isset($options['distcode'])) ): ?>
    <?php $options['distcode']='2000';?>
<?php endif; ?>
<?php if(!(isset($options['extracover'])) ): ?>
    <?php $options['extracover']=0;?>
<?php endif; ?>
<?php if(!(isset($options['usepackages'])) ): ?>
    <?php $options['usepackages']=0;?>
<?php endif; ?>  
<?php if(!(isset($options['handlingfee'])) ): ?>
    <?php $options['handlingfee']=0;?>
<?php endif; ?>  
<?php if(!(isset($options['minfee'])) ): ?>
    <?php $options['minfee']=0;?>
<?php endif; ?>  
<?php if(!(isset($options['express'])) ): ?>
    <?php $options['express']='regular';?>
<?php endif; ?>  
<?php if(!(isset($options['test'])) ): ?>
    <?php $options['test']='test';?>
<?php endif; ?>  
<?php if(!(isset($options['redirectTo'])) ): ?>
    <?php $options['redirectTo']='store/cart';?>
<?php endif; ?>  
<?php if(!(isset($options['sod'])) ): ?>
    <?php $options['sod']='default';?>
<?php endif; ?>  


              <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab"  href="#tab_1"><span>Details</span></a></li>
                </ul>
                <div class="tab-content">

                      <div id="tab_1" class="tab-pane active">




                      </div>
                  </div>
        </div>

<ul>
    <li class="">
        <label>Test Mode</label>
        <p>
       
        </p>
        <div class="input">
            <?php echo form_dropdown('options[test]', $form_data['test'] , set_value('options[test]', $options['test'])  ); ?>
        </div>
    </li>    
    <li class="">
        <label>API Key</label>
        <div class="input">
            <?php echo form_input('options[apikey]', set_value('options[apikey]', $options['apikey'])); ?>
        </div>
    </li>
    <li class="">
        <label>Distribution PostCode</label>
        <div class="input">
            <?php echo form_input('options[distcode]', set_value('options[distcode]', $options['distcode'])); ?>
        </div>
    </li>

    <li class="">
        <label>Extra Cover (ZERO if none-Parcel Post ONLY)</label>
        <div class="input">
            <?php echo form_input('options[extracover]', set_value('options[extracover]', $options['extracover'])); ?>
        </div>
    </li>
    <li class="">
        <label>Handling Fee</label>
        <div class="input">
            <?php echo form_input('options[handlingfee]', set_value('options[handlingfee]', $options['handlingfee'])); ?>
        </div>
    </li>    
    <li class="">
        <label>Minimum Fee</label>
        <div class="input">
            <?php echo form_input('options[minfee]', set_value('options[minfee]', $options['minfee'])); ?>
        </div>
    </li>       
    <li class="">
        <label>Delivery Option</label>
        <p>
            Choose From Regular or Express Shipping
        </p>
        <div class="input">
            <?php echo form_dropdown('options[express]', $form_data['express'] , set_value('options[express]', $options['express'])  ); ?>
        </div>
    </li>
    <li class="">
        <label>Redirection</label>
        <p>
            When errors occur, where do we redirect to ?
        </p>
        <div class="input">
            <?php echo form_dropdown('options[redirectTo]', $form_data['redirectTo'] , set_value('options[redirectTo]', $options['redirectTo'])  ); ?>
        </div>
    </li>
    <li class="">
        <label>Signature On Delivery ? </label>
        <p>
            If available, add the SOD post fee ?
        </p>
        <div class="input">
            <?php echo form_dropdown('options[sod]', $form_data['sod'] , set_value('options[sod]', $options['sod'])  ); ?>
        </div>
    </li>

    <li class="">
        <label>Packaging System</label>
        <p>
            <ul>
                <li><a target='new' href='https://github.com/dvdoug/BoxPacker'>Store Uses this BoxPacker library</a></li>
                <!--li><a target='new' href='http://www.3dbinpacking.com/'>http://www.3dbinpacking.com/</a></li-->
                <li><a target='new' href='http://en.wikipedia.org/wiki/Bin_packing_problem'>Wikipedia Article on Bin Packing</a></li>
            </ul>
        </p>
        <div class="input">
            <?php echo form_dropdown('options[usepackages]', array('packages'=>'Packaging Subsystem', 'items'=>'Do not use Packages') , set_value('options[usepackages]', $options['usepackages'])  ); ?>
        </div>
    </li>
</ul>

