<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
include_once('fastway_core.php');
class Fastway_ui extends Fastway_core
{

	public function __construct()
	{
		parent::__construct();		
	}
	public function pre_save($input)
	{
		return $input;
	}
	

	public function pre_output()
	{
		$data = [];
		$data['RFCODES'] = $this->getFranchiseForOZ($this->options['apikey']);

		if(count($data['RFCODES'] ) ==0)
		{
			$data['RFCODES'] = ['SYD'=>'Sydney'];
		}

		$data['KEEP_RESPONSES'] = array(
				0=>'No Thanks',
				1=>'Yes Please',
		);

		$data['MULTI_REGIONS'] = array(
				'true'=>'Yes (Preferred)',
				'false'=>'No',
		);

		$this->form_data = $data;
	}	


	protected function addHandling($options,$cost) {

		//add the handling
		if(isset($options['handling']))
		{
			$cost += floatval($options['handling']);
		}

		return $cost;
	}

	protected function setMin($options,$cost) {

		if(isset($options['mincharge']))
		{
			if( $options['mincharge'] > $cost )
			{
				$cost = $options['mincharge'];
			}
		}
		return $cost;
	}

	protected function setMax($options,$cost) {

		if(isset( $options['maxcharge']))
		{
			if( $options['maxcharge'] > 0 )
			{
				if( $options['maxcharge'] > $cost )
				{
					$cost = $options['maxcharge'];
				}
			}
		}
		return $cost;
	}

}
