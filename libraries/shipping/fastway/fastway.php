<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

require_once( dirname(__FILE__) . '/fastway_ui.php');


class Fastway_ShippingMethod extends Fastway_ui
{


	public function __construct()
	{
		parent::__construct();		
	}


	public function calc( $options, $items, $to_address = array() )
	{
		$shippable = new ViewObject();
		$shippable->cost = $this->_calc( $options, $items, $to_address );
		$shippable->tax  = $shippable->cost  * $this->tax_rate;
		$shippable->packing_slip = $this->_packing_slip;
		return $shippable;
	}


	private function _calc( $options, $items, $to_address = array() )
	{

		$this->load->library('store/packages_library');

		$_shippable_boxes = [];


		if(! $this->validateAPIKey($options) && $this->validateOptions())
		{
			//handle this and cancel
			$this->session->set_flashdata(JSONStatus::Error,'Unable to Validate AustPost API Key. Shipping for this order has not been calculated');
			return false;
		}

		//validation success


		return $this->__packages( $options, $items, $to_address );


	}


	private function __packages( $options, $items, $to_address = array() ) {


		if($options['usepackages'] == 'packages')
		{
			$this->_shippable_boxes = $packaged_items->packed_items;
			$this->_packing_slip = '';
		}
		else
		{
			$this->_shippable_boxes = $packaged_items->unpacked_items; //$this->packages_library->getShippableContainersCartItems($items);
			$this->_packing_slip = 'Packaging System not used.';		
		}

		return $this->__calc( $options, $items, $to_address );


	}

	private function __calc( $options, $items, $to_address = array() ) {
		

		$suburb = '';

		$ret_cost = 0;



		//only calc if items
		foreach($this->_shippable_boxes as $box) {

			//calc 1 at a time
			$ret_cost =  $this->doCalc2( $options, $box, $to_address->zip, $suburb,  $this->packages_library->getRedirOptions() );
		}


		$ret_cost = $this->addHandling($options,$ret_cost);
		$ret_cost = $this->setMin($options,$ret_cost);
		$ret_cost = $this->setMax($options,$ret_cost);

		// Return the cost
		return $ret_cost;
	}


	public function doCalc2( $options, $box, $zip=3000, $suburb = '', $redir_options =[] )
	{

		$dist_zip = (isset($options['distcode']))?$options['distcode']:3000;
		$rfc_code = (isset($options['RFCode']))?$options['RFCode']:'SYD';

		$api_key = $options['apikey'];

		$this->setAPI( $api_key );

		$_total_cost = 0;

		$this->box = $box;
		$this->suburb ='';
		$this->new_options = $options;
		$this->zip=$zip;

		$app = $this->getAppData($box,$suburb,$options,$zip);
	
        try
        {
        	$curl_url = $this->__url_ENDPOINTS['calc_postage'];
	        $_total_cost += ($box['qty'] * $this->callAPService( $app->get($curl_url) , $redir_options['redir'] , $box['package_type'], $options  ) );
        }
        catch (Exception $e)
        {
            $msg = "oops: ".$e->getMessage();
            $this->set_flashdata(JSONStatus::Error, $msg);
            redirect(NC_ROUTE.'/cart');
        }

        //clear
        $app = NULL;
		
		return $_total_cost;
	}



}