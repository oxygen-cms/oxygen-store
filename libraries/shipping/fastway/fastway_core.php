<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Fastway_core
{

    public function __get($var)
    {
        if (isset(get_instance()->$var))
        {
            return get_instance()->$var;
        }
    }

	public $name = 'Fastway Postage';
	public $description = 'Fast Way Postage';
	public $author = 'inspiredgroup.com.au';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';
	public $tax_rate = 0.1; //10%
	public $__api_key;
	public $international_shipping = false;	
	public $__url_ENDPOINTS = array(
			'list_franchises'=>'http://au.api.fastway.org/v4/psc/listrfs/',
			'calc_postage' =>'http://au.api.fastway.org/v4/psc/lookup/',
		);


	public function __construct()
	{

	}

	public $fields = 
	[
		[
			'field' => 'apikey',
			'label' => 'API Key',
			'rules' => 'trim|text',
		],
		[
			'field' => 'RFCode',
			'label' => 'RFCode FastWay franchise code',
			'rules' => 'trim|text',
		],
		[
			'field' => 'distcode',
			'label' => 'Distribution PostCode',
			'rules' => 'trim|text',
		],
		[
			'field' => 'usepackages',
			'label' => 'Use Package or Items',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'packagetype',
			'label' => 'Package Type',
			'rules' => 'trim|text',
		],
		[
			'field' => 'keepresponses',
			'label' => 'Keep CURL Responses',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'mincharge',
			'label' => 'Min Charge',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'maxcharge',
			'label' => 'Max Charge',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'handling',
			'label' => 'Handling Fee',
			'rules' => 'trim|numeric',
		],
		[
			'field' => 'multiregion',
			'label' => 'AllowMultipleRegions',
			'rules' => 'trim',
		],	
	];

	protected function validateAPIKey($options)
	{
		if(!(isset($options['apikey']))) {
			return false;
		}

		if(trim($options['apikey'] ) == '') {
			return false;
		}

		return true;

	}

	protected function validateOptions($options)
	{
		//perhaps try to connect to AustPost for further validation!!
		return true;
	}

	public function setAPI($api)
	{
		$this->__api_key = $api;
	}

	public function getAPI()
	{
		return $this->__api_key;
	}


	public function getFranchiseForOZ($api)
	{
		$app = new FWUrlParam();
		$app->set('CountryCode','1');
		$app->set('api_key', $api );

   		$url = $this->__url_ENDPOINTS['list_franchises'];

		$curl_url = $app->get($url);

   		$results = $this->getRemoteData( $curl_url, 'json', false );

   		$list = [];

        if (isset($results['result'][0]['FranchiseCode']))
        {
	        if (isset($results['result'][0]['FranchiseName']))
	        {
	        	/*
	        		//we are in business
	        		"FranchiseCode":"ADL",
					"FranchiseName":"Adelaide",
					"Phone":"(08) 8345 2300",
					"Fax":"(08) 8345 2388",
					"Add1":"756-758 Port Road",
					"Add2":"",
					"Add3":"",
					"Add4":"",
					"EmailAddress":"julie.baker@fastway.com.au"
				*/
		        // get first in list
				foreach($results['result'] as $rfc_result)
				{
			        $key = $rfc_result['FranchiseCode'];
			        $value =  $rfc_result['FranchiseName'];

			        $list[$key] = $value;
				}


	        	
        	}

        }

        return $list;
	}

	public function getFranchiseInfo($api, $fran_code='SYD')
	{
		$app = new FWUrlParam();
		$app->set('CountryCode','1');
		$app->set('FranchiseeCode',$fran_code);		
		$app->set('api_key', $api );

   		$url = $this->__url_ENDPOINTS['list_franchises'];
		$curl_url = $app->get($url);

   		$results = $this->getRemoteData( $curl_url );

   		$list = [];

        if (isset($results['result'][0]['FranchiseCode'])) {

	        if (isset($results['result'][0]['FranchiseName']))  {

			    return $results['result'][0];
        	}

        }

        return NULL;
	}
/*
    $package_types = 
    [

		'dynamic'		=>'Use Shipping Method'
		'letter'		=>'Envelope (Letter)',
		'satchel' 		=> 'Satchel (Generic)',
		'satchel_500'	=> 'Satchel (500g)',
		'satchel_3000'	=> 'Satchel (3 Kg)',
		'satchel_5000'	=> 'Satchel (5 Kg)',
		'parcel'		=>'Parcel (Box)',
		'wineparcel'	=>'Wine Parcel (Box)'								        		
    	
    ];
*/				
    /**
     * [callAPService description]
     * @param  [type] $apparams    [description]
     * @param  string $error_redir [description]
     * @return [type]              [description]
     */
    protected function callAPService( $url,  $error_redir = 'store/cart', $package_type ='parcel',$options=[] )
    {
    	//set for later
    	$this->error_redir = $error_redir;
    	$this->package_type = $package_type;
    	$this->options = $options;

        $results = $this->getRemoteData( $url, 'json', (bool) (int) $options['keepresponses'] );

        // Generally this means success call
        if (isset($results['result']['services'][0]['totalprice_frequent'])) {
        	return $this->handleSuccessResponse($results);
        }

		// Here we still have to re-call again with more info
		if (isset($results['result']['regions'])) {
			return $this->handleRegionResponse($results);
		}


		// Oops, we have an error
		if (isset($results['error'])) {
			$this->handleErrorResponse($results);
		}


        //SYS - System message
 		$this->session->set_flashdata( JSONStatus::Error , 'ER (SYS): Unknown');
        redirect($error_redir);
    }

    private function handleErrorResponse($results) {

        $this->session->set_flashdata( JSONStatus::Error , 'ER (SMR):'. $results['error'] );
    }


    private function handleRegionResponse($results) {

        $suburb = $results['result']['regions'][0]['Suburb'];

		$app = $this->getAppData($this->box,$suburb,$this->new_options,$this->zip);

		return $this->callAPService( $app->get($this->__url_ENDPOINTS['calc_postage']) ,  $this->error_redir , $this->package_type ,$this->options );
    }


    private function handleSuccessResponse($results) {


    	switch($this->package_type)
    	{   	
			case 'dynamic':
			case 'parcel':
			case 'wineparcel':        		
    		case 'parcel':
    		case 'satchel_3000':
    		case 'satchel_5000':                		
    			//search for the parcel
    			foreach($results['result']['services'] as $service) {
    				if($service['type'] == 'Parcel') {
    					return $service['totalprice_frequent'];
    				}
    			} 
    			break;    		
    		case 'satchel':
    		case 'satchel_500':
    		case 'letter':     		
    			foreach($results['result']['services'] as $service) {

    				//now get the one with the right size weight limuts
    				if($service['weightlimit'] >= $this->box['weight']) {

	    				//now get the one with the right size weight limuts
	    				if($service['type'] == 'Satchel') {
	    					return $service['totalprice_frequent'];
	    				} 

    				}    				

    			}
    			break;
    	}

    	//mm, kets just get the first option
		foreach($results['result']['services'] as $service)
		{
			return $service['totalprice_frequent'];
		} 

		return 0; 
    }


    private function getRemoteData( $url, $method='json', $store_result = true ) {
		
		$ch = curl_init();

		//$url = "http://au.api.fastway.org/v3/psc/lookup/MEL/SYD/200/5?api_key=fbb6f2f76be895dd580081dbab0fe803";
		curl_setopt($ch, CURLOPT_URL,$url);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$contents = curl_exec($ch);

		curl_close($ch);

		if($store_result)
		{
			$time = time();
			$time = substr($time,5);
			$date = new DateTime();
			$timestamp = $date->format("Y-m-d_") . $time;

			//stor the result for testing
			//$timestamp = date("Y-m-d__H.m.s.u");
			//$path = SHARED_ADDONPATH . "modules/store/bak/api_response/response_{$timestamp}_api.json";
			//file_put_contents($path, $contents );
		}


		//currently only supports json but future we need to support the xml as well
		if($method=='json')
			return json_decode($contents,true);
		else
			return json_decode($contents,true);
	}


	protected function getAppData($box,$suburb='',$options=[],$zip=3000) {

		$rfc_code = (isset($options['RFCode']))?$options['RFCode']:'SYD';
		$multi_region  = 'true';// (isset($options['multiregion']))?$options['multiregion']:'true';
		$api_key = $options['apikey'];

		$app = new FWUrlParam();
		$app->set('RFCode', $rfc_code );

		if( trim($suburb) != '')
			$app->set('Suburb', $suburb );

		$app->set('DestPostcode', $zip );
		$app->set('WeightInKg', $box['weight'] );
		$app->set('LengthInCm', $box['length'] );
		$app->set('WidthInCm' , $box['width'] );
		$app->set('HeightInCm', $box['height'] );
		$app->set('AllowMultipleRegions', $multi_region );
		$app->set('ShowBoxProduct', 'false' );
		$app->set('api_key', $api_key );

		return $app;		
	}

}

/**
 * This allows for multi-value options by same key
 * where the original does not allow this
 */
if ( ! class_exists('FWUrlParam')) 
{
    class FWUrlParam
    {
        protected $params;
        protected $first;


        public function __construct()
        {
            $this->first = true;
        }


        public function set($property, $value)
        {
            $this->_add($property, $value );
        }

       
        protected function _add($key,$value)
        {
            $this->params .= $this->first ? '?' : '&';
            $this->params .= "{$key}={$value}";
            $this->first = false;
        }

        public function get($end_point='')
        {
            return $end_point.$this->params;
        }


        public function clear()
        {
            $this->first = true;
            $this->params = '';
        }

    }
}