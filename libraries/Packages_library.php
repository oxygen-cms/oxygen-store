<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
require_once(dirname(__FILE__) . '/packer/Box.php');
require_once(dirname(__FILE__) . '/packer/BoxList.php');
require_once(dirname(__FILE__) . '/packer/Item.php');
require_once(dirname(__FILE__) . '/packer/ItemList.php');
require_once(dirname(__FILE__) . '/packer/PackedBox.php');
require_once(dirname(__FILE__) . '/packer/PackedBoxList.php');
require_once(dirname(__FILE__) . '/packer/Packer.php');
require_once(dirname(__FILE__) . '/packer/Package.php');

class Packages_library extends ViewObject
{

    protected $__trace;
    protected $__packers;
    protected $__packed_boxes;
    protected $__redir_options;

    protected $__packing_slip;


    public function __construct($params = array())
    {
        // initialize any fields
        $this->_packages = [];
        $this->__packers = [];
        $this->packedCompleteBoxes = $this->__packed_boxes = [];
        $this->__trace = '<br><h1>Trace Output</h1><br>';
        $this->__packing_slip = '';

        //set the redir options
        $this->__redir_options = [
            'redir'=> NC_ROUTE.'/cart',
            'mode'=>'front'
        ];

    }

    public function getRedirOptions()
    {
        return $this->__redir_options;  
    }



    private function _trace($title='TraceLine',$output=NULL,$line=0,$indent=0)
    {
        if(is_object($output) or is_array($output))
        {
            ob_start();
            var_dump($output);
            $output = ob_get_clean();
        }
        $margin = "{$indent}px;";
        $this->__trace .= "   <div style='margin-left:{$margin}'>";
        $this->__trace .= "      <h2>{$title}</h2>";
        $this->__trace .= "      <br> {$output}";
        $this->__trace .= "      <br><br>";
        $this->__trace .= "   </div>";
        $this->__trace .= "<hr>";
    }



    /**
     * 1. Get a Distict list of all package Groups
     * 2. Create a new Packer for each Group
     * 3.
     *
     *
     * @param  array  $cart_items [description]
     * @return [type]             [description]
     */
   public function pack( $_in_cart_items = [], $redir_options=['redir'=> NULL,'mode'=>'front'] )
   {


        //copy and do not use the reference items
        $cart_items = $_in_cart_items;

        if($redir_options['redir']==NULL)
        {
            $redir_options['redir'] = NC_ROUTE.'/cart';
        }


        $this->__packed_boxes = [];

        $this->load->model('store/packages_m');
        $this->load->model('store/products_front_m');
        $this->load->model('store/products_variances_m');

        $__pkg_groups = [];

        //set the redir options
        $this->__redir_options = $redir_options;


        //
        // 1.1 Only keep shipable items, and remove others
        //
        foreach ($cart_items as $key => $value)
        {
            if(isset($value['is_shippable']))
            {
                //its not shippable, lets remove it
                if( (int)($value['is_shippable'])==0)
                {
                    unset($cart_items[$key]);
                }
            }
        }
  

        //
        // 1.2 Define all the unique package Groups
        //
        foreach ($cart_items as $key => $value)
        {
            $__pkg_groups[ $value['pkg_group_id']  ] = []; 
        }


        // Trace
        // Status: Working
        // $this->_trace('Unique Package Groups',$__pkg_groups, __LINE__);



        //
        // 2. Create a Packer for each Group
        //
        foreach($__pkg_groups as $key => $value)
        {
            $this->__packers[$key]['packer'] = new DVDoug\BoxPacker\Packer($redir_options);
        }








        //
        // 3. Get all the available packages for the group
        //
        foreach($this->__packers as $key => $value)
        {
            $this->__packers[$key]['packages'] = $this->packages_m->selectPackagesByGroup($key);
        }






        //
        // 4. Assign all the packages to the packer
        //
        foreach($this->__packers as $key => $value)
        {


            foreach($value['packages'] as $package)
            {
                // Create a new definition of a box to the packer
                $this->__packers[$key]['packer']->addBox( new Package($package) );
            }


            // Remove the package
            //
            // No longer needed and a good
            unset($this->__packers[$key]['packages']);
        }





        //
        // 5. Now assign the items in the cart to the packers
        //
        foreach ($cart_items as $key => $cartItem)
        {
            $_packer = $this->__packers[$cartItem['pkg_group_id']]['packer'];

            //Get variance, we should do this earlier, but for concept ok.
            $variance = $this->products_variances_m->get($cartItem['id']);
            $product_name = $cartItem['name'] ;

            // Repeat the step multiple times until we have done the qty
            for($i = 0; $i < $cartItem['qty'];$i++)
            {
                $admin_link = "<a href='".NC_ADMIN_ROUTE."/product/variant/{$variance->id}'>{$product_name} - {$variance->name}</a>";
                $front_link = "<a href='".NC_ROUTE."/products/product/{$variance->product_id}'>{$product_name} - {$variance->name}</a>";

                $_packer->addItem( new Product( $admin_link, $front_link , $variance  ) );
            }


        }





        //
        // 6. Get all the packers to do their work. No slacking off.
        //
        $tcount = 0;
        foreach($this->__packers as $key => $packer)
        {
            $packer =$packer['packer'];
            //$packedBoxes =  $this->__packers[$key]['packer']->pack();
            //the false will force to use lower smaller packages as possible without redistributing weight
            $packedBoxes =  $packer->pack(true);


            $count = 0;

            $some_packer= [];
            $some_packer['box_count'] = count($packedBoxes);
            $some_packer['boxes'] = [];
            $this->_trace('NEW PACKER: ', 'This packer has ' .count($packedBoxes) . ' boxes<br>' , __LINE__, 0);

            $this->packedCompleteBoxes = $packedBoxes;
            foreach($packedBoxes as $packedBox)
            {

                $boxtype            = $packedBox->getBox();
                $rw                 = $packedBox->getRemainingWeight();
                $itemsInTheBox      = $packedBox->getItems();

                $curr_weight = $boxtype->maxWeight - $rw;

                $str = $this->buildBoxTrace( $boxtype , $curr_weight, $rw, $itemsInTheBox  );

                //Details
                $this->_trace('Details: '.$boxtype->reference, $str  , __LINE__, 100);

                $shippable_item = [];

                $shippable_item['package_name'] = $boxtype->getBoxID();
                $shippable_item['height'] = $boxtype->getOuterDepth();
                $shippable_item['width'] = $boxtype->getOuterWidth();
                $shippable_item['length'] = $boxtype->getOuterLength();
                $shippable_item['weight'] = $curr_weight;
                $shippable_item['qty'] = 1;
                $shippable_item['qty_in_box'] = count($itemsInTheBox);
                $shippable_item['additional_charge'] = $boxtype->getAdditionalCharge();
                $shippable_item['package_type'] = $boxtype->getPackageType();

                $this->__packed_boxes[] = $shippable_item;
            }

        }

        //start the packing slip
        $this->ps_addItemsIntoBox();


        //report complete
   }

   private function ps_addBox($box)
   {
        $this->__packing_slip .= ' '.$packedBox->getBox()->getBoxID();
   }
   private function ps_addItem($str)
   {
        $this->__packing_slip .= '    '.$item->getDescription();
   }   
   private function ps_addHeading()
   {
        $this->__packing_slip = '';
        $this->__packing_slip .= '<p>Packing Slip</p>';
   }
   private function ps_addFooter()
   {
        $this->__packing_slip .= '<br>End Slip<br>';
   }   
   private function ps_addSeperator()
   {
        $this->__packing_slip .= '<hr><br>';
   }
   private function ps_addItemsIntoBox()
   {
        $this->ps_addHeading();
        //if(isset($this->packedCompleteBoxes)) {
          foreach($this->packedCompleteBoxes as $box)
          {
              $this->ps_addBox($box);
              foreach ($itemsInTheBox as $item)
              {
                  $this->ps_addItem($item);
              }
              $this->ps_addSeperator();
          }
          $this->ps_addFooter();
        //}
   }   

   private function tableRowHDetails($c1='',$c2='',$c3='') {

        return "<tr><th>{$c1}</th><th>{$c2}</th><th>{$c3}</th></tr>";

   }
   private function tableRowDetails($c1='',$c2='',$c3='') {

        return "<tr><td>{$c1}</td><td>{$c2}</td><td>{$c3}</td></tr>";

   }
   private function getTabHTML($det='',$dim='',$vol='',$items) {
      $str ='<div class="tabs">';
      $str .='<ul class="tab-menu">';
      $str .='          <li><a href="#det-tab"><span>Overview</span></a></li>';
      $str .='          <li><a href="#dim-tab"><span>Dimensions</span></a></li>';
      $str .='          <li><a href="#vol-tab"><span>Weight</span></a></li>';
      $str .='          <li><a href="#items-tab"><span>Items in Box</span></a></li>';      
      $str .='      </ul>';
      $str .='      <div id="det-tab" class="form_inputs">';
      $str .='               '.$det;
      $str .='      </div>';      
      $str .='      <div id="dim-tab" class="form_inputs">';
      $str .='               '.$dim;
      $str .='      </div>';
      $str .='      <div id="vol-tab" class="form_inputs">';
      $str .='               '.$vol;
      $str .='      </div>';
      $str .='      <div id="items-tab" class="form_inputs">';
      $str .='               '.$items;
      $str .='      </div>';      
      $str .='  </div>';
      return $str;
   }

  
   private function  buildBoxTrace( $boxtype , $curr_weight, $rw, $itemsInTheBox )
   {
        $_count = count( $itemsInTheBox );

        $link = "<a href='admin/store/packages/edit/{$boxtype->getPackageID()}'>{$boxtype->reference}</a>";

        $det = '<table>';
        $det .= $this->tableRowHDetails(''              ,'Value');
        $det .= $this->tableRowDetails('Box Type:'       , $boxtype->getPackageType() ); 
        $det .= $this->tableRowDetails('Box Ref:'      , $link );        
        $det .= $this->tableRowDetails('Actual Weight:' , $curr_weight );
        $det .= $this->tableRowDetails('Items Count:'   , $_count );
        $det .= $this->tableRowDetails('Volume mm^3' ,$boxtype->getInnerVolume()  );         
        $det .= '</table>';

        $dim = '<table>';
        $dim .= $this->tableRowHDetails(''           ,'Inner'                    ,'Outer');
        $dim .= $this->tableRowDetails('Width'      ,$boxtype->getInnerWidth()  ,$boxtype->getOuterWidth() );        
        $dim .= $this->tableRowDetails('Height'     ,$boxtype->getInnerDepth()  ,$boxtype->getOuterDepth() );
        $dim .= $this->tableRowDetails('Length'     ,$boxtype->getInnerLength() ,$boxtype->getOuterLength());
        $dim .= '</table>';

        $vol = '<table>';
        $vol .= $this->tableRowHDetails(''           ,'Weight');
        $vol .= $this->tableRowDetails('Empty'      ,$boxtype->getEmptyWeight()  );        
        $vol .= $this->tableRowDetails('Actual'     ,$curr_weight );
        $vol .= $this->tableRowDetails('Remaining'  ,$rw );
        $vol .= $this->tableRowDetails('Max Allowed',$boxtype->getMaxWeight());
        $vol .= '</table>';


        $itims = '<table>';
        $itmList = [];
        $itims .= $this->tableRowHDetails('PID/VID' ,'Description', 'QTY');         
        foreach ($itemsInTheBox as $item)
        {
            if(isset( $itmList[$item->variance_id] )) {
                $itmList[$item->variance_id]['count']++;
            } else {
                $itmList[$item->variance_id]['count']=1;
            }
            $itmList[$item->variance_id]['item'] = $item;
        }              
        foreach ($itmList as $a_item)
        {
            $item = $a_item['item'];
            $count = $a_item['count'];
            $itims .= $this->tableRowDetails($item->product_id .' / '.$item->variance_id,$item->getDescription(), $count );
        }
        $itims .= '</table>';   

        $tabs = $this->getTabHTML($det,$dim,$vol,$itims);             

        return $tabs;
   }

   public function getShippableContainers()
   {
       return $this->__packed_boxes;
   }

   /**
    * Use this method if you do not want to use packages. This will get the standard array from the cart items
    * @param  [type] $cart_items [description]
    * @return [type]             [description]
    */
   public function getShippableContainersCartItems($cart_items)
   {
        $return_array = array();


        foreach($cart_items as $item)
        {

            $varianceid = $item['variance'];
            $qty = $item['qty'];

            $ra = [];
            $ra['package_name'] = (isset($item['name']))?$item['name']:'ItemContainer';  //slug of the item
            $ra['height'] = $item['height']; 
            $ra['width'] = $item['width']; 
            $ra['length'] = $item['length']; 
            $ra['weight'] = $item['weight']; 
            $ra['qty'] = $item['qty']; 
            $ra['qty_in_box'] = 1; 
            $ra['additional_charge'] = 0;    
            $ra['package_type'] = 'item';                   
            $return_array[]  = $ra; 
        }

        return $return_array;
   }


    public function get_shippable_boxes( $use_packages = true , $items = [] )
    {
        $_shippable_boxes = [];
        if($use_packages)
        {
            $this->packages_library->pack( $items );
            return $this->packages_library->getShippableContainers();
        }
        return $this->packages_library->getShippableContainersCartItems($items); 
    }   


   private function getProductByVariance($variance)
   {
       $variance = $this->products_variances_m->get($variance);

       return new Product($variance);
   }

   public function getTrace()
   {
        return $this->__trace;
   }

   public function getSlip()
   {
        return $this->__packing_slip;
   }

   public function getPackages()
   {
        return $this->_packages();
   }

}