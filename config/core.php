<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * !! NO NOT ALTER !!
 * 
 *
 * Please take care with this setting.
 * Please do not add/prepend the 'admin'
 * This MUST be the same name as the folder
 * that contains the src.
 *
 * Future enhancements will allow for custom
 * admin routes so the config setting should NOT
 * be changed
 */

$config['core/config']  							= 'common'; 


/**
 * Which config file should we use for installing
 * This can be handy if you install multiple sites and you may have
 * a pre-determed config for variou client types.
 *
 * All config items sould be set and for a template look at
 * _default_install_config.php.bak
 * 
 */
$config['core/install/settings']  					= 'common';





/*
 * This MUST match the name of the folder
 * Note: that you cant simply just change
 * the name of the folder and this variable.
 * Please NEVER change this value.
 * 
 */
$config['core/path']  								= 'store';
$config['core/admin_route']  						= 'store';




/*
 * The core route to use
 */
$config['core/route']  								= 'store';













/*
 * Allow base amount pricing for products
 * 
 */
$config['core/base_amount_pricing']  				= true;  





/**
 * This will require a shipping option
 * to be setup and enabled or the customer can not checkout
 * until a shipment method is selected.
 */
$config['core/require_shpping_options']  			= true;  




/**
 * @deprecated  
 * 
 * Role base override
 *
 * Allows a user assigned to a role to 
 * view any feature that is hidden. However this is not fully implemented
 */
$config['core/allow_rbo']  							= false;  

$config['core/packages/types']                      = 
[
        'Dynamic' => 
        [
            'dynamic'       =>'Auto (Let shipping method choose)'
        ],                                      
        'Fixed' => 
        [
            'letter'        =>'Envelope (Fixed)',
            'parcel'        =>'Parcel (Fixed)',                                         
        ]
];
/*
$config['core/packages/types']                      = 
[
        'Dynamic' => 
        [
            'dynamic'       =>'Use Shipping Method'
        ],                                      
        'Letters' => 
        [
            'letter'        =>'Envelope (Letter:Fixed)',
        ],
        'Satchels' => 
        [
            'satchel'       => 'Satchel (Generic)',
            'satchel_500'   => 'Satchel (500g)',
            'satchel_3000'  => 'Satchel (3 Kg)',
            'satchel_5000'  => 'Satchel (5 Kg)',
        ],                                          
        'Boxes' => 
        [
            'parcel'        =>'Parcel (Parcel:Fixed)',
            'wineparcel'    =>'Wine Parcel (Box)'                                               
        ]
];
*/

// Common product form template
$config['core/product/form']  							=  

"<div style='display:none;' id='product_fp_[[product_id]]' value='[[from_price]]'></div>
        <form action='{{x:uri}}/eavcart/add' enctype='multipart/form-data' id='form_[[product_id]]' name='form_[[product_id]]' method='post' class=''>
            Price:
            <span class='price'>
                <span id='store_price_[[product_id]]' class='amount' data-price=''>[[from_price]]</span>
            </span> 

            <div class='item-action' >

                {{store_userfields:htmlproduct id='[[product_id]]' }}

		        <input type='hidden' name='eavcartmode' value='eav'>

		        <input type='hidden' name='form_eav_product_id' value='[[product_id]]'>

                {{products:htmloptions product_id='[[product_id]]' }}
                        {{html}}
                {{/products:htmloptions}}

                <div style='display:none' id='p_product_variations_[[product_id]]'>
                    <div id='product_variations_[[product_id]]'>
                        <!--postback data-->
                    </div>
                </div>

                <div><span style='color:red' class='store-product-message_[[product_id]]'></span></div>
                 <br>
                <div class='product-quantity inline'>
                     <a href='' class='minus'>-</a>
                     <input name='qty' type='text' value='1' placeholder='QTY'>
                     <a href='' class='plus'>+</a>
                 </div>


                 <input type='submit' class='btn btn-default btn-xs add_to_cart_btn' value='add to cart' product_id='[[product_id]]'>
            </div>
        </form>";


/**
 * use this instead if you want to show a list of variations by default
 */
$config['core/product/form_2']                            =  

"<div style='display:none;' id='product_fp_[[product_id]]' value='[[from_price]]'></div>
        <form action='{{x:uri}}/eavcart/add' enctype='multipart/form-data' id='form_[[product_id]]' name='form_[[product_id]]' method='post' class=''>
            Price:
            <span class='price'>
                <span id='store_price_[[product_id]]' class='amount' data-price=''>[[from_price]]</span>
            </span> 

            <div class='item-action' >

                {{store_userfields:htmlproduct id='[[product_id]]' }}

                <input type='hidden' name='eavcartmode' value='eav'>

                <input type='hidden' name='form_eav_product_id' value='[[product_id]]'>

                {{products:htmloptions product_id='[[product_id]]' }}
                        {{html}}
                {{/products:htmloptions}}

                <div style='' id='p_product_variations_[[product_id]]'>
                    <div id='product_variations_[[product_id]]'>
                        <input type='hidden' id='eav_by_variance_id' name='eav_by_variance_id'>

                        {{products:variations product_id='[[product_id]]' }}
                            {{if has_multiple=='true'}}
                                Please select a variation:
                                <select id='eav_variances_id' name='eav_variances_id' class='eav_variances_id' product_id='[[product_id]]'>
                                      {{variation_values}}
                                            <option value='{{id}}'>{{name}}</option>
                                      {{/variation_values}}
                                 </select>
                            {{else}}
                                <input type='hidden' id='eav_variances_id' name='eav_variances_id' value='{{variance_id}}'>
                            {{endif}}
                        {{/products:variations}}
                    </div>
                </div>


                <div><span style='color:red' class='store-product-message_[[product_id]]'></span></div>
                 <br>
                <div class='product-quantity inline'>
                     <a href='' class='minus'>-</a>
                     <input name='qty' type='text' value='1' placeholder='QTY'>
                     <a href='' class='plus'>+</a>
                 </div>


                 <input type='submit' class='btn btn-default btn-xs add_to_cart_btn' value='add to cart' product_id='[[product_id]]'>
            </div>
        </form>";

$config['core/product/coverimage']  							=   
"       
        <a href='{{x:uri}}/products/product/[[product_id]]{{x:quantam}}' class='[[link_class]]' title='[[product_name]]'> 
            <img src='[[src]]/[[image_size]]'' alt='[[alt]]'>
        </a>    
";
$config['core/product/gallery_list']                              =   
"       
        <li>
            <a href='{{url:site}}files/large/[[file_id]]{{x:quantam}}/800' title='[[product_name]]' class='magnific'>
            <img src='[[src]]/800' alt='Gallery'></a>
        </li>
";
$config['core/product/coverimage_img_only']  					=   
"       
        <img src='[[src]]/[[image_size]]'' alt='[[alt]]'>   
";
$config['core/product/featured/tag']  = "<div class='[[class]]'>[[text]]</div>";

$config['core/product/price/wasnow']  = "
			<span class='price'>
				<del>
					<span class='amount'>[[was]]</span>
				</del>
				<ins>
					<span class='amount'>[[now]]</span>
				</ins>
			</span>";
