<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['admin/store/products(/)(:num)'] = 'admin/products/index/$2';
$route['admin/store/orders(/)(:num)'] = 'admin/orders/index/$2';

