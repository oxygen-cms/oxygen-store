<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 1. Cached menu for admin users based on permissions
 * 2. Hide attribute option for variances with no attributes
 * 3. if product/variance exist we can not allow add/edit of attributes in product type setup
 */

/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */
class Module_Store extends Module
{
    
    /**
     * Release version
     */
    public $version = '1.0.0';


    private function get_routes()
    {
        $this->load->config('store/core');
        $core_routes =  
        [
            [
                    'name'  => 'Store: Common Routes',
                    'uri'   => $this->config->item('core/path').'/(index|cart|eavcart|eavtest|checkout|closed)(:any)?',
                    'dest'  => $this->config->item('core/path').'/$1$2',
                    'can_change' => 1,
            ],
            [
                    'name'  => 'Store: Payment Processing',
                    'uri'   => $this->config->item('core/path').'/payment/(order|process|rc|cancel|callback)(/:num)?',
                    'dest'  => $this->config->item('core/path').'/payment/$1$2',
                    'can_change' => 1,
            ],
            [
                    'name'  => 'Store: Product Detail',
                    'uri'   => $this->config->item('core/path').'/products/product(/:any)?',
                    'dest'  => $this->config->item('core/path').'/products/product$1',
                    'can_change' => 1,
            ],                                            
            [
                    'name'  => 'Store: Products Listing',
                    'uri'   => $this->config->item('core/path').'/products(/:num)?',
                    'dest'  => $this->config->item('core/path').'/products/index$1',
                    'can_change' => 1,
            ],
            [
                    'name'  => 'Store: Products ADMIN Details',
                    'uri'   => $this->config->item('core/path').'/productsadmin(/:any)?',
                    'dest'  => $this->config->item('core/path').'/productsadmin$1',
                    'can_change' => 1,
            ],                            
            [
                    'name'  => 'Store: Home Page',
                    'uri'   => $this->config->item('core/path'),
                    'dest'  => $this->config->item('core/path').'/home',
                    'can_change' => 1,
            ]
           
        ];  

        return $core_routes;     
    }   

    /**
     * Load core libraries and classes : Global
     */
    public function __construct()
    {
        $this->load->library('store/storecore_library');        
        $this->lang->load('store/store_admin');
        $this->ci = get_instance();
    }



    /**
     * [info description]
     * @return [type] [description]
     */
    public function info()
    {
        $and_get_menu = $this->ci->uri->segment(3);
        $info =  [
            'name' => [
                'en' => 'Store',
            ],
            'description' => [              
                'en' => 'Store  - A full featured ecommerce solution for OxygenCMS',
            ],
            'clean_xss' => true,
            'frontend' => true,
            'backend' => true,
            'menu' => false,
            'author' => 'Sal McDonald',
            'roles' => 
            [
                'admin_store_subsystems',
                'admin_checkout',
                'admin_workflows',                
                //new roles for store
                'store_administrator', //the most basic of roles, should be given to EVERYONE who can manage some kind of store admin feature
                
                //all old roles are deprecated
                'admin_r_catalogue_edit',
                'admin_reports',
                'admin_orders',

                'admin_tax',
                //features
                'admin_affiliates',
            ],
            'enabled' => 1,
            'is_core' => 0,
            'icon' => 'fa fa-shopping-cart',
            'sections' => [],
        ];


        if( in_array( $and_get_menu , array( 'product_fields', 'products_types', 'attributes') ))
        {

            $info['sections']['product_fields'] = array(
                'name' => 'Product Fields',
                'uri' => 'admin/store/product_fields',
                'shortcuts' => array(
                     array('name' => 'store:common:create', 'uri' => 'admin/store/product_fields/create') 
                     )
            );  

            $info['sections']['producttypes'] = array(
                'name' => 'Product Types',
                'uri' => 'admin/store/products_types',
                'shortcuts' => array(
                     array('name' => 'store:common:create', 'uri' => 'admin/store/products_types/create' ) 
                     )
            );  


            $info['sections']['attributes'] = array(
                'name' => 'Attributes',
                'uri' => 'admin/store/attributes',
                'shortcuts' => array(
                     array('name' => 'store:common:create', 'uri' => 'admin/store/attributes/create' ) 
                     )
            );  

        }
        else {

                /* 
                 * We want to show the common 3 sections for all areas of 
                 * Store but not the features or subsystems
                 */
                if( ! in_array( $and_get_menu , [ 'subsystems', 'features'] )) {

                    if(Settings::get('storst_storetype')=='standard')  {
                        $info['sections']['orders'] = [
                            'name' => 'store:admin:orders',
                            'uri' => NC_ADMIN_ROUTE.'/orders',
                            'shortcuts' => []
                        ];
                    }

                    $info['sections']['products'] = [
                        'name' => 'store:admin:products',
                        'uri' => NC_ADMIN_ROUTE.'/products',
                        'shortcuts' => [ 
                            ['name' => 'store:products:create', 'uri' => NC_ADMIN_ROUTE.'/product/create' ],
                        ],
                    ];
                }
        }


        if(in_array( $and_get_menu , [ 'package_manager', 'packages', 'packages_groups'] ))
        {
            $info['sections']['packages'] = [
                'name' => 'store:admin:packages_groups',
                'uri' => NC_ADMIN_ROUTE.'/packages_groups',
                'shortcuts' => [ 
                        //['name' => 'store:packages:create', 'uri' => NC_ADMIN_ROUTE.'/packages/create','class' => 'add'],
                        ['name' => 'store:packages:create_group', 'uri' => NC_ADMIN_ROUTE.'/packages_groups/create','class' => 'add'], 
                    ],  
            ];            

        }

        /**
         * Common create menu item
         */
        if(in_array( $and_get_menu , ['coupons','workflows','tax'] ))
        {
            $info['sections'][$and_get_menu] = [
                'name' => 'store:admin:'.$and_get_menu,
                'uri' => NC_ADMIN_ROUTE.'/'.$and_get_menu,
                'shortcuts' => [
                        ['name' => 'store:admin:'.$and_get_menu.':create', 'uri' => NC_ADMIN_ROUTE.'/'.$and_get_menu.'/create','class' => 'add'] 
                    ]
            ];
        }


        /**
         * Common standard access
         */
        if(in_array( $and_get_menu , ['apis','carts', 'manage','shipping','gateways','customers','reports','sandbox'] ))
        {
            $info['sections'][$and_get_menu] = [
                'name' => 'store:admin:'.$and_get_menu,
                'uri' => NC_ADMIN_ROUTE.'/'.$and_get_menu,
                'shortcuts' => []
            ];
        }



        if(in_array( $and_get_menu , ['countries', 'shipping_zones','states'] ))
        {

            $info['sections']['countries'] = [
                'name' => 'Countries',
                'uri' => NC_ADMIN_ROUTE.'/countries',
                'shortcuts' => [ 
                                    ['name' => 'store:shipping:zones:create_country', 'uri' => NC_ADMIN_ROUTE.'/countries/create/' ,'class'=>'add'],
                               ]
            ];              
            $info['sections']['states'] = [
                'name' => 'States',
                'uri' => NC_ADMIN_ROUTE.'/states',
                'shortcuts' => [],
            ];   

            $info['sections']['states']['shortcuts'][0] =  ['name' => 'store:shipping:zones:create_state', 'uri' => NC_ADMIN_ROUTE.'/states/create/' ,'class'=>'add'];
            //tricky stuff
            if($this->uri->segment(3) == 'states' AND (($this->uri->segment(4) == 'bycountry') OR ($this->uri->segment(4) == 'create')) AND  $this->uri->segment(5) > 0)
            {
                $info['sections']['states']['shortcuts'][0]['uri'] =  NC_ADMIN_ROUTE.'/states/create/'.$this->uri->segment(5);     
            }
            $info['sections']['shipping_zones'] = [
                'name' => 'Zones',
                'uri' => NC_ADMIN_ROUTE.'/shipping_zones',
                'shortcuts' => [
                        ['name' => 'store:shipping:zones:create', 'uri' => NC_ADMIN_ROUTE.'/shipping_zones/create','class' => 'add'],
                ]  
            ];             
        }


        return $info;
    }


    /**
     * The main menu is now handled via a DB IO.
     * This function will peek into the DB and build the menu
     * TODO: Implement caching here
     */
    public function admin_menu(&$menu)
    {

        $this->ci->load->helper('store/store_admin');
        $display_menu       = true;
        $dev_menu = 'lang:store:admin:store_admin'; 
 
        $menu['lang:store:admin:shop']['menu_items'] = [];
        $menu['lang:store:admin:shop']['icon'] = 'fa fa-shopping-cart';
        $menu['lang:store:admin:shop_admin']['menu_items'] = [];
        $menu['lang:store:admin:shop_admin']['icon'] = 'fa fa-shopping-cart';

        if(group_has_role('store', 'admin_store_subsystems') )  {

            $menu['lang:store:admin:shop_setup_menu']['menu_items'] = [];

            $menu['lang:store:admin:shop_setup_menu']['menu_items'][] = 
                [
                    'name'          => 'lang:store:admin:store_subsystems',
                    'uri'           => NC_ADMIN_ROUTE.'/subsystems',
                    'icon'          => 'fa fa-gears',
                    'permission'    => '',
                    'menu_items'    => [],
                ];  
            $menu['lang:store:admin:shop_setup_menu']['menu_items'][] = 
                [
                    'name'          => 'lang:store:admin:store_features',
                    'uri'           => NC_ADMIN_ROUTE.'/features',
                    'icon'          => 'fa fa-gears',
                    'permission'    => '',
                    'menu_items'    => [],
                ];  
           
        }
        
        // if the table exist we can select all records from it!
        if( $this->db->table_exists('storedt_admin_menu') ) {   

            $menu_items = $this->db->order_by('order')->get('storedt_admin_menu')->result();

            foreach ($menu_items as $key => $value)
            {

                if(group_has_role('store', $value->role))  {

                        if(!isset($menu[$value->menu])) {
                            $menu[$value->menu]['menu_items'] = [];
                        }

                        $menu[$value->menu]['menu_items'][] = 
                            [
                                'name'          => $value->label,
                                'uri'           => $value->uri,
                                'icon'          => $value->icon,
                                'permission'    => '',
                                'menu_items'    => [],
                            ];                     

                }

            }
            //
            // if we only have 1 item in the menu, give context (name) to the parent folder so "Workflows" becomes Store / Workflows
            //
            if(count($menu['lang:store:admin:shop']['menu_items'])==1) {
                $menu['lang:store:admin:shop']['menu_items'][0]['name'] = 'Store / ' . $menu['lang:store:admin:shop']['menu_items'][0]['name'];
            }
            if(count($menu['lang:store:admin:shop_admin']['menu_items'])==1) {
                $menu['lang:store:admin:shop_admin']['menu_items'][0]['name'] = 'Store Settings / ' . $menu['lang:store:admin:shop_admin']['menu_items'][0]['name'];
            }   
            if(isset($menu['lang:store:admin:shop_setup_menu'])) {         
                if(count($menu['lang:store:admin:shop_setup_menu']['menu_items'])==1) {
                    $menu['lang:store:admin:shop_setup_menu']['menu_items'][0]['name'] = 'Store Setup / ' . $menu['lang:store:admin:shop_setup_menu']['menu_items'][0]['name'];
                }
            }
        }
        
        // Place menu on position #
        add_admin_menu_place('lang:store:admin:shop', 4);
        add_admin_menu_place('lang:store:admin:shop_admin', 5);
        add_admin_menu_place('lang:store:admin:shop_setup_menu', 6);
    }


    /**
     * Installs store
     * @return [type] [description]
     */
    public function install() 
    {
        $this->load->library('store/install_library'); 
        if($status = $this->install_library->install_store()) 
        {
            //continue on..
            $this->load->library('maintenance/routes_lib');
            Routes_lib::InstallModule('store',$this->get_routes());            
        } 
        else 
        {
            return false;
        }

        $data = 
        [
            'section'=>'row1',
            'name'=>'Average order Cost',
            'partial'=>'store/admin/dashboard/row1_order_avg',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);

        $data = 
        [
            'section'=>'row1',
            'name'=>'Average Orders by customer',
            'partial'=>'store/admin/dashboard/row1_avg_order_count',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);


        

        $data = 
        [
            'section'=>'row1',
            'name'=>'Active carts',
            'partial'=>'store/admin/dashboard/row1_carts',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);
    

        $data = 
        [
            'section'=>'row1',
            'name'=>'Todays revenue',
            'partial'=>'store/admin/dashboard/row1_revenue_today',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);
     

        $data = 
        [
            'section'=>'row1',
            'name'=>'Weekly revenue',
            'partial'=>'store/admin/dashboard/row1_revenue_week',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);
     
        $data = 
        [
            'section'=>'row1',
            'name'=>'Monthly revenue',
            'partial'=>'store/admin/dashboard/row1_revenue_monthly',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);
     
        $data = 
        [
            'section'=>'row1',
            'name'=>'Annual revenue',
            'partial'=>'store/admin/dashboard/row1_revenue_annual',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);    

        $data = 
        [
            'section'=>'row3',
            'name'=>'Most viewed Graph',
            'partial'=>'store/admin/dashboard/row3_graphs_most_viewed',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data);

        $data = 
        [
            'section'=>'row3',
            'name'=>'Orders Placed Graph',
            'partial'=>'store/admin/dashboard/row3_graphs_order_placed',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data); 


        $data = 
        [
            'section'=>'row3',
            'name'=>'Orders Placed Graph (6m)',
            'partial'=>'store/admin/dashboard/row3_graphs_6monthly',
            'order'=> 1, 
            'is_visible'=> 1,
            'module'=>'store'
        ];
        Events::trigger('install_dashboard_widget',$data); 


        return true;  
    }


    /**
     * Before uninstalling, check that all extension modules and features are uninstalled first
     */
    public function uninstall() {

        $this->load->library('store/install_library'); 
        $this->install_library->uninstall_store();  

        $this->load->library('maintenance/routes_lib');
        Routes_lib::UninstallModule('store');

        //remove the admin widgets
        $this->db->where('module','store')->delete('widgets_admin');

        return true;

    }
    
    public function disable() 
    {
        return true;
    }
    
    public function enable() 
    { 
        return true;
    }

    /**
     * Uprades all pending subsystems.
     */ 
    public function upgrade($old_version)
    {
        /*
        $this->dbforge->drop_column('storedt_order_invoice', 'orprice');

        $fields = 
        [
            'orprice' => array('type' => 'DECIMAL(10,2)', 'unsigned' => true, 'null' => true, 'default' => 0)
        ];
        $this->dbforge->add_column('storedt_order_invoice', $fields);

        //now we need to get the values over
        $invoice_items = $this->db->get('storedt_order_invoice')->result();
        foreach($invoice_items as $key=>$invoice_item)
        {
            $a = ($invoice_item->qty * $invoice_item->price);
            $b = $a + $invoice_item->discount;
            $c = $b / $invoice_item->qty;
            $neworprice = (floatval($c) );

            $update['orprice'] = round($neworprice,2);
            $this->db->where('id',$invoice_item->id)->update('storedt_order_invoice',$update);
        }
        */


        return true;
    }


    public function help()
    {
        $path = 'http://localhost/nitrodocs/modules/store';
        return "<iframe frameborder='0' src='$path?ajaxview=ajaxview' width='900' height='400'></iframe>"; 
    }


}
/* End of file Details.php */