<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**	
 * Oxygen-CMS 
 *
 * @author Sal McDonald (2013-2016)
 *
 * @package OxygenCMS\Core\
 *
 *
 * @copyright  Copyright (c) 2013-2016
 * @copyright  Oxygen-CMS
 * @copyright  oxygen-cms.com
 * @copyright  Sal McDonald
 *
 * @contribs PyroCMS Dev Team, PyroCMS Community, Oxygen-CMS Community
 *
 */

/**
 * Gets the title of the settings by slug
 */
if (!function_exists('sih_slug2title'))
{
	function sih_slug2title( $driver , $by = 'driver' )
	{
		$ci =& get_instance();
		if($ci->db->table_exists('storedt_systems'))
		{
			$settings = $ci->db->where('driver',$driver)->get('storedt_systems')->row();
			return ($settings) ? $settings->title : '' ;
		}
		return '';
	}
}